function getUrl(){
   var baseurl = document.getElementById('baseurl').value;
   return(baseurl);
  } 


function del(id,mes,table_id,controller,func){

  var x = confirm("Are you sure you want to delete this "+mes+"?");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: getUrl()+controller+'/'+func+'/'+id,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }

  function notifyDep(id){
    $.ajax({
      type: 'post',
      url: getUrl()+'pos/department_notify/'+id,
      success: function() {
        return true;
      },
    });
  }

  function changer(id,mes,table_id,column,value){
  var x = confirm("Are you sure you want to Change this "+mes+"?");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: window.location.href.replace()+'/status_change/'+id+'/'+column+'/'+value,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }

  function changes(id,mes,table_id,column,value,func){
  var x = confirm("Are you sure you want to Change this "+mes+"?");
    if (x == true){
        var table = $('#'+table_id+'-list-table').DataTable();
               $.ajax({
               type: 'post',
               url: window.location.href.replace()+'/'+func+'/'+id+'/'+column+'/'+value,
               success: function() {
                        table.ajax.reload();
                     },
                 });
               
      }else{
           return false;
     }

  }

function initDTable(tid,cols,furl,data='',ord=''){
  var combdata = '';
  var myorder = '';
  if (data != '') { combdata = data}else{combdata = '{}'};
  if (ord != '') { myorder = ord}else{myorder = 0}
     $(document).ready(function() {
       $('#'+tid+'').DataTable({
          "processing"  : true,
          "serverSide" : true,
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :20,
          "lengthMenu": [10, 25, 50,100],
          "responsive": true,
          "order": [[ myorder, "desc" ]],
          "columns": cols,                               
            "ajax": {
               'async': false,
                url  : furl,
                type : 'POST',
                data : combdata
                 },
           });
        } );
     }


 function removeButtonDesign(){
   let btn = function(api, node, config) {
     $(node).removeClass('dt-button');
     }
   return btn;  
   }
function initDTableButtons(tid,cols,furl,data='',ord='',reportData){
  //b =>buttons
  //l =>lenghth menu
  //f =>search 
  //all =>'Blfrtip'
  var combdata = '';
  var myorder = '';
  if (data != '') { combdata = data}else{combdata = '{}'};
  if (ord != '') { myorder = ord}else{myorder = 0}
     $(document).ready(function() {
       let table = $('#'+tid+'').DataTable({
          dom:"<'row'<'col-sm-1'l><'col-sm-3 'B><'col-sm-8'f>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>", 
          buttons:[ 
                    {
                      extend: 'collection',
                      className: 'btn btn-outline-secondary btn-sm',
                      text: 'Export <i class="far fa-file-alt "></i>',
                      background:false,
                      buttons: [
                                { extend: 'copy', className: 'dropdown-item', init: removeButtonDesign(),},
                                { extend: 'csv', className: '', init: removeButtonDesign(),},
                                { extend: 'excel', className: '', init: removeButtonDesign(), },
                                { 
                                  extend: 'pdf', 
                                  className: '',
                                  orientation: 'portrait',
                                  pageSize: 'A4',
                                  messageTop: ''+reportData.total+'                                From '+combdata.searchBy.fromDate+' To '+combdata.searchBy.toDate+'',
                                  title: ''+reportData.reportName+'',
                                  download: 'open',
                                  init: removeButtonDesign(), 
                                 },
                                //{ extend: 'print', className: '', init: removeButtonDesign(), },      
                               ],
                     },
                     {    
                      extend: 'colvis',
                      text: 'Visable',
                      background:false,
                      init: removeButtonDesign(),
                     },
                  ],
          "processing"  : true,
          "serverSide" : true,
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :20,
          "language": {
                        "lengthMenu":'<select class="form-control">'+
                          '<option value="10">10</option>'+
                          '<option value="20">20</option>'+
                          '<option value="30">30</option>'+
                          '<option value="40">40</option>'+
                          '<option value="50">50</option>'+
                          '<option value="-1">All</option>'+
                          '</select>'
                      },
          "responsive": true,
          // "createdRow": function( row, data, dataIndex){
          //     console.log(data);
          //       if( data[1] ==  `<span id="delivery_id" style="padding-left:7%;" class="info" style="color:#000000">1</span>`){
          //           $(row).addClass('btn-dark');
          //       }
          //       if( data[1] ==  `<span id="delivery_id" style="padding-left:7%;" class="info" style="color:#000000">2</span>`){
          //           $(row).addClass('btn-danger');
          //       }
          //   },
          "order": [[ myorder, "desc" ]],
          "columns": cols,                               
            "ajax": {
               'async': false,
                url  : furl,
                type : 'POST',
                data : combdata
                 },
           });
         
         table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)' );
         $('.dataTables_filter input', table.table().container())
            .off('.DT')
            .on('change', function (e) {
              table.search(this.value).draw();
              if(this.value === "") {
                  table.search("").draw();
              }
        });

        });
     }     



function initDTable_normal(tid,ord,cols,pageLength=''){
  if (pageLength=='') {pageLength=20}
     $(document).ready(function() {
       $('#'+tid+'').DataTable({
          "pagingType": "full_numbers",
          "searching": true,
          "draw" :true,
          "pageLength" :pageLength,
          "lengthMenu": [10, 25, 50,100],
          "responsive": true,
          "order": [[ ord, "desc" ]],
          "columns": cols,
           });
        } );
     } 


 $(document).ready(function(){
     function savedDataForm_Constructor(){
      if($('.savedFormsButton').attr('id') == undefined){return false;}
           this.formData     = $('.savedFormsButton').attr('id').split(","); 
           this.pageTitle    = $('.page-title-toSave').text(); 
           this.pageInfo     = $('.page-info-data').text(); 
           this.link         = document.location.href.replace(''+getUrl()+'','');
           this.titles       =  $('.savedPageTitle').map(function(){
                                 return [$(this).attr('id').replace('pageTitle','')];
                               }).get();
        
          }


      $('.savedFormsButton').click(function(){
          let formData = new savedDataForm_Constructor;
           if(formData.titles.includes(""+formData.pageTitle+"")){
               alert('Sorry You already saved this link before');
               return false;
            }
          var url =getUrl()+'admin/dashboard/save_form';
          $('.savedFormsButton').hide();
           $.ajax({
                url: url,
                dataType: "json",
                type : 'POST',
                data:{form_data:formData},
                success: function(data){
                   getSavedForms();
                }
              });
       });   

    favouritesChecker();
    function favouritesChecker(){
       let formData = new savedDataForm_Constructor;
       if(formData.titles == undefined){return false;}
        if(formData.titles.includes(""+formData.pageTitle+"")){
          $('.savedFormsButton #favouritIcon').removeClass('far fa-star');
          $('.savedFormsButton #favouritIcon').addClass('fas fa-star');
        }
     }              

 });       

function fieldChecker(id,path){
        var name = $("#"+id+"").val();
        $.ajax({
              url:getUrl()+path,
              dataType: "json",
              type : 'POST',
              data    : {name:name},
              success: function(data){
                         if (data =='exist') {
                             $("#"+id+"").removeClass("is-valid");
                             $("#"+id+"").addClass("is-invalid");
                             $(".invalid-feedback").show();
                             $(".modal-footer #modalSubmit").attr("disabled", true);
                           }else if(data =='not_exist'){
                             $("#"+id+"").removeClass("is-invalid");
                             $("#"+id+"").addClass("is-valid");
                             $(".invalid-feedback").hide();
                             $(".modal-footer #modalSubmit").attr("disabled", false);
                          }
                        }
                });
       }



function spinnerLoad(){
  $(document).ajaxStart(function(){
      $("#loader").show()
    });

  $(document).ajaxStop(function(){
      $("#loader").hide()
    });
}

function preventBrowserBack(){
  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };
}

function preventBrowserInspect(){
  document.onkeydown = function(e) {
      if(e.keyCode == 123) {
      return false;
      }
      if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
      return false;
      }
      if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
      return false;
      }
      if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
      return false;
      }
   }
}

$(document).ready(function(){
  $('#main_search').keyup(function() {
    mainSearch();
  });
});

function mainSearch(){
  var search = $('#main_search').val();
  var url= getUrl()+'/admin/dashboard/search';      
  $.ajax({
    url: url,
    dataType: "json",
    type : 'POST',
    data:{search:search},
    success: function(data){
      $("#main_search").empty();
      $("#result").empty();
      $.each(data, function(i,item){
        delete data['count'];
        if (data[i].id == undefined) {
          $("#result").append('<a>'+ data[i].key +'</a>');
        }else{
          $("#result").append('<a href="'+getUrl()+'prs/pr/'+data[i].id+'"><span class="mail-desc">'+data[i].serial+'</span></a><br/>'+data[i].internal_note+'<br/>');
        }
      });
    }
  })
}

 function printDiv(){
        let options = {
                        //debug: true,
                        importCSS: true,
                        importStyle: true,
                        // printContainer: true,
                         loadCSS: getUrl()+'assets/libs/datatables/extensions/Responsive/css/responsive.bootstrap.min.css',
                        // pageTitle: "",
                        // removeInline: true,
                        // removeInlineSelector: "body *",
                        // printDelay: 333,
                        // header: null,
                        // footer: null,   
                        // formValues: true,
                        // base: true,
                        // canvas: true, 
                        // beforePrintEvent: null,        
                        // beforePrint: null,             
                        // afterPrint: null   
                     };  
        $("#DivIdToPrint").printThis(options);
    }


function getViewAjax(controller,fun,form_id,insteadOf=''){ 
   var url = getUrl()+controller+'/'+fun+'/'+form_id;
   jQuery.ajaxSetup({async:false});
   $('#'+insteadOf+'').empty();
   $('#'+insteadOf+'').load( ""+url+"" );
}    