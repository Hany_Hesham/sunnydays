const pwaInstall = document.getElementById('pwaInstall');
const pwaInstallAvailableToast = document.getElementById('pwaInstallAvailableToast');



const buttonInstall = () => {
  pwaInstall.install();

  if (window.ga) {
    ga('send', 'event', 'pwa-install', 'button', 'accepted');
  }
}

const toastInstall = () => {
  pwaInstall.install();

  if (window.ga) {
    ga('send', 'event', 'pwa-install', 'toast', 'accepted');
  }
}

const getInstalledRelatedApps = () => {
  pwaInstall.getInstalledRelatedApps();
}

const closeInstallAvailableToast = () => {
  pwaInstallAvailableToast.close();

  if (window.ga) {
    ga('send', 'event', 'pwa-install', 'toast', 'dismissed');
  }
}

const handlePWAInstallAvailable = (e) => {
  console.log('pwa-install-available event', e);

  if (!(pwaInstall.relatedApps && pwaInstall.relatedApps.length)) {
    pwaInstallAvailableToast.show({text: 'Install this web app'});

    if (window.ga) {
      ga('send', 'event', 'pwa-install', 'available', pwaInstall.platforms);
    }
  }
}

const handlePWAInstallInstall = (e) => {
  console.log('pwa-install-install event', e);

  if (window.ga) {
    ga('send', 'event', 'pwa-install', pwaInstall.choiceResult.outcome, pwaInstall.choiceResult.platform);
  }
}

const handlePWAInstallInstalled = (e) => {
  console.log('pwa-install-installed event', e);

  document.getElementById('pwaInstallInstalledToast').show({text: 'The web app has been installed successfully'});

  if (window.ga) {
    ga('send', 'event', 'pwa-install', 'installed', pwaInstall.choiceResult.platform);
  }
}

const handlePWAInstallError = (e) => {
  console.error('pwa-install-error event', e);
  if (window.ga) {
    ga('send', 'event', 'pwa-install', 'error', pwaInstall.choiceResult.platform);
  }
} 

pwaInstall.addEventListener('pwa-install-available', handlePWAInstallAvailable);
pwaInstall.addEventListener('pwa-install-install', handlePWAInstallInstall);
pwaInstall.addEventListener('pwa-install-installed', handlePWAInstallInstalled);
pwaInstall.addEventListener('pwa-install-error', handlePWAInstallError);