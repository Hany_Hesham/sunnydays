 // setInterval(function(){getNotifications();}, 60000); 
 // setInterval(function(){getMessaages();}, 60000); 
 getSavedForms();

  function getNotifications(){

     var url= getUrl()+'/admin/dashboard/notifications';      
            $.ajax({
               'async':false,
                url: url,
                dataType: "json",
                type : 'POST',
                  success: function(data){
                   $("#notifiCount").empty();
                   $("#notifiCount").append(data.count);
                   $("#notifications").empty();
                    $("#notifications").append('<br><h5 class="m-b-0 centered">Notifications</h5>');
                    $.each(data, function(i,item){
                      delete data['count'];
                      if (data[i].id == undefined) {
                         $("#notifications").append('<div class="d-flex no-block align-items-center p-10"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0 centered">'+data[i].key+'</h5><span class="mail-desc"></span></div></div>');
                        }else{
                          $("#notifications").append('<div class="d-flex no-block align-items-center p-10 link border-top"><span class="btn btn btn-light btn-circle" onclick="delNotify('+data[i].id+')"><i class="m-r-10 mdi mdi-delete-variant text-danger"></i></span> <a onclick="delNotify('+data[i].id+')"  href="'+getUrl()+data[i].link+'" class=""><div class="m-l-10"><h5 class="m-b-0"><span id="'+data[i].head+'">'+data[i].head+'</span></h5><span class="mail-desc">'+data[i].message+'</span> </div>  </a> </div>');  
                       }            
                    });
                  }
             })
       }

  function delNotify(id){
     var url=getUrl()+'/admin/dashboard/delete_notify/'+id;      
          $.ajax({
             'async':false,
              url: url,
              dataType: "json",
                success: function(data){
                      getNotifications();    
                  },
               }); 
                
        } 

  function getSavedForms(){
     var url= getUrl()+'/admin/dashboard/userSaved_forms';      
            $.ajax({
               'async':false,
                url: url,
                dataType: "json",
                type : 'POST',
                  success: function(data){
                   $("#savedFormsCount").empty();
                   $("#savedFormsCount").append(data.count);
                   $("#savedForms").empty();
                    $("#savedForms").append('<br><h5 class="m-b-0 centered">Saved Froms</h5>');
                    $.each(data, function(i,item){
                      delete data['count'];
                      if (data[i].id == undefined) {
                         $("#savedForms").append('<div class="d-flex no-block align-items-center p-10"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0 centered">'+data[i].key+'</h5><span class="mail-desc"></span></div></div>');
                        }else{
                        $("#savedForms").append('<div class="d-flex no-block align-items-center p-10 link border-top"> <span class="btn btn btn-light btn-circle" onclick="delSavedForms('+data[i].id+')"><i class="mdi mdi-reply-all text-danger"></i></span> <a href="'+getUrl()+data[i].link+'" class=""><div class="m-l-10"><h5 class="m-b-0"><span class="savedPageTitle" id="pageTitle'+data[i].page_title+'">'+data[i].page_title+'</span></h5><span class="mail-desc">'+data[i].page_info+'</span> </div>  </a> </div>');  
                        //$("#savedForms").append('<div class="d-flex no-block align-items-center p-10  after-hover"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0" style="color:#27a9e3"><span><span class="savedPageTitle" id="pageTitle'+data[i].page_title+'">'+data[i].page_title+'</span><a onclick="delSavedForms('+data[i].id+')" href="javascript:void(0)" class="float-right" style="color:red;margin-right:5px;"><i class="fa fa-trash-alt sm wait-hover"></i></a><span></h5><a  href="'+getUrl()+data[i].link+'"><span class="mail-desc">'+data[i].page_info+'</span></a></div></div><div class="dropdown-divider"></div>');
                       }            
                    });
                  }
             })
       }

  function delSavedForms(id){
     var url=getUrl()+'/admin/dashboard/delete_savedForm/'+id;      
          $.ajax({
             'async':false,
              url: url,
              dataType: "json",
                success: function(data){
                      getSavedForms();    
                  },
               }); 
                
        }      

    function getMessaages(){
      var url= getUrl()+'/admin/dashboard/messages';      
      $.ajax({
        'async':false,
        url: url,
        dataType: "json",
        type : 'POST',
        success: function(data){
          $("#messgCount").empty();
          $("#messgCount").append(data.count);
          $("#messages").empty();
          $("#messages").append('<br><h5 class="m-b-0 centered">Messages</h5>');
          $.each(data, function(i,item){
            delete data['count'];
            if (data[i].id == undefined) {
              $("#messages").append('<div class="d-flex no-block align-items-center p-10"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0 centered">'+data[i].key+'</h5><span class="mail-desc"></span></div></div>');
            }else{
              $("#messages").append('<div class="d-flex no-block align-items-center p-10  after-hover"><div class="m-l-10" style="margin-left:10px;"><h5 class="m-b-0" style="color:#27a9e3">'+data[i].username+'<a onclick="delMessage('+data[i].id+')" href="javascript:void(0)" class="fa fa-trash-alt sm wait-hover float-right" style="color:red;margin-right:5px;"></a></h5><a onclick="readMessage('+data[i].id+')" href"" id="Toggled-'+data[i].id+'" class="card-header link border-top"><i class="fa fa-arrow-right"></i><span>See Message...</span></a><div id="Toggle-'+data[i].id+'" style="display:none;"><div class="card-body widget-content">'+data[i].message+'</div></div></div></div><div class="dropdown-divider"></div>');
            }            
          });
        }
      })
    }

    function delMessage(id){
      var url=getUrl()+'/admin/dashboard/delete_message/'+id;      
      $.ajax({
        'async':false,
        url: url,
        dataType: "json",
        success: function(data){
          getMessaages();    
        },
      }); 
    } 

    function readMessage(id){
      $('#Toggle-'+id).toggle();
      var url=getUrl()+'/admin/dashboard/read_message/'+id;      
      $.ajax({
        'async':false,
        url: url,
        dataType: "json",
        success: function(data){
          //getMessaages();    
        },
      }); 
    } 