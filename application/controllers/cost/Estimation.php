<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Estimation extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('cost/Estimation_model');
      $this->load->model('admin/Departments_model');
      $this->data['module']        = $this->Modules_model->get_module_by(31);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'cost/estimation/index_estimation';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function estimation_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Estimation_model->get_estimations($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'estimation\',\'estimation\',\'cost/estimation\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('cost/estimation/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('cost/estimation/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Cost Estimation #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';    
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['subject'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Estimation_model->get_all_estimations($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Estimation_model->get_estimations($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'cost/estimation');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','estimation');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'subject'                 => $this->input->post('subject'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $estimation_id  =  $this->Estimation_model->add_estimation($fdata);
        if ($estimation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Cost Estimation #'.$estimation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $estimation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['estimation_id']    = $estimation_id;
            $item_id                  = $this->Estimation_model->add_estimation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $estimation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $estimation_id);
        }
      }
      $data['view'] = 'cost/estimation/estimation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($estimation_id){
      $data['estimation']          = $this->Estimation_model->get_estimation($estimation_id);
      if (($data['estimation']['status'] == 2 || $data['estimation']['status'] == 1) && $data['estimation']['reback']) {
        $rrData = json_decode($data['estimation']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('estimation', array('status' => 3, 'role_id' => 0), "id = ".$estimation_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/estimation', $data['estimation'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, 0, 0, 0, 0, 0, 'Viewed Cost Estimation #'.$estimation_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$estimation_id,'files');
      $this->data['form_id']       = $estimation_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'cost/estimation/view_estimation';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($estimation_id){
      $data['estimation']             = $this->Estimation_model->get_estimation($estimation_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/estimation', $data['estimation'], $this->data['uhotels']);
      $data['estimation_items']          = $this->Estimation_model->get_estimation_items($estimation_id);
      $this->load->view('cost/estimation/view_details',$data);
    }  

    public function signers_items($estimation_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $estimation_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $estimation_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($estimation_id=''){
      $data['estimation']   = $this->Estimation_model->get_estimation($estimation_id);
      edit_checker($data['estimation'], $this->data['permission']['edit'], 'cost/estimation/view/'.$estimation_id);
      $data['estimation_items']          = $this->Estimation_model->get_estimation_items($estimation_id);
      $data['estimation_id']   = $estimation_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $estimation_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$estimation_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'subject'                 => $this->input->post('subject'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Estimation_model->edit_estimation($fdata, $estimation_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, 0,json_encode($data['estimation'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Cost Estimation #'.$estimation_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Estimation_model->get_estimation_item($item['id']);
            $item['estimation_id']  = $estimation_id;
            $updated                = $this->Estimation_model->edit_estimation_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['name']);
            }
          }else{
            $item['estimation_id']  = $estimation_id;
            $item_id                = $this->Estimation_model->add_estimation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['name'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $estimation_id);
      } 
      $data['view'] = 'cost/estimation/estimation_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['estimation']     = $this->Estimation_model->get_estimation($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'cost/estimation', $data['estimation'], $this->data['uhotels']);
      $data['estimation_items'] = $this->Estimation_model->get_estimation_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']           = get_file_code('files','estimation');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'subject'                 => $this->input->post('subject'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $estimation_id  =  $this->Estimation_model->add_estimation($fdata);
        if ($estimation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Cost Estimation #'.$estimation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $estimation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['estimation_id']    = $estimation_id;
            $item_id                  = $this->Estimation_model->add_estimation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $estimation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $estimation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $estimation_id);
        }
      }
      $data['view'] = 'cost/estimation/estimation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Estimation_model->get_estimation_item($id);
        $this->db->update('estimation_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['estimation_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'cost/estimation');
      $this->db->update('estimation', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Cost Estimation #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>