<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Pricing extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('cost/Pricing_model');
      $this->data['module']        = $this->Modules_model->get_module_by(38);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'cost/pricing/index_pricing';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function pricing_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Pricing_model->get_pricings($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'pricing\',\'pricing\',\'cost/pricing\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('cost/pricing/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('cost/pricing/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Pricing Daily Used Items #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['subject'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Pricing_model->get_all_pricings($this->data['uhotels']),
       "recordsFiltered" => $this->Pricing_model->get_pricings($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'cost/pricing');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['pitems']          = $this->General_model->get_meta_data('pricing_items');
      $data['gen_id']          = get_file_code('files','pricing');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'subject'                 => $this->input->post('subject'),
          'from_date'               => $this->input->post('from_date'),
          'to_date'                 => $this->input->post('to_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $pricing_id  =  $this->Pricing_model->add_pricing($fdata);
        if ($pricing_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Pricing Daily Used Items #'.$pricing_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $pricing_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['pricing_id']    = $pricing_id;
            $item_name                = metaName($item['item']);
            $item_id                  = $this->Pricing_model->add_pricing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $pricing_id);
        }
      }
      $data['view'] = 'cost/pricing/pricing_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($pricing_id){
      $data['pricing']          = $this->Pricing_model->get_pricing($pricing_id);
      if (($data['pricing']['status'] == 2 || $data['pricing']['status'] == 1) && $data['pricing']['reback']) {
        $rrData = json_decode($data['pricing']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('pricing', array('status' => 3, 'role_id' => 0), "id = ".$pricing_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/pricing', $data['pricing'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, 0, 0, 0, 0, 0, 'Viewed Pricing Daily Used Items #'.$pricing_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$pricing_id,'files');
      $this->data['form_id']       = $pricing_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'cost/pricing/view_pricing';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($pricing_id){
      $data['pricing']             = $this->Pricing_model->get_pricing($pricing_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/pricing', $data['pricing'], $this->data['uhotels']);
      $data['pricing_items']          = $this->Pricing_model->get_pricing_items($pricing_id);
      $this->load->view('cost/pricing/view_details',$data);
    }  

    public function signers_items($pricing_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $pricing_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $pricing_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($pricing_id=''){
      $data['pricing']   = $this->Pricing_model->get_pricing($pricing_id);
      edit_checker($data['pricing'], $this->data['permission']['edit'], 'cost/pricing/view/'.$pricing_id);
      $data['pricing_items']          = $this->Pricing_model->get_pricing_items($pricing_id);
      $data['pricing_id']  = $pricing_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['pitems']          = $this->General_model->get_meta_data('pricing_items');
      $data['gen_id']          = $pricing_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$pricing_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'subject'                 => $this->input->post('subject'),
          'from_date'               => $this->input->post('from_date'),
          'to_date'                 => $this->input->post('to_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Pricing_model->edit_pricing($fdata, $pricing_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, 0,json_encode($data['pricing'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Pricing Daily Used Items #'.$pricing_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Pricing_model->get_pricing_item($item['id']);
            $item['pricing_id']  = $pricing_id;
            $item_name              = metaName($item['item']);
            $updated                = $this->Pricing_model->edit_pricing_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item_name);
            }
          }else{
            $item['pricing_id']  = $pricing_id;
            $item_name              = metaName($item['item']);
            $item_id                = $this->Pricing_model->add_pricing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item_name.'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $pricing_id);
      } 
      $data['view'] = 'cost/pricing/pricing_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['pricing']     = $this->Pricing_model->get_pricing($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'cost/pricing', $data['pricing'], $this->data['uhotels']);
      $data['pricing_items'] = $this->Pricing_model->get_pricing_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['pitems']           = $this->General_model->get_meta_data('pricing_items');
      $data['gen_id']           = get_file_code('files','pricing');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'subject'                 => $this->input->post('subject'),
          'from_date'               => $this->input->post('from_date'),
          'to_date'                 => $this->input->post('to_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $pricing_id  =  $this->Pricing_model->add_pricing($fdata);
        if ($pricing_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Pricing Daily Used Items #'.$pricing_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $pricing_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['pricing_id']    = $pricing_id;
            $item_name                = metaName($item['item']);
            $item_id                  = $this->Pricing_model->add_pricing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $pricing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $pricing_id);
        }
      }
      $data['view'] = 'cost/pricing/pricing_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Pricing_model->get_pricing_item($id);
        $this->db->update('pricing_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['pricing_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'cost/pricing');
      $this->db->update('pricing', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Pricing Daily Used Items #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>