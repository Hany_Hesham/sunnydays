<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Po extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('cost/Po_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(50);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'cost/po/index_po';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function po_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Po_model->get_pos($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'po\',\'po\',\'cost/po\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('cost/po/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('cost/po/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Purchase Order #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['supplier'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['store'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['pr_no'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['po_no'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Po_model->get_all_pos($this->data['uhotels']),
       "recordsFiltered" => $this->Po_model->get_pos($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'cost/po');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','po');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'supplier'                => $this->input->post('supplier'),
          'store'                   => $this->input->post('store'),
          'pr_no'                   => $this->input->post('pr_no'),
          'po_no'                   => $this->input->post('po_no'),
          'po_date'                 => $this->input->post('po_date'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $po_id  =  $this->Po_model->add_po($fdata);
        if ($po_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $po_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Purchase Order #'.$po_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $po_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['po_id']        = $po_id;
            $item_id              = $this->Po_model->add_po_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $po_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $po_id);
        }
      }
      $data['view'] = 'cost/po/po_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($po_id){
      $data['po']          = $this->Po_model->get_po($po_id);
      if (($data['po']['status'] == 2 || $data['po']['status'] == 1) && $data['po']['reback']) {
        $rrData = json_decode($data['po']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('po', array('status' => 3, 'role_id' => 0), "id = ".$po_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/po', $data['po'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $po_id, 0, 0, 0, 0, 0, 'Viewed Purchase Order #'.$po_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$po_id,'files');
      $this->data['form_id']       = $po_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'cost/po/view_po';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($po_id){
      $data['po']             = $this->Po_model->get_po($po_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'cost/po', $data['po'], $this->data['uhotels']);
      $data['po_items']          = $this->Po_model->get_po_items($po_id);
      $this->load->view('cost/po/view_details',$data);
    }  

    public function signers_items($po_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $po_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $po_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($po_id=''){
      $data['po']   = $this->Po_model->get_po($po_id);
      edit_checker($data['po'], $this->data['permission']['edit'], 'cost/po/view/'.$po_id);
      $data['po_items']          = $this->Po_model->get_po_items($po_id);
      $data['po_id']   = $po_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $po_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$po_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'supplier'                => $this->input->post('supplier'),
          'store'                   => $this->input->post('store'),
          'pr_no'                   => $this->input->post('pr_no'),
          'po_no'                   => $this->input->post('po_no'),
          'po_date'                 => $this->input->post('po_date'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Po_model->edit_po($fdata, $po_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $po_id, 0,json_encode($data['po'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Purchase Order #'.$po_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Po_model->get_po_item($item['id']);
            $item['po_id']  = $po_id;
            $updated                = $this->Po_model->edit_po_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $po_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['description']);
            }
          }else{
            $item['po_id']  = $po_id;
            $item_id                = $this->Po_model->add_po_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $po_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['description'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $po_id);
      } 
      $data['view'] = 'cost/po/po_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['po']     = $this->Po_model->get_po($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'cost/po', $data['po'], $this->data['uhotels']);
      $data['po_items'] = $this->Po_model->get_po_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']           = get_file_code('files','po');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'supplier'                => $this->input->post('supplier'),
          'store'                   => $this->input->post('store'),
          'pr_no'                   => $this->input->post('pr_no'),
          'po_no'                   => $this->input->post('po_no'),
          'po_date'                 => $this->input->post('po_date'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $po_id  =  $this->Po_model->add_po($fdata);
        if ($po_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $po_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Purchase Order #'.$po_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $po_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['po_id']    = $po_id;
            $item_id                  = $this->Po_model->add_po_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $po_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $po_id);
        }
      }
      $data['view'] = 'cost/po/po_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Po_model->get_po_item($id);
        $this->db->update('po_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['po_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['description'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'cost/po');
      $this->db->update('po', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Purchase Order #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>