<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends MY_Controller {
   
   		public function __construct(){
        	parent::__construct();
     	}
		 
		public function index(){
			$datestring 				= '%Y-%m-%d';
			$data['modules_counter']   	= form_modules();
			$data['view'] 				= 'admin/dashboard_index';
			$this->load->view('admin/includes/layout',$data);
		}

		public function to_sign_ajax($sign_type=''){
		    $dt_att              = $this->datatables_att();
		    $custom_search       = $this->input->post('module_id');
	    	$module              = $this->Modules_model->get_module_by($this->input->post('module_id'));
		    $data                = array();
		    $i                   = 1;		      
	        $rows = form_to_sign($module,$dt_att);    
		    foreach($rows as $row) {
		        $arr = array(); 
		        $tools=array();
		        $tools = array();
			    $tools []= '<a href="'.base_url(''.$module['view_link'].'/'. $row['id']).'" target="_blank" class="btn btn-outline-cyan btn-sm"><strong> <i class="fas fa-parachute-box"></i>&nbsp;Full View</strong></a><a href="javascript: void(0);" onclick="get_formView(`'.$module['view_link'].'`,'.$row['id'].','.$module['id'].','.$row['sign_id'].')"class="btn btn-outline-dark  btn-sm"><strong> <i class="fas fa-leaf"></i>&nbsp;Summary</strong></a>';
			    $arr[] = ' <a  href="'.base_url(''.$module['view_link'].'/'. $row['id']).'" target="_blank"><strong style="color:'.$module['text_color'].'">'.$i.'. '.$module['name'].' #'.$row['id'].'</strong></a><br><span style="color:'.$module['text_color'].'"><strong class="text-dark">Sign as </strong>'.$row['role_name'].' </span>';
			    $arr[] = '<span style="color:'.$module['text_color'].'">'.$row['hotel_name'].'</span>';
			    $arr[] = '<span style="color:'.$module['text_color'].'">'.($sign_type==''?implode("", $tools):'').'</span>';
			    $arr[] = '<span style="color:'.$module['text_color'].'">'.$row['timestamp'].'</span>';
			    $data[] =$arr;
			    $i++;
		   	}
	        $output = array(
			    "draw" => $dt_att['draw'],
			    "recordsTotal"    => total_rows($module['tdatabase'],$module['id']),
			    "recordsFiltered" => form_to_sign($module,$dt_att,'count'),
			    "data" => $data
			);
	        echo json_encode($output);
	        exit();
	  	}

		public function notifications(){
			if (!$this->input->is_ajax_request()) {
		    	redirect('admin/dashboard');	
			}
	    	$notifs = $this->General_model->get_notifications($this->data['user_id'], $this->data['uhotels'], $this->data['dep_id']);
			$data = array();
			if ($notifs) {
		    	foreach ($notifs as $notif) {
					$data[] =$notif;
				}
				$data['count'] = count($notifs);  
			}else{
		    	$data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No notifications found</h5><br>'];
			}
			echo json_encode($data);		        
			exit();
		} 

		public function userSaved_forms(){
			if (!$this->input->is_ajax_request()) {
		     	redirect('admin/dashboard');	
			}
	      	$saved_forms = $this->General_model->get_savedForms($this->data['user_id']);
		    $data = array();
	        if ($saved_forms) {	
	          	foreach ($saved_forms as $saved_form) {
					$data[] =$saved_form;
			   	}
			  	$data['count'] = count($saved_forms);  
			}else{
		      	$data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No Saved Forms found</h5><br>'];
		   	}
         	echo json_encode($data);
		  	exit();
	    }

    	public function save_form(){
		   $this->General_model->add_savedForms($_SESSION['user_id'],$this->input->post('form_data'));
		   echo json_encode('success');
		   exit();
		}         
    
    	public function delete_savedForm($id){
		 	$this->db->delete('saved_forms', array('id' =>$id));
		 	$this->userSaved_forms();
		}	    

		public function delete_notify($id){
		 	$this->db->update('notifications', array('readedby' =>$this->data['user_id'],'deleted' => 1,'status' => 1), "id = ".$id);
		 	$this->notifications();
		}

		public function messages(){
			if (!$this->input->is_ajax_request()) {
			    redirect('admin/dashboard');	
			}
		    $messages = $this->General_model->get_messages($this->data['user_id']);
			$data = array();
			if ($messages) {
			  	foreach ($messages as $message) {
					$data[] =$message;
				}
				$data['count'] = count($messages);  
			}else{
			    $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No messages found</h5><br>'];
			}
			echo json_encode($data);
			exit();
		} 

		public function delete_message($id){
			$this->db->update('messages', array('deletedby' =>$this->data['user_id'],'deleted' => 1), "id = ".$id);
			$this->messages();
		}

		public function read_message($id){
			$this->db->update('messages', array('readedby' =>$this->data['user_id'],'readed' => 1), "id = ".$id);
			$this->messages();
		}

		public function search(){
			if (!$this->input->is_ajax_request()) {
			    redirect('admin/dashboard');	
			}
			$search = $this->input->post('search');
		    $prs = $this->General_model->get_searchPr($this->data['uhotels'], $search);
			$data = array();
			if ($prs) {
			  	foreach ($prs as $pr) {
					$data[] = $pr;
				}
			}else{
			    $data[] =['key'=>'<div class="dropdown-divider"></div><h5 class="m-b-0 centered">No PRs found</h5><br>'];
			}
			echo json_encode($data);
			exit();
		} 

	}

?>