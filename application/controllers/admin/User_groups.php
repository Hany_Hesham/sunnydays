<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class User_groups extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/User_groups_model');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by(4);
	        $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index(){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');	
			$data['view']          = 'admin/backend/user_groups_index';
			$this->load->view('admin/includes/layout',$data);
		}  

	    public function user_groups_ajax(){       
	        $dt_att  = $this->datatables_att();
	        $user_groups   = $this->User_groups_model->get_user_groups_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
	        $data = array();
	        foreach($user_groups as $user_group) {
	           	$arr = array(); 
	            $arr[] = '<span class="info" id="user_groupId">'.$user_group['id'].'</span>';
	            $arr[] = '<a data-toggle="modal" href="#smallmodal" id="user_group" class="edit-user_group"><strong id="user_groupName" class="info" style="font-size:14px;">'.$user_group['name'].'</strong></a>';
	            if ($user_group['deleted'] ==1) {
                	$arr[] ='<label class="customcheckbox">
                      	<input type="hidden" value="0" name="deleted" class="listCheckbox">
                        <input type="checkbox" name="deleted" class="listCheckbox" value="1" class="switch-input" checked  onclick="changer('.$user_group['id'].',\'user_group\',\'user_groups\',\'deleted\',\'0\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	}else{
              		$arr[] ='<label class="customcheckbox">
                       	<input type="hidden" value="0" name="deleted" class="listCheckbox">
                        <input type="checkbox" name="deleted" class="listCheckbox" value="1" class="switch-input" onclick="changer('.$user_group['id'].',\'user_group\',\'user_groups\',\'deleted\',\'1\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	}    
	            $data[] =$arr;
	        }
	        $output = array(
	            "draw" => $dt_att['draw'],
	            "recordsTotal"    => $this->User_groups_model->get_all_user_groups(),
	            "recordsFiltered" => $this->User_groups_model->get_user_groups_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	            "data" => $data
	        );
	        echo json_encode($output);
	        exit();
        }

        public function user_group_process(){
			$data = [
			    'name'          => $this->input->post('name')
			];
			if (!$this->input->post('id')) {
				$user_group_id = $this->User_groups_model->add_user_group($data);
				if($user_group_id){
				  	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
				  	loger('Create',$this->data['module']['id'],'user_groups',$user_group_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'Created User Group '.$data['name'].'');
		            redirect('admin/user_groups');
				   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been created Successfully!']);
				}
			}else{
	            $user_group      = $this->User_groups_model->get_by_id($this->input->post('id'));
				$updated    = $this->User_groups_model->update_user_group($this->input->post('id'),$data);
				if ($updated) {
				   	loger('Update',$this->data['module']['id'],'user_groups',$user_group['id'],0,json_encode($user_group, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'Updated User Group '.$data['name'].'');
				   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
		            redirect('admin/user_groups');
				}
			}      
	    }

	    public function status_change($id,$column,$value){
            $this->db->update('user_groups', array($column => $value), "id = ".$id);
            if ($this->input->is_ajax_request()) {
            	if ($value == 1) { $disabled ='disabled';}else{$disabled ='enabled';}
	            loger(''.$disabled.'',$this->data['module']['id'],'user_groups',$id,0,0,0,0,0,''.$disabled.' User Group#'.$id.'');
           	}
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is '.$disabled.' Successfully!']);
			redirect(base_url('admin/user_groups'));
		}

	}

?>	 