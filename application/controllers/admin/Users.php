<?php defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Users extends MY_Controller {
		
		public function __construct(){
			parent::__construct();
			$this->load->model('admin/User_model'); 
	        $this->load->model('admin/Hotels_model');
	        $this->load->model('admin/Modules_model');
	        $this->load->library('user_agent');
	        $this->data['module']       = $this->Modules_model->get_module_by(1);
	        $this->data['permission']   = user_access($this->data['module']['id']);
		}

		public function index(){
       		access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,'admin/dashboard');
        	$this->load->helper('date_helper');
        	$datestring              = '%Y-%m-%d';
        	$data['from_date']       = date("Y-m-d", strtotime("-2 year"));
        	$data['to_date']         = mdate($datestring, time());
        	$data['roles']           = get_roles(); 
        	$data['hotels']          = $this->Hotels_model->get_hotels();
			$data['view'] = 'admin/users/users_index';
			$this->load->view('admin/includes/layout',$data);
		}

    	public function users_ajax(){
        	$dt_att               = $this->datatables_att();
        	$dt_att['module_id']  = $this->data['module']['id'];
        	$custom_search        = $this->input->post('searchBy');
        	if (isset($custom_search['hotel_id']) && $custom_search['hotel_id'] !='') {
           		$users_hotels    = $this->User_model->get_users_by_granted_hotels($custom_search['hotel_id']);
        	}else{
            	$users_hotels    = $this->User_model->get_users_by_granted_hotels($this->data['uhotels']);
        	}
        	$uids = array();
	        foreach ($users_hotels as $user) {
	          	if (!in_array($user['user_id'], $uids)) {  
	               	$uids[] = $user['user_id'];
	           	}
	        }  
	    	$users  = $this->User_model->get_users($uids,$dt_att,$custom_search,'');
	    	$data   = array();
        	$i      = 1;
        	foreach($users as $user) {
           	$arr    = array(); 
           	$tools  = array();
           		if ($this->data['permission']['remove'] == 1) {
                    $tools[] = '<div><a href="javascript: void(0);" title="Delete User" onclick="del('.$user['id'].',\'user\',\'users\',\'admin/users\',\'del\')"><span style="color:red;"> Delete</span></a> |';
             	}
                if ($this->data['permission']['edit'] == 1) {
                    $tools[] .='<a href="'.base_url('admin/users/user/'. $user['id']).'" title="View User"><span> view</span></a></div>'; 
                }
                $arr[] = '<span style="color:#2962FF;padding-left:20%;">'.$i.'</span>';
                $arr[] = '<a href="'.base_url('admin/users/user/'. $user['id']).'" title="View User">
                           <span style =" color:#2962FF" >'.$user['username'].'</span></a>
                         <br>'.implode("", $tools);
                $arr[] = '<span style ="color:#2962FF" >'.$user['fullname'].'</span>';
                $arr[] = '<span style ="color:#2962FF" >'.$user['email'].'</span>';
                $arr[] = '<span style ="color:#2962FF" >'.$user['role_name'].'</span>';
                $arr[] = '<span '.($user['is_admin']==1?'class="badge badge-danger"':'class="badge badge-dark"').'>'.($user['is_admin']==1?'Admin':'User').'</span>';
          		if ($user['disabled'] ==1) {
                	$arr[] ='<label class="customcheckbox">
                      	<input type="hidden" value="0" name="disabled" class="listCheckbox">
                        <input type="checkbox" name="disabled" class="listCheckbox" value="1" class="switch-input" checked  onclick="changer('.$user['id'].',\'user\',\'users\',\'disabled\',\'0\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	}else{
              		$arr[] ='<label class="customcheckbox">
                       	<input type="hidden" value="0" name="disabled" class="listCheckbox">
                        <input type="checkbox" name="disabled" class="listCheckbox" value="1" class="switch-input" onclick="changer('.$user['id'].',\'user\',\'users\',\'disabled\',\'1\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	} 
                $data[] =$arr;
                $i++;
            }
           	$output = array(
                "draw" => $dt_att['draw'],
                "recordsTotal" => $this->User_model->get_all_users($uids),
                "recordsFiltered" => $this->User_model->get_users($uids, $dt_att,$custom_search,'count'),
                "data" => $data
            );
      		echo json_encode($output);
      		exit();
 		}

		
		public function user($uid=FALSE){
        	access_checker(0,0,0,$this->data['permission']['edit'],0,0,0,0,'admin/users');
            $this->load->model('admin/Modules_model'); 
            $data['roles']           = get_roles(); 
            $data['departments']     = get_departments(); 
            $data['hotels']          = $this->Hotels_model->get_hotels();
	        $data['modules']         = $this->Modules_model->get_modules();
            if ($uid) {
			   	$data['user']            = $this->User_model->get_user_by_id($uid);
			   	$data['selected_hotels'] = $this->User_model->get_user_hotels($uid);
			    foreach ($data['selected_hotels'] as $key => $s_hotel) {
			    	$selected_roles = $this->User_model->get_user_roles($s_hotel['hotel_id'],$uid);
			    	$data['selected_hotels'][$key]['selected_roles']  = json_decode(implode(',',$selected_roles),true);
				}
	            if($data['user']['permission'] == 0){   
	               	foreach ($data['modules'] as $key => $module) {
		             	$data['modules'][$key]['permission'] = $this->User_model->get_group_permissions($data['user']['role_id'], $module['id']);
		            }
		        }else{
			        foreach ($data['modules'] as $key => $module) {
		               	$data['modules'][$key]['permission'] = $this->User_model->get_access_modules($uid, $module['id']);
		            }
	            }
            }
			$data['view'] = 'admin/users/user_add';
			$this->load->view('admin/includes/layout', $data);
      	}

		public function add(){
			$user_id = $this->input->post('id');
			$data = array(
				'username' => $this->input->post('username'),
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'mobile_no' => $this->input->post('mobile_no'),
				'role_id' => $this->input->post('role_id'),
				'department' => json_encode($this->input->post('departments[]'), JSON_UNESCAPED_UNICODE),
				'password' =>  password_hash($this->input->post('password'), PASSWORD_BCRYPT),
				'created_at' => date('Y-m-d : h:m:s'),
				'updated_at' => date('Y-m-d : h:m:s'),
			);
			$file_name                  = do_upload('signature', 'signatures', 1);
			if ($file_name) {
				$data['signature']             = $file_name;  
			}
        	if ($user_id == NULL) {
        		if(!$data['email']){
                	$data['email'] = NULL;
            	}
        		if(!$data['mobile_no']){
                	$data['mobile_no'] = NULL;
            	} 
        		if(count(json_decode($data['department'])) == 0){
                	$data['department'] = NULL;
            	}  
				$uid = $this->User_model->add_user($data);
				foreach ($this->input->post('hotels[]')  as  $hotel) {
			    	$this->User_model->add_user_hotels($uid,$hotel);
				}
		  		loger('Create',$this->data['module']['id'],'Users',$uid,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'created User no '.$uid.'');
		  		$this->add_permission($uid,$data['role_id']);
		  		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'record is added Successfully!']);	
          		redirect('admin/users/user/'.$uid);
			}else{
		   		if(!$this->input->post('password')){
              		unset($data['password']);
             	}
        		if(!$data['email']){
                	$data['email'] = NULL;
            	}
        		if(!$data['mobile_no']){
                	$data['mobile_no'] = NULL;
            	} 
        		if(count(json_decode($data['department'])) == 0){
                	$data['department'] = NULL;
            	} 
	    		$user_hotels_ids = array(); 
	    		$user_hotels     = $this->User_model->get_user_hotels($user_id);
            	foreach ($user_hotels as $user_hotel) {
             		$user_hotels_ids[] = $user_hotel['hotel_id'];
             	}
            	if (count($this->input->post('hotels[]')) > count($user_hotels_ids)) {
            		$extra_hotels = array_diff($this->input->post('hotels[]'),$user_hotels_ids);
            		foreach ($extra_hotels  as  $hotel) {
                	 	$this->User_model->add_user_hotels($user_id,$hotel);
            	 	}
              	}elseif ((int)count($this->input->post('hotels[]')) < (int)count($user_hotels_ids)) {
              		$delted_hotels = array_diff($user_hotels_ids,$this->input->post('hotels[]'));
              		foreach ($delted_hotels  as  $hotel) {
                	 	$this->User_model->delete_user_hotels($user_id,$hotel);
            	 	}
              	}elseif ((int)count($this->input->post('hotels[]')) == (int)count($user_hotels_ids)) {
              		$delted_hotels = array_diff($user_hotels_ids,$this->input->post('hotels[]'));
              	 	foreach ($delted_hotels  as  $hotel) {
                	 	$this->User_model->delete_user_hotels($user_id,$hotel);
            	  	}
                	$extra_hotels = array_diff($this->input->post('hotels[]'),$user_hotels_ids);
            	  	foreach ($extra_hotels  as  $hotel) {
                	 	$this->User_model->add_user_hotels($user_id,$hotel);
            	  	}
               	}
		   		$old_user = $this->User_model->get_user_by_id($user_id); 
		   		$this->User_model->edit_user($data,$user_id);
		   		loger('Update',$this->data['module']['id'],'Users',$user_id,$user_id,json_encode($old_user, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'Updated User: '.$old_user['id'].'');
           		$this->session->set_flashdata(['alert'=>'succsess','msg'=>'record has been Updated Successfully!']);
           		redirect('admin/users/user/'.$user_id);
			}
		}

		public function hotel_roles(){
			foreach ($this->input->post('hotels') as $key => $hotel_roles) {
				if (isset($hotel_roles['role_id'])) {
					$form_data = json_encode($hotel_roles['role_id'], JSON_UNESCAPED_UNICODE);
					$updated = $this->User_model->update_user_hotels($hotel_roles['id'],$form_data);
			 	}
		  	}
        	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'record has been Updated Successfully!']);
        	redirect($this->agent->referrer());
		}

		public function add_permission($user_id,$role_id=''){
		  	if (!$this->input->post('accs')) {
		  		$role_permissions = $this->User_model->get_AllGroup_permissions($role_id);
		  	}elseif ($this->input->post('accs')) {
		  	   	$role_permissions = $this->input->post('accs');
		  	}
		    foreach ($role_permissions as $module) {
		  	    if (isset($module['role_id'])) {
			  	    unset($module['role_id']);
			  	    unset($module['id']);
		  	    }
		        $module['user_id'] = $user_id;
		        if (!isset($module['id']) || $module['id'] == '') {
		           	$Permission = $this->User_model->add_permission($module);
		        }else{
		            $updated = $this->User_model->update_permission_by_user_id($module['id'], $user_id, $module);
		        }
		    }
		   	if($Permission || $updated){
			    status_changer('users','permission','1',$user_id);
		        if ($updated) {
		           	loger('Update',$this->data['module']['id'],'Users',$user_id,0,0,0,0,0,'updated permissions to user no '.$user_id.'');
		        }
		        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'record has been Updated Successfully!']);
		        redirect(base_url('admin/users/user/'.$user_id));
		    }
	    } 

	    public function del($id){
            $this->db->update('users', array('deleted' => 1), "id = ".$id);
            loger('delete',$this->data['module']['id'],'Users',$id,0,0,0,0,0,'Deleted User no '.$id.'');
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is Deleted Successfully!']);
			redirect(base_url('admin/users'));
		}

		public function status_change($id,$column,$value){
            $this->db->update('users', array($column => $value), "id = ".$id);
            if ($this->input->is_ajax_request()) {
            	if ($value == 1) { $disabled ='disabled';}else{$disabled ='enabled';}
	            loger(''.$disabled.'',$this->data['module']['id'],'Users',$id,0,0,0,0,0,''.$disabled.' User no '.$id.'');
           	}
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is Deleted Successfully!']);
			redirect(base_url('admin/users'));
		}

		public function bulk(){
		    $this->load->model('admin/Modules_model'); 
            $data['roles']   = get_roles(); 
            $data['modules'] = $this->Modules_model->get_modules();
            $data['posted_role'] =  $this->User_model->get_role($this->input->post('role_id'));   
	        if ($this->input->post('role_id')) {    
	            foreach ($data['modules'] as $key => $module) {
		            $data['modules'][$key]['permission'] = $this->User_model->get_group_permissions($data['posted_role']['id'],$module['id']);
		        }
		    }
	        $users_hotels    = $this->User_model->get_users_by_granted_hotels($this->data['uhotels']);
            $uids = array();
	        foreach ($users_hotels as $user) {
	          	if (!in_array($user['user_id'], $uids)) {  
	               $uids[] = $user['user_id'];
	           	}
	        } 
		    $data['users']   = $this->User_model->get_all_users_by_uids_role_id($uids,$this->input->post('role_id'));
			$data['view'] = 'admin/users/add_bulk';
			$this->load->view('admin/includes/layout', $data);	
        }


	    public function add_bulk(){       
		    foreach ($this->input->post('users') as $user_id) {
		 	    $user  = $this->User_model->get_user_by_id($user_id); 	
		        foreach ($this->input->post('accs') as $module) {
		            $module['user_id'] = $user_id;
		            if ($user['permission']==0) {
                        $Permission = $this->User_model->add_permission($module);
                    }else{
		                $updated = $this->User_model->update_permission_bulk($module['module_id'], $user_id, $module);
		            }
		        }
	        	status_changer('users','permission','1',$user_id);
		    }        
           	if($Permission || $updated){
               	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'record is added Successfully!']);
               	redirect(base_url('admin/users'));
            }else{
           	  	redirect(base_url('admin/users/bulk'));
           	}
      	} 

        public function group(){
		    $this->load->model('admin/Modules_model'); 
            $data['roles']   = get_roles(); 
	        $data['modules'] = $this->Modules_model->get_modules();   
	        if ($this->input->post('role_id')) {
		        $data['role'] =  $this->User_model->get_role($this->input->post('role_id'));  
		        foreach ($data['modules'] as $key => $module) {
			        $data['modules'][$key]['permission'] = $this->User_model->get_group_permissions($data['role']['id'],$module['id']);
			    }
	        }
			$data['view'] = 'admin/users/add_group';
			$this->load->view('admin/includes/layout', $data);
		}

		public function add_role_name(){
		  	$this->load->library('user_agent');
		  	$name = $this->input->post('role_name');
            $role_id = $this->User_model->add_role_name($name);
            if ($role_id) {
            	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'record is added Successfully!']);
            	redirect($this->agent->referrer());
            }
		}

		public function add_group(){
		   	$role_id = $this->input->post('role');
	        foreach ($this->input->post('accs') as $module) {
                $module['role_id'] = $role_id;
	            if (!isset($module['id'])) {
	                $Permission = $this->User_model->add_group_permission($module);
	           	}else{
	              	$updated = $this->User_model->update_group_permission_by_id($module['id'], $role_id, $module);
	            }
	        }
	        if($Permission || $updated){
	          	status_changer('user_groups','permission','1',$role_id);
	            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'record is added Successfully!']);
          		redirect($this->agent->referrer());
	        }
        } 

        public function change_pwd(){
        	$this->load->library('user_agent');
			$user = $this->User_model->get_user_by_id($this->data['user_id'], TRUE);
			if($this->input->post()){
				$data = array(
					'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT)
				);
				$result =  $this->User_model->edit_user($data,$this->data['user_id']);
				if($result){
					$this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Password has been changed Successfully!']);
					redirect($this->agent->referrer());
				}else{
					$this->session->set_flashdata(['alert'=>'Failure','msg'=>'Failed to update the password!']);
					redirect($this->agent->referrer());
				}
			}
		}    

	}

?>