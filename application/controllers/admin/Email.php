<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Email extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
	        $this->load->model('admin/Email_model');
	        $this->load->library('user_agent');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by(6);
	        $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index($module_id='',$form_id=''){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
		    $data['module_id'] = $module_id;
		   	$data['form_id']   = $form_id;	
			$data['view'] = 'admin/email/emails_view';
		    $this->load->view('admin/includes/layout',$data);
		}  

	    public function email_ajax($module_id='',$form_id=''){
	        $single  = ['module_id'=>$module_id,'form_id'=>$form_id]; 
	        $dt_att  = $this->datatables_att();
	        $emails    = $this->Email_model->get_emails($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'',$single);
	        $data = array();
	        foreach($emails as $email) {
	            $arr = array(); 
	            $arr[] = '<span id="" style="padding-left:7%;" class="info">'.$email['id'].'</span>';
	            $arr[] = '<a class="edit-group" onclick="getEmailData('.$email['id'].')" href="javascript: void(0);" data-toggle="modal" data-target="#smallmodal" titel="view Details"><strong style="font-size:16px;"class="info">'.ucfirst($email['type']).'</strong></a>';
                $arr[] = '<span id="">'.$email['timestamp'].'</span>';  
                if ($email['module_name']) {
	            	$arr[] = '<strong style="font-size:16px;"class="info">'.$email['module_name'].'</strong>';
	            }else{
	            	$arr[] = '<strong style="font-size:16px;"class="info">Send PO To Supplier</strong>';
	            }
	            $arr[] = '<span id="" class="info">'.$email['module_name'].' No #.'.$email['form_id'].'</span>';
	            $arr[] = '<span id="">'.$email['fullname'].'</span>';
	            $arr[] = '<span id="">'.$email['message'].'</span>'; 
	            if ($email['finished'] == 1) {
	                $arr[] = '<span id=""> Sent At '.$email['sent_at'].'</span>';
	            }else{
		            $arr[] = '<a class="edit-group" onclick="sendEmail('.$email['id'].')" href="javascript: void(0);" titel="Send Email"><strong style="font-size:16px;"class="info"><i class="fas fa-envelope"></i></strong></a>';
	            }
	            $data[] =$arr;
	       	}
	        $output = array(
	            "draw" => $dt_att['draw'],
	            "recordsTotal"    => $this->Email_model->get_all_emails($single),
	            "recordsFiltered" => $this->Email_model->get_emails($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count', $single),
	            "data" => $data
	        );
	        echo json_encode($output);
	        exit();
        }

        public function sendEmail(){
        	$email_id = $this->input->post('email_id');
	        $emails    = $this->Email_model->get_email($email_id);
	        foreach ($emails as $key => $email) {
		        $sent = emailIt($email['module_id'], $email['form_id'], $email['emails'], $email['message']);
		        if ($sent) {
		          $this->Sign_model->updateMailData($email['id']);
		        }
	        }
	        echo json_encode(true);
	        exit();
	    }

	}

?>	 