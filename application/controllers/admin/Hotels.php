<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Hotels extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
            $this->load->model('admin/Hotels_model');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by(2);
	        $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index(){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');	
		    $data['groups']        = get_hotels_groups();
			$data['view']          = 'admin/backend/hotels_index';
			$this->load->view('admin/includes/layout',$data);
		}  

	    public function hotels_ajax(){       
	        $dt_att  = $this->datatables_att();
	        $hotels   = $this->Hotels_model->get_hotels_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'');
	        $data = array();
	        foreach($hotels as $hotel) {
	           	$arr = array(); 
	            $arr[] = '<span class="info" id="hotelId">'.$hotel['id'].'</span>';
	            $arr[] = '<a data-toggle="modal" href="#smallmodal" id="hotel" class="edit-hotel"><strong id="hotelName" class="info" style="font-size:14px;">'.$hotel['hotel_name'].'</strong></a>';
	            $arr[] = '<span class="hidden" id="group_id">'.$hotel['group_id'].'</span><span class="info" id="hotelGroup">'.$hotel['hotel_group'].'</span>';
	            $arr[] = '<span id="hotelCode" >'.$hotel['code'].'</span>';     
	            if ($hotel['deleted'] ==1) {
                	$arr[] ='<label class="customcheckbox">
                      	<input type="hidden" value="0" name="deleted" class="listCheckbox">
                        <input type="checkbox" name="deleted" class="listCheckbox" value="1" class="switch-input" checked  onclick="changer('.$hotel['id'].',\'hotel\',\'hotels\',\'deleted\',\'0\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	}else{
              		$arr[] ='<label class="customcheckbox">
                       	<input type="hidden" value="0" name="deleted" class="listCheckbox">
                        <input type="checkbox" name="deleted" class="listCheckbox" value="1" class="switch-input" onclick="changer('.$hotel['id'].',\'hotel\',\'hotels\',\'deleted\',\'1\')" '.($this->global_data['sessioned_user']['is_admin']==1?'':'disabled').'>
                        <span class="checkmark"></span>
                    </label>';
              	}    
	            $data[] =$arr;
	        }
	        $output = array(
	            "draw" => $dt_att['draw'],
	            "recordsTotal"    => $this->Hotels_model->get_all_hotels(),
	            "recordsFiltered" => $this->Hotels_model->get_hotels_ajax($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count'),
	            "data" => $data
	        );
	        echo json_encode($output);
	        exit();
        }

        public function hotel_process(){
			$data = [
			    'hotel_name'          => $this->input->post('hotel_name'),
			    'group_id'            => $this->input->post('group_id'),
			   	'code'                => $this->input->post('code')
			];
			$file_name                  = do_upload('logo', 'logos');
			if ($file_name) {
	    		$data['logo']             = $file_name;  
			}
			if (!$this->input->post('id')) {
				$hotel_id = $this->Hotels_model->add_hotel($data);
				if($hotel_id){
				  	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
				  	loger('Create',$this->data['module']['id'],'Hotels',$hotel_id,0,json_encode($data, JSON_UNESCAPED_UNICODE),0,0,0,'Created Hotel '.$data['hotel_name'].'');
		            redirect('admin/hotels');
				   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been created Successfully!']);
				}
			}else{
	            $hotel      = $this->Hotels_model->get_by_id($this->input->post('id'));
				$updated    = $this->Hotels_model->update_hotel($this->input->post('id'),$data);
				if ($updated) {
				   	loger('Update',$this->data['module']['id'],'Hotels',$hotel['id'],0,json_encode($hotel, JSON_UNESCAPED_UNICODE),json_encode($data, JSON_UNESCAPED_UNICODE),0,0,'Updated Hotel '.$data['hotel_name'].'');
				   	$this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
		            redirect('admin/hotels');
				}
			}      
	    }

	    public function status_change($id,$column,$value){
            $this->db->update('hotels', array($column => $value), "id = ".$id);
            if ($this->input->is_ajax_request()) {
            	if ($value == 1) { $disabled ='disabled';}else{$disabled ='enabled';}
	            loger(''.$disabled.'',$this->data['module']['id'],'Hotels',$id,0,0,0,0,0,''.$disabled.' Hotel#'.$id.'');
           	}
            $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is '.$disabled.' Successfully!']);
			redirect(base_url('admin/hotels'));
		}

	}

?>	 