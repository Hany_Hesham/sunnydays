<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Auth extends CI_Controller {

		public function __construct(){
			parent::__construct();
			$this->load->model('admin/Auth_model');
			$this->load->model('admin/User_model');
		}
		public function index(){
			redirect('admin/auth/login');
		}


		public function login(){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('username', 'username', 'trim|required');
				$this->form_validation->set_rules('password', 'Password', 'trim|required');

				if ($this->form_validation->run() == FALSE) {
					$this->load->view('admin/auth/login');
				}
				else {
					$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password')
					);
					$result = $this->Auth_model->login($data);
					if ($result == TRUE) {
						if (!is_null($result['otp'])) {
							otpNotify($result['id']);
							redirect('admin/auth/verify');
						}else{
							$admin_data = array(
								'user_id' => $result['id'],
							 	'name'    => $result['username'],
							 	'is_admin_login' => TRUE
							);
							$this->session->set_userdata($admin_data);
		   					if ($this->session->has_userdata('red_url')) {
				                $red_url = $this->session->userdata('red_url');
				                $this->session->unset_userdata('red_url');
				                redirect($red_url);
					        }else{
								redirect('admin/dashboard');
							}
						}

					}
					else{
						$data['msg'] = 'Invalid Email or Password!';
						$this->load->view('admin/auth/login', $data);
					}
				}
			}
			else{
				$this->load->view('admin/auth/login');
			}
		}

		public function verify(){
			if($this->input->post('submit')){
				$this->form_validation->set_rules('token', 'token', 'trim|required');
				if ($this->form_validation->run() == FALSE) {
					$this->load->view('admin/auth/verify');
				}
				else {
					$data = array(
					'token' => $this->input->post('token')
					);
					$result = $this->Auth_model->verify($data);
					if ($result == TRUE) {
						$admin_data = array(
							'user_id' => $result['id'],
						 	'name'    => $result['username'],
						 	'is_admin_login' => TRUE
						);
						$this->session->set_userdata($admin_data);
	   					if ($this->session->has_userdata('red_url')) {
			                $red_url = $this->session->userdata('red_url');
			                $this->session->unset_userdata('red_url');
			                redirect($red_url);
				        }else{
							redirect('admin/dashboard');
						}
					}
					else{
						$data['msg'] = 'Invalid Token!';
						$this->load->view('admin/auth/verify', $data);
					}
				}
			}
			else{
				$this->load->view('admin/auth/verify');
			}
		}	

				
		public function logout(){
			$this->session->sess_destroy();
			redirect(base_url('admin/auth/login'), 'refresh');
		}


			
	} 


?>