<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Log_activity extends MY_Controller {

		public function __construct(){
			parent::__construct();
            $this->load->model('admin/Modules_model');
	        $this->load->model('admin/General_model');
	        $this->load->model('admin/Log_model');
	        $this->load->library('user_agent');
	        $this->data['user_id']      = $this->global_data['sessioned_user']['id'];
			$this->data['username']     = $this->global_data['sessioned_user']['username'];
			$this->data['module']       = $this->Modules_model->get_module_by(5);
            $this->data['permission']   = user_access($this->data['module']['id']);
	        $this->data['uhotels']      = get_uhotels($this->data['user_id']);
		}

        public function index($module_id='',$form_id=''){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
		    $data['module_id'] = $module_id;
	        $data['form_id']   = $form_id;	
			$data['view'] = 'admin/log/logs_view';
		    $this->load->view('admin/includes/layout',$data);
		}  

	    public function log_ajax($module_id='',$form_id=''){
	        $single  = ['module_id'=>$module_id,'form_id'=>$form_id]; 
	    	$dt_att  = $this->datatables_att();
	        $logs    = $this->Log_model->get_logs($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'',$single);
	        $data = array();
	        foreach($logs as $log) {
	            $arr = array(); 
	            $arr[] = '<span id="" style="padding-left:7%;" class="info">'.$log['id'].'</span>';
	            $arr[] = '<a class="edit-group" onclick="getLogData('.$log['id'].')" href="javascript: void(0);" data-toggle="modal" data-target="#smallmodal" titel="view Details"><strong style="font-size:16px;"class="info">'.ucfirst($log['action']).'</strong></a>';
                $arr[] = '<span id="">'.$log['log_time'].'</span>';  
	            $arr[] = '<strong style="font-size:16px;"class="info">'.$log['target'].'</strong>';
	            $arr[] = '<span id="" class="info">'.$log['module_name'].' No #.'.$log['target_id'].'</span>';
	            if ($log['fullname']) {
	                $arr[] = '<span id="">'.$log['fullname'].'</span>';
	            }else{
	            	$arr[] = '<span id="">Supplier</span>';
                }
	                $arr[] = '<span id="">'.$log['comments'].'</span>';                
	                $data[] =$arr;
	        }
	    	$output = array(
	        	"draw" => $dt_att['draw'],
	            "recordsTotal"    => $this->Log_model->get_all_logs($single),
	            "recordsFiltered" => $this->Log_model->get_logs($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count', $single),
	            "data" => $data
	        );
          	echo json_encode($output);
	        exit();
        }

		public function indexed($user_id=''){
		    access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,0,'admin/dashboard');
		    $data['user_id'] = $user_id;
			$data['view'] = 'admin/log/loged_view';
		    $this->load->view('admin/includes/layout',$data);
		}  

	    public function loged_ajax($user_id=''){
	        $single  = ['user_id'=>$user_id]; 
	    	$dt_att  = $this->datatables_att();
	        $logs    = $this->Log_model->get_loged($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'',$single);
	        $data = array();
	        foreach($logs as $log) {
	            $arr = array(); 
	            $arr[] = '<span id="" style="padding-left:7%;" class="info">'.$log['id'].'</span>';
	            $arr[] = '<a class="edit-group" onclick="getLogData('.$log['id'].')" href="javascript: void(0);" data-toggle="modal" data-target="#smallmodal" titel="view Details"><strong style="font-size:16px;"class="info">'.ucfirst($log['action']).'</strong></a>';
                $arr[] = '<span id="">'.$log['log_time'].'</span>';  
	            $arr[] = '<strong style="font-size:16px;"class="info">'.$log['target'].'</strong>';
	            $arr[] = '<span id="" class="info">'.$log['module_name'].' No #.'.$log['target_id'].'</span>';
	            if ($log['fullname']) {
	                $arr[] = '<span id="">'.$log['fullname'].'</span>';
	            }else{
	            	$arr[] = '<span id="">Supplier</span>';
                }
	            $arr[] = '<span id="">'.$log['comments'].'</span>';                
	            $data[] =$arr;
	        }
	    	$output = array(
	        	"draw" => $dt_att['draw'],
	            "recordsTotal"    => $this->Log_model->get_all_loged($single),
	            "recordsFiltered" => $this->Log_model->get_loged($dt_att['start'],$dt_att['length'],$dt_att['search'],$dt_att['order'],$dt_att['col_name'],'count', $single),
	            "data" => $data
	        );
          	echo json_encode($output);
	      	exit();
        }
	}

?>	 