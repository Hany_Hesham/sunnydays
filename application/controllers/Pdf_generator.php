<?php defined('BASEPATH') OR exit('No direct script access allowed');

    class Pdf_generator extends CI_Controller {

        public function __construct(){
          parent::__construct();
          $this->load->model(array('admin/Hotels_model','admin/Modules_model',''));
          $this->load->helper('sign_helper');
          $this->load->helper('general_helper');
          $this->load->helper('mpdf_helper');
        }
        private function genrate_view_pdf($generated_html, $author, $pdf_name, $save = '', $watermark = 'SUNRISE E-SIGNATURE', $landscape= ''){
          $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
          try {
              $mpdf = new \Mpdf\Mpdf([
                'margin_left'   => 10,
                'margin_right'  => 10,
                'margin_bottom' => 25,
                'margin_footer' => 10
              ]);
              if ($landscape) {
	              $mpdf->AddPage('L');
              }
              $mpdf->SetProtection(array('print'));
              $mpdf->SetTitle($author);
              $mpdf->SetAuthor($author);
              $mpdf->SetWatermarkText($watermark);
              $mpdf->showWatermarkText = true;
              $mpdf->watermark_font = 'DejaVuSansCondensed';
              $mpdf->watermarkTextAlpha = 0.1;
              $mpdf->SetDisplayMode('fullpage');
              $mpdf->WriteHTML($generated_html);
              $mpdf->debug = true;
              ob_end_clean();
              $pdfFilePath =  $pdf_name.'.pdf';
              if ($save) {
                $mpdf->Output($pdfFilePath, "D");            
              }else{
                $mpdf->Output($pdfFilePath, "I");            
              }
          }catch (\Mpdf\MpdfException $e) { 
                 echo $e->getMessage();
          }
          echo json_encode($pdfFilePath);
          exit();
        }

        public function wakeup_print(){
          $this->load->model('front_office/wakeup_model');        
          $this->load->view('front_office/wakeup/wakeup_pdf');
          $module               = $this->Modules_model->get_module_by(67);
          $data['wakeup_items'] = $this->wakeup_model->get_wakeups_pdf_data($this->input->post());
          if (!$data['wakeup_items']) {
              $this->session->set_flashdata(['alert'=>'Failure','msg'=>'Sorry there is no wake up calls matching the given date!']);
              redirect('front_office/wakeup');
          }
          $data['hotel_logo']   = ($data['wakeup_items'] ? $data['wakeup_items'][0]['h_logo'] : '');
          $data['hotel_name']   = ($data['wakeup_items'] ? $data['wakeup_items'][0]['hotel_name'] : '');
          $data['from_date']    = $this->input->post()['from_date']; 
          $data['to_date']      = $this->input->post()['to_date'];
          $data['username']     = $this->data['username'];
          $fdata = array('user_id' => $_SESSION['user_id']);
          $generated_html = wakeup_pdf_generator($data);   
          loger('Print', $module['id'], $module['name'], 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print wakeup '.date('Y-m-d').'', 'print');
          $this->genrate_view_pdf($generated_html, $data['hotel_name'], 'wakeup '. date('Y-m-d h:m:s').'');
        }

         public function uniform_report_print(){
          $this->load->model('uniform/Uniform_order_model');        
          $this->load->view('uniform/uniform_order/uniform_report_pdf');
          $module               = $this->Modules_model->get_module_by(77);
          $year                           = $this->input->post('year');
          $type_id                        = $this->input->post('type_id');
          $season                         = $this->input->post('season');
          $hid                            = $this->input->post('hid');
          $items                          = $this->Uniform_order_model->get_uniqueItems();
          foreach ($items as $key => $test) {
            $items[$key]['orders']          = $this->Uniform_order_model->get_uniqueItems_orders($test['code'], $test['group_code'], $year, $type_id, $season, $hid);
            $items[$key]['total_38']        = 0;
            $items[$key]['total_40']        = 0;
            $items[$key]['total_42']        = 0;
            $items[$key]['total_44']        = 0;
            $items[$key]['total_46']        = 0;
            $items[$key]['total_48']        = 0;
            $items[$key]['total_s']         = 0;
            $items[$key]['total_m']         = 0;
            $items[$key]['total_l']         = 0;
            $items[$key]['total_xl']        = 0;
            $items[$key]['total_xxl']       = 0;
            $items[$key]['total_xxxl']      = 0;
            $items[$key]['total_linen']     = 0;
            $items[$key]['total_others']    = 0;
            $items[$key]['total_quantity']  = 0;
            $items[$key]['total_price']     = 0;
            foreach ($items[$key]['orders'] as $keys => $order) {
              $total                          =  0;
              $totalprice                     =  0;
              $items[$key]['total_38']        += $order['size_38'];
              $total                          += $order['size_38'];
              $items[$key]['total_40']        += $order['size_40'];
              $total                          += $order['size_40'];
              $items[$key]['total_42']        += $order['size_42'];
              $total                          += $order['size_42'];
              $items[$key]['total_44']        += $order['size_44'];
              $total                          += $order['size_44'];
              $items[$key]['total_46']        += $order['size_46'];
              $total                          += $order['size_46'];
              $items[$key]['total_48']        += $order['size_48'];
              $total                          += $order['size_48'];
              $items[$key]['total_s']         += $order['size_s'];
              $total                          += $order['size_s'];
              $items[$key]['total_m']         += $order['size_m'];
              $total                          += $order['size_m'];
              $items[$key]['total_l']         += $order['size_l'];
              $total                          += $order['size_l'];
              $items[$key]['total_xl']        += $order['size_xl'];
              $total                          += $order['size_xl'];
              $items[$key]['total_xxl']       += $order['size_xxl'];
              $total                          += $order['size_xxl'];
              $items[$key]['total_xxxl']      += $order['size_xxxl'];
              $total                          += $order['size_xxxl'];
              $items[$key]['total_linen']     += $order['size_linen'];
              $total                          += $order['size_linen'];
              $items[$key]['total_others']    += $order['size_others'];
              $totalquantity                  = $total + $order['size_others'];
              $totalprice                     = ($total * $items[$key]['price']) + ($order['size_others'] * $items[$key]['price'] * 1.15);
              $items[$key]['total_quantity']  += $totalquantity;
              $items[$key]['total_price']     += $totalprice;
            }
            unset($items[$key]['orders']);
            if ($items[$key]['total_price'] == 0 && $items[$key]['total_quantity'] == 0) {
              unset($items[$key]);
            }
          }
          $data['items']                = $items;
          $data['hotel']    = $this->gethotel_data($hid);
          $data['year']     = $year; 
          $data['type']     =  metaName($type_id);
          $data['season']   =  metaName($season);
          $data['username'] = $this->data['username'];
          $fdata = array('user_id' => $_SESSION['user_id']);
          $generated_html = uniform_report_pdf_generator($data);   
          loger('Print', $module['id'], $module['name'], 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print Uniform Report '.date('Y-m-d').'', 'print');
          $this->genrate_view_pdf($generated_html, $data['hotel_name'], 'Uniform Report '. date('Y-m-d h:m:s').'');
        }

      
      private function gethotel_data($hotels){
        if ( count($hotels) > 1 ) {
             $data['hotel']['hotel_name'] = '';
             $hotels      = $this->Hotels_model->get_hotels_by_ids($hotels);
             $data['hotel']=array();
             foreach ($hotels as $hotel) {
               $data['hotel']['name'][] = $hotel['code'];
               $data['hotel']['logo']   = 'SR-Master.png';
              }
              if (count($data['hotel']['name']) <= 21) {
                  $hotels_names_chunk = array_chunk($data['hotel']['name'],7);
                  foreach($hotels_names_chunk as $hotels_names)
                  {
                   $data['hotel']['hotel_name']  .= implode(',', $hotels_names).'<br>';
                  }
              }else{
                $data['hotel']['hotel_name'] .= 'SUNRISE RESORTS & CRUISES';
              }
             // die(print_r($data['hotel']['name']));
          }elseif ( count($hotels) == 1) {
             $data['hotel'] = $this->Hotels_model->get_by_id($hotels[0]);
         }
         return $data['hotel'];
      }

      public function memo_print($memo_id){
        $this->load->model('chairman/Memo_model');        
        $this->load->view('chairman/memo/memo_pdf');
        $module             = $this->Modules_model->get_module_by(64);
        $data['memo']   = $this->Memo_model->get_memo($memo_id);
        $data['signatures'] = getSigners($module['id'],$data['memo']['id']);
        $fdata = array('user_id' => $_SESSION['user_id']);
        $generated_html=memo_pdf_generator($data);   
        loger('Print', $module['id'], $module['name'], $memo_id, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print Memo #'.$memo_id.'', 'print');
        $this->genrate_view_pdf($generated_html, 'SUNRISE Resorts & Cruises', 'Memo #'. $data['memo']['id'].'');
      }

      public function manning_guide_print($manning_guide_id, $save = ''){
        $this->load->model('financial/Manning_guide_model');        
        $this->load->view('financial/manning_guide/manning_guide_pdf');
        $module             = $this->Modules_model->get_module_by(83);
        $data['manning_guide']   = $this->Manning_guide_model->get_manning_guide($manning_guide_id);
        $data['signatures'] = getSigners($module['id'],$data['manning_guide']['id']);
        $fdata = array('user_id' => $_SESSION['user_id']);
        $generated_html=manning_guide_pdf_generator($data);   
        loger('Print', $module['id'], $module['name'], $manning_guide_id, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print Manning Guide #'.$manning_guide_id.'', 'print');
        $this->genrate_view_pdf($generated_html, 'SUNRISE Resorts & Cruises', 'Manning Guide #'. $data['manning_guide']['id'].'', $save, '', '1');
      }

      public function salary_scale_print($salary_scale_id, $save = ''){
        $this->load->model('financial/Salary_scale_model');        
        $this->load->view('financial/salary_scale/salary_scale_pdf');
        $module             = $this->Modules_model->get_module_by(83);
        $data['salary_scale']   = $this->Salary_scale_model->get_salary_scale($salary_scale_id);
        $data['signatures'] = getSigners($module['id'],$data['salary_scale']['id']);
        $fdata = array('user_id' => $_SESSION['user_id']);
        $generated_html=salary_scale_pdf_generator($data);   
        loger('Print', $module['id'], $module['name'], $salary_scale_id, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print Salary Scale #'.$salary_scale_id.'', 'print');
        $this->genrate_view_pdf($generated_html, 'SUNRISE Resorts & Cruises', 'Salary Scale #'. $data['salary_scale']['id'].'', $save, '', '1');
      }

      public function chairman_memo_print($chairman_memo_id, $save = ''){
        $this->load->model('chairman/Chairman_memo_model');        
        $this->load->view('chairman/chairman_memo/chairman_memo_pdf');
        $module                   = $this->Modules_model->get_module_by(86);
        $data['chairman_memo']    = $this->Chairman_memo_model->get_chairman_memo($chairman_memo_id);
        $data['signatures']       = getSigners($module['id'],$data['chairman_memo']['id']);
        $data['toUsers']          = $this->User_model->get_owned_users(json_decode($data['chairman_memo']['tos']));
        $data['ccUsers']          = $this->User_model->get_owned_users(json_decode($data['chairman_memo']['cc']));
        $fdata = array('user_id' => $_SESSION['user_id']);
        $generated_html=chairman_memo_pdf_generator($data);   
        loger('Print', $module['id'], $module['name'], $chairman_memo_id, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print Chairman Memo #'.$chairman_memo_id.'', 'print');
        $this->genrate_view_pdf($generated_html, 'SUNRISE Resorts & Cruises', 'Chairman Memo #'. $data['chairman_memo']['id'].'', $save, $data['chairman_memo']['company_name']);
      }  

      public function fborder_report_print(){
          $this->load->model('front_office/Fborder_model');        
          $this->load->view('front_office/fborder/fborder_report_pdf');
          $module           = $this->Modules_model->get_module_by(27);
          $year             = $this->input->post('year');
          $hid              = $this->input->post('hid');
          $items            = $this->Fborder_model->get_allItems($year, $hid);
          $data['items']    = $items;
          $data['year']     = $year;
          $data['hotel']    = $this->gethotel_data($hid);
          $data['username'] = $this->data['username'];
          $fdata = array('user_id' => $_SESSION['user_id']);
          $generated_html = fborder_report_pdf_generator($data);   
          loger('Print', $module['id'], $module['name'], 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 0, 'Print FB Orders '.date('Y-m-d').'', 'print');
          $this->genrate_view_pdf($generated_html, $data['hotel_name'], 'FB Orders Report '. date('Y-m-d h:m:s').'');
        }

  }

?>   