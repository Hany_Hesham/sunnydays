<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Plastic extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('steward/Plastic_model');
      $this->load->model('admin/Departments_model');
      $this->data['module']        = $this->Modules_model->get_module_by(51);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'steward/plastic/index_plastic';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function plastic_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Plastic_model->get_plastics($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'plastic\',\'plastic\',\'steward/plastic\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('steward/plastic/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('steward/plastic/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Plastics Out Permit #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';    
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Plastic_model->get_all_plastics($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Plastic_model->get_plastics($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'steward/plastic');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','plastic');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $plastic_id  =  $this->Plastic_model->add_plastic($fdata);
        if ($plastic_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Plastics Out Permit #'.$plastic_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $plastic_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['plastic_id']    = $plastic_id;
            $item_id                  = $this->Plastic_model->add_plastic_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['item'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $plastic_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $plastic_id);
        }
      }
      $data['view'] = 'steward/plastic/plastic_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($plastic_id){
      $data['plastic']          = $this->Plastic_model->get_plastic($plastic_id);
      if (($data['plastic']['status'] == 2 || $data['plastic']['status'] == 1) && $data['plastic']['reback']) {
        $rrData = json_decode($data['plastic']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('plastic', array('status' => 3, 'role_id' => 0), "id = ".$plastic_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'steward/plastic', $data['plastic'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, 0, 0, 0, 0, 0, 'Viewed Plastics Out Permit #'.$plastic_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$plastic_id,'files');
      $this->data['form_id']       = $plastic_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'steward/plastic/view_plastic';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($plastic_id){
      $data['plastic']             = $this->Plastic_model->get_plastic($plastic_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'steward/plastic', $data['plastic'], $this->data['uhotels']);
      $data['plastic_items']          = $this->Plastic_model->get_plastic_items($plastic_id);
      $this->load->view('steward/plastic/view_details',$data);
    }  

    public function signers_items($plastic_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $plastic_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $plastic_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($plastic_id=''){
      $data['plastic']   = $this->Plastic_model->get_plastic($plastic_id);
      edit_checker($data['plastic'], $this->data['permission']['edit'], 'steward/plastic/view/'.$plastic_id);
      $data['plastic_items']          = $this->Plastic_model->get_plastic_items($plastic_id);
      $data['plastic_id']   = $plastic_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $plastic_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$plastic_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Plastic_model->edit_plastic($fdata, $plastic_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, 0,json_encode($data['plastic'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Plastics Out Permit #'.$plastic_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Plastic_model->get_plastic_item($item['id']);
            $item['plastic_id']  = $plastic_id;
            $updated                = $this->Plastic_model->edit_plastic_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['item']);
            }
          }else{
            $item['plastic_id']  = $plastic_id;
            $item_id                = $this->Plastic_model->add_plastic_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['item'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $plastic_id);
      } 
      $data['view'] = 'steward/plastic/plastic_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['plastic']     = $this->Plastic_model->get_plastic($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'steward/plastic', $data['plastic'], $this->data['uhotels']);
      $data['plastic_items'] = $this->Plastic_model->get_plastic_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']           = get_file_code('files','plastic');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $plastic_id  =  $this->Plastic_model->add_plastic($fdata);
        if ($plastic_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Plastics Out Permit #'.$plastic_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $plastic_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['plastic_id']    = $plastic_id;
            $item_id                  = $this->Plastic_model->add_plastic_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $plastic_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['item'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $plastic_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $plastic_id);
        }
      }
      $data['view'] = 'steward/plastic/plastic_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Plastic_model->get_plastic_item($id);
        $this->db->update('plastic_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['plastic_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['item'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'steward/plastic');
      $this->db->update('plastic', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Plastics Out Permit #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>