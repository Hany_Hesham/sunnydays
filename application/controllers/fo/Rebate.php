<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Rebate extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('fo/Rebate_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(15);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'fo/rebate/index_rebate';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function rebate_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Rebate_model->get_rebates($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'rebate\',\'rebate\',\'fo/rebate\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('fo/rebate/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('fo/rebate/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Rebate #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['acc_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Rebate_model->get_all_rebates($this->data['uhotels']),
       "recordsFiltered" => $this->Rebate_model->get_rebates($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'fo/rebate');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','rebate');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'acc_no'                  => $this->input->post('acc_no'),
          'exp'                     => $this->input->post('exp'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $rebate_id  =  $this->Rebate_model->add_rebate($fdata);
        if ($rebate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $rebate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Rebate #'.$rebate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $rebate_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $rebate_id);
        }
      }
      $data['view'] = 'fo/rebate/rebate_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($rebate_id){
      $data['rebate']       = $this->Rebate_model->get_rebate($rebate_id);
      if (($data['rebate']['status'] == 2 || $data['rebate']['status'] == 1) && $data['rebate']['reback']) {
        $rrData = json_decode($data['rebate']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('rebate', array('status' => 3, 'role_id' => 0), "id = ".$rebate_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/rebate', $data['rebate'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $rebate_id, 0, 0, 0, 0, 0, 'Viewed Rebate #'.$rebate_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$rebate_id,'files');
      $this->data['form_id']       = $rebate_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'fo/rebate/view_rebate';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($rebate_id){
      $data['rebate']             = $this->Rebate_model->get_rebate($rebate_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/rebate', $data['rebate'], $this->data['uhotels']);
      $this->load->view('fo/rebate/view_details',$data);
    }    

    public function signers_items($rebate_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $rebate_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $rebate_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($rebate_id=''){
      $data['rebate']   = $this->Rebate_model->get_rebate($rebate_id);
      edit_checker($data['rebate'], $this->data['permission']['edit'], 'fo/rebate/view/'.$rebate_id);
      $data['rebate_id']      = $rebate_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $rebate_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$rebate_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'acc_no'                  => $this->input->post('acc_no'),
          'exp'                     => $this->input->post('exp'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Rebate_model->edit_rebate($fdata, $rebate_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $rebate_id, 0,json_encode($data['rebate'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Rebate #'.$rebate_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $rebate_id);
      } 
      $data['view'] = 'fo/rebate/rebate_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['rebate']   = $this->Rebate_model->get_rebate($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'fo/rebate', $data['rebate'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','rebate');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'acc_no'                  => $this->input->post('acc_no'),
          'exp'                     => $this->input->post('exp'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $rebate_id  =  $this->Rebate_model->add_rebate($fdata);
        if ($rebate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $rebate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Rebate #'.$rebate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $rebate_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $rebate_id);
        }
      }
      $data['view'] = 'fo/rebate/rebate_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'fo/rebate');
      $this->db->update('rebate', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Rebate #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>