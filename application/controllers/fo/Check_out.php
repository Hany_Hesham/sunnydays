<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Check_out extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('fo/Check_out_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(14);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'fo/check_out/index_check_out';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function check_out_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Check_out_model->get_check_outs($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'check_out\',\'check_out\',\'fo/check_out\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('fo/check_out/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('fo/check_out/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Free Late Check Out authorization Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['arrival'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['departure'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['departure_time'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Check_out_model->get_all_check_outs($this->data['uhotels']),
       "recordsFiltered" => $this->Check_out_model->get_check_outs($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'fo/check_out');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','check_out');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'travel_agent'            => $this->input->post('travel_agent'),
          'departure_time'          => $this->input->post('departure_time'),
          'reasons'                 => $this->input->post('reasons'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $check_out_id  =  $this->Check_out_model->add_check_out($fdata);
        if ($check_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $check_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Free Late Check Out authorization Form #'.$check_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $check_out_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $check_out_id);
        }
      }
      $data['view'] = 'fo/check_out/check_out_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($check_out_id){
      $data['check_out']       = $this->Check_out_model->get_check_out($check_out_id);
      if (($data['check_out']['status'] == 2 || $data['check_out']['status'] == 1) && $data['check_out']['reback']) {
        $rrData = json_decode($data['check_out']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('check_out', array('status' => 3, 'role_id' => 0), "id = ".$check_out_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/check_out', $data['check_out'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $check_out_id, 0, 0, 0, 0, 0, 'Viewed Free Late Check Out authorization Form #'.$check_out_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$check_out_id,'files');
      $this->data['form_id']       = $check_out_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'fo/check_out/view_check_out';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($check_out_id){
      $data['check_out']             = $this->Check_out_model->get_check_out($check_out_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/check_out', $data['check_out'], $this->data['uhotels']);
      $this->load->view('fo/check_out/view_details',$data);
    }    

    public function signers_items($check_out_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $check_out_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $check_out_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($check_out_id=''){
      $data['check_out']   = $this->Check_out_model->get_check_out($check_out_id);
      edit_checker($data['check_out'], $this->data['permission']['edit'], 'fo/check_out/view/'.$check_out_id);
      $data['check_out_id']      = $check_out_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $check_out_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$check_out_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'travel_agent'            => $this->input->post('travel_agent'),
          'departure_time'          => $this->input->post('departure_time'),
          'reasons'                 => $this->input->post('reasons'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Check_out_model->edit_check_out($fdata, $check_out_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $check_out_id, 0,json_encode($data['check_out'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Free Late Check Out authorization Form #'.$check_out_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $check_out_id);
      } 
      $data['view'] = 'fo/check_out/check_out_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['check_out']   = $this->Check_out_model->get_check_out($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'fo/check_out', $data['check_out'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','check_out');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'room_no'                 => $this->input->post('room_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'travel_agent'            => $this->input->post('travel_agent'),
          'departure_time'          => $this->input->post('departure_time'),
          'reasons'                 => $this->input->post('reasons'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $check_out_id  =  $this->Check_out_model->add_check_out($fdata);
        if ($check_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $check_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Free Late Check Out authorization Form #'.$check_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $check_out_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $check_out_id);
        }
      }
      $data['view'] = 'fo/check_out/check_out_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'fo/check_out');
      $this->db->update('check_out', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Free Late Check Out authorization Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>