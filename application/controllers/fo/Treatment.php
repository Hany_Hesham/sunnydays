<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Treatment extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('fo/Treatment_model');
      $this->data['module']        = $this->Modules_model->get_module_by(27);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'fo/treatment/index_treatment';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function treatment_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Treatment_model->get_treatments($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'treatment\',\'treatment\',\'fo/treatment\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('fo/treatment/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('fo/treatment/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Treatment Order #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Treatment_model->get_all_treatments($this->data['uhotels']),
       "recordsFiltered" => $this->Treatment_model->get_treatments($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'fo/treatment');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['amenitys']        = $this->General_model->get_meta_data('amenitys');
      $data['vip_types']       = $this->General_model->get_meta_data('vip_types');
      $data['guest_statuss']   = $this->General_model->get_meta_data('guest_statuss');
      $data['gen_id']          = get_file_code('files','treatment');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $treatment_id  =  $this->Treatment_model->add_treatment($fdata);
        if ($treatment_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Treatment Order #'.$treatment_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $treatment_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['treatment_id']    = $treatment_id;
            if($item['amenity']){
              $amenity = json_encode($item['amenity'], JSON_UNESCAPED_UNICODE);
              unset($item['amenity']);
              $item['amenity']  = $amenity;
            }
            $item_id                  = $this->Treatment_model->add_treatment_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $treatment_id);
        }
      }
      $data['view'] = 'fo/treatment/treatment_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($treatment_id){
      $data['treatment']          = $this->Treatment_model->get_treatment($treatment_id);
      if (($data['treatment']['status'] == 2 || $data['treatment']['status'] == 1) && $data['treatment']['reback']) {
        $rrData = json_decode($data['treatment']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('treatment', array('status' => 3, 'role_id' => 0), "id = ".$treatment_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/treatment', $data['treatment'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, 0, 0, 0, 0, 0, 'Viewed Treatment Order #'.$treatment_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$treatment_id,'files');
      $this->data['form_id']       = $treatment_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'fo/treatment/view_treatment';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($treatment_id){
      $data['treatment']             = $this->Treatment_model->get_treatment($treatment_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/treatment', $data['treatment'], $this->data['uhotels']);
      $data['treatment_items']          = $this->Treatment_model->get_treatment_items($treatment_id);
      $this->load->view('fo/treatment/view_details',$data);
    }  

    public function signers_items($treatment_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $treatment_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $treatment_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($treatment_id=''){
      $data['treatment']   = $this->Treatment_model->get_treatment($treatment_id);
      edit_checker($data['treatment'], $this->data['permission']['edit'], 'fo/treatment/view/'.$treatment_id);
      $data['treatment_items']          = $this->Treatment_model->get_treatment_items($treatment_id);
      $data['treatment_id']    = $treatment_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['amenitys']        = $this->General_model->get_meta_data('amenitys');
      $data['vip_types']       = $this->General_model->get_meta_data('vip_types');
      $data['guest_statuss']   = $this->General_model->get_meta_data('guest_statuss');
      $data['gen_id']          = $treatment_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$treatment_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Treatment_model->edit_treatment($fdata, $treatment_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, 0,json_encode($data['treatment'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Treatment Order #'.$treatment_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Treatment_model->get_treatment_item($item['id']);
            $item['treatment_id']   = $treatment_id;
            $amenity = json_encode($item['amenity'], JSON_UNESCAPED_UNICODE);
            unset($item['amenity']);
            $item['amenity']  = $amenity;
            $updated                = $this->Treatment_model->edit_treatment_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['room']);
            }
          }else{
            $item['treatment_id']   = $treatment_id;
            $amenity = json_encode($item['amenity'], JSON_UNESCAPED_UNICODE);
            unset($item['amenity']);
            $item['amenity']  = $amenity;
            $item_id                = $this->Treatment_model->add_treatment_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $treatment_id);
      } 
      $data['view'] = 'fo/treatment/treatment_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['treatment']     = $this->Treatment_model->get_treatment($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'fo/treatment', $data['treatment'], $this->data['uhotels']);
      $data['treatment_items']  = $this->Treatment_model->get_treatment_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['amenitys']        = $this->General_model->get_meta_data('amenitys');
      $data['vip_types']       = $this->General_model->get_meta_data('vip_types');
      $data['guest_statuss']   = $this->General_model->get_meta_data('guest_statuss');
      $data['gen_id']           = get_file_code('files','treatment');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $treatment_id  =  $this->Treatment_model->add_treatment($fdata);
        if ($treatment_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Treatment Order #'.$treatment_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $treatment_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['treatment_id']    = $treatment_id;
            $amenity = json_encode($item['amenity'], JSON_UNESCAPED_UNICODE);
            unset($item['amenity']);
            $item['amenity']  = $amenity;
            $item_id                  = $this->Treatment_model->add_treatment_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $treatment_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $treatment_id);
        }
      }
      $data['view'] = 'fo/treatment/treatment_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Treatment_model->get_treatment_item($id);
        $this->db->update('treatment_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['treatment_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'fo/treatment');
      $this->db->update('treatment', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Treatment Order #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>