<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Rr_change extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('fo/Rr_change_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(17);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'fo/rr_change/index_rr_change';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function rr_change_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Rr_change_model->get_rr_changes($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'rr_change\',\'rr_change\',\'fo/rr_change\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('fo/rr_change/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('fo/rr_change/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Room/Rate Change #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Rr_change_model->get_all_rr_changes($this->data['uhotels']),
       "recordsFiltered" => $this->Rr_change_model->get_rr_changes($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'fo/rr_change');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','rr_change');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'from_room'               => $this->input->post('from_room'),
          'to_room'                 => $this->input->post('to_room'),
          'from_rate'               => $this->input->post('from_rate'),
          'to_rate'                 => $this->input->post('to_rate'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $rr_change_id  =  $this->Rr_change_model->add_rr_change($fdata);
        if ($rr_change_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $rr_change_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room/Rate Change #'.$rr_change_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $rr_change_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $rr_change_id);
        }
      }
      $data['view'] = 'fo/rr_change/rr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($rr_change_id){
      $data['rr_change']       = $this->Rr_change_model->get_rr_change($rr_change_id);
      if (($data['rr_change']['status'] == 2 || $data['rr_change']['status'] == 1) && $data['rr_change']['reback']) {
        $rrData = json_decode($data['rr_change']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('rr_change', array('status' => 3, 'role_id' => 0), "id = ".$rr_change_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/rr_change', $data['rr_change'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $rr_change_id, 0, 0, 0, 0, 0, 'Viewed Room/Rate Change #'.$rr_change_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$rr_change_id,'files');
      $this->data['form_id']       = $rr_change_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'fo/rr_change/view_rr_change';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($rr_change_id){
      $data['rr_change']             = $this->Rr_change_model->get_rr_change($rr_change_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/rr_change', $data['rr_change'], $this->data['uhotels']);
      $this->load->view('fo/rr_change/view_details',$data);
    }    

    public function signers_items($rr_change_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $rr_change_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $rr_change_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($rr_change_id=''){
      $data['rr_change']   = $this->Rr_change_model->get_rr_change($rr_change_id);
      edit_checker($data['rr_change'], $this->data['permission']['edit'], 'fo/rr_change/view/'.$rr_change_id);
      $data['rr_change_id']      = $rr_change_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $rr_change_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$rr_change_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'from_room'               => $this->input->post('from_room'),
          'to_room'                 => $this->input->post('to_room'),
          'from_rate'               => $this->input->post('from_rate'),
          'to_rate'                 => $this->input->post('to_rate'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Rr_change_model->edit_rr_change($fdata, $rr_change_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $rr_change_id, 0,json_encode($data['rr_change'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Room/Rate Change #'.$rr_change_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $rr_change_id);
      } 
      $data['view'] = 'fo/rr_change/rr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['rr_change']   = $this->Rr_change_model->get_rr_change($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'fo/rr_change', $data['rr_change'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','rr_change');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'from_room'               => $this->input->post('from_room'),
          'to_room'                 => $this->input->post('to_room'),
          'from_rate'               => $this->input->post('from_rate'),
          'to_rate'                 => $this->input->post('to_rate'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $rr_change_id  =  $this->Rr_change_model->add_rr_change($fdata);
        if ($rr_change_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $rr_change_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room/Rate Change #'.$rr_change_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $rr_change_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $rr_change_id);
        }
      }
      $data['view'] = 'fo/rr_change/rr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'fo/rr_change');
      $this->db->update('rr_change', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Room/Rate Change #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>