<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Reservations extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('fo/Reservations_model');
      $this->data['module']        = $this->Modules_model->get_module_by(24);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'fo/reservations/index_reservations';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function reservations_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Reservations_model->get_reservationss($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'reservations\',\'reservations\',\'fo/reservations\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('fo/reservations/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('fo/reservations/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Reservation Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['arrival'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['departure'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['type_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Reservations_model->get_all_reservationss($this->data['uhotels']),
       "recordsFiltered" => $this->Reservations_model->get_reservationss($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'fo/reservations');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']           = $this->General_model->get_meta_data('reservation_type');
      $data['room_types']      = $this->General_model->get_meta_data('room_types');
      $data['gen_id']          = get_file_code('files','reservations');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'name'                    => $this->input->post('name'),
          'agent'                   => $this->input->post('agent'),
          'contact'                 => $this->input->post('contact'),
          'phone'                   => $this->input->post('phone'),
          'fax'                     => $this->input->post('fax'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'payment_method'          => $this->input->post('payment_method'),
          'credit_card'             => $this->input->post('credit_card'),
          'voucher'                 => $this->input->post('voucher'),
          'receipt'                 => $this->input->post('receipt'),
          'instruction'             => $this->input->post('instruction'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $reservations_id  =  $this->Reservations_model->add_reservations($fdata);
        if ($reservations_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Reservation Form #'.$reservations_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $reservations_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['reservations_id']    = $reservations_id;
            $item_name                = metaName($item['type_id']);
            $item_id                  = $this->Reservations_model->add_reservations_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $reservations_id);
        }
      }
      $data['view'] = 'fo/reservations/reservations_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($reservations_id){
      $data['reservations']          = $this->Reservations_model->get_reservations($reservations_id);
      if (($data['reservations']['status'] == 2 || $data['reservations']['status'] == 1) && $data['reservations']['reback']) {
        $rrData = json_decode($data['reservations']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('reservations', array('status' => 3, 'role_id' => 0), "id = ".$reservations_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/reservations', $data['reservations'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, 0, 0, 0, 0, 0, 'Viewed Reservation Form #'.$reservations_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$reservations_id,'files');
      $this->data['form_id']       = $reservations_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'fo/reservations/view_reservations';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($reservations_id){
      $data['reservations']             = $this->Reservations_model->get_reservations($reservations_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'fo/reservations', $data['reservations'], $this->data['uhotels']);
      $data['reservations_items']          = $this->Reservations_model->get_reservations_items($reservations_id);
      $this->load->view('fo/reservations/view_details',$data);
    }  

    public function signers_items($reservations_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $reservations_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $reservations_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($reservations_id=''){
      $data['reservations']   = $this->Reservations_model->get_reservations($reservations_id);
      edit_checker($data['reservations'], $this->data['permission']['edit'], 'fo/reservations/view/'.$reservations_id);
      $data['reservations_items']          = $this->Reservations_model->get_reservations_items($reservations_id);
      $data['reservations_id'] = $reservations_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']           = $this->General_model->get_meta_data('reservation_type');
      $data['room_types']      = $this->General_model->get_meta_data('room_types');
      $data['gen_id']          = $reservations_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$reservations_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'name'                    => $this->input->post('name'),
          'agent'                   => $this->input->post('agent'),
          'contact'                 => $this->input->post('contact'),
          'phone'                   => $this->input->post('phone'),
          'fax'                     => $this->input->post('fax'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'payment_method'          => $this->input->post('payment_method'),
          'credit_card'             => $this->input->post('credit_card'),
          'voucher'                 => $this->input->post('voucher'),
          'receipt'                 => $this->input->post('receipt'),
          'instruction'             => $this->input->post('instruction'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Reservations_model->edit_reservations($fdata, $reservations_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, 0,json_encode($data['reservations'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Reservation Form #'.$reservations_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Reservations_model->get_reservations_item($item['id']);
            $item['reservations_id']  = $reservations_id;
            $item_name              = metaName($item['type_id']);
            $updated                = $this->Reservations_model->edit_reservations_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item_name);
            }
          }else{
            $item['reservations_id']  = $reservations_id;
            $item_name                = metaName($item['type_id']);
            $item_id                = $this->Reservations_model->add_reservations_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item_name.'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $reservations_id);
      } 
      $data['view'] = 'fo/reservations/reservations_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['reservations']     = $this->Reservations_model->get_reservations($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'fo/reservations', $data['reservations'], $this->data['uhotels']);
      $data['reservations_items'] = $this->Reservations_model->get_reservations_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']            = $this->General_model->get_meta_data('reservation_type');
      $data['room_types']       = $this->General_model->get_meta_data('room_types');
      $data['gen_id']           = get_file_code('files','reservations');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'name'                    => $this->input->post('name'),
          'agent'                   => $this->input->post('agent'),
          'contact'                 => $this->input->post('contact'),
          'phone'                   => $this->input->post('phone'),
          'fax'                     => $this->input->post('fax'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'payment_method'          => $this->input->post('payment_method'),
          'credit_card'             => $this->input->post('credit_card'),
          'voucher'                 => $this->input->post('voucher'),
          'receipt'                 => $this->input->post('receipt'),
          'instruction'             => $this->input->post('instruction'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $reservations_id  =  $this->Reservations_model->add_reservations($fdata);
        if ($reservations_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Reservation Form #'.$reservations_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $reservations_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['reservations_id']    = $reservations_id;
            $item_name                = metaName($item['type_id']);
            $item_id                  = $this->Reservations_model->add_reservations_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $reservations_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $reservations_id);
        }
      }
      $data['view'] = 'fo/reservations/reservations_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Reservations_model->get_reservations_item($id);
        $this->db->update('reservations_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['reservations_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room_type'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'fo/reservations');
      $this->db->update('reservations', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Reservation Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>