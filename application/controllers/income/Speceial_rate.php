<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Speceial_rate extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Speceial_rate_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(20);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/speceial_rate/index_speceial_rate';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function speceial_rate_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Speceial_rate_model->get_speceial_rates($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'speceial_rate\',\'speceial_rate\',\'income/speceial_rate\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/speceial_rate/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/speceial_rate/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Speceial Rate #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Speceial_rate_model->get_all_speceial_rates($this->data['uhotels']),
       "recordsFiltered" => $this->Speceial_rate_model->get_speceial_rates($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/speceial_rate');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','speceial_rate');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $speceial_rate_id  =  $this->Speceial_rate_model->add_speceial_rate($fdata);
        if ($speceial_rate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Speceial Rate #'.$speceial_rate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $speceial_rate_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['speceial_rate_id']    = $speceial_rate_id;
            $item_id                  = $this->Speceial_rate_model->add_speceial_rate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $speceial_rate_id);
        }
      }
      $data['view'] = 'income/speceial_rate/speceial_rate_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($speceial_rate_id){
      $data['speceial_rate']          = $this->Speceial_rate_model->get_speceial_rate($speceial_rate_id);
      if (($data['speceial_rate']['status'] == 2 || $data['speceial_rate']['status'] == 1) && $data['speceial_rate']['reback']) {
        $rrData = json_decode($data['speceial_rate']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('speceial_rate', array('status' => 3, 'role_id' => 0), "id = ".$speceial_rate_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/speceial_rate', $data['speceial_rate'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, 0, 0, 0, 0, 0, 'Viewed Speceial Rate #'.$speceial_rate_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$speceial_rate_id,'files');
      $this->data['form_id']       = $speceial_rate_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/speceial_rate/view_speceial_rate';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($speceial_rate_id){
      $data['speceial_rate']             = $this->Speceial_rate_model->get_speceial_rate($speceial_rate_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/speceial_rate', $data['speceial_rate'], $this->data['uhotels']);
      $data['speceial_rate_items']          = $this->Speceial_rate_model->get_speceial_rate_items($speceial_rate_id);
      $this->load->view('income/speceial_rate/view_details',$data);
    }  

    public function signers_items($speceial_rate_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $speceial_rate_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $speceial_rate_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($speceial_rate_id=''){
      $data['speceial_rate']   = $this->Speceial_rate_model->get_speceial_rate($speceial_rate_id);
      edit_checker($data['speceial_rate'], $this->data['permission']['edit'], 'income/speceial_rate/view/'.$speceial_rate_id);
      $data['speceial_rate_items']          = $this->Speceial_rate_model->get_speceial_rate_items($speceial_rate_id);
      $data['speceial_rate_id']   = $speceial_rate_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $speceial_rate_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$speceial_rate_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Speceial_rate_model->edit_speceial_rate($fdata, $speceial_rate_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, 0,json_encode($data['speceial_rate'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Speceial Rate #'.$speceial_rate_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Speceial_rate_model->get_speceial_rate_item($item['id']);
            $item['speceial_rate_id']  = $speceial_rate_id;
            $updated                = $this->Speceial_rate_model->edit_speceial_rate_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['room']);
            }
          }else{
            $item['speceial_rate_id']  = $speceial_rate_id;
            $item_id                = $this->Speceial_rate_model->add_speceial_rate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $speceial_rate_id);
      } 
      $data['view'] = 'income/speceial_rate/speceial_rate_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['speceial_rate']     = $this->Speceial_rate_model->get_speceial_rate($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/speceial_rate', $data['speceial_rate'], $this->data['uhotels']);
      $data['speceial_rate_items'] = $this->Speceial_rate_model->get_speceial_rate_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']           = get_file_code('files','speceial_rate');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $speceial_rate_id  =  $this->Speceial_rate_model->add_speceial_rate($fdata);
        if ($speceial_rate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Speceial Rate #'.$speceial_rate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $speceial_rate_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['speceial_rate_id']    = $speceial_rate_id;
            $item_id                  = $this->Speceial_rate_model->add_speceial_rate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $speceial_rate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $speceial_rate_id);
        }
      }
      $data['view'] = 'income/speceial_rate/speceial_rate_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Speceial_rate_model->get_speceial_rate_item($id);
        $this->db->update('speceial_rate_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['speceial_rate_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/speceial_rate');
      $this->db->update('speceial_rate', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Speceial Rate #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>