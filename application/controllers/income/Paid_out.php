<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Paid_out extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Paid_out_model');
      $this->data['module']        = $this->Modules_model->get_module_by(23);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/paid_out/index_paid_out';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function paid_out_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Paid_out_model->get_paid_outs($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'paid_out\',\'paid_out\',\'income/paid_out\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/paid_out/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/paid_out/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Paid Out #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['payment_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['payment'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_refund'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['company'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Paid_out_model->get_all_paid_outs($this->data['uhotels']),
       "recordsFiltered" => $this->Paid_out_model->get_paid_outs($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/paid_out');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['gen_id']          = get_file_code('files','paid_out');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment'                 => $this->input->post('payment'),
          'currency'                => $this->input->post('currency'),
          'payment_date'            => $this->input->post('payment_date'),
          'payment_method'          => $this->input->post('payment_method'),
          'room'                    => $this->input->post('room'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'nationality'             => $this->input->post('nationality'),
          'company'                 => $this->input->post('company'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $paid_out_id  =  $this->Paid_out_model->add_paid_out($fdata);
        if ($paid_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $paid_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Paid Out #'.$paid_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $paid_out_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $paid_out_id);
        }
      }
      $data['view'] = 'income/paid_out/paid_out_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($paid_out_id){
      $data['paid_out']       = $this->Paid_out_model->get_paid_out($paid_out_id);
      if (($data['paid_out']['status'] == 2 || $data['paid_out']['status'] == 1) && $data['paid_out']['reback']) {
        $rrData = json_decode($data['paid_out']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('paid_out', array('status' => 3, 'role_id' => 0), "id = ".$paid_out_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/paid_out', $data['paid_out'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $paid_out_id, 0, 0, 0, 0, 0, 'Viewed Paid Out #'.$paid_out_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$paid_out_id,'files');
      $this->data['form_id']       = $paid_out_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/paid_out/view_paid_out';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($paid_out_id){
      $data['paid_out']             = $this->Paid_out_model->get_paid_out($paid_out_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/paid_out', $data['paid_out'], $this->data['uhotels']);
      $this->load->view('income/paid_out/view_details',$data);
    }    

    public function signers_items($paid_out_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $paid_out_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $paid_out_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($paid_out_id=''){
      $data['paid_out']   = $this->Paid_out_model->get_paid_out($paid_out_id);
      edit_checker($data['paid_out'], $this->data['permission']['edit'], 'income/paid_out/view/'.$paid_out_id);
      $data['paid_out_id']      = $paid_out_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['gen_id']          = $paid_out_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$paid_out_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment'                 => $this->input->post('payment'),
          'currency'                => $this->input->post('currency'),
          'payment_date'            => $this->input->post('payment_date'),
          'payment_method'          => $this->input->post('payment_method'),
          'room'                    => $this->input->post('room'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'nationality'             => $this->input->post('nationality'),
          'company'                 => $this->input->post('company'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Paid_out_model->edit_paid_out($fdata, $paid_out_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $paid_out_id, 0,json_encode($data['paid_out'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Paid Out #'.$paid_out_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $paid_out_id);
      } 
      $data['view'] = 'income/paid_out/paid_out_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['paid_out']   = $this->Paid_out_model->get_paid_out($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/paid_out', $data['paid_out'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['gen_id']          = get_file_code('files','paid_out');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment'                 => $this->input->post('payment'),
          'currency'                => $this->input->post('currency'),
          'payment_date'            => $this->input->post('payment_date'),
          'payment_method'          => $this->input->post('payment_method'),
          'room'                    => $this->input->post('room'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'nationality'             => $this->input->post('nationality'),
          'company'                 => $this->input->post('company'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $paid_out_id  =  $this->Paid_out_model->add_paid_out($fdata);
        if ($paid_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $paid_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Paid Out #'.$paid_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $paid_out_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $paid_out_id);
        }
      }
      $data['view'] = 'income/paid_out/paid_out_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/paid_out');
      $this->db->update('paid_out', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Paid Out #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>