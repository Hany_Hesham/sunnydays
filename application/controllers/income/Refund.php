<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Refund extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Refund_model');
      $this->data['module']        = $this->Modules_model->get_module_by(21);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/refund/index_refund';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function refund_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Refund_model->get_refunds($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'refund\',\'refund\',\'income/refund\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/refund/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/refund/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Refund Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['payment_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['refund'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_refund'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['company'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Refund_model->get_all_refunds($this->data['uhotels']),
       "recordsFiltered" => $this->Refund_model->get_refunds($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/refund');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','refund');
      $data['currencies']      = get_currency();
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment_date'            => $this->input->post('payment_date'),
          'refund'                  => $this->input->post('refund'),
          'currency'                => $this->input->post('currency'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'company'                 => $this->input->post('company'),
          'payment'                 => $this->input->post('payment'),
          'payment_method'          => $this->input->post('payment_method'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'acc_no'                  => $this->input->post('acc_no'),
          'nationality'             => $this->input->post('nationality'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $refund_id  =  $this->Refund_model->add_refund($fdata);
        if ($refund_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $refund_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Refund Form #'.$refund_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $refund_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $refund_id);
        }
      }
      $data['view'] = 'income/refund/refund_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($refund_id){
      $data['refund']       = $this->Refund_model->get_refund($refund_id);
      if (($data['refund']['status'] == 2 || $data['refund']['status'] == 1) && $data['refund']['reback']) {
        $rrData = json_decode($data['refund']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('refund', array('status' => 3, 'role_id' => 0), "id = ".$refund_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/refund', $data['refund'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $refund_id, 0, 0, 0, 0, 0, 'Viewed Refund Form #'.$refund_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$refund_id,'files');
      $this->data['form_id']       = $refund_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/refund/view_refund';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($refund_id){
      $data['refund']             = $this->Refund_model->get_refund($refund_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/refund', $data['refund'], $this->data['uhotels']);
      $this->load->view('income/refund/view_details',$data);
    }    

    public function signers_items($refund_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $refund_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $refund_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($refund_id=''){
      $data['refund']   = $this->Refund_model->get_refund($refund_id);
      edit_checker($data['refund'], $this->data['permission']['edit'], 'income/refund/view/'.$refund_id);
      $data['refund_id']      = $refund_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['gen_id']          = $refund_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$refund_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment_date'            => $this->input->post('payment_date'),
          'refund'                  => $this->input->post('refund'),
          'currency'                => $this->input->post('currency'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'company'                 => $this->input->post('company'),
          'payment'                 => $this->input->post('payment'),
          'payment_method'          => $this->input->post('payment_method'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'acc_no'                  => $this->input->post('acc_no'),
          'nationality'             => $this->input->post('nationality'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Refund_model->edit_refund($fdata, $refund_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $refund_id, 0,json_encode($data['refund'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Refund Form #'.$refund_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $refund_id);
      } 
      $data['view'] = 'income/refund/refund_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['refund']   = $this->Refund_model->get_refund($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/refund', $data['refund'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['gen_id']          = get_file_code('files','refund');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'name'                    => $this->input->post('name'),
          'payment_date'            => $this->input->post('payment_date'),
          'refund'                  => $this->input->post('refund'),
          'currency'                => $this->input->post('currency'),
          'to_refund'               => $this->input->post('to_refund'),
          'written_to_refund'       => $this->input->post('written_to_refund'),
          'company'                 => $this->input->post('company'),
          'payment'                 => $this->input->post('payment'),
          'payment_method'          => $this->input->post('payment_method'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'acc_no'                  => $this->input->post('acc_no'),
          'nationality'             => $this->input->post('nationality'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $refund_id  =  $this->Refund_model->add_refund($fdata);
        if ($refund_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $refund_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Refund Form #'.$refund_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $refund_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $refund_id);
        }
      }
      $data['view'] = 'income/refund/refund_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/refund');
      $this->db->update('refund', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Refund Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>