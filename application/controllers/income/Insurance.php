<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Insurance extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Insurance_model');
      $this->data['module']        = $this->Modules_model->get_module_by(32);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/insurance/index_insurance';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function insurance_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Insurance_model->get_insurances($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'insurance\',\'insurance\',\'income/insurance\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/insurance/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/insurance/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Insurance Refund #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['payment_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['insurance'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['event'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['event_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Insurance_model->get_all_insurances($this->data['uhotels']),
       "recordsFiltered" => $this->Insurance_model->get_insurances($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/insurance');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','insurance');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'event'                   => $this->input->post('event'),
          'event_date'              => $this->input->post('event_date'),
          'location'                => $this->input->post('location'),
          'name'                    => $this->input->post('name'),
          'insurance'               => $this->input->post('insurance'),
          'no_guest'                => $this->input->post('no_guest'),
          'reciet'                  => $this->input->post('reciet'),
          'price_person'            => $this->input->post('price_person'),
          'payment_date'            => $this->input->post('payment_date'),
          'rent'                    => $this->input->post('rent'),
          'extra'                   => $this->input->post('extra'),
          'extra_price'             => $this->input->post('extra_price'),
          'discount'                => $this->input->post('discount'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $insurance_id  =  $this->Insurance_model->add_insurance($fdata);
        if ($insurance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $insurance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Insurance Refund #'.$insurance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $insurance_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $insurance_id);
        }
      }
      $data['view'] = 'income/insurance/insurance_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($insurance_id){
      $data['insurance']       = $this->Insurance_model->get_insurance($insurance_id);
      if (($data['insurance']['status'] == 2 || $data['insurance']['status'] == 1) && $data['insurance']['reback']) {
        $rrData = json_decode($data['insurance']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('insurance', array('status' => 3, 'role_id' => 0), "id = ".$insurance_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/insurance', $data['insurance'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $insurance_id, 0, 0, 0, 0, 0, 'Viewed Insurance Refund #'.$insurance_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$insurance_id,'files');
      $this->data['form_id']       = $insurance_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/insurance/view_insurance';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($insurance_id){
      $data['insurance']             = $this->Insurance_model->get_insurance($insurance_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/insurance', $data['insurance'], $this->data['uhotels']);
      $this->load->view('income/insurance/view_details',$data);
    }    

    public function signers_items($insurance_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $insurance_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $insurance_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($insurance_id=''){
      $data['insurance']   = $this->Insurance_model->get_insurance($insurance_id);
      edit_checker($data['insurance'], $this->data['permission']['edit'], 'income/insurance/view/'.$insurance_id);
      $data['insurance_id']      = $insurance_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $insurance_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$insurance_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'event'                   => $this->input->post('event'),
          'event_date'              => $this->input->post('event_date'),
          'location'                => $this->input->post('location'),
          'name'                    => $this->input->post('name'),
          'insurance'               => $this->input->post('insurance'),
          'no_guest'                => $this->input->post('no_guest'),
          'reciet'                  => $this->input->post('reciet'),
          'price_person'            => $this->input->post('price_person'),
          'payment_date'            => $this->input->post('payment_date'),
          'rent'                    => $this->input->post('rent'),
          'extra'                   => $this->input->post('extra'),
          'extra_price'             => $this->input->post('extra_price'),
          'discount'                => $this->input->post('discount'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Insurance_model->edit_insurance($fdata, $insurance_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $insurance_id, 0,json_encode($data['insurance'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Insurance Refund #'.$insurance_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $insurance_id);
      } 
      $data['view'] = 'income/insurance/insurance_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['insurance']   = $this->Insurance_model->get_insurance($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/insurance', $data['insurance'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','insurance');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'event'                   => $this->input->post('event'),
          'event_date'              => $this->input->post('event_date'),
          'location'                => $this->input->post('location'),
          'name'                    => $this->input->post('name'),
          'insurance'               => $this->input->post('insurance'),
          'no_guest'                => $this->input->post('no_guest'),
          'reciet'                  => $this->input->post('reciet'),
          'price_person'            => $this->input->post('price_person'),
          'payment_date'            => $this->input->post('payment_date'),
          'rent'                    => $this->input->post('rent'),
          'extra'                   => $this->input->post('extra'),
          'extra_price'             => $this->input->post('extra_price'),
          'discount'                => $this->input->post('discount'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $insurance_id  =  $this->Insurance_model->add_insurance($fdata);
        if ($insurance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $insurance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Insurance Refund #'.$insurance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $insurance_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $insurance_id);
        }
      }
      $data['view'] = 'income/insurance/insurance_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/insurance');
      $this->db->update('insurance', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Insurance Refund #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>