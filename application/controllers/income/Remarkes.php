<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Remarkes extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Remarkes_model');
      $this->data['module']        = $this->Modules_model->get_module_by(36);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/remarkes/index_remarkes';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function remarkes_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Remarkes_model->get_remarkess($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'remarkes\',\'remarkes\',\'income/remarkes\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/remarkes/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/remarkes/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Arrival Remarkes #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Remarkes_model->get_all_remarkess($this->data['uhotels']),
       "recordsFiltered" => $this->Remarkes_model->get_remarkess($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/remarkes');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','remarkes');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $remarkes_id  =  $this->Remarkes_model->add_remarkes($fdata);
        if ($remarkes_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Arrival Remarkes #'.$remarkes_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $remarkes_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['remarkes_id']    = $remarkes_id;
            $item_id                = $this->Remarkes_model->add_remarkes_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $remarkes_id);
        }
      }
      $data['view'] = 'income/remarkes/remarkes_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($remarkes_id){
      $data['remarkes']          = $this->Remarkes_model->get_remarkes($remarkes_id);
      if (($data['remarkes']['status'] == 2 || $data['remarkes']['status'] == 1) && $data['remarkes']['reback']) {
        $rrData = json_decode($data['remarkes']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('remarkes', array('status' => 3, 'role_id' => 0), "id = ".$remarkes_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/remarkes', $data['remarkes'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, 0, 0, 0, 0, 0, 'Viewed Arrival Remarkes #'.$remarkes_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$remarkes_id,'files');
      $this->data['form_id']       = $remarkes_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/remarkes/view_remarkes';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($remarkes_id){
      $data['remarkes']             = $this->Remarkes_model->get_remarkes($remarkes_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/remarkes', $data['remarkes'], $this->data['uhotels']);
      $data['remarkes_items']          = $this->Remarkes_model->get_remarkes_items($remarkes_id);
      $this->load->view('income/remarkes/view_details',$data);
    }  

    public function signers_items($remarkes_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $remarkes_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $remarkes_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($remarkes_id=''){
      $data['remarkes']   = $this->Remarkes_model->get_remarkes($remarkes_id);
      edit_checker($data['remarkes'], $this->data['permission']['edit'], 'income/remarkes/view/'.$remarkes_id);
      $data['remarkes_items']          = $this->Remarkes_model->get_remarkes_items($remarkes_id);
      $data['remarkes_id']  = $remarkes_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $remarkes_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$remarkes_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Remarkes_model->edit_remarkes($fdata, $remarkes_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, 0,json_encode($data['remarkes'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Arrival Remarkes #'.$remarkes_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Remarkes_model->get_remarkes_item($item['id']);
            $item['remarkes_id']  = $remarkes_id;
            $updated                = $this->Remarkes_model->edit_remarkes_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['room']);
            }
          }else{
            $item['remarkes_id']  = $remarkes_id;
            $item_id                = $this->Remarkes_model->add_remarkes_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $remarkes_id);
      } 
      $data['view'] = 'income/remarkes/remarkes_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['remarkes']     = $this->Remarkes_model->get_remarkes($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/remarkes', $data['remarkes'], $this->data['uhotels']);
      $data['remarkes_items'] = $this->Remarkes_model->get_remarkes_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']           = get_file_code('files','remarkes');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $remarkes_id  =  $this->Remarkes_model->add_remarkes($fdata);
        if ($remarkes_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Arrival Remarkes #'.$remarkes_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $remarkes_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['remarkes_id']    = $remarkes_id;
            $item_id                  = $this->Remarkes_model->add_remarkes_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $remarkes_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $remarkes_id);
        }
      }
      $data['view'] = 'income/remarkes/remarkes_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Remarkes_model->get_remarkes_item($id);
        $this->db->update('remarkes_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['remarkes_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/remarkes');
      $this->db->update('remarkes', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Arrival Remarkes #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>