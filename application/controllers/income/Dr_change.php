<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Dr_change extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Dr_change_model');
      $this->data['module']        = $this->Modules_model->get_module_by(34);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/dr_change/index_dr_change';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function dr_change_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Dr_change_model->get_dr_changes($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'dr_change\',\'dr_change\',\'income/dr_change\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/dr_change/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/dr_change/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Room Change #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['month'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Dr_change_model->get_all_dr_changes($this->data['uhotels']),
       "recordsFiltered" => $this->Dr_change_model->get_dr_changes($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/dr_change');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','dr_change');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $dr_change_id  =  $this->Dr_change_model->add_dr_change($fdata);
        if ($dr_change_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room Change #'.$dr_change_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $dr_change_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['dr_change_id']    = $dr_change_id;
            $item_id                  = $this->Dr_change_model->add_dr_change_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['old_room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $dr_change_id);
        }
      }
      $data['view'] = 'income/dr_change/dr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($dr_change_id){
      $data['dr_change']          = $this->Dr_change_model->get_dr_change($dr_change_id);
      if (($data['dr_change']['status'] == 2 || $data['dr_change']['status'] == 1) && $data['dr_change']['reback']) {
        $rrData = json_decode($data['dr_change']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('dr_change', array('status' => 3, 'role_id' => 0), "id = ".$dr_change_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/dr_change', $data['dr_change'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, 0, 0, 0, 0, 0, 'Viewed Room Change #'.$dr_change_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$dr_change_id,'files');
      $this->data['form_id']       = $dr_change_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/dr_change/view_dr_change';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($dr_change_id){
      $data['dr_change']             = $this->Dr_change_model->get_dr_change($dr_change_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/dr_change', $data['dr_change'], $this->data['uhotels']);
      $data['dr_change_items']          = $this->Dr_change_model->get_dr_change_items($dr_change_id);
      $this->load->view('income/dr_change/view_details',$data);
    }  

    public function signers_items($dr_change_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $dr_change_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $dr_change_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($dr_change_id=''){
      $data['dr_change']   = $this->Dr_change_model->get_dr_change($dr_change_id);
      edit_checker($data['dr_change'], $this->data['permission']['edit'], 'income/dr_change/view/'.$dr_change_id);
      $data['dr_change_items']          = $this->Dr_change_model->get_dr_change_items($dr_change_id);
      $data['dr_change_id']  = $dr_change_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $dr_change_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$dr_change_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Dr_change_model->edit_dr_change($fdata, $dr_change_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, 0,json_encode($data['dr_change'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Room Change #'.$dr_change_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Dr_change_model->get_dr_change_item($item['id']);
            $item['dr_change_id']  = $dr_change_id;
            $updated                = $this->Dr_change_model->edit_dr_change_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['old_room']);
            }
          }else{
            $item['dr_change_id']  = $dr_change_id;
            $item_id                = $this->Dr_change_model->add_dr_change_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['old_room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $dr_change_id);
      } 
      $data['view'] = 'income/dr_change/dr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['dr_change']     = $this->Dr_change_model->get_dr_change($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/dr_change', $data['dr_change'], $this->data['uhotels']);
      $data['dr_change_items'] = $this->Dr_change_model->get_dr_change_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']           = get_file_code('files','dr_change');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $dr_change_id  =  $this->Dr_change_model->add_dr_change($fdata);
        if ($dr_change_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room Change #'.$dr_change_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $dr_change_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['dr_change_id']    = $dr_change_id;
            $item_id                  = $this->Dr_change_model->add_dr_change_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $dr_change_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['old_room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $dr_change_id);
        }
      }
      $data['view'] = 'income/dr_change/dr_change_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Dr_change_model->get_dr_change_item($id);
        $this->db->update('dr_change_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['dr_change_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['old_room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/dr_change');
      $this->db->update('dr_change', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Room Change #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>