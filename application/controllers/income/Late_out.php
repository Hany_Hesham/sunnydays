<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Late_out extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Late_out_model');
      $this->data['module']        = $this->Modules_model->get_module_by(37);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/late_out/index_late_out';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function late_out_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Late_out_model->get_late_outs($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'late_out\',\'late_out\',\'income/late_out\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/late_out/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/late_out/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Late Check Out #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Late_out_model->get_all_late_outs($this->data['uhotels']),
       "recordsFiltered" => $this->Late_out_model->get_late_outs($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/late_out');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','late_out');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $late_out_id  =  $this->Late_out_model->add_late_out($fdata);
        if ($late_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Late Check Out #'.$late_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $late_out_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['late_out_id']    = $late_out_id;
            $item_id                  = $this->Late_out_model->add_late_out_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $late_out_id);
        }
      }
      $data['view'] = 'income/late_out/late_out_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($late_out_id){
      $data['late_out']          = $this->Late_out_model->get_late_out($late_out_id);
      if (($data['late_out']['status'] == 2 || $data['late_out']['status'] == 1) && $data['late_out']['reback']) {
        $rrData = json_decode($data['late_out']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('late_out', array('status' => 3, 'role_id' => 0), "id = ".$late_out_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/late_out', $data['late_out'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, 0, 0, 0, 0, 0, 'Viewed Late Check Out #'.$late_out_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$late_out_id,'files');
      $this->data['form_id']       = $late_out_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/late_out/view_late_out';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($late_out_id){
      $data['late_out']             = $this->Late_out_model->get_late_out($late_out_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/late_out', $data['late_out'], $this->data['uhotels']);
      $data['late_out_items']          = $this->Late_out_model->get_late_out_items($late_out_id);
      $this->load->view('income/late_out/view_details',$data);
    }  

    public function signers_items($late_out_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $late_out_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $late_out_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($late_out_id=''){
      $data['late_out']   = $this->Late_out_model->get_late_out($late_out_id);
      edit_checker($data['late_out'], $this->data['permission']['edit'], 'income/late_out/view/'.$late_out_id);
      $data['late_out_items']          = $this->Late_out_model->get_late_out_items($late_out_id);
      $data['late_out_id']  = $late_out_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = $late_out_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$late_out_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Late_out_model->edit_late_out($fdata, $late_out_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, 0,json_encode($data['late_out'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Late Check Out #'.$late_out_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Late_out_model->get_late_out_item($item['id']);
            $item['late_out_id']  = $late_out_id;
            $updated                = $this->Late_out_model->edit_late_out_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['room']);
            }
          }else{
            $item['late_out_id']  = $late_out_id;
            $item_id                = $this->Late_out_model->add_late_out_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $late_out_id);
      } 
      $data['view'] = 'income/late_out/late_out_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['late_out']     = $this->Late_out_model->get_late_out($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/late_out', $data['late_out'], $this->data['uhotels']);
      $data['late_out_items'] = $this->Late_out_model->get_late_out_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']           = get_file_code('files','late_out');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $late_out_id  =  $this->Late_out_model->add_late_out($fdata);
        if ($late_out_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Late Check Out #'.$late_out_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $late_out_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['late_out_id']    = $late_out_id;
            $item_id                  = $this->Late_out_model->add_late_out_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $late_out_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $late_out_id);
        }
      }
      $data['view'] = 'income/late_out/late_out_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Late_out_model->get_late_out_item($id);
        $this->db->update('late_out_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['late_out_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/late_out');
      $this->db->update('late_out', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Late Check Out #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>