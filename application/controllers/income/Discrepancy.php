<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Discrepancy extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('income/Discrepancy_model');
      $this->data['module']        = $this->Modules_model->get_module_by(25);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'income/discrepancy/index_discrepancy';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function discrepancy_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Discrepancy_model->get_discrepancys($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'discrepancy\',\'discrepancy\',\'income/discrepancy\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('income/discrepancy/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('income/discrepancy/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Room Discrepancy #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['day_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Discrepancy_model->get_all_discrepancys($this->data['uhotels']),
       "recordsFiltered" => $this->Discrepancy_model->get_discrepancys($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'income/discrepancy');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['day_parts']       = $this->General_model->get_meta_data('day_parts');
      $data['gen_id']          = get_file_code('files','discrepancy');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'part_id'                 => $this->input->post('part_id'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $discrepancy_id  =  $this->Discrepancy_model->add_discrepancy($fdata);
        if ($discrepancy_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room Discrepancy #'.$discrepancy_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $discrepancy_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['discrepancy_id']    = $discrepancy_id;
            $item_id                  = $this->Discrepancy_model->add_discrepancy_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $discrepancy_id);
        }
      }
      $data['view'] = 'income/discrepancy/discrepancy_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($discrepancy_id){
      $data['discrepancy']          = $this->Discrepancy_model->get_discrepancy($discrepancy_id);
      if (($data['discrepancy']['status'] == 2 || $data['discrepancy']['status'] == 1) && $data['discrepancy']['reback']) {
        $rrData = json_decode($data['discrepancy']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('discrepancy', array('status' => 3, 'role_id' => 0), "id = ".$discrepancy_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/discrepancy', $data['discrepancy'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, 0, 0, 0, 0, 0, 'Viewed Room Discrepancy #'.$discrepancy_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$discrepancy_id,'files');
      $this->data['form_id']       = $discrepancy_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'income/discrepancy/view_discrepancy';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($discrepancy_id){
      $data['discrepancy']             = $this->Discrepancy_model->get_discrepancy($discrepancy_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'income/discrepancy', $data['discrepancy'], $this->data['uhotels']);
      $data['discrepancy_items']          = $this->Discrepancy_model->get_discrepancy_items($discrepancy_id);
      $this->load->view('income/discrepancy/view_details',$data);
    }  

    public function signers_items($discrepancy_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $discrepancy_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $discrepancy_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($discrepancy_id=''){
      $data['discrepancy']   = $this->Discrepancy_model->get_discrepancy($discrepancy_id);
      edit_checker($data['discrepancy'], $this->data['permission']['edit'], 'income/discrepancy/view/'.$discrepancy_id);
      $data['discrepancy_items']          = $this->Discrepancy_model->get_discrepancy_items($discrepancy_id);
      $data['discrepancy_id']  = $discrepancy_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['day_parts']       = $this->General_model->get_meta_data('day_parts');
      $data['gen_id']          = $discrepancy_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$discrepancy_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'part_id'                 => $this->input->post('part_id'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Discrepancy_model->edit_discrepancy($fdata, $discrepancy_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, 0,json_encode($data['discrepancy'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Room Discrepancy #'.$discrepancy_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Discrepancy_model->get_discrepancy_item($item['id']);
            $item['discrepancy_id']  = $discrepancy_id;
            $updated                = $this->Discrepancy_model->edit_discrepancy_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['room']);
            }
          }else{
            $item['discrepancy_id']  = $discrepancy_id;
            $item_id                = $this->Discrepancy_model->add_discrepancy_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['room'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $discrepancy_id);
      } 
      $data['view'] = 'income/discrepancy/discrepancy_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['discrepancy']     = $this->Discrepancy_model->get_discrepancy($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'income/discrepancy', $data['discrepancy'], $this->data['uhotels']);
      $data['discrepancy_items'] = $this->Discrepancy_model->get_discrepancy_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['day_parts']        = $this->General_model->get_meta_data('day_parts');
      $data['gen_id']           = get_file_code('files','discrepancy');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'part_id'                 => $this->input->post('part_id'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $discrepancy_id  =  $this->Discrepancy_model->add_discrepancy($fdata);
        if ($discrepancy_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Room Discrepancy #'.$discrepancy_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $discrepancy_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['discrepancy_id']    = $discrepancy_id;
            $item_id                  = $this->Discrepancy_model->add_discrepancy_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $discrepancy_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['room'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $discrepancy_id);
        }
      }
      $data['view'] = 'income/discrepancy/discrepancy_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Discrepancy_model->get_discrepancy_item($id);
        $this->db->update('discrepancy_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['discrepancy_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['room'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'income/discrepancy');
      $this->db->update('discrepancy', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Room Discrepancy #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>