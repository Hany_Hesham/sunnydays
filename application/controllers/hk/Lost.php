<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Lost extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hk/Lost_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(16);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hk/lost/index_lost';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function lost_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Lost_model->get_losts($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'lost\',\'lost\',\'hk/lost\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hk/lost/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hk/lost/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Gifts and Findings Exit Permit #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Lost_model->get_all_losts($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Lost_model->get_losts($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hk/lost');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','lost');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'name'                    => $this->input->post('name'),
          'item'                    => $this->input->post('item'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $lost_id  =  $this->Lost_model->add_lost($fdata);
        if ($lost_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $lost_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Gifts and Findings Exit Permit #'.$lost_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $lost_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $lost_id);
        }
      }
      $data['view'] = 'hk/lost/lost_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($lost_id){
      $data['lost']       = $this->Lost_model->get_lost($lost_id);
      if (($data['lost']['status'] == 2 || $data['lost']['status'] == 1) && $data['lost']['reback']) {
        $rrData = json_decode($data['lost']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('lost', array('status' => 3, 'role_id' => 0), "id = ".$lost_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/lost', $data['lost'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $lost_id, 0, 0, 0, 0, 0, 'Viewed Gifts and Findings Exit Permit #'.$lost_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$lost_id,'files');
      $this->data['form_id']       = $lost_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hk/lost/view_lost';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($lost_id){
      $data['lost']             = $this->Lost_model->get_lost($lost_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/lost', $data['lost'], $this->data['uhotels']);
      $this->load->view('hk/lost/view_details',$data);
    }    

    public function signers_items($lost_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $lost_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $lost_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($lost_id=''){
      $data['lost']   = $this->Lost_model->get_lost($lost_id);
      edit_checker($data['lost'], $this->data['permission']['edit'], 'hk/lost/view/'.$lost_id);
      $data['lost_id']      = $lost_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $lost_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$lost_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'name'                    => $this->input->post('name'),
          'item'                    => $this->input->post('item'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Lost_model->edit_lost($fdata, $lost_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $lost_id, 0,json_encode($data['lost'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Gifts and Findings Exit Permit #'.$lost_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $lost_id);
      } 
      $data['view'] = 'hk/lost/lost_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['lost']   = $this->Lost_model->get_lost($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hk/lost', $data['lost'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','lost');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'name'                    => $this->input->post('name'),
          'item'                    => $this->input->post('item'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $lost_id  =  $this->Lost_model->add_lost($fdata);
        if ($lost_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $lost_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Gifts and Findings Exit Permit #'.$lost_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $lost_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $lost_id);
        }
      }
      $data['view'] = 'hk/lost/lost_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hk/lost');
      $this->db->update('lost', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Gifts and Findings Exit Permit #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>