<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Maintenance extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hk/Maintenance_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(41);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hk/maintenance/index_maintenance';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function maintenance_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Maintenance_model->get_maintenances($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'maintenance\',\'maintenance\',\'hk/maintenance\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hk/maintenance/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hk/maintenance/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Maintenance Request #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['del_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Maintenance_model->get_all_maintenances($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Maintenance_model->get_maintenances($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hk/maintenance');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','maintenance');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'workshop'                => $this->input->post('workshop'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $maintenance_id  =  $this->Maintenance_model->add_maintenance($fdata);
        if ($maintenance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Maintenance Request #'.$maintenance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $maintenance_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['maintenance_id']    = $maintenance_id;
            $item_id                  = $this->Maintenance_model->add_maintenance_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['item'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $maintenance_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $maintenance_id);
        }
      }
      $data['view'] = 'hk/maintenance/maintenance_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($maintenance_id){
      $data['maintenance']          = $this->Maintenance_model->get_maintenance($maintenance_id);
      if (($data['maintenance']['status'] == 2 || $data['maintenance']['status'] == 1) && $data['maintenance']['reback']) {
        $rrData = json_decode($data['maintenance']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('maintenance', array('status' => 3, 'role_id' => 0), "id = ".$maintenance_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/maintenance', $data['maintenance'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, 0, 0, 0, 0, 0, 'Viewed Maintenance Request #'.$maintenance_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$maintenance_id,'files');
      $this->data['form_id']       = $maintenance_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hk/maintenance/view_maintenance';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($maintenance_id){
      $data['maintenance']             = $this->Maintenance_model->get_maintenance($maintenance_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/maintenance', $data['maintenance'], $this->data['uhotels']);
      $data['maintenance_items']          = $this->Maintenance_model->get_maintenance_items($maintenance_id);
      $this->load->view('hk/maintenance/view_details',$data);
    }  

    public function signers_items($maintenance_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $maintenance_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $maintenance_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($maintenance_id=''){
      $data['maintenance']   = $this->Maintenance_model->get_maintenance($maintenance_id);
      edit_checker($data['maintenance'], $this->data['permission']['edit'], 'hk/maintenance/view/'.$maintenance_id);
      $data['maintenance_items']          = $this->Maintenance_model->get_maintenance_items($maintenance_id);
      $data['maintenance_id']   = $maintenance_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $maintenance_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$maintenance_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'workshop'                => $this->input->post('workshop'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Maintenance_model->edit_maintenance($fdata, $maintenance_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, 0,json_encode($data['maintenance'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Maintenance Request #'.$maintenance_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Maintenance_model->get_maintenance_item($item['id']);
            $item['maintenance_id']  = $maintenance_id;
            $updated                = $this->Maintenance_model->edit_maintenance_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['item']);
            }
          }else{
            $item['maintenance_id']  = $maintenance_id;
            $item_id                = $this->Maintenance_model->add_maintenance_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['item'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $maintenance_id);
      } 
      $data['view'] = 'hk/maintenance/maintenance_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['maintenance']     = $this->Maintenance_model->get_maintenance($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hk/maintenance', $data['maintenance'], $this->data['uhotels']);
      $data['maintenance_items'] = $this->Maintenance_model->get_maintenance_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['gen_id']           = get_file_code('files','maintenance');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'workshop'                => $this->input->post('workshop'),
          'del_date'                => $this->input->post('del_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $maintenance_id  =  $this->Maintenance_model->add_maintenance($fdata);
        if ($maintenance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Maintenance Request #'.$maintenance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $maintenance_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['maintenance_id']    = $maintenance_id;
            $item_id                  = $this->Maintenance_model->add_maintenance_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $maintenance_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['item'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $maintenance_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $maintenance_id);
        }
      }
      $data['view'] = 'hk/maintenance/maintenance_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Maintenance_model->get_maintenance_item($id);
        $this->db->update('maintenance_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['maintenance_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['item'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hk/maintenance');
      $this->db->update('maintenance', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Maintenance Request #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>