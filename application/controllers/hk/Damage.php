<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Damage extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hk/Damage_model');
      $this->load->model('admin/Departments_model');
      $this->data['module']        = $this->Modules_model->get_module_by(39);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hk/damage/index_damage';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function damage_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Damage_model->get_damages($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'damage\',\'damage\',\'hk/damage\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hk/damage/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hk/damage/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Damage Report #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['room'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['item'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Damage_model->get_all_damages($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Damage_model->get_damages($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hk/damage');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','damage');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'date'                    => $this->input->post('date'),
          'room'                    => $this->input->post('room'),
          'item'                    => $this->input->post('item'),
          'area'                    => $this->input->post('area'),
          'damage'                  => $this->input->post('damage'),
          'cost'                    => $this->input->post('cost'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $damage_id  =  $this->Damage_model->add_damage($fdata);
        if ($damage_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $damage_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Damage Report #'.$damage_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $damage_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $damage_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $damage_id);
        }
      }
      $data['view'] = 'hk/damage/damage_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($damage_id){
      $data['damage']       = $this->Damage_model->get_damage($damage_id);
      if (($data['damage']['status'] == 2 || $data['damage']['status'] == 1) && $data['damage']['reback']) {
        $rrData = json_decode($data['damage']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('damage', array('status' => 3, 'role_id' => 0), "id = ".$damage_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/damage', $data['damage'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $damage_id, 0, 0, 0, 0, 0, 'Viewed Damage Report #'.$damage_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$damage_id,'files');
      $this->data['form_id']       = $damage_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hk/damage/view_damage';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($damage_id){
      $data['damage']             = $this->Damage_model->get_damage($damage_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hk/damage', $data['damage'], $this->data['uhotels']);
      $this->load->view('hk/damage/view_details',$data);
    }    

    public function signers_items($damage_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $damage_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $damage_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($damage_id=''){
      $data['damage']   = $this->Damage_model->get_damage($damage_id);
      edit_checker($data['damage'], $this->data['permission']['edit'], 'hk/damage/view/'.$damage_id);
      $data['damage_id']       = $damage_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $damage_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$damage_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'date'                    => $this->input->post('date'),
          'room'                    => $this->input->post('room'),
          'item'                    => $this->input->post('item'),
          'area'                    => $this->input->post('area'),
          'damage'                  => $this->input->post('damage'),
          'cost'                    => $this->input->post('cost'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Damage_model->edit_damage($fdata, $damage_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $damage_id, 0,json_encode($data['damage'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Damage Report #'.$damage_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $damage_id);
      } 
      $data['view'] = 'hk/damage/damage_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['damage']   = $this->Damage_model->get_damage($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hk/damage', $data['damage'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','damage');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'date'                    => $this->input->post('date'),
          'room'                    => $this->input->post('room'),
          'item'                    => $this->input->post('item'),
          'area'                    => $this->input->post('area'),
          'damage'                  => $this->input->post('damage'),
          'cost'                    => $this->input->post('cost'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $damage_id  =  $this->Damage_model->add_damage($fdata);
        if ($damage_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $damage_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Damage Report #'.$damage_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $damage_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $damage_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $damage_id);
        }
      }
      $data['view'] = 'hk/damage/damage_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hk/damage');
      $this->db->update('damage', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Damage Report #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>