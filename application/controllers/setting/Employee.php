<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Employee extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('setting/Employee_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(43);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'setting/employee/index_employee';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function employee_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Employee_model->get_employees($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'employee\',\'employee\',\'setting/employee\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('setting/employee/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('setting/employee/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Employee Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span>'.$row['hotel_name'].'</span>';    
        $arr[] = '<span>'.$row['code'].'</span>';
        $arr[] = '<span>'.$row['name'].'</span>';
        $arr[] = '<span>'.$row['position_name'].'</span>';
        $arr[] = '<span>'.$row['fullname'].'</span>';
        $arr[] = '<span>'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Employee_model->get_all_employees($this->data['uhotels']),
       "recordsFiltered" => $this->Employee_model->get_employees($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'setting/employee');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','employee');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position'                => $this->input->post('position'),
          'remarks'                 => $this->input->post('remarks'),
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $employee_id  =  $this->Employee_model->add_employee($fdata);
        if ($employee_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $employee_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Employee Form #'.$employee_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $employee_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/employee/view/'.$employee_id);
        }
      }
      $data['view'] = 'setting/employee/employee_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($employee_id){
      $data['employee']       = $this->Employee_model->get_employee($employee_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/employee', $data['employee'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $employee_id, 0, 0, 0, 0, 0, 'Viewed Employee Form #'.$employee_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$employee_id,'files');
      $this->data['form_id']       = $employee_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'setting/employee/view_employee';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($employee_id){
      $data['employee']             = $this->Employee_model->get_employee($employee_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/employee', $data['employee'], $this->data['uhotels']);
      $this->load->view('setting/employee/view_details',$data);
    }    

    public function edit($employee_id=''){
      $data['employee']   = $this->Employee_model->get_employee($employee_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/employee', $data['employee'], $this->data['uhotels']);
      $data['employee_id']      = $employee_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $employee_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$employee_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position'                => $this->input->post('position'),
          'remarks'                 => $this->input->post('remarks'),
        ];    
        $this->Employee_model->edit_employee($fdata, $employee_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $employee_id, 0,json_encode($data['employee'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Employee Form #'.$employee_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        redirect('setting/employee/view/'.$employee_id);
      } 
      $data['view'] = 'setting/employee/employee_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['employee']   = $this->Employee_model->get_employee($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'setting/employee', $data['employee'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','employee');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position'                => $this->input->post('position'),
          'remarks'                 => $this->input->post('remarks'),
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $employee_id  =  $this->Employee_model->add_employee($fdata);
        if ($employee_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $employee_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Employee Form #'.$employee_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $employee_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/employee/view/'.$employee_id);
        }
      }
      $data['view'] = 'setting/employee/employee_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'setting/employee');
      $this->db->update('employee', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Employee Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>