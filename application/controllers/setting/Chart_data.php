<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Chart_data extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('setting/Chart_data_model');
      $this->data['module']        = $this->Modules_model->get_module_by(44);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'setting/chart_data/index_chart_data';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function chart_data_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Chart_data_model->get_chart_datas($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'chart_data\',\'chart_data\',\'setting/chart_data\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('setting/chart_data/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('setting/chart_data/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Chart Form #.'.$i.'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span>'.$row['hotel_name'].'</span>';    
        $arr[] = '<span>'.$row['type_name'].'</span>';
        $arr[] = '<span>'.$row['type_data'].'</span>';
        $arr[] = '<span>'.$row['rank'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Chart_data_model->get_all_chart_datas($this->data['uhotels']),
       "recordsFiltered" => $this->Chart_data_model->get_chart_datas($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'setting/chart_data');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['gen_id']          = get_file_code('files','chart_data');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'chart_account',
          'rank'                    => $this->input->post('rank'),
        ];      
        $chart_data_id  =  $this->Chart_data_model->add_chart_data($fdata);
        if ($chart_data_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $chart_data_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Chart Form #'.$chart_data_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $chart_data_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/chart_data/view/'.$chart_data_id);
        }
      }
      $data['view'] = 'setting/chart_data/chart_data_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($chart_data_id){
      $data['chart_data']       = $this->Chart_data_model->get_chart_data($chart_data_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/chart_data', $data['chart_data'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $chart_data_id, 0, 0, 0, 0, 0, 'Viewed Chart Form #'.$chart_data_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$chart_data_id,'files');
      $this->data['form_id']       = $chart_data_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'setting/chart_data/view_chart_data';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($chart_data_id){
      $data['chart_data']             = $this->Chart_data_model->get_chart_data($chart_data_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/chart_data', $data['chart_data'], $this->data['uhotels']);
      $this->load->view('setting/chart_data/view_details',$data);
    }    

    public function edit($chart_data_id=''){
      $data['chart_data']   = $this->Chart_data_model->get_chart_data($chart_data_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/chart_data', $data['chart_data'], $this->data['uhotels']);
      $data['chart_data_id']      = $chart_data_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $chart_data_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$chart_data_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'chart_account',
          'rank'                    => $this->input->post('rank'),
        ];    
        $this->Chart_data_model->edit_chart_data($fdata, $chart_data_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $chart_data_id, 0,json_encode($data['chart_data'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Chart Form #'.$chart_data_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        redirect('setting/chart_data/view/'.$chart_data_id);
      } 
      $data['view'] = 'setting/chart_data/chart_data_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['chart_data']   = $this->Chart_data_model->get_chart_data($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'setting/chart_data', $data['chart_data'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','chart_data');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'chart_account',
          'rank'                    => $this->input->post('rank'),
        ];     
        $chart_data_id  =  $this->Chart_data_model->add_chart_data($fdata);
        if ($chart_data_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $chart_data_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Chart Form #'.$chart_data_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $chart_data_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/chart_data/view/'.$chart_data_id);
        }
      }
      $data['view'] = 'setting/chart_data/chart_data_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'setting/chart_data');
      $this->db->update('chart_data', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Chart Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>