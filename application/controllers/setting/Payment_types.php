<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Payment_types extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('setting/Payment_types_model');
      $this->data['module']        = $this->Modules_model->get_module_by(55);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'setting/payment_types/index_payment_types';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function payment_types_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Payment_types_model->get_payment_typess($dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'payment_types\',\'payment_types\',\'setting/payment_types\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('setting/payment_types/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('setting/payment_types/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Payment Type #.'.$i.'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span>'.$row['type_name'].'</span>';
        $arr[] = '<span>'.$row['type_data'].'</span>';
        $arr[] = '<span>'.$row['rank'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Payment_types_model->get_all_payment_typess(),
       "recordsFiltered" => $this->Payment_types_model->get_payment_typess($dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'setting/payment_types');
      $data['gen_id']          = get_file_code('files','payment_types');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'payment_types',
          'rank'                    => $this->input->post('rank'),
        ];      
        $payment_types_id  =  $this->Payment_types_model->add_payment_types($fdata);
        if ($payment_types_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_types_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Type #'.$payment_types_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_types_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/payment_types/view/'.$payment_types_id);
        }
      }
      $data['view'] = 'setting/payment_types/payment_types_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($payment_types_id){
      $data['payment_types']       = $this->Payment_types_model->get_payment_types($payment_types_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_types', $data['payment_types'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $payment_types_id, 0, 0, 0, 0, 0, 'Viewed Payment Type #'.$payment_types_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_types_id,'files');
      $this->data['form_id']       = $payment_types_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'setting/payment_types/view_payment_types';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($payment_types_id){
      $data['payment_types']             = $this->Payment_types_model->get_payment_types($payment_types_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_types', $data['payment_types'], $this->data['uhotels']);
      $this->load->view('setting/payment_types/view_details',$data);
    }    

    public function edit($payment_types_id=''){
      $data['payment_types']   = $this->Payment_types_model->get_payment_types($payment_types_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_types', $data['payment_types'], $this->data['uhotels']);
      $data['payment_types_id']       = $payment_types_id; 
      $data['gen_id']                 = $payment_types_id; 
      $data['uploads']                = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_types_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'payment_types',
          'rank'                    => $this->input->post('rank'),
        ];    
        $this->Payment_types_model->edit_payment_types($fdata, $payment_types_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $payment_types_id, 0,json_encode($data['payment_types'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Payment Type #'.$payment_types_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        redirect('setting/payment_types/view/'.$payment_types_id);
      } 
      $data['view'] = 'setting/payment_types/payment_types_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['payment_types']   = $this->Payment_types_model->get_payment_types($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'setting/payment_types', $data['payment_types'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['gen_id']          = get_file_code('files','payment_types');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'type_name'               => $this->input->post('type_name'),
          'type_data'               => $this->input->post('type_data'),
          'meta_keyword'            => 'payment_types',
          'rank'                    => $this->input->post('rank'),
        ];     
        $payment_types_id  =  $this->Payment_types_model->add_payment_types($fdata);
        if ($payment_types_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_types_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Type #'.$payment_types_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_types_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/payment_types/view/'.$payment_types_id);
        }
      }
      $data['view'] = 'setting/payment_types/payment_types_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'setting/payment_types');
      $this->db->update('payment_types', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Payment Type #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>