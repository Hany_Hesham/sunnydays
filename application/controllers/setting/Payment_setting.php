<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Payment_setting extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('setting/Payment_setting_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(56);
      $this->data['permission']    = user_access($this->data['module']['id']);
      $this->data['module_ids']    = array('22', '28', '54');
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'setting/payment_setting/index_payment_setting';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function payment_setting_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Payment_setting_model->get_payment_settings($dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'payment_setting\',\'payment_setting\',\'setting/payment_setting\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('setting/payment_setting/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('setting/payment_setting/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Payment Setting #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span>'.$row['module_name'].'</span>';    
        $arr[] = '<span>'.$row['name'].'</span>';
        $arr[] = '<span>'.$row['limit'].'</span>';
        if ($row['active'] ==1) {
          $arr[] ='<label class="customcheckbox">
                <input type="hidden" value="0" name="active" class="listCheckbox">
                <input type="checkbox" name="active" class="listCheckbox" value="1" class="switch-input" checked  onclick="changer('.$row['id'].',\'payment_setting\',\'payment_setting\',\'active\',\'0\')">
                <span class="checkmark"></span>
            </label>';
        }else{
          $arr[] ='<label class="customcheckbox">
                 <input type="hidden" value="0" name="active" class="listCheckbox">
                <input type="checkbox" name="active" class="listCheckbox" value="1" class="switch-input" onclick="changer('.$row['id'].',\'Payment Setting\',\'payment_setting\',\'active\',\'1\')">
                <span class="checkmark"></span>
            </label>';
        } 
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Payment_setting_model->get_all_payment_settings(),
       "recordsFiltered" => $this->Payment_setting_model->get_payment_settings($dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'setting/payment_setting');
      $data['modules']         = $this->Modules_model->get_modules_by_ids($this->data['module_ids']);
      $data['gen_id']          = get_file_code('files','payment_setting');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'module_id'            => $this->input->post('module_id'),
          'name'                 => $this->input->post('name'),
          'limit'                => $this->input->post('limit'),
        ];      
        $payment_setting_id  =  $this->Payment_setting_model->add_payment_setting($fdata);
        if ($payment_setting_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_setting_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Setting #'.$payment_setting_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_setting_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/payment_setting');
        }
      }
      $data['view'] = 'setting/payment_setting/payment_setting_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($payment_setting_id){
      $data['payment_setting']     = $this->Payment_setting_model->get_payment_setting($payment_setting_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_setting', $data['payment_setting'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $payment_setting_id, 0, 0, 0, 0, 0, 'Viewed Payment Setting #'.$payment_setting_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_setting_id,'files');
      $this->data['form_id']       = $payment_setting_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'setting/payment_setting/view_payment_setting';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($payment_setting_id){
      $data['payment_setting']             = $this->Payment_setting_model->get_payment_setting($payment_setting_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_setting', $data['payment_setting'], $this->data['uhotels']);
      $this->load->view('setting/payment_setting/view_details',$data);
    }    

    public function edit($payment_setting_id=''){
      $data['payment_setting']      = $this->Payment_setting_model->get_payment_setting($payment_setting_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'setting/payment_setting', $data['payment_setting'], $this->data['uhotels']);
      $data['payment_setting_id']   = $payment_setting_id; 
      $data['modules']              = $this->Modules_model->get_modules_by_ids($this->data['module_ids']);
      $data['gen_id']               = $payment_setting_id; 
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_setting_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'module_id'            => $this->input->post('module_id'),
          'name'                 => $this->input->post('name'),
          'limit'                => $this->input->post('limit'),
        ];    
        $this->Payment_setting_model->edit_payment_setting($fdata, $payment_setting_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $payment_setting_id, 0,json_encode($data['payment_setting'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Payment Setting #'.$payment_setting_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        redirect('setting/payment_setting');
      } 
      $data['view'] = 'setting/payment_setting/payment_setting_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['payment_setting']   = $this->Payment_setting_model->get_payment_setting($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'setting/payment_setting', $data['payment_setting'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['modules']         = $this->Modules_model->get_modules_by_ids($this->data['module_ids']);
      $data['gen_id']          = get_file_code('files','payment_setting');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'module_id'            => $this->input->post('module_id'),
          'name'                 => $this->input->post('name'),
          'limit'                => $this->input->post('limit'),

        ];     
        $payment_setting_id  =  $this->Payment_setting_model->add_payment_setting($fdata);
        if ($payment_setting_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_setting_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Setting #'.$payment_setting_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_setting_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          redirect('setting/payment_setting');
        }
      }
      $data['view'] = 'setting/payment_setting/payment_setting_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function status_change($id,$column,$value){
      $this->db->update('payment_setting', array($column => $value), "id = ".$id);
      if ($this->input->is_ajax_request()) {
        if ($value == 1) { $disabled ='Activated';}else{$disabled ='Deactivated';}
        loger(''.$disabled.'',$this->data['module']['id'],'Users',$id,0,0,0,0,0,''.$disabled.' Payment Setting #'.$id.'');
       }
      $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record is Deleted Successfully!']);
      redirect(base_url('admin/users'));
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'setting/payment_setting');
      $this->db->update('payment_setting', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Payment Setting #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>