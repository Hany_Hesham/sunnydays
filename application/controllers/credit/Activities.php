<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Activities extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('credit/Activities_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(18);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'credit/activities/index_activities';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function activities_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Activities_model->get_activitiess($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'activities\',\'activities\',\'credit/activities\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('credit/activities/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('credit/activities/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Monthly Activities Rent #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['month'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Activities_model->get_all_activitiess($this->data['uhotels']),
       "recordsFiltered" => $this->Activities_model->get_activitiess($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'credit/activities');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['activitiez']      = $this->General_model->get_meta_data('activities');
      $data['gen_id']          = get_file_code('files','activities');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $activities_id  =  $this->Activities_model->add_activities($fdata);
        if ($activities_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Monthly Activities Rent #'.$activities_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $activities_id, 'files');
          $file_name    = do_upload('file', 'activities');
          $this->import_activitiess($file_name, $activities_id);
          foreach ($this->input->post('items') as $key => $item) {
            $item['activities_id']    = $activities_id;
            $item_name                = metaName($item['activity']);
            $item_id                  = $this->Activities_model->add_activities_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $activities_id);
        }
      }
      $data['view'] = 'credit/activities/activities_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($activities_id){
      $data['activities']          = $this->Activities_model->get_activities($activities_id);
      if (($data['activities']['status'] == 2 || $data['activities']['status'] == 1) && $data['activities']['reback']) {
        $rrData = json_decode($data['activities']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('activities', array('status' => 3, 'role_id' => 0), "id = ".$activities_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'credit/activities', $data['activities'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $activities_id, 0, 0, 0, 0, 0, 'Viewed Monthly Activities Rent #'.$activities_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$activities_id,'files');
      $this->data['form_id']       = $activities_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'credit/activities/view_activities';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($activities_id){
      $data['activities']             = $this->Activities_model->get_activities($activities_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'credit/activities', $data['activities'], $this->data['uhotels']);
      $data['activities_items']          = $this->Activities_model->get_activities_items($activities_id);
      $this->load->view('credit/activities/view_details',$data);
    }  

    public function signers_items($activities_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $activities_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $activities_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($activities_id=''){
      $data['activities']   = $this->Activities_model->get_activities($activities_id);
      edit_checker($data['activities'], $this->data['permission']['edit'], 'credit/activities/view/'.$activities_id);
      $data['activities_items']          = $this->Activities_model->get_activities_items($activities_id);
      $data['activities_id']   = $activities_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['activitiez']      = $this->General_model->get_meta_data('activities');
      $data['gen_id']          = $activities_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$activities_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Activities_model->edit_activities($fdata, $activities_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $activities_id, 0,json_encode($data['activities'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Monthly Activities Rent #'.$activities_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Activities_model->get_activities_item($item['id']);
            $item['activities_id']  = $activities_id;
            $item_name              = metaName($item['activity']);
            $updated                = $this->Activities_model->edit_activities_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $activities_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item_name);
            }
          }else{
            $item['activities_id']  = $activities_id;
            $item_name              = metaName($item['activity']);
            $item_id                = $this->Activities_model->add_activities_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item_name.'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $activities_id);
      } 
      $data['view'] = 'credit/activities/activities_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['activities']     = $this->Activities_model->get_activities($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'credit/activities', $data['activities'], $this->data['uhotels']);
      $data['activities_items'] = $this->Activities_model->get_activities_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['activitiez']       = $this->General_model->get_meta_data('activities');
      $data['gen_id']           = get_file_code('files','activities');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'month'                   => $this->input->post('month'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $activities_id  =  $this->Activities_model->add_activities($fdata);
        if ($activities_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Monthly Activities Rent #'.$activities_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $activities_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['activities_id']    = $activities_id;
            $item_name                = metaName($item['activity']);
            $item_id                  = $this->Activities_model->add_activities_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $activities_id);
        }
      }
      $data['view'] = 'credit/activities/activities_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Activities_model->get_activities_item($id);
        $this->db->update('activities_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['activities_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['activity_name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'credit/activities');
      $this->db->update('activities', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Monthly Activities Rent #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

    public function import_activitiess($file, $activities_id){
      $activitiess = fopen(base_url('assets/uploads/activities/'.$file), 'r');
      $i = 1;
      while ($records = fgetcsv($activitiess)) {
        if ($records[0] != 'activity') {
          $activity = metaID($records[0], 'activities');
          if ($activity) {
            $this->db->insert('activities_item',array('activities_id'=>$activities_id, 'activity'=>$activity, 'rent'=>$records[1], 'discount'=>$records[2], 'remarks'=>$records[3]));    
            loger('Create', $this->data['module']['id'], $this->data['module']['name'], $activities_id, 0, 0, 0, 0, 0,'Added Item:'.$records[0].'');
          }
          $i++;
        }
      }
      echo "affected rows".$i;
      fclose($activitiess);
    }

  }

?>