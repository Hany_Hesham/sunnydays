<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Reports extends MY_Controller {

		public function __construct(){
			parent::__construct();
      $this->load->model('admin/Modules_model');
      $this->load->model('admin/Hotels_model');
      $this->load->model('Reports_model');
      $this->load->helper('date_helper');
      $this->load->helper('mpdf_helper');
  		$this->data['module']      = $this->Modules_model->get_module_by(8);
      $this->data['permission']  = user_access($this->data['module']['id']);
		}
    
    public function index($report_name=''){
      access_checker($this->data['permission']['g_view'],$this->data['permission']['view'],0,0,0,0,0,'admin/dashboard'); 
      $data['reports']         = $this->Reports_model->get_reports();
      $data['modules']         = $this->Reports_model->get_reports_modules(); 
      $data['departments']     = get_departments($this->data['dep_id']);  
      $data['statuss']         = get_statuss();  
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);          
      $data['report_name']     = $report_name; 
      $datestring              = '%Y-%m-%d';
      $data['from_date']       = date("Y-m-d", strtotime("first day of this month"));
      $data['to_date']         = mdate($datestring,time());
      $data['view']            = 'reports/reports_gen';
      $this->load->view('admin/includes/layout',$data);
    }

    public function filter_generator($report_fun=''){
      $data = array();
      if ($report_fun == 'generate_complimentary_report'){
        $data['complimentary_companies'] = $this->Reports_model->get_complimentary_companies();
      }
      $this->load->view('reports/report_extra_filters',$data);
    }

    public function mpdfMaker(){
      $data['tableData']    = json_decode($this->input->post('tableData'),true);
      $data['report_name']  = $this->input->post('reportname');
      $data['from_date']    = $this->input->post('fromDate');
      $data['to_date']      = $this->input->post('toDate');
      $data['hotels_count'] = count($this->input->post('hotelId'));
      $data['username']     =  $this->data['username'];
      if (count($this->input->post('hotelId')) > 1 ) {
        $hotels      = $this->Hotels_model->get_hotels_by_ids($this->input->post('hotelId'));
        $data['hotel']=array();
        foreach ($hotels as $hotel) {
          $data['hotel']['name'][] = $hotel['code'];
          $data['hotel']['logo']   = 'logo.png';
        }
        $data['hotel']['hotel_name']  = implode(',', $data['hotel']['name']);
      }elseif (count($this->input->post('hotelId'))==1) {
        $data['hotel']      = $this->Hotels_model->get_by_id($this->input->post('hotelId')[0]);
      }
      $generated_html=report_pdf_generator($data);   
      $path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
      try {
        $mpdf = new \Mpdf\Mpdf([
          'margin_left' => 10,
          'margin_right' => 10,
          'margin_bottom' => 25,
          'margin_footer' => 10
        ]);
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle($data['hotel']['hotel_name']);
        $mpdf->SetAuthor($data['hotel']['hotel_name']);
        $mpdf->SetWatermarkText("Work-Flow");
        $mpdf->showWatermarkText = true;
        $mpdf->watermark_font = 'DejaVuSansCondensed';
        $mpdf->watermarkTextAlpha = 0.1;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($generated_html);
        $mpdf->debug = true;
        ob_end_clean();
        $pdfFilePath = $data['report_name'].'.pdf';
        $mpdf->Output(FCPATH.'assets/pdf_generated/'.$pdfFilePath, "F"); 
      }catch (\Mpdf\MpdfException $e) { 
        echo $e->getMessage();
      }
      echo json_encode($pdfFilePath);
      exit();
    }

    /**
     * ***************************
     * General Reports Section   *
     *****************************
     */



    /**
      * 1- Salary Advance Report
    */

    public function generate_salary_advance_report()
    {
      $data['table_fun']     = 'salary_advance_report_generator';
      $this->load->view('reports/general/salary_advance_report',$data);
    }    

    public function salary_advance_report_generator()
    {     
      $dt_att        = $this->datatables_att();
      $custom_search = $this->input->post('searchBy');
      $rows          = $this->Reports_model->get_salary_advance_report($this->data['uhotels'],$dt_att,$custom_search,'');
      $data          = array();
      $i = 1;
      $total = 0;
      
      foreach($rows as $row) {
        $arr = array(); 
        $arr[] = $i;
        $arr[] = '<a  href="'.base_url('general/salary_advance/view/'. $row['id']).'"
        target="_blank" titel="View Item"><strong>Salary Advance Request #.'.$row['id'].'</strong></a>';
        $arr[] = $row['hotel_name']; 
        $arr[] = $row['dep_name'];    
        $arr[] = $row['position_name'];
        $arr[] = $row['clock_no'];  
        $arr[] = $row['name'];
        $arr[] = money_formater($row['advance'],'EGP');  
        $arr[] = $row['status_name'];
        $arr[] = $row['role_name'];              
        $data[] =$arr;
        $i++;
        $total += $row['advance'];
      }
      
      $arr = []; 
      $arr[] = '';
      $arr[] = '<strong style="font-size:18px; color:#810808;">Total</strong>';
      $arr[] = ''; 
      $arr[] = '';    
      $arr[] = '';
      $arr[] = '';  
      $arr[] = '';
      $arr[] = '<strong style="font-size:14px; color:#810808;">'.money_formater($total,'EGP').'</strong>';  
      $arr[] = '';
      $arr[] = ''; 
      $data[] =$arr; 

      $output = array(
          "draw"            => $dt_att['draw'],
          "recordsTotal"    => count($this->Reports_model->get_all_salary_advance($this->data['uhotels'])),
          "recordsFiltered" => $this->Reports_model->get_salary_advance_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
          "data"            => $data
          );
      echo json_encode($output);
      exit();
    }

    /**
      * 1- complimentary Report
    */

    public function generate_complimentary_report()
    {
      $data['table_fun']     = 'complimentary_report_generator';
      $this->load->view('reports/general/complimentary_report',$data);
    }    

    public function complimentary_report_generator()
    {     
      $dt_att        = $this->datatables_att();
      $custom_search = $this->input->post('searchBy');
      $rows          = $this->Reports_model->get_complimentary_report($this->data['uhotels'],$dt_att,$custom_search,'');
      $data          = array();
      $i = 1;
      $total = 0;
      
      foreach($rows as $row) {
        $arr = array(); 
        $arr[] = $i;
        $arr[] = '<a  href="'.base_url('general/complimentary/view/'. $row['id']).'"
        target="_blank" titel="View Item"><strong>Complimentary #.'.$row['id'].'</strong></a>';
        $arr[] = $row['hotel_name']; 
        $arr[] = $row['dep_name'];    
        $arr[] = $row['company'];  
        $arr[] = $row['conf_no'];
        $arr[] = $row['name'];
        $arr[] = $row['arrival']; 
        $arr[] = $row['departure'];
        $arr[] = $row['room_type'];  
        $arr[] = $row['payment'];
        $arr[] = $row['reasons'];  
        $arr[] = $row['status_name'];
        $arr[] = $row['role_name'];              
        $data[] =$arr;
        $i++;
      } 

      $output = array(
          "draw"            => $dt_att['draw'],
          "recordsTotal"    => count($this->Reports_model->get_all_complimentary($this->data['uhotels'])),
          "recordsFiltered" => $this->Reports_model->get_complimentary_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
          "data"            => $data
          );
      echo json_encode($output);
      exit();
    }



     /**
     * ***************************
     * Payable Reports Section   *
     *****************************
     */



    /**
      * 1- petty cash Report
    */

    public function generate_petty_cash_report()
    {
      $data['table_fun']     = 'petty_cash_report_generator';
      $this->load->view('reports/payable/petty_cash_report',$data);
    }    

    public function petty_cash_report_generator()
    {     
      $dt_att        = $this->datatables_att();
      $custom_search = $this->input->post('searchBy');
      $rows          = $this->Reports_model->get_petty_cash_report($this->data['uhotels'],$dt_att,$custom_search,'');
      $data          = array();
      $i = 1;
      $totals = [
        ['symbol' => 'EGP', 'name' => 'Total EGP', 'total' => 0],
        ['symbol' => '$', 'name' => 'Total USD', 'total' => 0],
        ['symbol' => '€', 'name' => 'Total EURO','total' => 0],
        ['symbol' => '£', 'name' => 'Total GBP', 'total' => 0]
      ];
      
      foreach($rows as $row) {
        $arr = array(); 
        $arr[] = $i;
        $arr[] = '<a  href="'.base_url('payable/petty_cash/view/'. $row['petty_cash_id']).'"
        target="_blank" titel="View Item"><strong>Petty Cash Disbursement #.'.$row['petty_cash_id'].'</strong></a>';
        $arr[] = $row['hotel_name']; 
        $arr[] = $row['name'];    
        $arr[] = $row['position_name'];
        $arr[] = $row['acc_no'];  
        $arr[] = $row['description'];
        $arr[] = $row['notes'];
        $arr[] = $row['amount'].' '.$row['currency'];  
        $arr[] = $row['status_name'];
        $arr[] = $row['role_name'];              
        $data[] =$arr;
        $i++;

        if($row['currency'] == 'EGP') $totals[0]['total'] += $row['amount'];
        if($row['currency'] == '$') $totals[1]['total'] += $row['amount'];
        if($row['currency'] == '€') $totals[2]['total'] += $row['amount'];
        if($row['currency'] == '£') $totals[3]['total'] += $row['amount'];
      }
      foreach($totals as $total){
        if ($total['total'] > 0 ) {
          $arr = []; 
          $arr[] = '';
          $arr[] = '<strong style="font-size:18px; color:#810808;">'. $total['name'].'</strong>';
          $arr[] = ''; 
          $arr[] = '';    
          $arr[] = '';
          $arr[] = '';  
          $arr[] = '';
          $arr[] = '';
          $arr[] = '<strong style="font-size:14px; color:#810808;">'.$total['total'].' '.$total['symbol'].'</strong>';  
          $arr[] = '';
          $arr[] = ''; 
          $data[] =$arr; 
        }
      }

      $output = array(
          "draw"            => $dt_att['draw'],
          "recordsTotal"    => count($this->Reports_model->get_all_petty_cash($this->data['uhotels'])),
          "recordsFiltered" => $this->Reports_model->get_petty_cash_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
          "data"            => $data
          );
      echo json_encode($output);
      exit();
    }

    /**
      * 1- Payment voucher Report
    */

    public function generate_payment_voucher_report()
    {
      $data['table_fun']     = 'payment_voucher_report_generator';
      $this->load->view('reports/payable/payment_voucher_report',$data);
    }    

    public function payment_voucher_report_generator()
    {     
      $dt_att        = $this->datatables_att();
      $custom_search = $this->input->post('searchBy');
      $rows          = $this->Reports_model->get_payment_voucher_report($this->data['uhotels'],$dt_att,$custom_search,'');
      $data          = array();
      $i = 1;
      $totals = [
        ['symbol' => 'EGP', 'name' => 'Total EGP', 'total' => 0],
        ['symbol' => '$', 'name' => 'Total USD', 'total' => 0],
        ['symbol' => '€', 'name' => 'Total EURO','total' => 0],
        ['symbol' => '£', 'name' => 'Total GBP', 'total' => 0]
      ];
      
      foreach($rows as $row) {
        $arr = array(); 
        $arr[] = $i;
        $arr[] = '<a  href="'.base_url('payable/payment_voucher/view/'. $row['payment_voucher_id']).'"
        target="_blank" titel="View Item"><strong>Payment Voucher #.'.$row['payment_voucher_id'].'</strong></a>';
        $arr[] = $row['hotel_name']; 
        $arr[] = $row['explanation'];    
        $arr[] = $row['acc_no'];  
        $arr[] = $row['description'];
        $arr[] = $row['notes'];
        $arr[] = $row['amount'].' '.$row['currency'];  
        $arr[] = $row['status_name'];
        $arr[] = $row['role_name'];              
        $data[] =$arr;
        $i++;

        if($row['currency'] == 'EGP') $totals[0]['total'] += $row['amount'];
        if($row['currency'] == '$') $totals[1]['total'] += $row['amount'];
        if($row['currency'] == '€') $totals[2]['total'] += $row['amount'];
        if($row['currency'] == '£') $totals[3]['total'] += $row['amount'];
      }
      foreach($totals as $total){
        if ($total['total'] > 0 ) {
          $arr = []; 
          $arr[] = '';
          $arr[] = '<strong style="font-size:18px; color:#810808;">'. $total['name'].'</strong>';
          $arr[] = ''; 
          $arr[] = '';    
          $arr[] = '';
          $arr[] = '';  
          $arr[] = '';
          $arr[] = '<strong style="font-size:14px; color:#810808;">'.$total['total'].' '.$total['symbol'].'</strong>';  
          $arr[] = '';
          $arr[] = ''; 
          $data[] =$arr; 
        }
      }

      $output = array(
          "draw"            => $dt_att['draw'],
          "recordsTotal"    => count($this->Reports_model->get_all_payment_voucher($this->data['uhotels'])),
          "recordsFiltered" => $this->Reports_model->get_payment_voucher_report( $this->data['uhotels'],$dt_att,$custom_search,'count'),
          "data"            => $data
          );
      echo json_encode($output);
      exit();
    }

  
	}

?>	 