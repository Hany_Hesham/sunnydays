<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Petty_cash extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('payable/Petty_cash_model');
      $this->data['module']        = $this->Modules_model->get_module_by(22);
      $this->data['permission']    = user_access($this->data['module']['id']);
      $this->data['limit']         = $this->get_limits(22);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'payable/petty_cash/index_petty_cash';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function petty_cash_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Petty_cash_model->get_petty_cashs($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'petty_cash\',\'petty_cash\',\'payable/petty_cash\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('payable/petty_cash/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('payable/petty_cash/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Petty Cash Disbursement #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['type_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['explanation'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['amount'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Petty_cash_model->get_all_petty_cashs($this->data['uhotels']),
       "recordsFiltered" => $this->Petty_cash_model->get_petty_cashs($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'payable/petty_cash');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['types']           = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']          = get_file_code('files','petty_cash');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position_name'           => $this->input->post('position_name'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $petty_cash_id  =  $this->Petty_cash_model->add_petty_cash($fdata);
        if ($petty_cash_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Petty Cash Disbursement #'.$petty_cash_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $petty_cash_id, 'files');
          $total_amount = 0;
          foreach ($this->input->post('items') as $key => $item) {
            $total_amount += $item['amount'];
            $item['petty_cash_id']    = $petty_cash_id;
            $item_id                  = $this->Petty_cash_model->add_petty_cash_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['acc_no'].'');
            }
          }
          $this->db->update('petty_cash', array('amount' => $total_amount), "id = ".$petty_cash_id);
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $petty_cash_id);
        }
      }
      $data['view'] = 'payable/petty_cash/petty_cash_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($petty_cash_id){
      $data['petty_cash']          = $this->Petty_cash_model->get_petty_cash($petty_cash_id);
      if (($data['petty_cash']['status'] == 2 || $data['petty_cash']['status'] == 1) && $data['petty_cash']['reback']) {
        $rrData = json_decode($data['petty_cash']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('petty_cash', array('status' => 3, 'role_id' => 0), "id = ".$petty_cash_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/petty_cash', $data['petty_cash'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, 0, 0, 0, 0, 0, 'Viewed Petty Cash Disbursement #'.$petty_cash_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$petty_cash_id,'files');
      $this->data['form_id']       = $petty_cash_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'payable/petty_cash/view_petty_cash';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($petty_cash_id){
      $data['petty_cash']             = $this->Petty_cash_model->get_petty_cash($petty_cash_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/petty_cash', $data['petty_cash'], $this->data['uhotels']);
      $data['petty_cash_items']          = $this->Petty_cash_model->get_petty_cash_items($petty_cash_id);
      $this->load->view('payable/petty_cash/view_details',$data);
    }  

    public function signers_items($petty_cash_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $petty_cash_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $petty_cash_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($petty_cash_id=''){
      $data['petty_cash']   = $this->Petty_cash_model->get_petty_cash($petty_cash_id);
      edit_checker($data['petty_cash'], $this->data['permission']['edit'], 'payable/petty_cash/view/'.$petty_cash_id);
      $data['petty_cash_items']          = $this->Petty_cash_model->get_petty_cash_items($petty_cash_id);
      $data['petty_cash_id']   = $petty_cash_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['types']           = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']          = $petty_cash_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$petty_cash_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position_name'           => $this->input->post('position_name'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Petty_cash_model->edit_petty_cash($fdata, $petty_cash_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, 0,json_encode($data['petty_cash'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Petty Cash Disbursement #'.$petty_cash_id );
        $total_amount = 0;
        foreach ($this->input->post('items') as $key => $item) {
          $total_amount += $item['amount'];
          if (isset($item['id'])) {
            $item_data              = $this->Petty_cash_model->get_petty_cash_item($item['id']);
            $item['petty_cash_id']  = $petty_cash_id;
            $updated                = $this->Petty_cash_model->edit_petty_cash_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['acc_no']);
            }
          }else{
            $item['petty_cash_id']  = $petty_cash_id;
            $item_id                = $this->Petty_cash_model->add_petty_cash_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['acc_no'].'');
            }
          }
        }
        $this->db->update('petty_cash', array('amount' => $total_amount), "id = ".$petty_cash_id);
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $petty_cash_id);
      } 
      $data['view'] = 'payable/petty_cash/petty_cash_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['petty_cash']     = $this->Petty_cash_model->get_petty_cash($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'payable/petty_cash', $data['petty_cash'], $this->data['uhotels']);
      $data['petty_cash_items'] = $this->Petty_cash_model->get_petty_cash_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']       = get_currency();
      $data['types']            = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']           = get_file_code('files','petty_cash');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'code'                    => $this->input->post('code'),
          'name'                    => $this->input->post('name'),
          'position_name'           => $this->input->post('position_name'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $petty_cash_id  =  $this->Petty_cash_model->add_petty_cash($fdata);
        if ($petty_cash_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Petty Cash Disbursement #'.$petty_cash_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $petty_cash_id, 'files');
          $total_amount = 0;
          foreach ($this->input->post('items') as $key => $item) {
            $total_amount += $item['amount'];
            unset($item['id']);
            $item['petty_cash_id']    = $petty_cash_id;
            $item_id                  = $this->Petty_cash_model->add_petty_cash_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $petty_cash_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['acc_no'].'');
            }
          }
          $this->db->update('petty_cash', array('amount' => $total_amount), "id = ".$petty_cash_id);
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $petty_cash_id);
        }
      }
      $data['view'] = 'payable/petty_cash/petty_cash_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Petty_cash_model->get_petty_cash_item($id);
        $this->db->update('petty_cash_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['petty_cash_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['acc_no'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'payable/petty_cash');
      $this->db->update('petty_cash', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Petty Cash Disbursement #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>