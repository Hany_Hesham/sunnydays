<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Payment_voucher extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('payable/Payment_voucher_model');
      $this->data['module']        = $this->Modules_model->get_module_by(28);
      $this->data['permission']    = user_access($this->data['module']['id']);
      $this->data['limit']         = $this->get_limits(28);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'payable/payment_voucher/index_payment_voucher';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function payment_voucher_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Payment_voucher_model->get_payment_vouchers($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'payment_voucher\',\'payment_voucher\',\'payable/payment_voucher\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('payable/payment_voucher/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('payable/payment_voucher/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Payment Voucher #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';    
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['type_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['explanation'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['amount'].' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Payment_voucher_model->get_all_payment_vouchers($this->data['uhotels']),
       "recordsFiltered" => $this->Payment_voucher_model->get_payment_vouchers($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'payable/payment_voucher');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['types']           = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']          = get_file_code('files','payment_voucher');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $payment_voucher_id  =  $this->Payment_voucher_model->add_payment_voucher($fdata);
        if ($payment_voucher_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Voucher #'.$payment_voucher_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_voucher_id, 'files');
          $total_amount = 0;
          foreach ($this->input->post('items') as $key => $item) {
            $total_amount += $item['amount'];
            $item['payment_voucher_id']    = $payment_voucher_id;
            $item_id                  = $this->Payment_voucher_model->add_payment_voucher_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['acc_no'].'');
            }
          }
          $this->db->update('payment_voucher', array('amount' => $total_amount), "id = ".$payment_voucher_id);
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $payment_voucher_id);
        }
      }
      $data['view'] = 'payable/payment_voucher/payment_voucher_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($payment_voucher_id){
      $data['payment_voucher']          = $this->Payment_voucher_model->get_payment_voucher($payment_voucher_id);
      if (($data['payment_voucher']['status'] == 2 || $data['payment_voucher']['status'] == 1) && $data['payment_voucher']['reback']) {
        $rrData = json_decode($data['payment_voucher']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('payment_voucher', array('status' => 3, 'role_id' => 0), "id = ".$payment_voucher_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/payment_voucher', $data['payment_voucher'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, 0, 0, 0, 0, 0, 'Viewed Payment Voucher #'.$payment_voucher_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_voucher_id,'files');
      $this->data['form_id']       = $payment_voucher_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'payable/payment_voucher/view_payment_voucher';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($payment_voucher_id){
      $data['payment_voucher']             = $this->Payment_voucher_model->get_payment_voucher($payment_voucher_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/payment_voucher', $data['payment_voucher'], $this->data['uhotels']);
      $data['payment_voucher_items']          = $this->Payment_voucher_model->get_payment_voucher_items($payment_voucher_id);
      $this->load->view('payable/payment_voucher/view_details',$data);
    }  

    public function signers_items($payment_voucher_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $payment_voucher_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $payment_voucher_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($payment_voucher_id=''){
      $data['payment_voucher']   = $this->Payment_voucher_model->get_payment_voucher($payment_voucher_id);
      edit_checker($data['payment_voucher'], $this->data['permission']['edit'], 'payable/payment_voucher/view/'.$payment_voucher_id);
      $data['payment_voucher_items']          = $this->Payment_voucher_model->get_payment_voucher_items($payment_voucher_id);
      $data['payment_voucher_id']   = $payment_voucher_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']      = get_currency();
      $data['types']           = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']          = $payment_voucher_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$payment_voucher_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Payment_voucher_model->edit_payment_voucher($fdata, $payment_voucher_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, 0,json_encode($data['payment_voucher'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Payment Voucher #'.$payment_voucher_id );
        $total_amount = 0;
        foreach ($this->input->post('items') as $key => $item) {
          $total_amount += $item['amount'];
          if (isset($item['id'])) {
            $item_data              = $this->Payment_voucher_model->get_payment_voucher_item($item['id']);
            $item['payment_voucher_id']  = $payment_voucher_id;
            $updated                = $this->Payment_voucher_model->edit_payment_voucher_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['acc_no']);
            }
          }else{
            $item['payment_voucher_id']  = $payment_voucher_id;
            $item_id                = $this->Payment_voucher_model->add_payment_voucher_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['acc_no'].'');
            }
          }
        }
        $this->db->update('payment_voucher', array('amount' => $total_amount), "id = ".$payment_voucher_id);
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $payment_voucher_id);
      } 
      $data['view'] = 'payable/payment_voucher/payment_voucher_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['payment_voucher']     = $this->Payment_voucher_model->get_payment_voucher($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'payable/payment_voucher', $data['payment_voucher'], $this->data['uhotels']);
      $data['payment_voucher_items'] = $this->Payment_voucher_model->get_payment_voucher_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['currencies']       = get_currency();
      $data['types']            = $this->General_model->get_meta_data('payment_types');
      $data['gen_id']           = get_file_code('files','payment_voucher');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'currency'                => $this->input->post('currency'),
          'explanation'             => $this->input->post('explanation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $payment_voucher_id  =  $this->Payment_voucher_model->add_payment_voucher($fdata);
        if ($payment_voucher_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Payment Voucher #'.$payment_voucher_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $payment_voucher_id, 'files');
          $total_amount = 0;
          foreach ($this->input->post('items') as $key => $item) {
            $total_amount += $item['amount'];
            unset($item['id']);
            $item['payment_voucher_id']    = $payment_voucher_id;
            $item_id                  = $this->Payment_voucher_model->add_payment_voucher_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $payment_voucher_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['acc_no'].'');
            }
          }
          $this->db->update('payment_voucher', array('amount' => $total_amount), "id = ".$payment_voucher_id);
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $payment_voucher_id);
        }
      }
      $data['view'] = 'payable/payment_voucher/payment_voucher_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Payment_voucher_model->get_payment_voucher_item($id);
        $this->db->update('payment_voucher_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['payment_voucher_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['acc_no'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'payable/payment_voucher');
      $this->db->update('payment_voucher', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Payment Voucher #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>