<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Bank_transfer extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('payable/Bank_transfer_model');
      $this->data['module']        = $this->Modules_model->get_module_by(54);
      $this->data['permission']    = user_access($this->data['module']['id']);
      $this->data['limit']         = $this->get_limits(54);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'payable/bank_transfer/index_bank_transfer';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function bank_transfer_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Bank_transfer_model->get_bank_transfers($this->data['uhotels'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'bank_transfer\',\'bank_transfer\',\'payable/bank_transfer\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('payable/bank_transfer/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('payable/bank_transfer/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Bank Transfer #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';    
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['type_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['from_bank'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_bank'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.number_format($row['amount'],2).' '.$row['currency'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Bank_transfer_model->get_all_bank_transfers($this->data['uhotels']),
       "recordsFiltered" => $this->Bank_transfer_model->get_bank_transfers($this->data['uhotels'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'payable/bank_transfer');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']           = $this->General_model->get_meta_data('transfer_type');
      $data['currencies']      = get_currency();
      $data['gen_id']          = get_file_code('files','bank_transfer');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'from_bank'               => $this->input->post('from_bank'),
          'from_acc_no'             => $this->input->post('from_acc_no'),
          'to_name'                 => $this->input->post('to_name'),
          'to_bank'                 => $this->input->post('to_bank'),
          'to_acc_no'               => $this->input->post('to_acc_no'),
          'amount'                  => $this->input->post('amount'),
          'currency'                => $this->input->post('currency'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $bank_transfer_id  =  $this->Bank_transfer_model->add_bank_transfer($fdata);
        if ($bank_transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $bank_transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Bank Transfer #'.$bank_transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $bank_transfer_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $bank_transfer_id);
        }
      }
      $data['view'] = 'payable/bank_transfer/bank_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($bank_transfer_id){
      $data['bank_transfer']       = $this->Bank_transfer_model->get_bank_transfer($bank_transfer_id);
      if (($data['bank_transfer']['status'] == 2 || $data['bank_transfer']['status'] == 1) && $data['bank_transfer']['reback']) {
        $rrData = json_decode($data['bank_transfer']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('bank_transfer', array('status' => 3, 'role_id' => 0), "id = ".$bank_transfer_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/bank_transfer', $data['bank_transfer'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $bank_transfer_id, 0, 0, 0, 0, 0, 'Viewed Bank Transfer #'.$bank_transfer_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$bank_transfer_id,'files');
      $this->data['form_id']       = $bank_transfer_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'payable/bank_transfer/view_bank_transfer';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($bank_transfer_id){
      $data['bank_transfer']             = $this->Bank_transfer_model->get_bank_transfer($bank_transfer_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'payable/bank_transfer', $data['bank_transfer'], $this->data['uhotels']);
      $this->load->view('payable/bank_transfer/view_details',$data);
    }    

    public function signers_items($bank_transfer_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $bank_transfer_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $bank_transfer_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($bank_transfer_id=''){
      $data['bank_transfer']   = $this->Bank_transfer_model->get_bank_transfer($bank_transfer_id);
      edit_checker($data['bank_transfer'], $this->data['permission']['edit'], 'payable/bank_transfer/view/'.$bank_transfer_id);
      $data['bank_transfer_id']      = $bank_transfer_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']           = $this->General_model->get_meta_data('transfer_type');
      $data['currencies']      = get_currency();
      $data['gen_id']          = $bank_transfer_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$bank_transfer_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'from_bank'               => $this->input->post('from_bank'),
          'from_acc_no'             => $this->input->post('from_acc_no'),
          'to_name'                 => $this->input->post('to_name'),
          'to_bank'                 => $this->input->post('to_bank'),
          'to_acc_no'               => $this->input->post('to_acc_no'),
          'amount'                  => $this->input->post('amount'),
          'currency'                => $this->input->post('currency'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',

        ];    
        $this->Bank_transfer_model->edit_bank_transfer($fdata, $bank_transfer_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $bank_transfer_id, 0,json_encode($data['bank_transfer'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Bank Transfer #'.$bank_transfer_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $bank_transfer_id);
      } 
      $data['view'] = 'payable/bank_transfer/bank_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['bank_transfer']   = $this->Bank_transfer_model->get_bank_transfer($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'payable/bank_transfer', $data['bank_transfer'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['types']           = $this->General_model->get_meta_data('transfer_type');
      $data['currencies']      = get_currency();
      $data['gen_id']          = get_file_code('files','bank_transfer');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type_id'                 => $this->input->post('type_id'),
          'from_bank'               => $this->input->post('from_bank'),
          'from_acc_no'             => $this->input->post('from_acc_no'),
          'to_name'                 => $this->input->post('to_name'),
          'to_bank'                 => $this->input->post('to_bank'),
          'to_acc_no'               => $this->input->post('to_acc_no'),
          'amount'                  => $this->input->post('amount'),
          'currency'                => $this->input->post('currency'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $bank_transfer_id  =  $this->Bank_transfer_model->add_bank_transfer($fdata);
        if ($bank_transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $bank_transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Bank Transfer #'.$bank_transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $bank_transfer_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->signers($this->data['module']['id'], $bank_transfer_id);
        }
      }
      $data['view'] = 'payable/bank_transfer/bank_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'payable/bank_transfer');
      $this->db->update('bank_transfer', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Bank Transfer #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>