<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Employment extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hr/Employment_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(46);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hr/employment/index_employment';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function employment_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Employment_model->get_employments($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'employment\',\'employment\',\'hr/employment\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hr/employment/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hr/employment/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Application For Employment #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['position_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['clock_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['card_id'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Employment_model->get_all_employments($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Employment_model->get_employments($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hr/employment');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','employment');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'salary'                  => $this->input->post('salary'),
          'nationality'             => $this->input->post('nationality'),
          'birth_date'              => $this->input->post('birth_date'),
          'religion'                => $this->input->post('religion'),
          'military'                => $this->input->post('military'),
          'card_id'                 => $this->input->post('card_id'),
          'issue_date'              => $this->input->post('issue_date'),
          'address'                 => $this->input->post('address'),
          'phone'                   => $this->input->post('phone'),
          'marital'                 => $this->input->post('marital'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $employment_id  =  $this->Employment_model->add_employment($fdata);
        if ($employment_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $employment_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Application For Employment #'.$employment_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $employment_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $employment_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $employment_id);
        }
      }
      $data['view'] = 'hr/employment/employment_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($employment_id){
      $data['employment']       = $this->Employment_model->get_employment($employment_id);
      if (($data['employment']['status'] == 2 || $data['employment']['status'] == 1) && $data['employment']['reback']) {
        $rrData = json_decode($data['employment']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('employment', array('status' => 3, 'role_id' => 0), "id = ".$employment_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/employment', $data['employment'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $employment_id, 0, 0, 0, 0, 0, 'Viewed Application For Employment #'.$employment_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$employment_id,'files');
      $this->data['form_id']       = $employment_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hr/employment/view_employment';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($employment_id){
      $data['employment']             = $this->Employment_model->get_employment($employment_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/employment', $data['employment'], $this->data['uhotels']);
      $this->load->view('hr/employment/view_details',$data);
    }    

    public function signers_items($employment_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $employment_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $employment_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($employment_id=''){
      $data['employment']   = $this->Employment_model->get_employment($employment_id);
      edit_checker($data['employment'], $this->data['permission']['edit'], 'hr/employment/view/'.$employment_id);
      $data['employment_id']      = $employment_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $employment_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$employment_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'salary'                  => $this->input->post('salary'),
          'nationality'             => $this->input->post('nationality'),
          'birth_date'              => $this->input->post('birth_date'),
          'religion'                => $this->input->post('religion'),
          'military'                => $this->input->post('military'),
          'card_id'                 => $this->input->post('card_id'),
          'issue_date'              => $this->input->post('issue_date'),
          'address'                 => $this->input->post('address'),
          'phone'                   => $this->input->post('phone'),
          'marital'                 => $this->input->post('marital'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Employment_model->edit_employment($fdata, $employment_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $employment_id, 0,json_encode($data['employment'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Application For Employment #'.$employment_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $employment_id);
      } 
      $data['view'] = 'hr/employment/employment_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['employment']   = $this->Employment_model->get_employment($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hr/employment', $data['employment'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','employment');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'salary'                  => $this->input->post('salary'),
          'nationality'             => $this->input->post('nationality'),
          'birth_date'              => $this->input->post('birth_date'),
          'religion'                => $this->input->post('religion'),
          'military'                => $this->input->post('military'),
          'card_id'                 => $this->input->post('card_id'),
          'issue_date'              => $this->input->post('issue_date'),
          'address'                 => $this->input->post('address'),
          'phone'                   => $this->input->post('phone'),
          'marital'                 => $this->input->post('marital'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $employment_id  =  $this->Employment_model->add_employment($fdata);
        if ($employment_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $employment_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Application For Employment #'.$employment_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $employment_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $employment_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $employment_id);
        }
      }
      $data['view'] = 'hr/employment/employment_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hr/employment');
      $this->db->update('employment', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Application For Employment #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>