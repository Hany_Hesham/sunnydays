<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Transfer extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hr/Transfer_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(49);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hr/transfer/index_transfer';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function transfer_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Transfer_model->get_transfers($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'transfer\',\'transfer\',\'hr/transfer\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hr/transfer/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hr/transfer/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Transfer Request #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['from_hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_hotel_name'].'</span>'; 
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Transfer_model->get_all_transfers($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Transfer_model->get_transfers($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      from_to_access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hr/transfer');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','transfer');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $from_role_id = get_roleHotel_byid('departments',$this->input->post('from_dep_code'));
        $to_role_id = get_roleHotel_byid('departments',$this->input->post('to_dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code'           => $this->input->post('from_dep_code'),
          'to_dep_code'             => $this->input->post('to_dep_code'),
          'from_position'           => $this->input->post('from_position'),
          'to_position'             => $this->input->post('to_position'),
          'name'                    => $this->input->post('name'),
          'nationality'             => $this->input->post('nationality'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'from_salary'             => $this->input->post('from_salary'),
          'to_salary'               => $this->input->post('to_salary'),
          'salary_max'              => $this->input->post('salary_max'),
          'salary_med'              => $this->input->post('salary_med'),
          'salary_min'              => $this->input->post('salary_min'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $transfer_id  =  $this->Transfer_model->add_transfer($fdata);
        if ($transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Transfer Request #'.$transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $transfer_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $transfer_id, $from_role_id, 0, $fdata['from_hid'], 1);
          $this->signers($this->data['module']['id'], $transfer_id, 1, 1);
          $this->Sign_model->addSignature($this->data['module']['id'], $transfer_id, $to_role_id, 0, $fdata['to_hid'], 2);
          $this->signers($this->data['module']['id'], $transfer_id, 2, 1);
          $this->db->update('transfer', array('stage' => 'First'), "id = ".$transfer_id);
          $this->stage($this->data['module']['id'], $transfer_id, 1, 1);
        }
      }
      $data['view'] = 'hr/transfer/transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($transfer_id){
      $data['transfer']       = $this->Transfer_model->get_transfer($transfer_id);
      if (($data['transfer']['status'] == 2 || $data['transfer']['status'] == 1) && $data['transfer']['reback']) {
        $rrData = json_decode($data['transfer']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('transfer', array('status' => 3, 'role_id' => 0), "id = ".$transfer_id);
        }
      }
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/transfer', $data['transfer'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $transfer_id, 0, 0, 0, 0, 0, 'Viewed Transfer Request #'.$transfer_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$transfer_id,'files');
      $this->data['form_id']       = $transfer_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hr/transfer/view_transfer';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($type, $transfer_id){
      $data['transfer']             = $this->Transfer_model->get_transfer($transfer_id);
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/transfer', $data['transfer'], $this->data['uhotels']);
      $data['hotel_type']           = $type;      
      $this->load->view('hr/transfer/view_details',$data);
    }    

    public function signers_items($type, $transfer_id, $stage = FALSE, $status = FALSE){
      $data['signatures']       = getSigners($this->data['module']['id'], $transfer_id, $type);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $transfer_id;
      if (($stage == 'First' && $type == 1) || ($stage == 'Second' && $type == 2) || ($stage == 'First' && $type == 2 && $status == 2)) {
        $data['doned']          = 0;      
      }else{
        $data['doned']          = 1;              
      }    
      $this->load->view('admin/html_parts/signers',$data);
    }

    public function edit($transfer_id=''){
      $data['transfer']   = $this->Transfer_model->get_transfer($transfer_id);
      edit_checker($data['transfer'], $this->data['permission']['edit'], 'hr/transfer/view/'.$transfer_id);
      $data['transfer_id']      = $transfer_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $transfer_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$transfer_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code_dep_code'  => $this->input->post('from_dep_code_dep_code'),
          'to_dep_code_dep_code'    => $this->input->post('to_dep_code_dep_code'),
          'from_position'           => $this->input->post('from_position'),
          'to_position'             => $this->input->post('to_position'),
          'name'                    => $this->input->post('name'),
          'nationality'             => $this->input->post('nationality'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'from_salary'             => $this->input->post('from_salary'),
          'to_salary'               => $this->input->post('to_salary'),
          'salary_max'              => $this->input->post('salary_max'),
          'salary_med'              => $this->input->post('salary_med'),
          'salary_min'              => $this->input->post('salary_min'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Transfer_model->edit_transfer($fdata, $transfer_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $transfer_id, 0,json_encode($data['transfer'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Transfer Request #'.$transfer_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $transfer_id, 1, 1);
        $this->signers($this->data['module']['id'], $transfer_id, 2);
      } 
      $data['view'] = 'hr/transfer/transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['transfer']   = $this->Transfer_model->get_transfer($copied);
      from_to_access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hr/transfer', $data['transfer'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','transfer');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $from_role_id = get_roleHotel_byid('departments',$this->input->post('from_dep_code'));
        $to_role_id = get_roleHotel_byid('departments',$this->input->post('to_dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code_dep_code'  => $this->input->post('from_dep_code_dep_code'),
          'to_dep_code_dep_code'    => $this->input->post('to_dep_code_dep_code'),
          'from_position'           => $this->input->post('from_position'),
          'to_position'             => $this->input->post('to_position'),
          'name'                    => $this->input->post('name'),
          'nationality'             => $this->input->post('nationality'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'from_salary'             => $this->input->post('from_salary'),
          'to_salary'               => $this->input->post('to_salary'),
          'salary_max'              => $this->input->post('salary_max'),
          'salary_med'              => $this->input->post('salary_med'),
          'salary_min'              => $this->input->post('salary_min'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $transfer_id  =  $this->Transfer_model->add_transfer($fdata);
        if ($transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Transfer Request #'.$transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $transfer_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $transfer_id, $from_role_id, 0, $fdata['from_hid'], 1);
          $this->signers($this->data['module']['id'], $transfer_id, 1, 1);
          $this->Sign_model->addSignature($this->data['module']['id'], $transfer_id, $to_role_id, 0, $fdata['to_hid'], 2);
          $this->signers($this->data['module']['id'], $transfer_id, 2);
        }
      }
      $data['view'] = 'hr/transfer/transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      from_to_access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hr/transfer');
      $this->db->update('transfer', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Transfer Request #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>