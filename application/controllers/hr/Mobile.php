<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Mobile extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hr/Mobile_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(13);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hr/mobile/index_mobile';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function mobile_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Mobile_model->get_mobiles($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'mobile\',\'mobile\',\'hr/mobile\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hr/mobile/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        if ($row['pos_id']) {
          $position = $row['pos_name'];
        } else {
          $position = $row['position_name'];
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hr/mobile/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Mobile Allowance #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$position.'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Mobile_model->get_all_mobiles($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Mobile_model->get_mobiles($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hr/mobile');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      // $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','mobile');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'code'                    => $this->input->post('code'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'position'                => $this->input->post('position'),
          'name'                    => $this->input->post('name'),
          'allowance'               => $this->input->post('allowance'),
          'hire_date'               => $this->input->post('hire_date'),
          'level'                   => $this->input->post('level'),
          'effective_date'          => $this->input->post('effective_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $mobile_id  =  $this->Mobile_model->add_mobile($fdata);
        if ($mobile_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $mobile_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Mobile Allowance #'.$mobile_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $mobile_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $mobile_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $mobile_id);
        }
      }
      $data['view'] = 'hr/mobile/mobile_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($mobile_id){
      $data['mobile']       = $this->Mobile_model->get_mobile($mobile_id);
      if (($data['mobile']['status'] == 2 || $data['mobile']['status'] == 1) && $data['mobile']['reback']) {
        $rrData = json_decode($data['mobile']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('mobile', array('status' => 3, 'role_id' => 0), "id = ".$mobile_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/mobile', $data['mobile'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $mobile_id, 0, 0, 0, 0, 0, 'Viewed Mobile Allowance #'.$mobile_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$mobile_id,'files');
      $this->data['form_id']       = $mobile_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hr/mobile/view_mobile';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($mobile_id){
      $data['mobile']             = $this->Mobile_model->get_mobile($mobile_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/mobile', $data['mobile'], $this->data['uhotels']);
      $this->load->view('hr/mobile/view_details',$data);
    }    

    public function signers_items($mobile_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $mobile_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $mobile_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($mobile_id=''){
      $data['mobile']   = $this->Mobile_model->get_mobile($mobile_id);
      edit_checker($data['mobile'], $this->data['permission']['edit'], 'hr/mobile/view/'.$mobile_id);
      $data['mobile_id']      = $mobile_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      // $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $mobile_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$mobile_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'code'                    => $this->input->post('code'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'position'                => $this->input->post('position'),
          'name'                    => $this->input->post('name'),
          'allowance'               => $this->input->post('allowance'),
          'hire_date'               => $this->input->post('hire_date'),
          'level'                   => $this->input->post('level'),
          'effective_date'          => $this->input->post('effective_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Mobile_model->edit_mobile($fdata, $mobile_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $mobile_id, 0,json_encode($data['mobile'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Mobile Allowance #'.$mobile_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $mobile_id);
      } 
      $data['view'] = 'hr/mobile/mobile_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['mobile']   = $this->Mobile_model->get_mobile($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hr/mobile', $data['mobile'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','mobile');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'code'                    => $this->input->post('code'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'position'                => $this->input->post('position'),
          'name'                    => $this->input->post('name'),
          'allowance'               => $this->input->post('allowance'),
          'hire_date'               => $this->input->post('hire_date'),
          'level'                   => $this->input->post('level'),
          'effective_date'          => $this->input->post('effective_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $mobile_id  =  $this->Mobile_model->add_mobile($fdata);
        if ($mobile_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $mobile_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Mobile Allowance #'.$mobile_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $mobile_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $mobile_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $mobile_id);
        }
      }
      $data['view'] = 'hr/mobile/mobile_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hr/mobile');
      $this->db->update('mobile', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Mobile Allowance #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>