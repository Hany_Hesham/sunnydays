<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Change_status extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hr/Change_status_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(11);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hr/change_status/index_change_status';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function change_status_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Change_status_model->get_change_statuss($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'change_status\',\'change_status\',\'hr/change_status\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hr/change_status/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        if ($row['pos_id']) {
          $position         = $row['pos_name'];
        } else {
          $position         = $row['position_name'];
        }
        if ($row['new_deps']) {
          $new_department   = $row['new_depart_name'];
        } else {
          $new_department   = $row['new_dep_name'];
        }
        if ($row['new_positions']) {
          $new_position     = $row['new_pos_name'];
        } else {
          $new_position     = $row['new_position_name'];
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hr/change_status/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Change Of Status #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$new_department.'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$position.'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$new_position.'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Change_status_model->get_all_change_statuss($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Change_status_model->get_change_statuss($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hr/change_status');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departmentz']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','change_status');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'clock_no'                => $this->input->post('clock_no'),
          'new_clock'               => $this->input->post('new_clock'),
          'name'                    => $this->input->post('name'),
          'hire_date'               => $this->input->post('hire_date'),
          'dep_id'                  => $this->input->post('dep_id'),
          'position'                => $this->input->post('position'),
          'pos_id'                  => $this->input->post('pos_id'),
          'new_dep'                 => $this->input->post('new_dep'),
          'new_deps'                => $this->input->post('new_deps'),
          'new_position'            => $this->input->post('new_position'),
          'new_positions'           => $this->input->post('new_positions'),
          'level'                   => $this->input->post('level'),
          'basic_salary'            => $this->input->post('basic_salary'),
          'new_salary'              => $this->input->post('new_salary'),
          'last_increase'           => $this->input->post('last_increase'),
          'effective_date'          => $this->input->post('effective_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $change_status_id  =  $this->Change_status_model->add_change_status($fdata);
        if ($change_status_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $change_status_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Change Of Status #'.$change_status_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $change_status_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $change_status_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $change_status_id);
        }
      }
      $data['view'] = 'hr/change_status/change_status_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($change_status_id){
      $data['change_status']       = $this->Change_status_model->get_change_status($change_status_id);
      if (($data['change_status']['status'] == 2 || $data['change_status']['status'] == 1) && $data['change_status']['reback']) {
        $rrData = json_decode($data['change_status']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('change_status', array('status' => 3, 'role_id' => 0), "id = ".$change_status_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/change_status', $data['change_status'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $change_status_id, 0, 0, 0, 0, 0, 'Viewed Change Of Status #'.$change_status_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$change_status_id,'files');
      $this->data['form_id']       = $change_status_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hr/change_status/view_change_status';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($change_status_id){
      $data['change_status']             = $this->Change_status_model->get_change_status($change_status_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/change_status', $data['change_status'], $this->data['uhotels']);
      $this->load->view('hr/change_status/view_details',$data);
    }    

    public function signers_items($change_status_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $change_status_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $change_status_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($change_status_id=''){
      $data['change_status']   = $this->Change_status_model->get_change_status($change_status_id);
      edit_checker($data['change_status'], $this->data['permission']['edit'], 'hr/change_status/view/'.$change_status_id);
      $data['change_status_id']      = $change_status_id; 
      $data['hotels']         = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departmentz']    = $this->Departments_model->get_departments();
      $data['departments']    = $this->General_model->get_Data_all('api_departments', $data['change_status']['hid'], 'dep_code');
      $data['user_groups']    = $this->General_model->get_Data_all('api_user_groups', $data['change_status']['hid'], 'code');
      $data['gen_id']         = $change_status_id; 
      $data['uploads']        = $this->General_model->get_file_from_table($this->data['module']['id'],$change_status_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'clock_no'                => $this->input->post('clock_no'),
          'new_clock'               => $this->input->post('new_clock'),
          'name'                    => $this->input->post('name'),
          'hire_date'               => $this->input->post('hire_date'),
          'dep_id'                  => $this->input->post('dep_id'),
          'position'                => $this->input->post('position'),
          'pos_id'                  => $this->input->post('pos_id'),
          'new_dep'                 => $this->input->post('new_dep'),
          'new_deps'                => $this->input->post('new_deps'),
          'new_position'            => $this->input->post('new_position'),
          'new_positions'           => $this->input->post('new_positions'),
          'level'                   => $this->input->post('level'),
          'basic_salary'            => $this->input->post('basic_salary'),
          'new_salary'              => $this->input->post('new_salary'),
          'last_increase'           => $this->input->post('last_increase'),
          'effective_date'          => $this->input->post('effective_date'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Change_status_model->edit_change_status($fdata, $change_status_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $change_status_id, 0,json_encode($data['change_status'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Change Of Status #'.$change_status_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $change_status_id);
      } 
      $data['view'] = 'hr/change_status/change_status_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    // public function copy($copied='', $copy = FALSE){
    //   $data['change_status']   = $this->Change_status_model->get_change_status($copied);
    //   access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hr/change_status', $data['change_status'], $this->data['uhotels']);
    //   $data['copy']            = $copy;
    //   $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
    //   $data['departments']     = $this->Departments_model->get_departments();
    //   $data['user_groups']     = $this->User_groups_model->get_user_groups();
    //   $data['gen_id']          = get_file_code('files','change_status');
    //   $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
    //   if ($this->input->post('submit')) {
    //     $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
    //     $fdata =[
    //       'uid'                     => $this->data['user_id'],
    //       'hid'                     => $this->input->post('hid'),
    //       'dep_code'                => $this->input->post('dep_code'),
    //       'clock_no'                => $this->input->post('clock_no'),
    //       'new_clock'               => $this->input->post('new_clock'),
    //       'name'                    => $this->input->post('name'),
    //       'hire_date'               => $this->input->post('hire_date'),
    //       'dep_id'                  => $this->input->post('dep_id'),
    //       'position'                => $this->input->post('position'),
    //       'pos_id'                  => $this->input->post('pos_id'),
    //       'new_dep'                 => $this->input->post('new_dep'),
    //       'new_deps'                => $this->input->post('new_deps'),
    //       'new_position'            => $this->input->post('new_position'),
    //       'new_positions'           => $this->input->post('new_positions'),
    //       'level'                   => $this->input->post('level'),
    //       'basic_salary'            => $this->input->post('basic_salary'),
    //       'new_salary'              => $this->input->post('new_salary'),
    //       'last_increase'           => $this->input->post('last_increase'),
    //       'effective_date'          => $this->input->post('effective_date'),
    //       'remarks'                 => $this->input->post('remarks'),
    //       'status'                  => '1',
    //       'timestamp'               => date("Y-m-d H:i:s"),
    //     ];     
    //     $change_status_id  =  $this->Change_status_model->add_change_status($fdata);
    //     if ($change_status_id) {
    //       loger('Create', $this->data['module']['id'], $this->data['module']['name'], $change_status_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Change Of Status #'.$change_status_id.'');  
    //       $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $change_status_id, 'files');
    //       $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
    //       $this->Sign_model->addSignature($this->data['module']['id'], $change_status_id, $role_id, 0, $fdata['hid']);
    //       $this->signers($this->data['module']['id'], $change_status_id);
    //     }
    //   }
    //   $data['view'] = 'hr/change_status/change_status_process';
    //   $this->load->view('admin/includes/layout',$data);       
    // } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hr/change_status');
      $this->db->update('change_status', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Change Of Status #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>