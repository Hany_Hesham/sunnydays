<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Settlement extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('hr/Settlement_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(48);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'hr/settlement/index_settlement';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function settlement_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Settlement_model->get_settlements($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'settlement\',\'settlement\',\'hr/settlement\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('hr/settlement/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('hr/settlement/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Settlement Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['position_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['clock_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Settlement_model->get_all_settlements($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Settlement_model->get_settlements($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'hr/settlement');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','settlement');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'salary'                  => $this->input->post('salary'),
          'working_days'            => $this->input->post('working_days'),
          'balance'                 => $this->input->post('balance'),
          'remaining'               => $this->input->post('remaining'),
          'sick_days'               => $this->input->post('sick_days'),
          'penalty'                 => $this->input->post('penalty'),
          'leaves'                  => $this->input->post('leaves'),
          'unpaid'                  => $this->input->post('unpaid'),
          'others'                  => $this->input->post('others'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $settlement_id  =  $this->Settlement_model->add_settlement($fdata);
        if ($settlement_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $settlement_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Settlement Form #'.$settlement_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $settlement_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $settlement_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $settlement_id);
        }
      }
      $data['view'] = 'hr/settlement/settlement_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($settlement_id){
      $data['settlement']       = $this->Settlement_model->get_settlement($settlement_id);
      if (($data['settlement']['status'] == 2 || $data['settlement']['status'] == 1) && $data['settlement']['reback']) {
        $rrData = json_decode($data['settlement']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('settlement', array('status' => 3, 'role_id' => 0), "id = ".$settlement_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/settlement', $data['settlement'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $settlement_id, 0, 0, 0, 0, 0, 'Viewed Settlement Form #'.$settlement_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$settlement_id,'files');
      $this->data['form_id']       = $settlement_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'hr/settlement/view_settlement';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($settlement_id){
      $data['settlement']             = $this->Settlement_model->get_settlement($settlement_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'hr/settlement', $data['settlement'], $this->data['uhotels']);
      $this->load->view('hr/settlement/view_details',$data);
    }    

    public function signers_items($settlement_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $settlement_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $settlement_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($settlement_id=''){
      $data['settlement']   = $this->Settlement_model->get_settlement($settlement_id);
      edit_checker($data['settlement'], $this->data['permission']['edit'], 'hr/settlement/view/'.$settlement_id);
      $data['settlement_id']      = $settlement_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = $settlement_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$settlement_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'salary'                  => $this->input->post('salary'),
          'working_days'            => $this->input->post('working_days'),
          'balance'                 => $this->input->post('balance'),
          'remaining'               => $this->input->post('remaining'),
          'sick_days'               => $this->input->post('sick_days'),
          'penalty'                 => $this->input->post('penalty'),
          'leaves'                  => $this->input->post('leaves'),
          'unpaid'                  => $this->input->post('unpaid'),
          'others'                  => $this->input->post('others'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Settlement_model->edit_settlement($fdata, $settlement_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $settlement_id, 0,json_encode($data['settlement'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Settlement Form #'.$settlement_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $settlement_id);
      } 
      $data['view'] = 'hr/settlement/settlement_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['settlement']   = $this->Settlement_model->get_settlement($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'hr/settlement', $data['settlement'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','settlement');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'salary'                  => $this->input->post('salary'),
          'working_days'            => $this->input->post('working_days'),
          'balance'                 => $this->input->post('balance'),
          'remaining'               => $this->input->post('remaining'),
          'sick_days'               => $this->input->post('sick_days'),
          'penalty'                 => $this->input->post('penalty'),
          'leaves'                  => $this->input->post('leaves'),
          'unpaid'                  => $this->input->post('unpaid'),
          'others'                  => $this->input->post('others'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $settlement_id  =  $this->Settlement_model->add_settlement($fdata);
        if ($settlement_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $settlement_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Settlement Form #'.$settlement_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $settlement_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $settlement_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $settlement_id);
        }
      }
      $data['view'] = 'hr/settlement/settlement_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'hr/settlement');
      $this->db->update('settlement', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Settlement Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>