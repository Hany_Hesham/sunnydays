<?php defined('BASEPATH') OR exit('No direct script access allowed');
  class Cron extends CI_Controller{
    function __construct() {
      parent::__construct();
      $this->load->model('admin/Hotels_model');
      $this->load->model('admin/Sign_model');
      $this->load->helper('mail_helper');
    }

    public function test(){
      send_Message();
    }

    public function all(){
      $this->approved();
      $this->edit();
      $this->create();
      $this->reject();
    }

    public function signature(){
      $emails   = $this->Sign_model->get_all_mails();
      $this->mailers($emails);
    }

    public function approved(){
      $emails   = $this->Sign_model->get_all_mails('Approved');
      $this->mailers($emails);
    }

    public function edit(){
      $emails   = $this->Sign_model->get_all_mails('Edit');
      $this->mailers($emails);
    }

    public function create(){
      $emails   = $this->Sign_model->get_all_mails('Create');
      $this->mailers($emails);
    }

    public function reject(){
      $emails   = $this->Sign_model->get_all_mails('Reject');
      $this->mailers($emails);
    }

    // public function mailers($emails){
    //   foreach($emails as $email) {
    //     if ($email['emails']) {
    //       $sent = emailIt($email['module_id'], $email['form_id'], $email['emails'], $email['message']);
    //       if ($sent) {
    //         sleep(10);
    //         $this->Sign_model->updateMailData($email['id']);
    //       }
    //     }
    //   }
    // }

    public function mailers($emails){
      foreach($emails as $email) {
        if ($email['emails']) {
          $sended = $this->Sign_model->sentChecker($email['module_id'], $email['form_id'], $email['emails']);
          if ($sended == 0 ) {
            $sent = emailIt($email['module_id'], $email['form_id'], $email['emails'], $email['message']);
            if ($sent) {
              $update= $this->Sign_model->updateMailData($email['id']);
              sleep(10);
            }
          }else{
            $update= $this->Sign_model->updateMailData($email['id']);
          }
        }else{
          $update= $this->Sign_model->updateMailData($email['id']);
        }
      }
    }

    public function backup_me(){
        $this->load->helper('file');
        $this->load->dbutil();
        $prefs = array(     
            'format'      => 'zip',             
            'filename'    => 'my_db_backup.sql'
            );
        $backup =& $this->dbutil->backup($prefs); 
        $db_name = 'my_db_backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        write_file(FCPATH.'backups/'.$db_name, $backup); 
        $this->load->helper('download');
        force_download($db_name, $backup);
      } 
   
  }

?>