<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Evaluation extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Evaluation_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(33);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/evaluation/index_evaluation';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function evaluation_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Evaluation_model->get_evaluations($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'evaluation\',\'evaluation\',\'general/evaluation\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/evaluation/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/evaluation/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Performance Evaluation #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['evaluation_type'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['position_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Evaluation_model->get_all_evaluations($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Evaluation_model->get_evaluations($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/evaluation');
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['user_groups']      = $this->User_groups_model->get_user_groups();
      $data['types']            = $this->General_model->get_meta_data('evaluation_type');
      $data['recommendations']  = $this->General_model->get_meta_data('career_recommendations');
      $data['evaluation_areas'] = $this->General_model->get_meta_data('evaluation_areas');
      $data['evaluations']      = $this->General_model->get_meta_data('evaluation');
      $data['gen_id']           = get_file_code('files','evaluation');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type'                    => $this->input->post('type'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'section'                 => $this->input->post('section'),
          'name'                    => $this->input->post('name'),
          'hire_date'               => $this->input->post('hire_date'),
          'promotion_date'          => $this->input->post('promotion_date'),
          'age'                     => $this->input->post('age'),
          'education'               => $this->input->post('education'),
          'best_performance'        => $this->input->post('best_performance'),
          'improvement'             => $this->input->post('improvement'),
          'training'                => $this->input->post('training'),
          'recommendation'          => $this->input->post('recommendation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $evaluation_id  =  $this->Evaluation_model->add_evaluation($fdata);
        if ($evaluation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Performance Evaluation #'.$evaluation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $evaluation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['evaluation_id']    = $evaluation_id;
            $item_name                = metaName($item['areaid']);
            $item_id                  = $this->Evaluation_model->add_evaluation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          foreach ($this->input->post('itemz') as $key => $item) {
            $item['evaluation_id']    = $evaluation_id;
            $item_id                  = $this->Evaluation_model->add_evaluation_itemd($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          if($fdata['type'] == 6403){
            $this->Sign_model->addSignature($this->data['module']['id'], $evaluation_id, $role_id, 0, $fdata['hid']);
          }
          $this->signers($this->data['module']['id'], $evaluation_id);
        }
      }
      $data['view'] = 'general/evaluation/evaluation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($evaluation_id){
      $data['evaluation']          = $this->Evaluation_model->get_evaluation($evaluation_id);
      if (($data['evaluation']['status'] == 2 || $data['evaluation']['status'] == 1) && $data['evaluation']['reback']) {
        $rrData = json_decode($data['evaluation']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('evaluation', array('status' => 3, 'role_id' => 0), "id = ".$evaluation_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/evaluation', $data['evaluation'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, 0, 0, 0, 0, 0, 'Viewed Performance Evaluation #'.$evaluation_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$evaluation_id,'files');
      $this->data['form_id']       = $evaluation_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/evaluation/view_evaluation';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($evaluation_id){
      $data['evaluation']             = $this->Evaluation_model->get_evaluation($evaluation_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/evaluation', $data['evaluation'], $this->data['uhotels']);
      $data['evaluation_items']          = $this->Evaluation_model->get_evaluation_items($evaluation_id);
      $data['evaluation_itemz']          = $this->Evaluation_model->get_evaluation_itemsd($evaluation_id);
      $this->load->view('general/evaluation/view_details',$data);
    }  

    public function signers_items($evaluation_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $evaluation_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $evaluation_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($evaluation_id=''){
      $data['evaluation']       = $this->Evaluation_model->get_evaluation($evaluation_id);
      edit_checker($data['evaluation'], $this->data['permission']['edit'], 'general/evaluation/view/'.$evaluation_id);
      $data['evaluation_items'] = $this->Evaluation_model->get_evaluation_items($evaluation_id);
      $data['evaluation_itemz'] = $this->Evaluation_model->get_evaluation_itemsd($evaluation_id);
      $data['evaluation_id']    = $evaluation_id; 
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['user_groups']      = $this->User_groups_model->get_user_groups();
      $data['types']            = $this->General_model->get_meta_data('evaluation_type');
      $data['recommendations']  = $this->General_model->get_meta_data('career_recommendations');
      $data['evaluation_areas'] = $this->General_model->get_meta_data('evaluation_areas');
      $data['evaluations']      = $this->General_model->get_meta_data('evaluation');
      $data['gen_id']           = $evaluation_id; 
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$evaluation_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'type'                    => $this->input->post('type'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'section'                 => $this->input->post('section'),
          'name'                    => $this->input->post('name'),
          'hire_date'               => $this->input->post('hire_date'),
          'promotion_date'          => $this->input->post('promotion_date'),
          'age'                     => $this->input->post('age'),
          'education'               => $this->input->post('education'),
          'best_performance'        => $this->input->post('best_performance'),
          'improvement'             => $this->input->post('improvement'),
          'training'                => $this->input->post('training'),
          'recommendation'          => $this->input->post('recommendation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Evaluation_model->edit_evaluation($fdata, $evaluation_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, 0,json_encode($data['evaluation'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Performance Evaluation #'.$evaluation_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Evaluation_model->get_evaluation_item($item['id']);
            $item['evaluation_id']  = $evaluation_id;
            $item_name              = metaName($item['areaid']);
            $updated                = $this->Evaluation_model->edit_evaluation_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item_name);
            }
          }else{
            $item['evaluation_id']  = $evaluation_id;
            $item_name              = metaName($item['areaid']);
            $item_id                = $this->Evaluation_model->add_evaluation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item_name.'');
            }
          }
        }
        foreach ($this->input->post('itemz') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Evaluation_model->get_evaluation_itemd($item['id']);
            $item['evaluation_id']  = $evaluation_id;
            $updated                = $this->Evaluation_model->edit_evaluation_itemd($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['name']);
            }
          }else{
            $item['evaluation_id']  = $evaluation_id;
            $item_id                = $this->Evaluation_model->add_evaluation_itemd($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['name'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $evaluation_id);
      } 
      $data['view'] = 'general/evaluation/evaluation_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['evaluation']       = $this->Evaluation_model->get_evaluation($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/evaluation', $data['evaluation'], $this->data['uhotels']);
      $data['evaluation_items'] = $this->Evaluation_model->get_evaluation_items($copied);
      $data['evaluation_itemz'] = $this->Evaluation_model->get_evaluation_itemsd($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['user_groups']      = $this->User_groups_model->get_user_groups();
      $data['types']            = $this->General_model->get_meta_data('evaluation_type');
      $data['recommendations']  = $this->General_model->get_meta_data('career_recommendations');
      $data['evaluation_areas'] = $this->General_model->get_meta_data('evaluation_areas');
      $data['evaluations']      = $this->General_model->get_meta_data('evaluation');
      $data['gen_id']           = get_file_code('files','evaluation');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'type'                    => $this->input->post('type'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'section'                 => $this->input->post('section'),
          'name'                    => $this->input->post('name'),
          'hire_date'               => $this->input->post('hire_date'),
          'promotion_date'          => $this->input->post('promotion_date'),
          'age'                     => $this->input->post('age'),
          'education'               => $this->input->post('education'),
          'best_performance'        => $this->input->post('best_performance'),
          'improvement'             => $this->input->post('improvement'),
          'training'                => $this->input->post('training'),
          'recommendation'          => $this->input->post('recommendation'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $evaluation_id  =  $this->Evaluation_model->add_evaluation($fdata);
        if ($evaluation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Performance Evaluation #'.$evaluation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $evaluation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['evaluation_id']    = $evaluation_id;
            unset($item['id']);
            $item_name                = metaName($item['areaid']);
            $item_id                  = $this->Evaluation_model->add_evaluation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item_name.'');
            }
          }
          foreach ($this->input->post('itemz') as $key => $item) {
            $item['evaluation_id']    = $evaluation_id;
            unset($item['id']);
            $item_id                  = $this->Evaluation_model->add_evaluation_itemd($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $evaluation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          if($fdata['type'] == 6403){
            $this->Sign_model->addSignature($this->data['module']['id'], $evaluation_id, $role_id, 0, $fdata['hid']);
          }
          $this->signers($this->data['module']['id'], $evaluation_id);
        }
      }
      $data['view'] = 'general/evaluation/evaluation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Evaluation_model->get_evaluation_item($id);
        $this->db->update('evaluation_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['evaluation_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['area_name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function delete_itemsd(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Evaluation_model->get_evaluation_itemd($id);
        $this->db->update('evaluation_itemd', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['evaluation_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/evaluation');
      $this->db->update('evaluation', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Performance Evaluation #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>