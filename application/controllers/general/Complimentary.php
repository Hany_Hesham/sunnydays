<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Complimentary extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Complimentary_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(12);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/complimentary/index_complimentary';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function complimentary_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Complimentary_model->get_complimentarys($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'complimentary\',\'complimentary\',\'general/complimentary\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/complimentary/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/complimentary/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Complimentary Form #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';    
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['conf_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['arrival'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['departure'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Complimentary_model->get_all_complimentarys($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Complimentary_model->get_complimentarys($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/complimentary');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','complimentary');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'conf_no'                 => $this->input->post('conf_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'company'                 => $this->input->post('company'),
          'room_type'               => $this->input->post('room_type'),
          'reasons'                 => $this->input->post('reasons'),
          'payment'                 => $this->input->post('payment'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $complimentary_id  =  $this->Complimentary_model->add_complimentary($fdata);
        if ($complimentary_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $complimentary_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Complimentary Form #'.$complimentary_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $complimentary_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $complimentary_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $complimentary_id);
        }
      }
      $data['view'] = 'general/complimentary/complimentary_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($complimentary_id){
      $data['complimentary']       = $this->Complimentary_model->get_complimentary($complimentary_id);
      if (($data['complimentary']['status'] == 2 || $data['complimentary']['status'] == 1) && $data['complimentary']['reback']) {
        $rrData = json_decode($data['complimentary']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('complimentary', array('status' => 3, 'role_id' => 0), "id = ".$complimentary_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/complimentary', $data['complimentary'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $complimentary_id, 0, 0, 0, 0, 0, 'Viewed Complimentary Form #'.$complimentary_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$complimentary_id,'files');
      $this->data['form_id']       = $complimentary_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/complimentary/view_complimentary';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($complimentary_id){
      $data['complimentary']             = $this->Complimentary_model->get_complimentary($complimentary_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/complimentary', $data['complimentary'], $this->data['uhotels']);
      $this->load->view('general/complimentary/view_details',$data);
    }    

    public function signers_items($complimentary_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $complimentary_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $complimentary_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($complimentary_id=''){
      $data['complimentary']   = $this->Complimentary_model->get_complimentary($complimentary_id);
      edit_checker($data['complimentary'], $this->data['permission']['edit'], 'general/complimentary/view/'.$complimentary_id);
      $data['complimentary_id']      = $complimentary_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $complimentary_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$complimentary_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'conf_no'                 => $this->input->post('conf_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'company'                 => $this->input->post('company'),
          'room_type'               => $this->input->post('room_type'),
          'reasons'                 => $this->input->post('reasons'),
          'payment'                 => $this->input->post('payment'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Complimentary_model->edit_complimentary($fdata, $complimentary_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $complimentary_id, 0,json_encode($data['complimentary'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Complimentary Form #'.$complimentary_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $complimentary_id);
      } 
      $data['view'] = 'general/complimentary/complimentary_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['complimentary']   = $this->Complimentary_model->get_complimentary($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/complimentary', $data['complimentary'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','complimentary');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'conf_no'                 => $this->input->post('conf_no'),
          'name'                    => $this->input->post('name'),
          'arrival'                 => $this->input->post('arrival'),
          'departure'               => $this->input->post('departure'),
          'company'                 => $this->input->post('company'),
          'room_type'               => $this->input->post('room_type'),
          'reasons'                 => $this->input->post('reasons'),
          'payment'                 => $this->input->post('payment'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $complimentary_id  =  $this->Complimentary_model->add_complimentary($fdata);
        if ($complimentary_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $complimentary_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Complimentary Form #'.$complimentary_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $complimentary_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $complimentary_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $complimentary_id);
        }
      }
      $data['view'] = 'general/complimentary/complimentary_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/complimentary');
      $this->db->update('complimentary', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Complimentary Form #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>