<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Assets_transfer extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Assets_transfer_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(52);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/assets_transfer/index_assets_transfer';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function assets_transfer_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Assets_transfer_model->get_assets_transfers($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'assets_transfer\',\'assets_transfer\',\'general/assets_transfer\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/assets_transfer/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/assets_transfer/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Assets Transfer Request #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['from_hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['to_hotel_name'].'</span>'; 
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['type_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['transfer_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['reason'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Assets_transfer_model->get_all_assets_transfers($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Assets_transfer_model->get_assets_transfers($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      from_to_access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/assets_transfer');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['types']           = $this->General_model->get_meta_data('assets_transfer_types');
      $data['gen_id']          = get_file_code('files','assets_transfer');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $from_role_id   = get_roleHotel_byid('departments',$this->input->post('from_dep_code'));
        $to_role_id     = get_roleHotel_byid('departments',$this->input->post('to_dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code'           => $this->input->post('from_dep_code'),
          'to_dep_code'             => $this->input->post('to_dep_code'),
          'type_id'                 => $this->input->post('type_id'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $assets_transfer_id  =  $this->Assets_transfer_model->add_assets_transfer($fdata);
        if ($assets_transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Assets Transfer Request #'.$assets_transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $assets_transfer_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['assets_transfer_id']     = $assets_transfer_id;
            $item_id                        = $this->Assets_transfer_model->add_assets_transfer_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $assets_transfer_id, $from_role_id, 1, $fdata['from_hid'], 1);
          $this->signers($this->data['module']['id'], $assets_transfer_id, 1, 1);
          $this->Sign_model->addSignature($this->data['module']['id'], $assets_transfer_id, $to_role_id, 0, $fdata['to_hid'], 2);
          $this->signers($this->data['module']['id'], $assets_transfer_id, 2, 1);
          $this->db->update('assets_transfer', array('stage' => 'First'), "id = ".$assets_transfer_id);
          $this->stage($this->data['module']['id'], $assets_transfer_id, 1, 1);
        }
      }
      $data['view'] = 'general/assets_transfer/assets_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($assets_transfer_id){
      $this->update_stage($assets_transfer_id);
      $data['assets_transfer']       = $this->Assets_transfer_model->get_assets_transfer($assets_transfer_id);
      if (($data['assets_transfer']['status'] == 2 || $data['assets_transfer']['status'] == 1) && $data['assets_transfer']['reback']) {
        $rrData = json_decode($data['assets_transfer']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('assets_transfer', array('status' => 3, 'role_id' => 0), "id = ".$assets_transfer_id);
        }
      }
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/assets_transfer', $data['assets_transfer'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, 0, 0, 0, 0, 0, 'Viewed Assets Transfer Request #'.$assets_transfer_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$assets_transfer_id,'files');
      $this->data['form_id']       = $assets_transfer_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/assets_transfer/view_assets_transfer';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($assets_transfer_id){
      $data['assets_transfer']              = $this->Assets_transfer_model->get_assets_transfer($assets_transfer_id);
      from_to_access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/assets_transfer', $data['assets_transfer'], $this->data['uhotels']);
      $data['assets_transfer_items']        = $this->Assets_transfer_model->get_assets_transfer_items($assets_transfer_id);
      $this->load->view('general/assets_transfer/view_details',$data);
    }    

    public function update_stage($assets_transfer_id){
        $First    = getSigners($this->data['module']['id'],$assets_transfer_id, 1);
        $Second   = getSigners($this->data['module']['id'],$assets_transfer_id, 2);
        $cunt  = 0;
        $cunts = 0;
        foreach ($First as $Firstd) {
          if (isset($Firstd['queue'])) {
            $cunt++;
          }
        }
        foreach ($Second as $Secondd) {
          if (isset($Secondd['queue'])) {
            $cunts++;
          }
        }
        if ($cunt > 0) {
          $this->Sign_model->updateStage($this->data['module']['tdatabase'], $assets_transfer_id, 1);
        }elseif ($cunts > 0) {
          $this->Sign_model->updateStage($this->data['module']['tdatabase'], $assets_transfer_id, 2);
        }
    }

    public function signers_items($type, $assets_transfer_id, $stage = FALSE, $status = FALSE){
      $data['signatures']       = getSigners($this->data['module']['id'], $assets_transfer_id, $type);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $assets_transfer_id;
      if (($stage == 'First' && $type == 1) || ($stage == 'Second' && $type == 2) || ($stage == 'First' && $type == 2 && $status == 2)) {
        $data['doned']          = 0;      
      }else{
        $data['doned']          = 1;              
      }    
      $this->load->view('admin/html_parts/signers',$data);
    }

    public function edit($assets_transfer_id=''){
      $data['assets_transfer']          = $this->Assets_transfer_model->get_assets_transfer($assets_transfer_id);
      edit_checker($data['assets_transfer'], $this->data['permission']['edit'], 'general/assets_transfer/view/'.$assets_transfer_id);
      $data['assets_transfer_id']       = $assets_transfer_id; 
      $data['hotels']                   = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']              = $this->Departments_model->get_departments();
      $data['types']                    = $this->General_model->get_meta_data('assets_transfer_types');
      $data['assets_transfer_items']    = $this->Assets_transfer_model->get_assets_transfer_items($assets_transfer_id);
      $data['gen_id']                   = $assets_transfer_id; 
      $data['uploads']                  = $this->General_model->get_file_from_table($this->data['module']['id'],$assets_transfer_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code'           => $this->input->post('from_dep_code'),
          'to_dep_code'             => $this->input->post('to_dep_code'),
          'type_id'                 => $this->input->post('type_id'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Assets_transfer_model->edit_assets_transfer($fdata, $assets_transfer_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, 0,json_encode($data['assets_transfer'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Assets Transfer Request #'.$assets_transfer_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data                    = $this->Assets_transfer_model->get_assets_transfer_item($item['id']);
            $item['assets_transfer_id']   = $assets_transfer_id;
            $updated                      = $this->Assets_transfer_model->edit_assets_transfer_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['name']);
            }
          }else{
            $item['assets_transfer_id']   = $assets_transfer_id;
            $item_id                      = $this->Assets_transfer_model->add_assets_transfer_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['name'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $assets_transfer_id, 1, 1);
        $this->signers($this->data['module']['id'], $assets_transfer_id, 2);
        redirect('general/assets_transfer/view/'.$assets_transfer_id);
      } 
      $data['view'] = 'general/assets_transfer/assets_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['assets_transfer']            = $this->Assets_transfer_model->get_assets_transfer($copied);
      from_to_access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/assets_transfer', $data['assets_transfer'], $this->data['uhotels']);
      $data['copy']                       = $copy;
      $data['hotels']                     = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']                = $this->Departments_model->get_departments();
      $data['types']                      = $this->General_model->get_meta_data('assets_transfer_types');
      $data['assets_transfer_items']      = $this->Assets_transfer_model->get_assets_transfer_items($copied);
      $data['gen_id']                     = get_file_code('files','assets_transfer');
      $data['uploads']                    = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $from_role_id = get_roleHotel_byid('departments',$this->input->post('from_dep_code'));
        $to_role_id = get_roleHotel_byid('departments',$this->input->post('to_dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'from_hid'                => $this->input->post('from_hid'),
          'to_hid'                  => $this->input->post('to_hid'),
          'from_dep_code'           => $this->input->post('from_dep_code'),
          'to_dep_code'             => $this->input->post('to_dep_code'),
          'type_id'                 => $this->input->post('type_id'),
          'transfer_date'           => $this->input->post('transfer_date'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $assets_transfer_id  =  $this->Assets_transfer_model->add_assets_transfer($fdata);
        if ($assets_transfer_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Assets Transfer Request #'.$assets_transfer_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $assets_transfer_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['assets_transfer_id']     = $assets_transfer_id;
            $item_id                        = $this->Assets_transfer_model->add_assets_transfer_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $assets_transfer_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $assets_transfer_id, $from_role_id, 1, $fdata['from_hid'], 1);
          $this->signers($this->data['module']['id'], $assets_transfer_id, 1, 1);
          $this->Sign_model->addSignature($this->data['module']['id'], $assets_transfer_id, $to_role_id, 0, $fdata['to_hid'], 2);
          $this->signers($this->data['module']['id'], $assets_transfer_id, 2, 1);
          $this->db->update('assets_transfer', array('stage' => 'First'), "id = ".$assets_transfer_id);
          $this->stage($this->data['module']['id'], $assets_transfer_id, 1, 1);
        }
      }
      $data['view'] = 'general/assets_transfer/assets_transfer_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function delete_items(){
      if ($this->input->post('id')) {
        $id     = $this->input->post('id');
        $item   = $this->Assets_transfer_model->get_assets_transfer_item($id);
        $this->db->update('assets_transfer_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['assets_transfer_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      from_to_access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/assets_transfer');
      $this->db->update('assets_transfer', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Assets Transfer Request #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>