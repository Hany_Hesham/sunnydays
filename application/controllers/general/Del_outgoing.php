<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Del_outgoing extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Outgoing_model');
      $this->load->model('general/Del_outgoing_model');
      $this->data['module']         = $this->Modules_model->get_module_by(45);
      $this->data['permission']     = user_access($this->data['module']['id']);
      $this->data['out_permission'] = user_access(29);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/del_outgoing/index_del_outgoing';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function del_outgoing_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Del_outgoing_model->get_del_outgoings($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/del_outgoing/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/del_outgoing/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Outgoing Delivery Record #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Del_outgoing_model->get_all_del_outgoings($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Del_outgoing_model->get_del_outgoings($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add($outgoing_id){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/del_outgoing');
      $data['outgoing']        = $this->Outgoing_model->get_outgoing($outgoing_id);
      $data['gen_id']          = get_file_code('files','del_outgoing');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->get('mids')) {
        $data['del_outgoing_items']    = $this->Del_outgoing_model->get_outgoing_items($this->input->get('mids'));
      }
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$data['outgoing']['dep_code']);
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $data['outgoing']['hid'],
          'outgoing_id'             => $data['outgoing']['id'],
          'dep_code'                => $data['outgoing']['dep_code'],
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $del_outgoing_id  =  $this->Del_outgoing_model->add_del_outgoing($fdata);
        if ($del_outgoing_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $del_outgoing_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Outgoing Delivery Record #'.$del_outgoing_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $del_outgoing_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['del_outgoing_id']    = $del_outgoing_id;
            $item['outgoing_id']        = $outgoing_id;
            $outgoing_item              = $this->Outgoing_model->get_outgoing_item($item['item_id']);
            $item_qyt                   = $this->Del_outgoing_model->get_item_qty($outgoing_id,$item['item_id']);
            if (((float)$outgoing_item['quantity']-((float)$item['recieved']+(float)$item_qyt['total_recieved'])) >= 0) {
              $this->Del_outgoing_model->update_items_recieved($outgoing_id,$item['item_id'],array('recieved'=>((float)$item['recieved']+(float)$item_qyt['total_recieved'])));
              $item_id = $this->Del_outgoing_model->add_del_outgoing_item($item);
              if (((float)$outgoing_item['quantity']-((float)$item['recieved']+(float)$item_qyt['total_recieved'])) == 0) {
                $this->Del_outgoing_model->update_items_delivered($outgoing_id,$item['item_id'],1);
              }elseif (((float)$outgoing_item['quantity']-((float)$item['recieved']+(float)$item_qyt['total_recieved'])) > 0) {
                $this->Del_outgoing_model->update_items_delivered($outgoing_id,$item['item_id'],0);
              }
            }
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $del_outgoing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$outgoing_item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $del_outgoing_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $del_outgoing_id);
        }
      }
      $data['view'] = 'general/del_outgoing/del_outgoing_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($del_outgoing_id){
      $data['del_outgoing']          = $this->Del_outgoing_model->get_del_outgoing($del_outgoing_id);
      if (($data['del_outgoing']['status'] == 2 || $data['del_outgoing']['status'] == 1) && $data['del_outgoing']['reback']) {
        $rrData = json_decode($data['del_outgoing']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('del_outgoing', array('status' => 3, 'role_id' => 0), "id = ".$del_outgoing_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/del_outgoing', $data['del_outgoing'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $del_outgoing_id, 0, 0, 0, 0, 0, 'Viewed Outgoing Delivery Record #'.$del_outgoing_id.''); 
      $data['del_outgoing_items']       = $this->Del_outgoing_model->get_del_outgoing_items($del_outgoing_id);
      $outgoing_id                      = $data['del_outgoing']['outgoing_id'];
      if (count($data['del_outgoing_items']) == 0) {
        $this->db->update('del_outgoing', array('deleted' => 1), "id = ".$del_outgoing_id);
        redirect('general/outgoing/view/'.$outgoing_id);
      }
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$del_outgoing_id,'files');
      $this->data['form_id']       = $del_outgoing_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/del_outgoing/view_del_outgoing';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($del_outgoing_id){
      $data['del_outgoing']             = $this->Del_outgoing_model->get_del_outgoing($del_outgoing_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/del_outgoing', $data['del_outgoing'], $this->data['uhotels']);
      $data['del_outgoing_items']       = $this->Del_outgoing_model->get_del_outgoing_items($del_outgoing_id);
      $this->load->view('general/del_outgoing/view_details',$data);
    }  

    public function signers_items($del_outgoing_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $del_outgoing_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $del_outgoing_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($del_outgoing_id=''){
      $data['del_outgoing']         = $this->Del_outgoing_model->get_del_outgoing($del_outgoing_id);
      edit_checker($data['del_outgoing'], $this->data['permission']['edit'], 'general/del_outgoing/view/'.$del_outgoing_id);
      $data['del_outgoing_items']   = $this->Del_outgoing_model->get_del_outgoing_items($del_outgoing_id);
      $data['del_outgoing_id']      = $del_outgoing_id; 
      $data['gen_id']               = $del_outgoing_id; 
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$del_outgoing_id,'files');
      if ($this->input->post('submit')) {
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Del_outgoing_model->get_del_outgoing_item($item['id']);
            $outgoing_item          = $this->Outgoing_model->get_outgoing_item($item['item_id']);
            $item_qyt               = $this->Del_outgoing_model->get_item_qty($item_data['outgoing_id'],$item['item_id']);
            $differ                 = ((float)$item['recieved'] - (float)$item_data['recieved']);
            if (((float)$outgoing_item['quantity']-((float)$differ+(float)$item_qyt['total_recieved'])) >= 0) {
              $this->Del_outgoing_model->update_items_recieved($item_data['outgoing_id'],$item['item_id'],array('recieved'=>((float)$differ+(float)$item_qyt['total_recieved'])));
              $updated              = $this->Del_outgoing_model->edit_del_outgoing_item($item,$item['id']);
              if (((float)$outgoing_item['quantity']-((float)$differ+(float)$item_qyt['total_recieved'])) == 0) {
                $this->Del_outgoing_model->update_items_delivered($item_data['outgoing_id'],$item['item_id'], 1);
              }elseif (((float)$outgoing_item['quantity']-((float)$differ+(float)$item_qyt['total_recieved'])) > 0) {
                $this->Del_outgoing_model->update_items_delivered($item_data['outgoing_id'],$item['item_id'], 0);
                if ($item['recieved'] == 0) {
                  $this->db->update('del_outgoing_item', array('deleted' => 1), "id = ".$item['id']);
                }
              }
            }
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $del_outgoing_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['description']);
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $del_outgoing_id);
      } 
      $data['view'] = 'general/del_outgoing/del_outgoing_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

  }

?>