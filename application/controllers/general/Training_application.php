<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Training_application extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Training_application_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(10);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/training_application/index_training_application';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function training_application_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Training_application_model->get_training_applications($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'training_application\',\'training_application\',\'general/training_application\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/training_application/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/training_application/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Application For Training #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Training_application_model->get_all_training_applications($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Training_application_model->get_training_applications($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/training_application');
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['genders']          = $this->General_model->get_meta_data('gender');
      $data['marital_statuses'] = $this->General_model->get_meta_data('marital_status');
      $data['gen_id']           = get_file_code('files','training_application');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'             => $this->data['user_id'],
          'hid'             => $this->input->post('hid'),
          'dep_code'        => $this->input->post('dep_code'),
          'name'            => $this->input->post('name'),
          'gender'          => $this->input->post('gender'),
          'birth_date'      => $this->input->post('birth_date'),
          'birth_location'  => $this->input->post('birth_location'),
          'nationality'     => $this->input->post('nationality'),
          'religion'        => $this->input->post('religion'),
          'card_id'         => $this->input->post('card_id'),
          'military'        => $this->input->post('military'),
          'marital'         => $this->input->post('marital'),
          'address'         => $this->input->post('address'),
          'telephone'       => $this->input->post('telephone'),
          'address2'        => $this->input->post('address2'),
          'starting_date'   => $this->input->post('starting_date'),
          'period'          => $this->input->post('period'),
          'institute'       => $this->input->post('institute'),
          'reward'          => $this->input->post('reward'),
          'remarks'         => $this->input->post('remarks'),
          'status'          => '1',
          'timestamp'       => date("Y-m-d H:i:s"),
        ];      
        $training_application_id  =  $this->Training_application_model->add_training_application($fdata);
        if ($training_application_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $training_application_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Application For Training #'.$training_application_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $training_application_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $training_application_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $training_application_id);
        }
      }
      $data['view'] = 'general/training_application/training_application_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($training_application_id){
      $data['training_application'] = $this->Training_application_model->get_training_application($training_application_id);
      if (($data['training_application']['status'] == 2 || $data['training_application']['status'] == 1) && $data['training_application']['reback']) {
        $rrData = json_decode($data['training_application']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('training_application', array('status' => 3, 'role_id' => 0), "id = ".$training_application_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/training_application', $data['training_application'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $training_application_id, 0, 0, 0, 0, 0, 'Viewed Application For Training #'.$training_application_id.''); 
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$training_application_id,'files');
      $this->data['form_id']        = $training_application_id;
      $data['messaged']             = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount']  = count($data['messaged']);
      $data['view']                 = 'general/training_application/view_training_application';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($training_application_id){
      $data['training_application']             = $this->Training_application_model->get_training_application($training_application_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/training_application', $data['training_application'], $this->data['uhotels']);
      $this->load->view('general/training_application/view_details',$data);
    }    

    public function signers_items($training_application_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $training_application_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $training_application_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($training_application_id=''){
      $data['training_application']   = $this->Training_application_model->get_training_application($training_application_id);
      edit_checker($data['training_application'], $this->data['permission']['edit'], 'general/training_application/view/'.$training_application_id);
      $data['training_application_id']      = $training_application_id; 
      $data['hotels']                       = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']                  = $this->Departments_model->get_departments();
      $data['genders']                      = $this->General_model->get_meta_data('gender');
      $data['marital_statuses']             = $this->General_model->get_meta_data('marital_status');
      $data['gen_id']                       = $training_application_id; 
      $data['uploads']                      = $this->General_model->get_file_from_table($this->data['module']['id'],$training_application_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'             => $this->input->post('hid'),
          'dep_code'        => $this->input->post('dep_code'),
          'name'            => $this->input->post('name'),
          'gender'          => $this->input->post('gender'),
          'birth_date'      => $this->input->post('birth_date'),
          'birth_location'  => $this->input->post('birth_location'),
          'nationality'     => $this->input->post('nationality'),
          'religion'        => $this->input->post('religion'),
          'card_id'         => $this->input->post('card_id'),
          'military'        => $this->input->post('military'),
          'marital'         => $this->input->post('marital'),
          'address'         => $this->input->post('address'),
          'telephone'       => $this->input->post('telephone'),
          'address2'        => $this->input->post('address2'),
          'starting_date'   => $this->input->post('starting_date'),
          'period'          => $this->input->post('period'),
          'institute'       => $this->input->post('institute'),
          'reward'          => $this->input->post('reward'),
          'remarks'         => $this->input->post('remarks'),
          'status'          => '1',
        ];    
        $this->Training_application_model->edit_training_application($fdata, $training_application_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $training_application_id, 0,json_encode($data['training_application'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Application For Training #'.$training_application_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $training_application_id);
      } 
      $data['view'] = 'general/training_application/training_application_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['training_application']   = $this->Training_application_model->get_training_application($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/training_application', $data['training_application'], $this->data['uhotels']);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['genders']          = $this->General_model->get_meta_data('gender');
      $data['marital_statuses'] = $this->General_model->get_meta_data('marital_status');
      $data['gen_id']           = get_file_code('files','training_application');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'             => $this->data['user_id'],
          'hid'             => $this->input->post('hid'),
          'dep_code'        => $this->input->post('dep_code'),
          'name'            => $this->input->post('name'),
          'gender'          => $this->input->post('gender'),
          'birth_date'      => $this->input->post('birth_date'),
          'birth_location'  => $this->input->post('birth_location'),
          'nationality'     => $this->input->post('nationality'),
          'religion'        => $this->input->post('religion'),
          'card_id'         => $this->input->post('card_id'),
          'military'        => $this->input->post('military'),
          'marital'         => $this->input->post('marital'),
          'address'         => $this->input->post('address'),
          'telephone'       => $this->input->post('telephone'),
          'address2'        => $this->input->post('address2'),
          'starting_date'   => $this->input->post('starting_date'),
          'period'          => $this->input->post('period'),
          'institute'       => $this->input->post('institute'),
          'reward'          => $this->input->post('reward'),
          'remarks'         => $this->input->post('remarks'),
          'status'          => '1',
          'timestamp'       => date("Y-m-d H:i:s"),
        ];     
        $training_application_id  =  $this->Training_application_model->add_training_application($fdata);
        if ($training_application_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $training_application_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Application For Training #'.$training_application_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $training_application_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $training_application_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $training_application_id);
        }
      }
      $data['view'] = 'general/training_application/training_application_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/training_application');
      $this->db->update('training_application', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Application For Training #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>