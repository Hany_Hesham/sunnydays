<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Resignation extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Resignation_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(53);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/resignation/index_resignation';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function resignation_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Resignation_model->get_resignations($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'resignation\',\'resignation\',\'general/resignation\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/resignation/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/resignation/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Resignation Request #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['position_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['clock_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['last_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Resignation_model->get_all_resignations($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Resignation_model->get_resignations($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/resignation');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['gen_id']          = get_file_code('files','resignation');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'opinion'                 => $this->input->post('opinion'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $resignation_id  =  $this->Resignation_model->add_resignation($fdata);
        if ($resignation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $resignation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Resignation Request #'.$resignation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $resignation_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $resignation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $resignation_id);
        }
      }
      $data['view'] = 'general/resignation/resignation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($resignation_id){
      $data['resignation']       = $this->Resignation_model->get_resignation($resignation_id);
      if (($data['resignation']['status'] == 2 || $data['resignation']['status'] == 1) && $data['resignation']['reback']) {
        $rrData = json_decode($data['resignation']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('resignation', array('status' => 3, 'role_id' => 0), "id = ".$resignation_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/resignation', $data['resignation'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $resignation_id, 0, 0, 0, 0, 0, 'Viewed Resignation Request #'.$resignation_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$resignation_id,'files');
      $this->data['form_id']       = $resignation_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/resignation/view_resignation';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($resignation_id){
      $data['resignation']             = $this->Resignation_model->get_resignation($resignation_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/resignation', $data['resignation'], $this->data['uhotels']);
      $this->load->view('general/resignation/view_details',$data);
    }    

    public function signers_items($resignation_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $resignation_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $resignation_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($resignation_id=''){
      $data['resignation']   = $this->Resignation_model->get_resignation($resignation_id);
      edit_checker($data['resignation'], $this->data['permission']['edit'], 'general/resignation/view/'.$resignation_id);
      $data['resignation_id']       = $resignation_id; 
      $data['hotels']               = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']          = $this->Departments_model->get_departments();
      $data['user_groups']          = $this->User_groups_model->get_user_groups();
      $data['types']                = $this->General_model->get_meta_data('resignation_type');
      $data['gen_id']               = $resignation_id; 
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$resignation_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'opinion'                 => $this->input->post('opinion'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Resignation_model->edit_resignation($fdata, $resignation_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $resignation_id, 0,json_encode($data['resignation'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Resignation Request #'.$resignation_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $resignation_id);
      } 
      $data['view'] = 'general/resignation/resignation_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['resignation']   = $this->Resignation_model->get_resignation($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/resignation', $data['resignation'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['types']           = $this->General_model->get_meta_data('resignation_type');
      $data['gen_id']          = get_file_code('files','resignation');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'hiring_date'             => $this->input->post('hiring_date'),
          'last_date'               => $this->input->post('last_date'),
          'opinion'                 => $this->input->post('opinion'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $resignation_id  =  $this->Resignation_model->add_resignation($fdata);
        if ($resignation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $resignation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Resignation Request #'.$resignation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $resignation_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $resignation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $resignation_id);
        }
      }
      $data['view'] = 'general/resignation/resignation_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/resignation');
      $this->db->update('resignation', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Resignation Request #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>