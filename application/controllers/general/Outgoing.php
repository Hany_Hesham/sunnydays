<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Outgoing extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Outgoing_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']         = $this->Modules_model->get_module_by(29);
      $this->data['permission']     = user_access($this->data['module']['id']);
      $this->data['del_permission'] = user_access(45);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/outgoing/index_outgoing';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function outgoing_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Outgoing_model->get_outgoings($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'outgoing\',\'outgoing\',\'general/outgoing\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/outgoing/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/outgoing/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Outgoing Record #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['out_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['return_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Outgoing_model->get_all_outgoings($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Outgoing_model->get_outgoings($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/outgoing');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['out_reasons']     = $this->General_model->get_meta_data('out_reason');
      $data['gen_id']          = get_file_code('files','outgoing');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'return_date'             => $this->input->post('return_date'),
          'address'                 => $this->input->post('address'),
          'reason'                  => $this->input->post('reason'),
          'resson_id'               => json_encode($this->input->post('resson_id'), JSON_UNESCAPED_UNICODE),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $outgoing_id  =  $this->Outgoing_model->add_outgoing($fdata);
        if ($outgoing_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Outgoing Record #'.$outgoing_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $outgoing_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['outgoing_id']    = $outgoing_id;
            $item_id                  = $this->Outgoing_model->add_outgoing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $outgoing_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $outgoing_id);
        }
      }
      $data['view'] = 'general/outgoing/outgoing_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($outgoing_id){
      $data['outgoing']          = $this->Outgoing_model->get_outgoing($outgoing_id);
      if (($data['outgoing']['status'] == 2 || $data['outgoing']['status'] == 1) && $data['outgoing']['reback']) {
        $rrData = json_decode($data['outgoing']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('outgoing', array('status' => 3, 'role_id' => 0), "id = ".$outgoing_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/outgoing', $data['outgoing'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, 0, 0, 0, 0, 0, 'Viewed Outgoing Record #'.$outgoing_id.''); 
      $data['deliveries']           = $this->Outgoing_model->get_outgoing_deliveries($outgoing_id);
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$outgoing_id,'files');
      $this->data['form_id']       = $outgoing_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/outgoing/view_outgoing';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($outgoing_id){
      $data['outgoing']             = $this->Outgoing_model->get_outgoing($outgoing_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/outgoing', $data['outgoing'], $this->data['uhotels']);
      $data['outgoing_items']       = $this->Outgoing_model->get_outgoing_items($outgoing_id);
      $this->load->view('general/outgoing/view_details',$data);
    }  

    public function signers_items($outgoing_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $outgoing_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $outgoing_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($outgoing_id=''){
      $data['outgoing']        = $this->Outgoing_model->get_outgoing($outgoing_id);
      edit_checker($data['outgoing'], $this->data['permission']['edit'], 'general/outgoing/view/'.$outgoing_id);
      $data['outgoing_items']  = $this->Outgoing_model->get_outgoing_items($outgoing_id);
      $data['outgoing_id']     = $outgoing_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['out_reasons']     = $this->General_model->get_meta_data('out_reason');
      $data['gen_id']          = $outgoing_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$outgoing_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'return_date'             => $this->input->post('return_date'),
          'address'                 => $this->input->post('address'),
          'reason'                  => $this->input->post('reason'),
          'resson_id'               => json_encode($this->input->post('resson_id'), JSON_UNESCAPED_UNICODE),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Outgoing_model->edit_outgoing($fdata, $outgoing_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, 0,json_encode($data['outgoing'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Outgoing Record #'.$outgoing_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Outgoing_model->get_outgoing_item($item['id']);
            $item['outgoing_id']  = $outgoing_id;
            $updated                = $this->Outgoing_model->edit_outgoing_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['description']);
            }
          }else{
            $item['outgoing_id']  = $outgoing_id;
            $item_id                = $this->Outgoing_model->add_outgoing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['description'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $outgoing_id);
      } 
      $data['view'] = 'general/outgoing/outgoing_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function out_with($outgoing_id=''){
      $data['outgoing']   = $this->Outgoing_model->get_outgoing($outgoing_id);
      if ($this->input->post()) {
        $fdata =[
          'out_with'      => $this->input->post('out_with')
        ];    
        $this->Outgoing_model->edit_outgoing($fdata, $outgoing_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, 0,json_encode($data['outgoing'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Outgoing Record #'.$outgoing_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        redirect('general/outgoing/view/'.$outgoing_id);
      }      
    } 

    public function copy($copied='', $copy = FALSE){
      $data['outgoing']         = $this->Outgoing_model->get_outgoing($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/outgoing', $data['outgoing'], $this->data['uhotels']);
      $data['outgoing_items']   = $this->Outgoing_model->get_outgoing_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['out_reasons']      = $this->General_model->get_meta_data('out_reason');
      $data['gen_id']           = get_file_code('files','outgoing');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'return_date'             => $this->input->post('return_date'),
          'address'                 => $this->input->post('address'),
          'reason'                  => $this->input->post('reason'),
          'resson_id'               => json_encode($this->input->post('resson_id'), JSON_UNESCAPED_UNICODE),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $outgoing_id  =  $this->Outgoing_model->add_outgoing($fdata);
        if ($outgoing_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Outgoing Record #'.$outgoing_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $outgoing_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['outgoing_id']    = $outgoing_id;
            $item_id                  = $this->Outgoing_model->add_outgoing_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $outgoing_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $outgoing_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $outgoing_id);
        }
      }
      $data['view'] = 'general/outgoing/outgoing_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Outgoing_model->get_outgoing_item($id);
        $this->db->update('outgoing_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['outgoing_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['description'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/outgoing');
      $this->db->update('outgoing', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Outgoing Record #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>