<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Salary_advance extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Salary_advance_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(42);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/salary_advance/index_salary_advance';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function salary_advance_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Salary_advance_model->get_salary_advances($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'salary_advance\',\'salary_advance\',\'general/salary_advance\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/salary_advance/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        if ($row['pos_id']) {
          $position = $row['pos_name'];
        } else {
          $position = $row['position_name'];
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/salary_advance/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Salary Advance Request #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$position.'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['clock_no'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['advance'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Salary_advance_model->get_all_salary_advances($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Salary_advance_model->get_salary_advances($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/salary_advance');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      // $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['types']           = $this->General_model->get_meta_data('salary_advance_type');
      $data['gen_id']          = get_file_code('files','salary_advance');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'balance'                 => $this->input->post('balance'),
          'salary'                  => $this->input->post('salary'),
          'advance'                 => $this->input->post('advance'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $salary_advance_id  =  $this->Salary_advance_model->add_salary_advance($fdata);
        if ($salary_advance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $salary_advance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Salary Advance Request #'.$salary_advance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $salary_advance_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $salary_advance_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $salary_advance_id);
        }
      }
      $data['view'] = 'general/salary_advance/salary_advance_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($salary_advance_id){
      $data['salary_advance']       = $this->Salary_advance_model->get_salary_advance($salary_advance_id);
      if (($data['salary_advance']['status'] == 2 || $data['salary_advance']['status'] == 1) && $data['salary_advance']['reback']) {
        $rrData = json_decode($data['salary_advance']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('salary_advance', array('status' => 3, 'role_id' => 0), "id = ".$salary_advance_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/salary_advance', $data['salary_advance'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $salary_advance_id, 0, 0, 0, 0, 0, 'Viewed Salary Advance Request #'.$salary_advance_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$salary_advance_id,'files');
      $this->data['form_id']       = $salary_advance_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/salary_advance/view_salary_advance';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($salary_advance_id){
      $data['salary_advance']             = $this->Salary_advance_model->get_salary_advance($salary_advance_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/salary_advance', $data['salary_advance'], $this->data['uhotels']);
      $this->load->view('general/salary_advance/view_details',$data);
    }    

    public function signers_items($salary_advance_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $salary_advance_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $salary_advance_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }   

    public function edit($salary_advance_id=''){
      $data['salary_advance']   = $this->Salary_advance_model->get_salary_advance($salary_advance_id);
      edit_checker($data['salary_advance'], $this->data['permission']['edit'], 'general/salary_advance/view/'.$salary_advance_id);
      $data['salary_advance_id']      = $salary_advance_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      // $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['types']           = $this->General_model->get_meta_data('salary_advance_type');
      $data['gen_id']          = $salary_advance_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$salary_advance_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'balance'                 => $this->input->post('balance'),
          'salary'                  => $this->input->post('salary'),
          'advance'                 => $this->input->post('advance'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Salary_advance_model->edit_salary_advance($fdata, $salary_advance_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $salary_advance_id, 0,json_encode($data['salary_advance'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Salary Advance Request #'.$salary_advance_id );
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $salary_advance_id);
      } 
      $data['view'] = 'general/salary_advance/salary_advance_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['salary_advance']   = $this->Salary_advance_model->get_salary_advance($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/salary_advance', $data['salary_advance'], $this->data['uhotels']);
      $data['copy']            = $copy;
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      // $data['user_groups']     = $this->User_groups_model->get_user_groups();
      $data['types']           = $this->General_model->get_meta_data('salary_advance_type');
      $data['gen_id']          = get_file_code('files','salary_advance');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'position'                => $this->input->post('position'),
          'dep_id'                  => $this->input->post('dep_id'),
          'pos_id'                  => $this->input->post('pos_id'),
          'clock_no'                => $this->input->post('clock_no'),
          'name'                    => $this->input->post('name'),
          'balance'                 => $this->input->post('balance'),
          'salary'                  => $this->input->post('salary'),
          'advance'                 => $this->input->post('advance'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $salary_advance_id  =  $this->Salary_advance_model->add_salary_advance($fdata);
        if ($salary_advance_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $salary_advance_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Salary Advance Request #'.$salary_advance_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $salary_advance_id, 'files');
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $salary_advance_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $salary_advance_id);
        }
      }
      $data['view'] = 'general/salary_advance/salary_advance_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/salary_advance');
      $this->db->update('salary_advance', array('deleted' => 1), "id = ".$id);
      loger('Delete',$this->data['module']['id'],$this->data['module']['name'],$id,0,0,0,0,0,'Deleted Salary Advance Request #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    }

  }

?>