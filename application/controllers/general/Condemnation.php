<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Condemnation extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Condemnation_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(30);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/condemnation/index_condemnation';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function condemnation_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Condemnation_model->get_condemnations($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'condemnation\',\'condemnation\',\'general/condemnation\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/condemnation/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/condemnation/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Condemnation Record #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Condemnation_model->get_all_condemnations($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Condemnation_model->get_condemnations($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/condemnation');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','condemnation');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $condemnation_id  =  $this->Condemnation_model->add_condemnation($fdata);
        if ($condemnation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Condemnation Record #'.$condemnation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $condemnation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['condemnation_id']    = $condemnation_id;
            $item_id                    = $this->Condemnation_model->add_condemnation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $condemnation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $condemnation_id);
        }
      }
      $data['view'] = 'general/condemnation/condemnation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($condemnation_id){
      $data['condemnation']           = $this->Condemnation_model->get_condemnation($condemnation_id);
      if (($data['condemnation']['status'] == 2 || $data['condemnation']['status'] == 1) && $data['condemnation']['reback']) {
        $rrData = json_decode($data['condemnation']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('condemnation', array('status' => 3, 'role_id' => 0), "id = ".$condemnation_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/condemnation', $data['condemnation'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, 0, 0, 0, 0, 0, 'Viewed Condemnation Record #'.$condemnation_id.''); 
      $data['uploads']                = $this->General_model->get_file_from_table($this->data['module']['id'],$condemnation_id,'files');
      $this->data['form_id']          = $condemnation_id;
      $data['messaged']               = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount']    = count($data['messaged']);
      $data['view']                   = 'general/condemnation/view_condemnation';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($condemnation_id){
      $data['condemnation']             = $this->Condemnation_model->get_condemnation($condemnation_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/condemnation', $data['condemnation'], $this->data['uhotels']);
      $data['condemnation_items']          = $this->Condemnation_model->get_condemnation_items($condemnation_id);
      $this->load->view('general/condemnation/view_details',$data);
    }  

    public function signers_items($condemnation_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $condemnation_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $condemnation_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($condemnation_id=''){
      $data['condemnation']   = $this->Condemnation_model->get_condemnation($condemnation_id);
      edit_checker($data['condemnation'], $this->data['permission']['edit'], 'general/condemnation/view/'.$condemnation_id);
      $data['condemnation_items']   = $this->Condemnation_model->get_condemnation_items($condemnation_id);
      $data['condemnation_id']      = $condemnation_id; 
      $data['hotels']               = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']          = $this->Departments_model->get_departments();
      $data['gen_id']               = $condemnation_id; 
      $data['uploads']              = $this->General_model->get_file_from_table($this->data['module']['id'],$condemnation_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Condemnation_model->edit_condemnation($fdata, $condemnation_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, 0,json_encode($data['condemnation'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Condemnation Record #'.$condemnation_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Condemnation_model->get_condemnation_item($item['id']);
            $item['condemnation_id']  = $condemnation_id;
            $updated                = $this->Condemnation_model->edit_condemnation_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['name']);
            }
          }else{
            $item['condemnation_id']  = $condemnation_id;
            $item_id                = $this->Condemnation_model->add_condemnation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['name'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $condemnation_id);
      } 
      $data['view'] = 'general/condemnation/condemnation_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['condemnation']       = $this->Condemnation_model->get_condemnation($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/condemnation', $data['condemnation'], $this->data['uhotels']);
      $data['condemnation_items'] = $this->Condemnation_model->get_condemnation_items($copied);
      $data['copy']               = $copy;
      $data['hotels']             = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']        = $this->Departments_model->get_departments();
      $data['gen_id']             = get_file_code('files','condemnation');
      $data['uploads']            = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'reason'                  => $this->input->post('reason'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $condemnation_id  =  $this->Condemnation_model->add_condemnation($fdata);
        if ($condemnation_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Condemnation Record #'.$condemnation_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $condemnation_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['condemnation_id']    = $condemnation_id;
            $item_id                    = $this->Condemnation_model->add_condemnation_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $condemnation_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['name'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $condemnation_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $condemnation_id);
        }
      }
      $data['view'] = 'general/condemnation/condemnation_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Condemnation_model->get_condemnation_item($id);
        $this->db->update('condemnation_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['condemnation_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['name'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/condemnation');
      $this->db->update('condemnation', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Condemnation Record #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>