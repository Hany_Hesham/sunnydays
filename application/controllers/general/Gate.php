<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class Gate extends MY_Controller {

    public function __construct(){
      parent::__construct();
      $this->load->model('general/Gate_model');
      $this->load->model('admin/Departments_model');
      $this->load->model('admin/User_groups_model');
      $this->data['module']        = $this->Modules_model->get_module_by(19);
      $this->data['permission']    = user_access($this->data['module']['id']);
    }

    public function index(){
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'admin/dashboard');
      $data['view']         = 'general/gate/index_gate';
      $this->load->view('admin/includes/layout',$data);
    }  

    public function gate_ajax(){
      $dt_att              = $this->datatables_att();
      $dt_att['module_id'] = $this->data['module']['id'];
      $custom_search       = $this->input->post('searchBy');
      $rows                = $this->Gate_model->get_gates($this->data['uhotels'], $this->data['dep_id'], $dt_att, $custom_search, '');
      $data = array();
      $i    = 1;
      foreach($rows as $row) {
        $arr = array(); 
        $tools=array();
        if ($this->data['permission']['remove'] == 1) {
          $tools[] = '<div class="wait-hover"><a href="javascript: void(0);" onclick="del('.$row['id'].',\'gate\',\'gate\',\'general/gate\',\'del\')" titel="Delete item"><span style="color:red;">Delete</span></a> | ';
        }
        if ($this->data['permission']['edit'] == 1) {   
          $tools[] .='<a class="edit-item" href="'.base_url('general/gate/edit/'. $row['id']).'"><span>Edit</span></a></div>'; 
        }
        $arr[] = '<span style="color:'.$row['status_color'].';padding-left:7%;">'.$i.'</span>';
        $arr[] = '<div class="after-hover"><a  href="'.base_url('general/gate/view/'. $row['id']).'"
                   target="_blank" titel="View Item"><strong>Gate Pass #.'.$row['id'].'</strong></a><br>'.implode("", $tools).'</div>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['hotel_name'].'</span>';           
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['dep_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['out_date'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['status_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['role_name'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['fullname'].'</span>';
        $arr[] = '<span style="color:'.$row['status_color'].'">'.$row['timestamp'].'</span>';
        $data[] =$arr;
        $i++;
      }
      $output = array(
       "draw" => $dt_att['draw'],
       "recordsTotal"    => $this->Gate_model->get_all_gates($this->data['uhotels'], $this->data['dep_id']),
       "recordsFiltered" => $this->Gate_model->get_gates($this->data['uhotels'], $this->data['dep_id'], $dt_att,$custom_search,'count'),
       "data" => $data
      );
      echo json_encode($output);
      exit();
    }

    public function add(){
      access_checker(0,0,0,$this->data['permission']['creat'],0,0,0,'general/gate');
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = get_file_code('files','gate');
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'address'                 => $this->input->post('address'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];      
        $gate_id  =  $this->Gate_model->add_gate($fdata);
        if ($gate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $gate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Gate Pass #'.$gate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $gate_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            $item['gate_id']    = $gate_id;
            $item_id                  = $this->Gate_model->add_gate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $gate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $gate_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $gate_id);
        }
      }
      $data['view'] = 'general/gate/gate_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function view($gate_id){
      $data['gate']          = $this->Gate_model->get_gate($gate_id);
      if (($data['gate']['status'] == 2 || $data['gate']['status'] == 1) && $data['gate']['reback']) {
        $rrData = json_decode($data['gate']['reback']);
        if ($rrData->type == 'Reject') {
          $this->db->update('gate', array('status' => 3, 'role_id' => 0), "id = ".$gate_id);
        }
      }
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/gate', $data['gate'], $this->data['uhotels']);
      loger('View', $this->data['module']['id'], $this->data['module']['name'], $gate_id, 0, 0, 0, 0, 0, 'Viewed Gate Pass #'.$gate_id.''); 
      $data['uploads']             = $this->General_model->get_file_from_table($this->data['module']['id'],$gate_id,'files');
      $this->data['form_id']       = $gate_id;
      $data['messaged']            = $this->General_model->get_messages($this->data['user_id'], $this->data['module']['id'], $this->data['form_id']);
      $this->data['commentsCount'] = count($data['messaged']);
      $data['view']                = 'general/gate/view_gate';
      $this->load->view('admin/includes/layout',$data);
    }

    public function viewrates($gate_id){
      $data['gate']             = $this->Gate_model->get_gate($gate_id);
      access_checker($this->data['permission']['g_view'], $this->data['permission']['view'], 0, 0, 0, 0, 0, 'general/gate', $data['gate'], $this->data['uhotels']);
      $data['gate_items']          = $this->Gate_model->get_gate_items($gate_id);
      $this->load->view('general/gate/view_details',$data);
    }  

    public function signers_items($gate_id){
      $data['signatures']       = getSigners($this->data['module']['id'], $gate_id);
      $data['module']           = $this->data['module'];
      $data['signature_path']   = $this->data['signature_path'];
      $data['form_id']          = $gate_id;
      $data['doned']            = 0;
      $this->load->view('admin/html_parts/signers',$data);
    }      

    public function edit($gate_id=''){
      $data['gate']   = $this->Gate_model->get_gate($gate_id);
      edit_checker($data['gate'], $this->data['permission']['edit'], 'general/gate/view/'.$gate_id);
      $data['gate_items']          = $this->Gate_model->get_gate_items($gate_id);
      $data['gate_id']   = $gate_id; 
      $data['hotels']          = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']     = $this->Departments_model->get_departments();
      $data['gen_id']          = $gate_id; 
      $data['uploads']         = $this->General_model->get_file_from_table($this->data['module']['id'],$gate_id,'files');
      if ($this->input->post('submit')) {
        $fdata =[
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'address'                 => $this->input->post('address'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
        ];    
        $this->Gate_model->edit_gate($fdata, $gate_id);
        loger('Update', $this->data['module']['id'], $this->data['module']['name'], $gate_id, 0,json_encode($data['gate'], JSON_UNESCAPED_UNICODE),json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,'Updated Gate Pass #'.$gate_id );
        foreach ($this->input->post('items') as $key => $item) {
          if (isset($item['id'])) {
            $item_data              = $this->Gate_model->get_gate_item($item['id']);
            $item['gate_id']  = $gate_id;
            $updated                = $this->Gate_model->edit_gate_item($item, $item['id']);
            if ($updated > 0) {
              loger('Update', $this->data['module']['id'], $this->data['module']['name'], $gate_id, $item['id'], json_encode($item_data, JSON_UNESCAPED_UNICODE), json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0, 'Updated Item: '.$item['description']);
            }
          }else{
            $item['gate_id']  = $gate_id;
            $item_id                = $this->Gate_model->add_gate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $gate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'added item:'.$item['description'].'');
            }
          }
        }
        $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been updated Successfully!']);
        $this->signers($this->data['module']['id'], $gate_id);
      } 
      $data['view'] = 'general/gate/gate_process';
      $this->load->view('admin/includes/layout',$data);       
    } 

    public function copy($copied='', $copy = FALSE){
      $data['gate']     = $this->Gate_model->get_gate($copied);
      access_checker(0, 0, 0, $this->data['permission']['creat'], 0, 0, 0, 'general/gate', $data['gate'], $this->data['uhotels']);
      $data['gate_items'] = $this->Gate_model->get_gate_items($copied);
      $data['copy']             = $copy;
      $data['hotels']           = $this->Hotels_model->get_hotels($this->data['uhotels']);
      $data['departments']      = $this->Departments_model->get_departments();
      $data['gen_id']           = get_file_code('files','gate');
      $data['uploads']          = $this->General_model->get_file_from_table($this->data['module']['id'],$data['gen_id'],'files');
      if ($this->input->post('submit')) {
        $role_id = get_roleHotel_byid('departments',$this->input->post('dep_code'));
        $fdata =[
          'uid'                     => $this->data['user_id'],
          'hid'                     => $this->input->post('hid'),
          'dep_code'                => $this->input->post('dep_code'),
          'out_date'                => $this->input->post('out_date'),
          'address'                 => $this->input->post('address'),
          'remarks'                 => $this->input->post('remarks'),
          'status'                  => '1',
          'timestamp'               => date("Y-m-d H:i:s"),
        ];     
        $gate_id  =  $this->Gate_model->add_gate($fdata);
        if ($gate_id) {
          loger('Create', $this->data['module']['id'], $this->data['module']['name'], $gate_id, 0, 0, json_encode($fdata, JSON_UNESCAPED_UNICODE), 0, 0, 'Created Gate Pass #'.$gate_id.'');  
          $this->General_model->update_files($this->input->post('gen_id'), $this->data['module']['id'], $gate_id, 'files');
          foreach ($this->input->post('items') as $key => $item) {
            unset($item['id']);
            $item['gate_id']    = $gate_id;
            $item_id                  = $this->Gate_model->add_gate_item($item);
            if ($item_id) {
              loger('Create', $this->data['module']['id'], $this->data['module']['name'], $gate_id, $item_id, 0, json_encode($item, JSON_UNESCAPED_UNICODE), 0, 0,'Added Item:'.$item['description'].'');
            }
          }
          $this->session->set_flashdata(['alert'=>'succsess','msg'=>'Record has been added Successfully!']);
          $this->Sign_model->addSignature($this->data['module']['id'], $gate_id, $role_id, 0, $fdata['hid']);
          $this->signers($this->data['module']['id'], $gate_id);
        }
      }
      $data['view'] = 'general/gate/gate_process';
      $this->load->view('admin/includes/layout',$data);       
    }   

    public function delete_items(){
      if ($this->input->post('id')) {
        $id = $this->input->post('id');
        $item = $this->Gate_model->get_gate_item($id);
        $this->db->update('gate_item', array('deleted' => 1), "id = ".$id);
        loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $item['gate_id'], $item['id'], 0, 0, 0, 0,'Deleted Item:'.$item['description'].'');
        $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Item is Deleted Successfully!']);
        echo json_encode(true);
        exit();
      }  
    }

    public function del($id){
      access_checker(0,0,0,0,$this->data['permission']['remove'],0,0,0,'general/gate');
      $this->db->update('gate', array('deleted' => 1), "id = ".$id);
      loger('Delete', $this->data['module']['id'], $this->data['module']['name'], $id, 0, 0, 0, 0, 0,'Deleted Gate Pass #'.$id.'');
      $this->session->set_flashdata(['alert'=>'Succsess','msg'=>'Record is Deleted Successfully!']);
    } 

  }

?>