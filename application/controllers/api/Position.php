<?php defined('BASEPATH') OR exit('No direct script access allowed');

    require __DIR__.'/REST_Controller.php';

    class Position extends REST_Controller {
        function __construct() {
            parent::__construct();
        }

        public function data_put() {
            $update_data = json_decode(file_get_contents('php://input'), true);
            if(empty($update_data)) {
                $message = array(
                    'status' => FALSE,
                    'message' => 'No Location Data'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $total_imported = 0;
                $total_updated  = 0;
                foreach ($update_data as $positions) {
                    foreach ($positions['data'] as $position) {
                        if (isset($position['POS_CODE']) && $position['POS_CODE'] != '') {
                            $posted = $this->Api_model->get_position($positions['connection'], $position['POS_CODE']);
                            if ($posted) {
                                $fdata =[
                                    'name'          => $position['POS_DESC_E'],
                                    'name_ar'       => $position['POS_DESC_A'],
                                    'level'         => $position['POS_LEVEL_CODE'],
                                    'budget'        => $position['BUDGET'],
                                    'actual'        => $position['Actual'],
                                    'min'           => $position['POS_MIN_SAL'],
                                    'max'           => $position['POS_MAX_SAL'],
                                    'updated_at'    => date("Y-m-d H:i:s")
                                ];
                                $this->db->where('api_user_groups.id', $posted['id']);   
                                $this->db->update('api_user_groups', $fdata);
                                $total_updated++;
                            }else{
                                $fdata =[
                                    'connection'    => $positions['connection'],
                                    'name'          => $position['POS_DESC_E'],
                                    'name_ar'       => $position['POS_DESC_A'],
                                    'code'          => $position['POS_CODE'],
                                    'level'         => $position['POS_LEVEL_CODE'],
                                    'budget'        => $position['BUDGET'],
                                    'actual'        => $position['Actual'],
                                    'min'           => $position['POS_MIN_SAL'],
                                    'max'           => $position['POS_MAX_SAL'],
                                    'timestamp'     => date("Y-m-d H:i:s")
                                ];
                                $this->db->insert('api_user_groups', $fdata);
                                $total_imported++;
                            }
                        }
                    }
                }
                if($update_data > 0 && !empty($update_data)){
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Total Position imported-updated [' . $total_imported . ' - '. $total_updated .']'
                    );
                    $this->response($message, REST_Controller::HTTP_OK);            
                }else{
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'API Position Update Fail.'
                    );
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

    }

?>