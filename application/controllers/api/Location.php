<?php defined('BASEPATH') OR exit('No direct script access allowed');

    require __DIR__.'/REST_Controller.php';

    class Location extends REST_Controller {
        function __construct() {
            parent::__construct();
        }

        public function data_put() {
            $update_data = json_decode(file_get_contents('php://input'), true);
            if(empty($update_data)) {
                $message = array(
                    'status' => FALSE,
                    'message' => 'No Location Data'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $total_imported = 0;
                $total_updated  = 0;
                foreach ($update_data as $locations) {
                    foreach ($locations['data'] as $location) {
                        if (isset($location['CODE']) && $location['CODE'] != '') {
                            $localed = $this->Api_model->get_location($locations['connection'], $location['CODE']);
                            if ($localed) {
                                $fdata =[
                                    'name'          => $location['DESC_E'],
                                    'updated_at'    => date("Y-m-d H:i:s")
                                ];
                                $this->db->where('api_location.id', $localed['id']);   
                                $this->db->update('api_location', $fdata);
                                $total_updated++;
                            }else{
                                $fdata =[
                                    'connection'    => $locations['connection'],
                                    'code'          => $location['CODE'],
                                    'name'          => $location['DESC_E'],
                                    'timestamp'     => date("Y-m-d H:i:s")
                                ];
                                $this->db->insert('api_location', $fdata);
                                $total_imported++;
                            }
                        }
                    }
                }
                if($update_data > 0 && !empty($update_data)){
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Total Location imported-updated [' . $total_imported . ' - '. $total_updated .']'
                    );
                    $this->response($message, REST_Controller::HTTP_OK);    
                }else{
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'API Location Update Fail.'
                    );
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

    }

?>