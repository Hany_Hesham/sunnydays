<?php defined('BASEPATH') OR exit('No direct script access allowed');

    require __DIR__.'/REST_Controller.php';

    class Employee extends REST_Controller {
        function __construct() {
            parent::__construct();
        }

        public function data_put() {
            $update_data = json_decode(file_get_contents('php://input'), true);
            if(empty($update_data)) {
                $message = array(
                    'status' => FALSE,
                    'message' => 'No Employee Data'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $total_imported = 0;
                $total_updated  = 0;

                    foreach ($update_data as $employee) {
                        if (isset($employee['EMP_CODE']) && $employee['EMP_CODE'] != '') {
                            $localed = $this->Api_model->get_location($employee['pms'], $employee['Location']);
                            $departed = $this->Api_model->get_department($employee['pms'], $employee['Department']);
                            $posted = $this->Api_model->get_position($employee['pms'], $employee['Position']);
                            $emped = $this->Api_model->get_employee($employee['pms'], $employee['EMP_CODE']);
                            if ($emped) {
                                $fdata =[
                                    'emp_hid'           => $localed['hid'],
                                    'emp_lid'           => $localed['id'],
                                    'emp_did'           => $departed['id'],
                                    'emp_pid'           => $posted['id'],
                                    'emp_id'            => $employee['EMP_PASSPOR'],
                                    'emp_ename'         => $employee['EMP_NAME_E'],
                                    'emp_aname'         => $employee['EMP_NAME_A'],
                                    'emp_location'      => $employee['Location'],
                                    'emp_dep'           => $employee['Department'],
                                    'emp_position'      => $employee['Position'],
                                    'emp_eaddress'      => $employee['EMP_ADDRESS'],
                                    'emp_aaddress'      => $employee['EMP_ADDRESSA'],
                                    'emp_phone'         => $employee['EMP_TEL'],
                                    'emp_birthdate'     => $employee['EMP_BR_DATE'],
                                    'emp_contractdate'  => $employee['EMP_CON_EX_DATE'],
                                    'emp_hiringdate'    => $employee['EMP_HIRING_DATE'],
                                    'emp_chaindate'     => $employee['ChainDate'],
                                    'emp_grosssalary'   => $employee['EMP_gross_salary'],
                                    'emp_level'         => $employee['Level'],
                                    'emp_duebalance'    => $employee['Due'],
                                    'emp_balancecon'    => $employee['Cons'],
                                    'emp_balanceopen'   => $employee['OpenBal'],
                                    'emp_balncemonth'   => $employee['DueMonth'],
                                    'active'            => 1,
                                    'updated_at'        => date("Y-m-d H:i:s")
                                ];
                                $this->db->where('api_employee.id', $emped['id']);   
                                $this->db->update('api_employee', $fdata);
                                $total_updated++;
                            }else{
                                $fdata =[
                                    'connection'        => $employee['pms'],
                                    'emp_hid'           => $localed['hid'],
                                    'emp_lid'           => $localed['id'],
                                    'emp_did'           => $departed['id'],
                                    'emp_pid'           => $posted['id'],
                                    'emp_code'          => $employee['EMP_CODE'],
                                    'emp_id'            => $employee['EMP_PASSPOR'],
                                    'emp_ename'         => $employee['EMP_NAME_E'],
                                    'emp_aname'         => $employee['EMP_NAME_A'],
                                    'emp_location'      => $employee['Location'],
                                    'emp_dep'           => $employee['Department'],
                                    'emp_position'      => $employee['Position'],
                                    'emp_eaddress'      => $employee['EMP_ADDRESS'],
                                    'emp_aaddress'      => $employee['EMP_ADDRESSA'],
                                    'emp_phone'         => $employee['EMP_TEL'],
                                    'emp_birthdate'     => $employee['EMP_BR_DATE'],
                                    'emp_contractdate'  => $employee['EMP_CON_EX_DATE'],
                                    'emp_hiringdate'    => $employee['EMP_HIRING_DATE'],
                                    'emp_chaindate'     => $employee['ChainDate'],
                                    'emp_grosssalary'   => $employee['EMP_gross_salary'],
                                    'emp_level'         => $employee['Level'],
                                    'emp_duebalance'    => $employee['Due'],
                                    'emp_balancecon'    => $employee['Cons'],
                                    'emp_balanceopen'   => $employee['OpenBal'],
                                    'emp_balncemonth'   => $employee['DueMonth'],
                                    'active'            => 1,
                                    'timestamp'         => date("Y-m-d H:i:s")
                                ];
                                $this->db->insert('api_employee', $fdata);
                                $total_imported++;
                            }
                        }
                    }

                if($update_data > 0 && !empty($update_data)){
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Total Employee imported-updated [' . $total_imported . ' - '. $total_updated .']'
                    );
                    $this->response($message, REST_Controller::HTTP_OK);        
                }else{
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'API Employee Update Fail.'
                    );
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

    }

?>