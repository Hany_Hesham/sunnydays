<?php defined('BASEPATH') OR exit('No direct script access allowed');

    require __DIR__.'/REST_Controller.php';

    class Department extends REST_Controller {
        function __construct() {
            parent::__construct();
        }

        public function data_put() {
            $update_data = json_decode(file_get_contents('php://input'), true);
            if(empty($update_data)) {
                $message = array(
                    'status' => FALSE,
                    'message' => 'No Location Data'
                );
                $this->response($message, REST_Controller::HTTP_NOT_FOUND);
            } else {
                $total_imported = 0;
                $total_updated  = 0;
                foreach ($update_data as $departments) {
                    foreach ($departments['data'] as $department) {
                        if (isset($department['DEP_CODE']) && $department['DEP_CODE'] != '') {
                            $departed = $this->Api_model->get_department($departments['connection'], $department['DEP_CODE']);
                            if ($departed) {
                                $fdata =[
                                    'dep_name'          => $department['DEP_DESC_E'],
                                    'dep_name_ar'       => $department['DEP_DESC_A'],
                                    'budget'            => $department['dep_budget'],
                                    'updated_at'        => date("Y-m-d H:i:s")
                                ];
                                $this->db->where('api_departments.id', $departed['id']);   
                                $this->db->update('api_departments', $fdata);
                                $total_updated++;
                            }else{
                                $fdata =[
                                    'connection'        => $departments['connection'],
                                    'dep_code'          => $department['DEP_CODE'],
                                    'dep_name'          => $department['DEP_DESC_E'],
                                    'dep_name_ar'       => $department['DEP_DESC_A'],
                                    'budget'            => $department['dep_budget'],
                                    'timestamp'         => date("Y-m-d H:i:s")
                                ];
                                $this->db->insert('api_departments', $fdata);
                                $total_imported++;
                            }
                        }
                    }
                }
                if($update_data > 0 && !empty($update_data)){
                    // success
                    $message = array(
                        'status' => TRUE,
                        'message' => 'Total Department imported-updated [' . $total_imported . ' - '. $total_updated .']'
                    );
                    $this->response($message, REST_Controller::HTTP_OK);    
                }else{
                    // error
                    $message = array(
                        'status' => FALSE,
                        'message' => 'API Department Update Fail.'
                    );
                    $this->response($message, REST_Controller::HTTP_NOT_FOUND);
                }
            }
        }

    }

?>