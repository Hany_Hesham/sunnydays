<?php

	class MY_Controller extends CI_Controller{
		
		function __construct(){
			parent::__construct();
      $this->data['signature_path'] = base_url().'assets/signatures/';
			$redirect_path = '/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3);
      $this->session->set_flashdata('redirect', $redirect_path);
			if (!$this->session->has_userdata('is_admin_login')) {
		    if (strpos($this->uri->uri_string(), 'admin/auth/login') === FALSE) {
		      $this->session->set_userdata(array(
		        'red_url' => $this->uri->uri_string()
		      ));
		    }
	     	redirect(base_url('admin/auth/login'));
	  	}else{       	
				$this->global_data['sessioned_user'] = $this->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
				$this->load->vars($this->global_data);
		  	$this->data['modules']          	 				= $this->General_model->get_modules();
		  	$this->data['menuModules']       	 				= $this->General_model->get_menuModules();
	    	$this->data['user_id']             				= $this->global_data['sessioned_user']['id'];
				$this->data['username']            				= $this->global_data['sessioned_user']['username'];
			  $this->data['uhotels']             				= get_uhotels($this->data['user_id']);
			  $this->data['uroles']      		     				= getUserRoles();
				$this->data['dep_id']              				= getUserdepartments();
				$this->data['udepartments']        				= get_udepartments($this->data['dep_id']);
				$this->data['log']                       	= $this->Modules_model->get_module_by('','Log Activity');
				$this->data['email']                     	= $this->Modules_model->get_module_by('','Email');
			  $this->data['log_permission']            	= user_access($this->data['log']['id']);
			  $this->data['email_permission']          	= user_access($this->data['email']['id']);
				// $this->data['notification']              	= $this->Modules_model->get_module_by('','Notification');
			  // $this->data['notification_permission']   	= user_access($this->data['notification']['id']);
	  	}
		}

		function datatables_att(){

			 $draw        = intval($this->input->post("draw"));
	         $start       = intval($this->input->post("start"));
	         $length      = intval($this->input->post("length"));
	         $order       = $this->input->post("order[0][dir]");
	         $order_col   = $this->input->post("order[0][column]");
	         $col_name    = $this->input->post("columns[$order_col][name]");
	         $search      = trim($this->input->post("search[value]"));
	         if ($length == -1) {$length = '10000000000000000000';}

	         $dt_att = [
	         	         'draw'      => $draw,
                         'start'     => $start,
                         'length'    => $length,
                         'order'     => $order,
                         'order_col' => $order_col,
                         'col_name'  => $col_name,
                         'search'    => $search,                         
	                   ];
	               
	         return $dt_att;
		}   


	public function get_submenu($model,$func,$type_id=false,$hid=false){

		   if (!$type_id) {
		   	    if ($this->input->post()) {
		   	    	$type_id = $this->input->post('search');
		   	    }
		      }

		      if (!$hid) {
		   	    if ($this->input->post()) {
		   	    	$hid = $this->input->post('hid');
		   	    }
		      }

			$parts = $this->$model->$func($type_id, $hid);
	      
	        $data = array();
	        
	        if ($parts) {
	        	
	          foreach ($parts as $part) {
					
				$data[] =$part;
			  }
			
	        }else{
	       
	        	$data[] =['key'=>'OOps No data for such choice'];
	       
	        }

			   
		   echo json_encode($data);
	        
	       
	       exit();

		}  

		public function search_metaData(){
		  if ($this->input->post()) {
		   	$type_id = $this->input->post('search');
		   	$hid = $this->input->post('hid');
		   	$keyword = $this->input->post('keyword');
				$parts = $this->General_model->search_metaData($type_id, $hid, $keyword);
		  }
	    $data = array();
	    if (isset($parts) && $parts) {
	      foreach ($parts as $part) {
					$data[] =$part;
			  }
	    }else{
	      $data[] =['key'=>'OOps No data for such choice'];
	    }  
		  echo json_encode($data); 
	    exit();
		} 

		public function search_employee($hid, $type_id){
			if ($this->input->post()) {
				$type_id = $this->input->post('search');
				$hid = $this->input->post('hid');
			}
			$parts = $this->General_model->search_employee($type_id, $hid);
		  	if (isset($parts) && $parts['emp_code']) {
				echo json_encode($parts);
				exit();
		  	}else{
				exit();
			}
		} 
		
		public function search_APIEmployee($hid, $code){
			if ($this->input->post()) {
				$code = $this->input->post('search');
				$hid = $this->input->post('hid');
			}
			$parts = $this->General_model->search_APIEmployee($code, $hid);
		  	if (isset($parts) && $parts) {
				echo json_encode($parts);
				exit();
		  	}
		} 

		public function get_allData(){
			if ($this->input->post()) {
			  	$hid 	= $this->input->post('hid');
			  	$table 	= $this->input->post('table');
			  	$code 	= $this->input->post('code');
			}
			$parts = $this->General_model->get_Data_all($table, $hid, $code);
			$data = array();
			if ($parts) {
			  	foreach ($parts as $part) {
					$data[] =$part;
			  	}
			}else{
			  	$data[] =['key'=>'OOps No data for such choice'];
			}
			echo json_encode($data);          
			exit();
	  	} 

		public function get_limits($module_id){
			if ($this->input->post()) {
				$module_id = $this->input->post('module_id');
			}
			$parts = $this->General_model->get_limits($module_id);
		  	if (isset($parts) && $parts) {
				return $parts['limit'];
		  	}
		} 

		public function sync_employe_data(){
	      	$employee = @$this->Rooms_model->get_employe_data($this->input->post('hid'),$this->input->post('clock_id'));
	      	echo json_encode($employee);
	      	exit();
	    } 

	    public function sync_room_data(){
	    	$data = array('hid' => $this->input->post('hid'), 'room' => $this->input->post('room'));
	      	$room = $this->Rooms_model->searchRoom($data);
	      	echo json_encode($room);
	      	exit();
	    } 

		public function getSigners(){
		   	if ($this->input->post()) {
		   	   	$search = $this->input->post('search');
		   	}
			$parts = getIfSigner($search);
	        $data = array();
	        if ($parts) {
	          	foreach ($parts as $part) {
					$data[] =$part;
			  	}
	        }else{
	        	$data[] =['key'=>'OOps No data for such choice'];
	        }
		   	echo json_encode($data);	        
	       	exit();
		} 

		public function get_meta_keys_data(){
		   	if ($this->input->post()) {
		   	   	$keyword 	= $this->input->post('keyword');
		   	   	$type 		= $this->input->post('type');
		   	}
			$parts = $this->General_model->get_meta_keys_data($keyword, $type);
	        $data = array();
	        if ($parts) {
	          	foreach ($parts as $part) {
					$data[] = $part;
			  	}
	        }else{
	        	$data[] =['key'=>'OOps No data for such choice'];
	        }
		   	echo json_encode($data);	        
	       	exit();
		} 

     
    public function upload($module_id,$form_id,$folder,$type=FALSE) {

        $file_name = do_upload("upload",$folder);

        if (!$file_name) {

              die(json_encode($this->data['error']));

          } else {
          	   	 $id= $this->General_model->add_file_intable($module_id,$this->input->post('table'),$form_id, $file_name,$_SESSION['user_id'],$type);

              die("{}");

            }

         }


     
    public function remove_file($form_id, $id,$module_id) {

        $file_name = $_POST['key'];

          if (!$id) {

              die(json_encode($this->data['error']));

          } else {

          	if (!$this->input->post('key')) {
                  
                  $this->General_model->remove_file($id,'',$module_id);

          	  }else{

                  $this->General_model->remove_file($id,$this->input->post('key'),$module_id);
          	  }

              die("{}");

          }

        }  	


        public function check_field($table,$col,$exist_value=''){

			 $item   = $this->General_model->search_in($table,$col,$this->input->post('name'),$exist_value); 

			 if ($item) {
	              echo json_encode('exist');
		          exit();
			   }else{
			   	  echo json_encode('not_exist');
		          exit();
			   }
            
		  }
  
		public function signers($module_id, $form_id, $typed = FALSE, $finished = FALSE){
			addEditSigners($module_id, $form_id, $typed);
			if (!$finished) {
				redirect($this->stage($module_id, $form_id, 1, $typed));
			}
        } 

        public function stage($module_id, $form_id, $new = FALSE, $typed = FALSE){
		   	notify_signers($module_id, $form_id, $typed);
		   	$module = $this->Modules_model->get_module_by($module_id);
      		$url = base_url($module['view_link'].'/'.$form_id);
      		if ($this->input->is_ajax_request()) {
	    	    echo json_encode(true);
		        exit();
		      }  
			redirect($url);
		}

		public function sign($module_id, $form_id, $id, $typed = FALSE){
		   	sign($module_id, $form_id, $id);
		   	$fdata=[
				'form_id'          => $form_id,
				'uid'            => $this->global_data['sessioned_user']['id']
			];
	    	loger('Signature',$module_id,'',$form_id,0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,0,'Signature Form no '.$id.'');
		   	redirect($this->stage($module_id, $form_id, FALSE, $typed));
		}

		public function sign_for($module_id, $form_id, $id, $uid, $typed = FALSE){
		   	signFor($module_id, $form_id, $id, $uid);
		   	$fdata=[
				'form_id'          => $form_id,
				'uid'            => $this->global_data['sessioned_user']['id'],
				'uidfor'            => $uid
			];
	    	loger('Signature',$module_id,'',$form_id,0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,0,'Signature Form no '.$id.'');
		   	redirect($this->stage($module_id, $form_id, FALSE, $typed));
		}

		public function unSign($module_id, $form_id, $id, $typed = FALSE){
		   	unSign($module_id, $form_id, $id);
		   	$fdata=[
				'form_id'          => $form_id,
				'uid'            => $this->global_data['sessioned_user']['id']
			];
	    	loger('Signature',$module_id,'',$form_id,0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,0,'Sign Form no '.$id.'');
		   	redirect($this->stage($module_id, $form_id, FALSE, $typed));
		}

		public function reject($module_id, $form_id, $typed = FALSE){
		   	$id = $this->input->post('id');
		   	$reason = $this->input->post('reason');
		   	reject($module_id, $form_id, $id, $reason);
		   	notify_reject($module_id, $form_id);
		   	$fdata=[
				'form_id'        => $form_id,
				'reason'         => $reason,
				'uid'            => $this->global_data['sessioned_user']['id']
			];
	    	loger('Signature',$module_id,'',$form_id,0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,0,'Reject Form no '.$id.'');
		   	redirect($this->stage($module_id, $form_id, FALSE, $typed));
		}

		public function resign($module_id, $form_id, $typed = FALSE){
		   	//$id = $this->input->post('id');
		   	$reason = $this->input->post('reason');
		   	$rank = $this->input->post('rank');
		   	resign($module_id, $form_id, $reason, $rank, $typed);
		   	$fdata=[
				'form_id'       => $form_id,
				'reason'        => $reason,
				'rank'          => $rank,
				'uid'           => $this->global_data['sessioned_user']['id']
			];
	    	loger('Signature',$module_id,'',$form_id,0,json_encode($fdata, JSON_UNESCAPED_UNICODE),0,0,0,'Rebacked Form no '.$id.'');
		   	redirect($this->stage($module_id, $form_id, FALSE, $typed));
		}

		public function sendmail($module_id, $form_id, $emails, $type, $message){
		   	emailIt($module_id, $form_id, $emails, $type, $message);
		   	redirect($this->agent->referrer());
		}

		public function messagePrivate(){
			$to_uid = $this->input->post('to_uid');
		   	$message = $this->input->post('message');
		   	messaged($_SESSION['user_id'], $message, 1, $to_uid, '', '');
		   	redirect($this->agent->referrer());
		}

		public function messageComment(){
		   	$form_id = $this->input->post('form_id');
		   	$module_id = $this->input->post('module_id');
		   	$message = $this->input->post('message');
		   	messaged($_SESSION['user_id'], $message, 0, '', $module_id, $form_id);
		   	redirect($this->agent->referrer());
		}

		public function prAPI($data){
	        /* API URL */
	        $url = 'http://srp.sunrise-resorts.com/api/prs/';
	        /* Init cURL resource */
	        $ch = curl_init($url);
	        /* pass encoded JSON string to the POST fields */
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, true);
	        /* set the content type json */
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	            'authtoken: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoiQ1JNIiwibmFtZSI6IkNSTSIsInBhc3N3b3JkIjpudWxsLCJBUElfVElNRSI6MTU5NDM4NTQ1OX0.AwiaqClDd8qVsddxLBQi_naM2bobOHFPjXDJRNY7S7U'
	        ));
	        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
	        /* execute request */
	        $result = curl_exec($ch);
	        $this->db->update('uniform_order', array('pr' => $result), "id = ".$data['form_id']);
	        /* close cURL resource */
	        curl_close($ch);
	        redirect($this->agent->referrer());
	    }

	    public function reservationAPI($hid){
	    	$hotelData = get_hotelServer_from_interfaces($hid,'PMS');
	        $ch = curl_init($hotelData['server_ip'].'?type='.$hotelData['db_name'].'&hotel='.$hotelData['db_driver']);

    		curl_setopt_array($ch, array(CURLOPT_RETURNTRANSFER => TRUE, CURLOPT_HTTPHEADER => array('authtoken: '. $hotelData['pass'])));
    		$result = curl_exec($ch);

    		echo $result;
				
	    }

	    function map_colnames($input) {
		    return isset($this->data['colnames'][$input]) ? $this->data['colnames'][$input] : $input;
  		}

  		function cleanData(&$str) {
		    if($str == 't') $str = 'TRUE';
		    if($str == 'f') $str = 'FALSE';
		    if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
      			$str = "'$str'";
		    }
		    if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';
  		}

	    function ExportFile($data) {
			$out = fopen("php://output", 'w');
 		 	$flag = false;
  			foreach($data as $row) {
    			if(!$flag) {
      				$firstline = array_map(array($this, 'map_colnames'), array_keys($row));
      				fputcsv($out, $firstline, ',', '"');
      				$flag = true;
    			}
    			array_walk($row, array($this, 'cleanData'));
    			fputcsv($out, array_values($row), ',', '"');
  			}
  			fclose($out);
  			exit;
		}

  	}

?>