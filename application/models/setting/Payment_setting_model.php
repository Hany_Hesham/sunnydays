<?php

  class Payment_setting_model extends CI_Model{

    public function get_payment_settings($t_attr, $custom_search='', $filter_count=''){
      $this->db->select('payment_setting.*, modules.name AS module_name');
      $this->db->join('modules','payment_setting.module_id = modules.id','left');
      $this->db->where('payment_setting.deleted', 0);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','payment_setting',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(payment_setting.id like "%'.$t_attr['search'].'%" OR payment_setting.name like "%'.$t_attr['search'].'%" OR payment_setting.limit like "%'.$t_attr['search'].'%" OR modules.name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("payment_setting");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("payment_setting")->result_array();
      }
    }

    public function get_all_payment_settings(){
      $this->db->select('payment_setting.*');
      $this->db->where('payment_setting.deleted', 0);
      return $this->db->count_all_results("payment_setting");
    }   

    public function add_payment_setting($data){
      $this->db->insert('payment_setting', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_payment_setting($payment_setting_id) {
      $this->db->select('payment_setting.*, modules.name AS module_name');
      $this->db->join('modules','payment_setting.module_id = modules.id','left');
      $this->db->where('payment_setting.id', $payment_setting_id);
      $this->db->where('payment_setting.deleted', 0);   
      $query = $this->db->get('payment_setting');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_payment_setting($data, $payment_setting_id) {
      $this->db->where('payment_setting.id', $payment_setting_id);   
      $this->db->update('payment_setting', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>