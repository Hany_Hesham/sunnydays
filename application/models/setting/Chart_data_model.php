<?php

  class Chart_data_model extends CI_Model{

    public function get_chart_datas($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('meta_data.*, hotels.hotel_name');
      $this->db->join('hotels','meta_data.hid = hotels.id','left');
      $this->db->where('meta_data.meta_keyword', 'chart_account');
      $this->db->where('meta_data.deleted', 0);
      $this->db->where_in('meta_data.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','meta_data',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(meta_data.id like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR meta_data.type_data like "%'.$t_attr['search'].'%" OR meta_data.rank like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("meta_data");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("meta_data")->result_array();
      }
    }

    public function get_all_chart_datas($uhotels){
      $this->db->select('meta_data.*');
      $this->db->where_in('meta_data.hid',$uhotels);
      $this->db->where('meta_data.meta_keyword', 'chart_account');
      $this->db->where('meta_data.deleted', 0);
      return $this->db->count_all_results("meta_data");
    }   

    public function add_chart_data($data){
      $this->db->insert('meta_data', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_chart_data($chart_data_id) {
      $this->db->select('meta_data.*, hotels.hotel_name, hotels.logo as h_logo');
      $this->db->join('hotels','meta_data.hid = hotels.id','left');
      $this->db->where('meta_data.id', $chart_data_id);
      $this->db->where('meta_data.deleted', 0);   
      $query = $this->db->get('meta_data');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_chart_data($data, $chart_data_id) {
      $this->db->where('meta_data.id', $chart_data_id);   
      $this->db->update('meta_data', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>