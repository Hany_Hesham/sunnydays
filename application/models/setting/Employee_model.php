<?php

  class Employee_model extends CI_Model{

    public function get_employees($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('employee.*, users.fullname, hotels.hotel_name, user_groups.name AS position_name');
      $this->db->join('users','employee.uid = users.id','left');
      $this->db->join('hotels','employee.hid = hotels.id','left');
      $this->db->join('user_groups','employee.position = user_groups.id','left');
      $this->db->where('employee.deleted', 0);
      $this->db->where_in('employee.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','employee',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(employee.id like "%'.$t_attr['search'].'%" OR employee.code like "%'.$t_attr['search'].'%" OR employee.name like "%'.$t_attr['search'].'%" OR employee.remarks like "%'.$t_attr['search'].'%" OR employee.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("employee");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("employee")->result_array();
      }
    }

    public function get_all_employees($uhotels){
      $this->db->select('employee.*');
      $this->db->where_in('employee.hid',$uhotels);
      $this->db->where('employee.deleted', 0);
      return $this->db->count_all_results("employee");
    }   

    public function add_employee($data){
      $this->db->insert('employee', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_employee($employee_id) {
      $this->db->select('employee.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, user_groups.name AS position_name');
      $this->db->join('users','employee.uid = users.id','left');
      $this->db->join('hotels','employee.hid = hotels.id','left');
      $this->db->join('user_groups','employee.position = user_groups.id','left');
      $this->db->where('employee.id', $employee_id);
      $this->db->where('employee.deleted', 0);   
      $query = $this->db->get('employee');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_employee($data, $employee_id) {
      $this->db->where('employee.id', $employee_id);   
      $this->db->update('employee', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>