<?php

  class Payment_types_model extends CI_Model{

    public function get_payment_typess($t_attr, $custom_search='', $filter_count=''){
      $this->db->select('meta_data.*');
      $this->db->where('meta_data.meta_keyword', 'payment_types');
      $this->db->where('meta_data.deleted', 0);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','meta_data',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(meta_data.id like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR meta_data.type_data like "%'.$t_attr['search'].'%" OR meta_data.rank like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("meta_data");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("meta_data")->result_array();
      }
    }

    public function get_all_payment_typess(){
      $this->db->select('meta_data.*');
      $this->db->where('meta_data.meta_keyword', 'payment_types');
      $this->db->where('meta_data.deleted', 0);
      return $this->db->count_all_results("meta_data");
    }   

    public function add_payment_types($data){
      $this->db->insert('meta_data', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_payment_types($payment_types_id) {
      $this->db->select('meta_data.*');
      $this->db->where('meta_data.id', $payment_types_id);
      $this->db->where('meta_data.deleted', 0);   
      $query = $this->db->get('meta_data');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_payment_types($data, $payment_types_id) {
      $this->db->where('meta_data.id', $payment_types_id);   
      $this->db->update('meta_data', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>