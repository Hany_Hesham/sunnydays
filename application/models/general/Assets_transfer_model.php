<?php

  class Assets_transfer_model extends CI_Model{

    public function get_assets_transfers($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('assets_transfer.*, users.fullname, from_hotels.hotel_name AS from_hotel_name, to_hotels.hotel_name AS to_hotel_name, from_departments.dep_name AS from_dep_name, to_departments.dep_name AS to_dep_name, meta_data.type_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','assets_transfer.uid = users.id','left');
      $this->db->join('hotels AS from_hotels','assets_transfer.from_hid = from_hotels.id','left');
      $this->db->join('departments AS from_departments','assets_transfer.from_dep_code = from_departments.id','left');
      $this->db->join('hotels AS to_hotels','assets_transfer.to_hid = to_hotels.id','left');
      $this->db->join('departments AS to_departments','assets_transfer.to_dep_code = to_departments.id','left');
      $this->db->join('meta_data','assets_transfer.type_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','assets_transfer.role_id = role_group.id','left');
      $this->db->join('status','assets_transfer.status = status.id','left');
      $this->db->where('assets_transfer.deleted', 0);
      $this->db->where('((assets_transfer.from_hid IN('.implode(',',$uhotels).') AND assets_transfer.from_dep_code IN('.implode(',',$udepartments).')) OR (assets_transfer.to_hid IN('.implode(',',$uhotels).') AND assets_transfer.to_dep_code IN('.implode(',',$udepartments).')))');
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','assets_transfer',$t_attr['module_id'],1);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(assets_transfer.id like "%'.$t_attr['search'].'%" OR assets_transfer.transfer_date like "%'.$t_attr['search'].'%" OR assets_transfer.reason like "%'.$t_attr['search'].'%" OR assets_transfer.remarks like "%'.$t_attr['search'].'%" OR assets_transfer.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR from_hotels.hotel_name like "%'.$t_attr['search'].'%"OR to_hotels.hotel_name like "%'.$t_attr['search'].'%" OR from_departments.dep_name like "%'.$t_attr['search'].'%" OR to_departments.dep_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("assets_transfer");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("assets_transfer")->result_array();
      }
    }

    public function get_all_assets_transfers($uhotels, $udepartments){
      $this->db->select('assets_transfer.*');
      $this->db->where('((assets_transfer.from_hid IN('.implode(',',$uhotels).') AND assets_transfer.from_dep_code IN('.implode(',',$udepartments).')) OR (assets_transfer.to_hid IN('.implode(',',$uhotels).') AND assets_transfer.to_dep_code IN('.implode(',',$udepartments).')))');
      $this->db->where('assets_transfer.deleted', 0);
      return $this->db->count_all_results("assets_transfer");
    }   

    public function add_assets_transfer($data){
      $this->db->insert('assets_transfer', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    public function add_assets_transfer_item($data){
      $this->db->insert('assets_transfer_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    } 

    function get_assets_transfer($assets_transfer_id) {
      $this->db->select('assets_transfer.*, users.fullname, from_hotels.hotel_name AS from_hotel_name, from_hotels.logo as from_h_logo, to_hotels.hotel_name AS to_hotel_name, to_hotels.logo as to_h_logo, from_departments.dep_name AS from_dep_name, to_departments.dep_name AS to_dep_name, meta_data.type_name, role_group.name AS role_name, status.status_name');
      $this->db->join('users','assets_transfer.uid = users.id','left');
      $this->db->join('hotels AS from_hotels','assets_transfer.from_hid = from_hotels.id','left');
      $this->db->join('departments AS from_departments','assets_transfer.from_dep_code = from_departments.id','left');
      $this->db->join('hotels AS to_hotels','assets_transfer.to_hid = to_hotels.id','left');
      $this->db->join('departments AS to_departments','assets_transfer.to_dep_code = to_departments.id','left');
      $this->db->join('meta_data','assets_transfer.type_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','assets_transfer.role_id = role_group.id','left');
      $this->db->join('status','assets_transfer.status = status.id','left');
      $this->db->where('assets_transfer.id', $assets_transfer_id);
      $this->db->where('assets_transfer.deleted', 0);   
      $query = $this->db->get('assets_transfer');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_assets_transfer_items($assets_transfer_id) {
      $this->db->select('assets_transfer_item.*');
      $this->db->where(array('assets_transfer_item.deleted'=>0,'assets_transfer_item.assets_transfer_id'=>$assets_transfer_id));
      return $this->db->get('assets_transfer_item')->result_array();
    }

    function edit_assets_transfer($data, $assets_transfer_id) {
      $this->db->where('assets_transfer.id', $assets_transfer_id);   
      $this->db->update('assets_transfer', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_assets_transfer_item($item_id) {
      $this->db->select('assets_transfer_item.*');
      $this->db->where(array('assets_transfer_item.deleted'=>0,'assets_transfer_item.id'=>$item_id));
      return $this->db->get('assets_transfer_item')->row_array();
    }

    function edit_assets_transfer_item($data, $item_id) {
      $this->db->where('assets_transfer_item.id', $item_id);   
      $this->db->update('assets_transfer_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>