<?php

  class Gate_model extends CI_Model{

    public function get_gates($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('gate.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','gate.uid = users.id','left');
      $this->db->join('hotels','gate.hid = hotels.id','left');
      $this->db->join('departments','gate.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','gate.role_id = role_group.id','left');
      $this->db->join('status','gate.status = status.id','left');
      $this->db->where('gate.deleted', 0);
      $this->db->where_in('gate.hid',$uhotels);
      $this->db->where_in('gate.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','gate',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(gate.id like "%'.$t_attr['search'].'%" OR gate.out_date like "%'.$t_attr['search'].'%" OR gate.address like "%'.$t_attr['search'].'%" OR gate.remarks like "%'.$t_attr['search'].'%" OR gate.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("gate");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("gate")->result_array();
      }
    }

    public function get_all_gates($uhotels, $udepartments){
      $this->db->select('gate.*');
      $this->db->where_in('gate.hid',$uhotels);
      $this->db->where_in('gate.dep_code',$udepartments);
      $this->db->where('gate.deleted', 0);
      return $this->db->count_all_results("gate");
    }   

    public function add_gate($data){
      $this->db->insert('gate', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_gate_item($data){
      $this->db->insert('gate_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_gate($gate_id) {
      $this->db->select('gate.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','gate.uid = users.id','left');
      $this->db->join('hotels','gate.hid = hotels.id','left');
      $this->db->join('departments','gate.dep_code = departments.id','left');
      $this->db->join('status','gate.status = status.id','left');
      $this->db->where('gate.id', $gate_id);
      $this->db->where('gate.deleted', 0);   
      $query = $this->db->get('gate');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_gate_items($gate_id) {
      $this->db->select('gate_item.*');
      $this->db->where(array('gate_item.deleted'=>0,'gate_item.gate_id'=>$gate_id));
      return $this->db->get('gate_item')->result_array();
    }

    function edit_gate($data, $gate_id) {
      $this->db->where('gate.id', $gate_id);   
      $this->db->update('gate', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_gate_item($item_id) {
      $this->db->select('gate_item.*');
      $this->db->where(array('gate_item.deleted'=>0,'gate_item.id'=>$item_id));
      return $this->db->get('gate_item')->row_array();
    }

    function edit_gate_item($data, $item_id) {
      $this->db->where('gate_item.id', $item_id);   
      $this->db->update('gate_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>