<?php

  class Salary_advance_model extends CI_Model{

    public function get_salary_advances($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('salary_advance.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','salary_advance.uid = users.id','left');
      $this->db->join('hotels','salary_advance.hid = hotels.id','left');
      $this->db->join('departments','salary_advance.dep_code = departments.id','left');
      $this->db->join('user_groups','salary_advance.position = user_groups.id','left');
      $this->db->join('api_departments','salary_advance.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','salary_advance.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups AS role_group','salary_advance.role_id = role_group.id','left');
      $this->db->join('status','salary_advance.status = status.id','left');
      $this->db->where('salary_advance.deleted', 0);
      $this->db->where_in('salary_advance.hid',$uhotels);
      $this->db->where_in('salary_advance.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','salary_advance',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(salary_advance.id like "%'.$t_attr['search'].'%" OR salary_advance.clock_no like "%'.$t_attr['search'].'%" OR salary_advance.name like "%'.$t_attr['search'].'%" OR salary_advance.remarks like "%'.$t_attr['search'].'%" OR salary_advance.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("salary_advance");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("salary_advance")->result_array();
      }
    }

    public function get_all_salary_advances($uhotels, $udepartments){
      $this->db->select('salary_advance.*');
      $this->db->where_in('salary_advance.hid',$uhotels);
      $this->db->where_in('salary_advance.dep_code',$udepartments);
      $this->db->where('salary_advance.deleted', 0);
      return $this->db->count_all_results("salary_advance");
    }   

    public function add_salary_advance($data){
      $this->db->insert('salary_advance', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_salary_advance($salary_advance_id) {
      $this->db->select('salary_advance.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, status.status_name');
      $this->db->join('users','salary_advance.uid = users.id','left');
      $this->db->join('hotels','salary_advance.hid = hotels.id','left');
      $this->db->join('departments','salary_advance.dep_code = departments.id','left');
      $this->db->join('user_groups','salary_advance.position = user_groups.id','left');
      $this->db->join('api_departments','salary_advance.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','salary_advance.pos_id = api_user_groups.id','left');
      $this->db->join('status','salary_advance.status = status.id','left');
      $this->db->where('salary_advance.id', $salary_advance_id);
      $this->db->where('salary_advance.deleted', 0);   
      $query = $this->db->get('salary_advance');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_salary_advance($data, $salary_advance_id) {
      $this->db->where('salary_advance.id', $salary_advance_id);   
      $this->db->update('salary_advance', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>