<?php

  class Del_outgoing_model extends CI_Model{

    public function get_del_outgoings($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('del_outgoing.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','del_outgoing.uid = users.id','left');
      $this->db->join('hotels','del_outgoing.hid = hotels.id','left');
      $this->db->join('departments','del_outgoing.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','del_outgoing.role_id = role_group.id','left');
      $this->db->join('status','del_outgoing.status = status.id','left');
      $this->db->where('del_outgoing.deleted', 0);
      $this->db->where_in('del_outgoing.hid',$uhotels);
      $this->db->where_in('del_outgoing.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','del_outgoing',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(del_outgoing.id like "%'.$t_attr['search'].'%" OR del_outgoing.remarks like "%'.$t_attr['search'].'%" OR del_outgoing.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("del_outgoing");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("del_outgoing")->result_array();
      }
    }

    public function get_all_del_outgoings($uhotels, $udepartments){
      $this->db->select('del_outgoing.*');
      $this->db->where_in('del_outgoing.hid',$uhotels);
      $this->db->where_in('del_outgoing.dep_code',$udepartments);
      $this->db->where('del_outgoing.deleted', 0);
      return $this->db->count_all_results("del_outgoing");
    }   

    public function get_outgoing_items($ids){
      $ids_form = str_replace(',,', ',', $ids);
      $ids_form = str_replace(',,', ',', $ids_form);
      if ($ids_form[0]==','){$new_ids = ltrim($ids_form, $ids_form[0]); }else{$new_ids = $ids_form;}
      $query ='SELECT id AS item_id,quantity,description,remarks,recieved,unit
        FROM outgoing_item
        where id IN('.$new_ids.')  AND quantity-recieved > 0 
        AND outgoing_item.deleted = 0';
      return $this->db->query($query)->result_array();
    }

    public function get_item_qty($outgoing_id,$item_id){
      $query ='SELECT sum(recieved) AS total_recieved FROM del_outgoing_item
        where del_outgoing_item.outgoing_id = '.$outgoing_id.' AND del_outgoing_item.item_id = '.$item_id.'
        AND del_outgoing_item.deleted = 0';
      return $this->db->query($query)->row_array();
    }

    public function update_items_recieved($outgoing_id,$item_id,$data){
      $query = 'UPDATE outgoing_item SET outgoing_item.recieved = "'.$data['recieved'].'" 
        WHERE outgoing_id = "'.$outgoing_id.'" AND outgoing_item.id = "'.$item_id.'" ';
      $records = $this->db->query($query);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    } 

    public function update_items_delivered($outgoing_id,$item_id,$delivered){
      if ($delivered == 1) {
        $data=['delivered'=>$delivered,'delivered_date'=> date("Y-m-d")];
      }else{
        $data=['delivered'=>$delivered,'delivered_date'=> ''];
      }
      $this->db->where(array('outgoing_item.outgoing_id'=>$outgoing_id,'outgoing_item.id'=>$item_id));   
      $this->db->update('outgoing_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    } 

    public function add_del_outgoing($data){
      $this->db->insert('del_outgoing', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_del_outgoing_item($data){
      $this->db->insert('del_outgoing_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_del_outgoing($del_outgoing_id) {
      $this->db->select('del_outgoing.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','del_outgoing.uid = users.id','left');
      $this->db->join('hotels','del_outgoing.hid = hotels.id','left');
      $this->db->join('departments','del_outgoing.dep_code = departments.id','left');
      $this->db->join('status','del_outgoing.status = status.id','left');
      $this->db->where('del_outgoing.id', $del_outgoing_id);
      $this->db->where('del_outgoing.deleted', 0);   
      $query = $this->db->get('del_outgoing');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_del_outgoing_items($del_outgoing_id) {
      $this->db->select('del_outgoing_item.*, outgoing_item.quantity, outgoing_item.unit, outgoing_item.description');
      $this->db->join('outgoing_item','del_outgoing_item.item_id = outgoing_item.id','left');
      $this->db->where(array('del_outgoing_item.deleted'=>0,'del_outgoing_item.del_outgoing_id'=>$del_outgoing_id));
      return $this->db->get('del_outgoing_item')->result_array();
    }

    function edit_del_outgoing($data, $del_outgoing_id) {
      $this->db->where('del_outgoing.id', $del_outgoing_id);   
      $this->db->update('del_outgoing', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_del_outgoing_item($item_id) {
      $this->db->select('del_outgoing_item.*');
      $this->db->where(array('del_outgoing_item.deleted'=>0,'del_outgoing_item.id'=>$item_id));
      return $this->db->get('del_outgoing_item')->row_array();
    }

    function edit_del_outgoing_item($data, $item_id) {
      $this->db->where('del_outgoing_item.id', $item_id);   
      $this->db->update('del_outgoing_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>