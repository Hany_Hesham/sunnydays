<?php

  class Complimentary_model extends CI_Model{

    public function get_complimentarys($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('complimentary.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','complimentary.uid = users.id','left');
      $this->db->join('hotels','complimentary.hid = hotels.id','left');
      $this->db->join('departments','complimentary.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','complimentary.role_id = role_group.id','left');
      $this->db->join('status','complimentary.status = status.id','left');
      $this->db->where('complimentary.deleted', 0);
      $this->db->where_in('complimentary.hid',$uhotels);
      $this->db->where_in('complimentary.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','complimentary',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(complimentary.id like "%'.$t_attr['search'].'%" OR complimentary.conf_no like "%'.$t_attr['search'].'%" OR complimentary.name like "%'.$t_attr['search'].'%" OR complimentary.arrival like "%'.$t_attr['search'].'%" OR complimentary.departure like "%'.$t_attr['search'].'%" OR complimentary.company like "%'.$t_attr['search'].'%" OR complimentary.room_type like "%'.$t_attr['search'].'%" OR complimentary.reasons like "%'.$t_attr['search'].'%" OR complimentary.payment like "%'.$t_attr['search'].'%" OR complimentary.remarks like "%'.$t_attr['search'].'%" OR complimentary.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("complimentary");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("complimentary")->result_array();
      }
    }

    public function get_all_complimentarys($uhotels, $udepartments){
      $this->db->select('complimentary.*');
      $this->db->where_in('complimentary.hid',$uhotels);
      $this->db->where_in('complimentary.dep_code',$udepartments);
      $this->db->where('complimentary.deleted', 0);
      return $this->db->count_all_results("complimentary");
    }   

    public function add_complimentary($data){
      $this->db->insert('complimentary', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_complimentary($complimentary_id) {
      $this->db->select('complimentary.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','complimentary.uid = users.id','left');
      $this->db->join('hotels','complimentary.hid = hotels.id','left');
      $this->db->join('departments','complimentary.dep_code = departments.id','left');
      $this->db->join('status','complimentary.status = status.id','left');
      $this->db->where('complimentary.id', $complimentary_id);
      $this->db->where('complimentary.deleted', 0);   
      $query = $this->db->get('complimentary');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_complimentary($data, $complimentary_id) {
      $this->db->where('complimentary.id', $complimentary_id);   
      $this->db->update('complimentary', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>