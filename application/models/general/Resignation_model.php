<?php

  class Resignation_model extends CI_Model{

    public function get_resignations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('resignation.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','resignation.uid = users.id','left');
      $this->db->join('hotels','resignation.hid = hotels.id','left');
      $this->db->join('departments','resignation.dep_code = departments.id','left');
      $this->db->join('user_groups','resignation.position = user_groups.id','left');
      $this->db->join('user_groups AS role_group','resignation.role_id = role_group.id','left');
      $this->db->join('status','resignation.status = status.id','left');
      $this->db->where('resignation.deleted', 0);
      $this->db->where_in('resignation.hid',$uhotels);
      $this->db->where_in('resignation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','resignation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(resignation.id like "%'.$t_attr['search'].'%" OR resignation.clock_no like "%'.$t_attr['search'].'%" OR resignation.name like "%'.$t_attr['search'].'%" OR resignation.remarks like "%'.$t_attr['search'].'%" OR resignation.hiring_date like "%'.$t_attr['search'].'%" OR resignation.last_date like "%'.$t_attr['search'].'%" OR resignation.opinion like "%'.$t_attr['search'].'%" OR resignation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("resignation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("resignation")->result_array();
      }
    }

    public function get_all_resignations($uhotels, $udepartments){
      $this->db->select('resignation.*');
      $this->db->where_in('resignation.hid',$uhotels);
      $this->db->where_in('resignation.dep_code',$udepartments);
      $this->db->where('resignation.deleted', 0);
      return $this->db->count_all_results("resignation");
    }   

    public function add_resignation($data){
      $this->db->insert('resignation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_resignation($resignation_id) {
      $this->db->select('resignation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, status.status_name');
      $this->db->join('users','resignation.uid = users.id','left');
      $this->db->join('hotels','resignation.hid = hotels.id','left');
      $this->db->join('departments','resignation.dep_code = departments.id','left');
      $this->db->join('user_groups','resignation.position = user_groups.id','left');
      $this->db->join('status','resignation.status = status.id','left');
      $this->db->where('resignation.id', $resignation_id);
      $this->db->where('resignation.deleted', 0);   
      $query = $this->db->get('resignation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_resignation($data, $resignation_id) {
      $this->db->where('resignation.id', $resignation_id);   
      $this->db->update('resignation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>