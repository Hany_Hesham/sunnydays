<?php

  class Vacation_model extends CI_Model{

    public function get_vacations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('vacation.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, meta_data.type_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','vacation.uid = users.id','left');
      $this->db->join('hotels','vacation.hid = hotels.id','left');
      $this->db->join('departments','vacation.dep_code = departments.id','left');
      $this->db->join('user_groups','vacation.position = user_groups.id','left');
      $this->db->join('api_departments','vacation.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','vacation.pos_id = api_user_groups.id','left');
      $this->db->join('meta_data','vacation.type_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','vacation.role_id = role_group.id','left');
      $this->db->join('status','vacation.status = status.id','left');
      $this->db->where('vacation.deleted', 0);
      $this->db->where_in('vacation.hid',$uhotels);
      $this->db->where_in('vacation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','vacation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(vacation.id like "%'.$t_attr['search'].'%" OR vacation.clock_no like "%'.$t_attr['search'].'%" OR vacation.name like "%'.$t_attr['search'].'%" OR vacation.from_date like "%'.$t_attr['search'].'%" OR vacation.to_date like "%'.$t_attr['search'].'%" OR vacation.hire_date like "%'.$t_attr['search'].'%" OR vacation.address like "%'.$t_attr['search'].'%" OR vacation.remarks like "%'.$t_attr['search'].'%" OR vacation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("vacation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("vacation")->result_array();
      }
    }

    public function get_all_vacations($uhotels, $udepartments){
      $this->db->select('vacation.*');
      $this->db->where_in('vacation.hid',$uhotels);
      $this->db->where_in('vacation.dep_code',$udepartments);
      $this->db->where('vacation.deleted', 0);
      return $this->db->count_all_results("vacation");
    }   

    public function add_vacation($data){
      $this->db->insert('vacation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_vacation($vacation_id) {
      $this->db->select('vacation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, meta_data.type_name, status.status_name');
      $this->db->join('users','vacation.uid = users.id','left');
      $this->db->join('hotels','vacation.hid = hotels.id','left');
      $this->db->join('departments','vacation.dep_code = departments.id','left');
      $this->db->join('user_groups','vacation.position = user_groups.id','left');
      $this->db->join('api_departments','vacation.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','vacation.pos_id = api_user_groups.id','left');
      $this->db->join('meta_data','vacation.type_id = meta_data.id','left');
      $this->db->join('status','vacation.status = status.id','left');
      $this->db->where('vacation.id', $vacation_id);
      $this->db->where('vacation.deleted', 0);   
      $query = $this->db->get('vacation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_vacation($data, $vacation_id) {
      $this->db->where('vacation.id', $vacation_id);   
      $this->db->update('vacation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>