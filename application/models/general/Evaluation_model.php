<?php

  class Evaluation_model extends CI_Model{

    public function get_evaluations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('evaluation.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, meta_data.type_name AS recommendation_name, meta_data1.type_name AS evaluation_type, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','evaluation.uid = users.id','left');
      $this->db->join('hotels','evaluation.hid = hotels.id','left');
      $this->db->join('departments','evaluation.dep_code = departments.id','left');
      $this->db->join('user_groups','evaluation.position = user_groups.id','left');
      $this->db->join('meta_data','evaluation.recommendation = meta_data.id','left');
      $this->db->join('meta_data AS meta_data1','evaluation.type = meta_data1.id','left');
      $this->db->join('user_groups AS role_group','evaluation.role_id = role_group.id','left');
      $this->db->join('status','evaluation.status = status.id','left');
      $this->db->where('evaluation.deleted', 0);
      $this->db->where_in('evaluation.hid',$uhotels);
      $this->db->where_in('evaluation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','evaluation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(evaluation.id like "%'.$t_attr['search'].'%" OR evaluation.section like "%'.$t_attr['search'].'%" OR evaluation.name like "%'.$t_attr['search'].'%" OR evaluation.hire_date like "%'.$t_attr['search'].'%" OR evaluation.promotion_date like "%'.$t_attr['search'].'%" OR evaluation.education like "%'.$t_attr['search'].'%" OR evaluation.best_performance like "%'.$t_attr['search'].'%" OR evaluation.training like "%'.$t_attr['search'].'%" OR evaluation.improvement like "%'.$t_attr['search'].'%" OR evaluation.remarks like "%'.$t_attr['search'].'%" OR evaluation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR meta_data1.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("evaluation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("evaluation")->result_array();
      }
    }

    public function get_all_evaluations($uhotels, $udepartments){
      $this->db->select('evaluation.*');
      $this->db->where_in('evaluation.hid',$uhotels);
      $this->db->where_in('evaluation.dep_code',$udepartments);
      $this->db->where('evaluation.deleted', 0);
      return $this->db->count_all_results("evaluation");
    }   

    public function add_evaluation($data){
      $this->db->insert('evaluation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_evaluation_item($data){
      $this->db->insert('evaluation_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    } 

    public function add_evaluation_itemd($data){
      $this->db->insert('evaluation_itemd', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_evaluation($evaluation_id) {
      $this->db->select('evaluation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, meta_data.type_name AS recommendation_name, meta_data1.type_name AS evaluation_type, status.status_name');
      $this->db->join('users','evaluation.uid = users.id','left');
      $this->db->join('hotels','evaluation.hid = hotels.id','left');
      $this->db->join('departments','evaluation.dep_code = departments.id','left');
      $this->db->join('user_groups','evaluation.position = user_groups.id','left');
      $this->db->join('meta_data','evaluation.recommendation = meta_data.id','left');
      $this->db->join('meta_data AS meta_data1','evaluation.type = meta_data1.id','left');
      $this->db->join('status','evaluation.status = status.id','left');
      $this->db->where('evaluation.id', $evaluation_id);
      $this->db->where('evaluation.deleted', 0);   
      $query = $this->db->get('evaluation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_evaluation_items($evaluation_id) {
      $this->db->select('evaluation_item.*, meta_data.type_name AS area_name, meta_data1.type_name AS evaluation_name');
      $this->db->join('meta_data','evaluation_item.areaid = meta_data.id','left');
      $this->db->join('meta_data AS meta_data1','evaluation_item.evaluation = meta_data1.id','left');
      $this->db->where(array('evaluation_item.deleted'=>0,'evaluation_item.evaluation_id'=>$evaluation_id));
      return $this->db->get('evaluation_item')->result_array();
    }

    function get_evaluation_itemsd($evaluation_id) {
      $this->db->select('evaluation_itemd.*');
      $this->db->where(array('evaluation_itemd.deleted'=>0,'evaluation_itemd.evaluation_id'=>$evaluation_id));
      return $this->db->get('evaluation_itemd')->result_array();
    }

    function edit_evaluation($data, $evaluation_id) {
      $this->db->where('evaluation.id', $evaluation_id);   
      $this->db->update('evaluation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_evaluation_item($item_id) {
      $this->db->select('evaluation_item.*, meta_data.type_name AS area_name, meta_data1.type_name AS evaluation_name');
      $this->db->join('meta_data','evaluation_item.areaid = meta_data.id','left');
      $this->db->join('meta_data AS meta_data1','evaluation_item.evaluation = meta_data1.id','left');
      $this->db->where(array('evaluation_item.deleted'=>0,'evaluation_item.id'=>$item_id));
      return $this->db->get('evaluation_item')->row_array();
    }

    function get_evaluation_itemd($item_id) {
      $this->db->select('evaluation_itemd.*');
      $this->db->where(array('evaluation_itemd.deleted'=>0,'evaluation_itemd.id'=>$item_id));
      return $this->db->get('evaluation_itemd')->row_array();
    }

    function edit_evaluation_item($data, $item_id) {
      $this->db->where('evaluation_item.id', $item_id);   
      $this->db->update('evaluation_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }

    function edit_evaluation_itemd($data, $item_id) {
      $this->db->where('evaluation_itemd.id', $item_id);   
      $this->db->update('evaluation_itemd', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>