<?php

  class Outgoing_model extends CI_Model{

    public function get_outgoings($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('outgoing.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','outgoing.uid = users.id','left');
      $this->db->join('hotels','outgoing.hid = hotels.id','left');
      $this->db->join('departments','outgoing.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','outgoing.role_id = role_group.id','left');
      $this->db->join('status','outgoing.status = status.id','left');
      $this->db->where('outgoing.deleted', 0);
      $this->db->where_in('outgoing.hid',$uhotels);
      $this->db->where_in('outgoing.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','outgoing',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(outgoing.id like "%'.$t_attr['search'].'%" OR outgoing.out_date like "%'.$t_attr['search'].'%" OR outgoing.return_date like "%'.$t_attr['search'].'%" OR outgoing.address like "%'.$t_attr['search'].'%" OR outgoing.resson_id like "%'.$t_attr['search'].'%" OR outgoing.out_with like "%'.$t_attr['search'].'%" OR outgoing.remarks like "%'.$t_attr['search'].'%" OR outgoing.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("outgoing");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("outgoing")->result_array();
      }
    }

    public function get_all_outgoings($uhotels, $udepartments){
      $this->db->select('outgoing.*');
      $this->db->where_in('outgoing.hid',$uhotels);
      $this->db->where_in('outgoing.dep_code',$udepartments);
      $this->db->where('outgoing.deleted', 0);
      return $this->db->count_all_results("outgoing");
    }   

    public function add_outgoing($data){
      $this->db->insert('outgoing', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_outgoing_item($data){
      $this->db->insert('outgoing_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_outgoing($outgoing_id) {
      $this->db->select('outgoing.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','outgoing.uid = users.id','left');
      $this->db->join('hotels','outgoing.hid = hotels.id','left');
      $this->db->join('departments','outgoing.dep_code = departments.id','left');
      $this->db->join('status','outgoing.status = status.id','left');
      $this->db->where('outgoing.id', $outgoing_id);
      $this->db->where('outgoing.deleted', 0);   
      $query = $this->db->get('outgoing');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_outgoing_items($outgoing_id) {
      $this->db->select('outgoing_item.*');
      $this->db->where(array('outgoing_item.deleted'=>0,'outgoing_item.outgoing_id'=>$outgoing_id));
      return $this->db->get('outgoing_item')->result_array();
    }

    function edit_outgoing($data, $outgoing_id) {
      $this->db->where('outgoing.id', $outgoing_id);   
      $this->db->update('outgoing', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_outgoing_item($item_id) {
      $this->db->select('outgoing_item.*');
      $this->db->where(array('outgoing_item.deleted'=>0,'outgoing_item.id'=>$item_id));
      return $this->db->get('outgoing_item')->row_array();
    }

    function edit_outgoing_item($data, $item_id) {
      $this->db->where('outgoing_item.id', $item_id);   
      $this->db->update('outgoing_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }

    function get_outgoing_deliveries($outgoing_id){
      $this->db->where('del_outgoing.outgoing_id',$outgoing_id);
      return $this->db->get_where('del_outgoing',array('del_outgoing.deleted'=>0))->result_array();
    } 
    
  }

?>