<?php

  class Training_application_model extends CI_Model{

    public function get_training_applications($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('training_application.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color, gender.type_name AS gender_name, marital_status.type_name AS marital_name');
      $this->db->join('users','training_application.uid = users.id','left');
      $this->db->join('hotels','training_application.hid = hotels.id','left');
      $this->db->join('departments','training_application.dep_code = departments.id','left');
      $this->db->join('meta_data AS gender','training_application.gender = gender.id','left');
      $this->db->join('meta_data AS marital_status','training_application.marital = marital_status.id','left');
      $this->db->join('user_groups AS role_group','training_application.role_id = role_group.id','left');
      $this->db->join('status','training_application.status = status.id','left');
      $this->db->where('training_application.deleted', 0);
      $this->db->where_in('training_application.hid',$uhotels);
      $this->db->where_in('training_application.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','training_application',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(training_application.id like "%'.$t_attr['search'].'%" OR training_application.name like "%'.$t_attr['search'].'%" OR training_application.birth_location like "%'.$t_attr['search'].'%" OR training_application.nationality like "%'.$t_attr['search'].'%" OR training_application.religion like "%'.$t_attr['search'].'%" OR training_application.card_id like "%'.$t_attr['search'].'%" OR training_application.address like "%'.$t_attr['search'].'%" OR training_application.telephone like "%'.$t_attr['search'].'%" OR training_application.address2 like "%'.$t_attr['search'].'%" OR training_application.institute like "%'.$t_attr['search'].'%" OR training_application.birth_date like "%'.$t_attr['search'].'%" OR training_application.starting_date like "%'.$t_attr['search'].'%" OR training_application.remarks like "%'.$t_attr['search'].'%" OR training_application.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR gender.type_name like "%'.$t_attr['search'].'%" OR marital_status.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("training_application");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("training_application")->result_array();
      }
    }

    public function get_all_training_applications($uhotels, $udepartments){
      $this->db->select('training_application.*');
      $this->db->where_in('training_application.hid',$uhotels);
      $this->db->where_in('training_application.dep_code',$udepartments);
      $this->db->where('training_application.deleted', 0);
      return $this->db->count_all_results("training_application");
    }   

    public function add_training_application($data){
      $this->db->insert('training_application', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_training_application($training_application_id) {
      $this->db->select('training_application.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name, gender.type_name AS gender_name, marital_status.type_name AS marital_name');
      $this->db->join('users','training_application.uid = users.id','left');
      $this->db->join('hotels','training_application.hid = hotels.id','left');
      $this->db->join('departments','training_application.dep_code = departments.id','left');
      $this->db->join('meta_data AS gender','training_application.gender = gender.id','left');
      $this->db->join('meta_data AS marital_status','training_application.marital = marital_status.id','left');
      $this->db->join('status','training_application.status = status.id','left');
      $this->db->where('training_application.id', $training_application_id);
      $this->db->where('training_application.deleted', 0);   
      $query = $this->db->get('training_application');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_training_application($data, $training_application_id) {
      $this->db->where('training_application.id', $training_application_id);   
      $this->db->update('training_application', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>