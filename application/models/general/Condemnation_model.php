<?php

  class Condemnation_model extends CI_Model{

    public function get_condemnations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('condemnation.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','condemnation.uid = users.id','left');
      $this->db->join('hotels','condemnation.hid = hotels.id','left');
      $this->db->join('departments','condemnation.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','condemnation.role_id = role_group.id','left');
      $this->db->join('status','condemnation.status = status.id','left');
      $this->db->where('condemnation.deleted', 0);
      $this->db->where_in('condemnation.hid',$uhotels);
      $this->db->where_in('condemnation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','condemnation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(condemnation.id like "%'.$t_attr['search'].'%" OR condemnation.reason like "%'.$t_attr['search'].'%" OR condemnation.remarks like "%'.$t_attr['search'].'%" OR condemnation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("condemnation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("condemnation")->result_array();
      }
    }

    public function get_all_condemnations($uhotels, $udepartments){
      $this->db->select('condemnation.*');
      $this->db->where_in('condemnation.hid',$uhotels);
      $this->db->where_in('condemnation.dep_code',$udepartments);
      $this->db->where('condemnation.deleted', 0);
      return $this->db->count_all_results("condemnation");
    }   

    public function add_condemnation($data){
      $this->db->insert('condemnation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_condemnation_item($data){
      $this->db->insert('condemnation_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_condemnation($condemnation_id) {
      $this->db->select('condemnation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','condemnation.uid = users.id','left');
      $this->db->join('hotels','condemnation.hid = hotels.id','left');
      $this->db->join('departments','condemnation.dep_code = departments.id','left');
      $this->db->join('status','condemnation.status = status.id','left');
      $this->db->where('condemnation.id', $condemnation_id);
      $this->db->where('condemnation.deleted', 0);   
      $query = $this->db->get('condemnation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_condemnation_items($condemnation_id) {
      $this->db->select('condemnation_item.*');
      $this->db->where(array('condemnation_item.deleted'=>0,'condemnation_item.condemnation_id'=>$condemnation_id));
      return $this->db->get('condemnation_item')->result_array();
    }

    function edit_condemnation($data, $condemnation_id) {
      $this->db->where('condemnation.id', $condemnation_id);   
      $this->db->update('condemnation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_condemnation_item($item_id) {
      $this->db->select('condemnation_item.*');
      $this->db->where(array('condemnation_item.deleted'=>0,'condemnation_item.id'=>$item_id));
      return $this->db->get('condemnation_item')->row_array();
    }

    function edit_condemnation_item($data, $item_id) {
      $this->db->where('condemnation_item.id', $item_id);   
      $this->db->update('condemnation_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>