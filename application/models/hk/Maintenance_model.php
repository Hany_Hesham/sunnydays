<?php

  class Maintenance_model extends CI_Model{

    public function get_maintenances($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('maintenance.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','maintenance.uid = users.id','left');
      $this->db->join('hotels','maintenance.hid = hotels.id','left');
      $this->db->join('departments','maintenance.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','maintenance.role_id = role_group.id','left');
      $this->db->join('status','maintenance.status = status.id','left');
      $this->db->where('maintenance.deleted', 0);
      $this->db->where_in('maintenance.hid',$uhotels);
      $this->db->where_in('maintenance.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','maintenance',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(maintenance.id like "%'.$t_attr['search'].'%" OR maintenance.workshop like "%'.$t_attr['search'].'%" OR maintenance.del_date like "%'.$t_attr['search'].'%" OR maintenance.remarks like "%'.$t_attr['search'].'%" OR maintenance.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("maintenance");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("maintenance")->result_array();
      }
    }

    public function get_all_maintenances($uhotels, $udepartments){
      $this->db->select('maintenance.*');
      $this->db->where_in('maintenance.hid',$uhotels);
      $this->db->where_in('maintenance.dep_code',$udepartments);
      $this->db->where('maintenance.deleted', 0);
      return $this->db->count_all_results("maintenance");
    }   

    public function add_maintenance($data){
      $this->db->insert('maintenance', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_maintenance_item($data){
      $this->db->insert('maintenance_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_maintenance($maintenance_id) {
      $this->db->select('maintenance.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','maintenance.uid = users.id','left');
      $this->db->join('hotels','maintenance.hid = hotels.id','left');
      $this->db->join('departments','maintenance.dep_code = departments.id','left');
      $this->db->join('status','maintenance.status = status.id','left');
      $this->db->where('maintenance.id', $maintenance_id);
      $this->db->where('maintenance.deleted', 0);   
      $query = $this->db->get('maintenance');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_maintenance_items($maintenance_id) {
      $this->db->select('maintenance_item.*');
      $this->db->where(array('maintenance_item.deleted'=>0,'maintenance_item.maintenance_id'=>$maintenance_id));
      return $this->db->get('maintenance_item')->result_array();
    }

    function edit_maintenance($data, $maintenance_id) {
      $this->db->where('maintenance.id', $maintenance_id);   
      $this->db->update('maintenance', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_maintenance_item($item_id) {
      $this->db->select('maintenance_item.*');
      $this->db->where(array('maintenance_item.deleted'=>0,'maintenance_item.id'=>$item_id));
      return $this->db->get('maintenance_item')->row_array();
    }

    function edit_maintenance_item($data, $item_id) {
      $this->db->where('maintenance_item.id', $item_id);   
      $this->db->update('maintenance_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>