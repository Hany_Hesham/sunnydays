<?php

  class Damage_model extends CI_Model{

    public function get_damages($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('damage.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','damage.uid = users.id','left');
      $this->db->join('hotels','damage.hid = hotels.id','left');
      $this->db->join('departments','damage.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','damage.role_id = role_group.id','left');
      $this->db->join('status','damage.status = status.id','left');
      $this->db->where('damage.deleted', 0);
      $this->db->where_in('damage.hid',$uhotels);
      $this->db->where_in('damage.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','damage',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(damage.id like "%'.$t_attr['search'].'%" OR damage.date like "%'.$t_attr['search'].'%" OR damage.room like "%'.$t_attr['search'].'%" OR damage.item like "%'.$t_attr['search'].'%" OR damage.area like "%'.$t_attr['search'].'%" OR damage.damage like "%'.$t_attr['search'].'%" OR damage.remarks like "%'.$t_attr['search'].'%" OR damage.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("damage");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("damage")->result_array();
      }
    }

    public function get_all_damages($uhotels, $udepartments){
      $this->db->select('damage.*');
      $this->db->where_in('damage.hid',$uhotels);
      $this->db->where_in('damage.dep_code',$udepartments);
      $this->db->where('damage.deleted', 0);
      return $this->db->count_all_results("damage");
    }   

    public function add_damage($data){
      $this->db->insert('damage', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_damage($damage_id) {
      $this->db->select('damage.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','damage.uid = users.id','left');
      $this->db->join('hotels','damage.hid = hotels.id','left');
      $this->db->join('departments','damage.dep_code = departments.id','left');
      $this->db->join('status','damage.status = status.id','left');
      $this->db->where('damage.id', $damage_id);
      $this->db->where('damage.deleted', 0);   
      $query = $this->db->get('damage');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_damage($data, $damage_id) {
      $this->db->where('damage.id', $damage_id);   
      $this->db->update('damage', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>