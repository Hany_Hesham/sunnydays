<?php

  class Lost_model extends CI_Model{

    public function get_losts($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('lost.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','lost.uid = users.id','left');
      $this->db->join('hotels','lost.hid = hotels.id','left');
      $this->db->join('departments','lost.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','lost.role_id = role_group.id','left');
      $this->db->join('status','lost.status = status.id','left');
      $this->db->where('lost.deleted', 0);
      $this->db->where_in('lost.hid',$uhotels);
      $this->db->where_in('lost.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','lost',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(lost.id like "%'.$t_attr['search'].'%" OR lost.name like "%'.$t_attr['search'].'%" OR lost.item like "%'.$t_attr['search'].'%" OR lost.remarks like "%'.$t_attr['search'].'%" OR lost.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("lost");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("lost")->result_array();
      }
    }

    public function get_all_losts($uhotels, $udepartments){
      $this->db->select('lost.*');
      $this->db->where_in('lost.hid',$uhotels);
      $this->db->where_in('lost.dep_code',$udepartments);
      $this->db->where('lost.deleted', 0);
      return $this->db->count_all_results("lost");
    }   

    public function add_lost($data){
      $this->db->insert('lost', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_lost($lost_id) {
      $this->db->select('lost.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','lost.uid = users.id','left');
      $this->db->join('hotels','lost.hid = hotels.id','left');
      $this->db->join('departments','lost.dep_code = departments.id','left');
      $this->db->join('status','lost.status = status.id','left');
      $this->db->where('lost.id', $lost_id);
      $this->db->where('lost.deleted', 0);   
      $query = $this->db->get('lost');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_lost($data, $lost_id) {
      $this->db->where('lost.id', $lost_id);   
      $this->db->update('lost', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>