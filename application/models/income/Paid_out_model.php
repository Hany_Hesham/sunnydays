<?php

  class Paid_out_model extends CI_Model{

    public function get_paid_outs($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('paid_out.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','paid_out.uid = users.id','left');
      $this->db->join('hotels','paid_out.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','paid_out.role_id = role_group.id','left');
      $this->db->join('status','paid_out.status = status.id','left');
      $this->db->where('paid_out.deleted', 0);
      $this->db->where_in('paid_out.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','paid_out',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(paid_out.id like "%'.$t_attr['search'].'%" OR paid_out.name like "%'.$t_attr['search'].'%" OR paid_out.payment_date like "%'.$t_attr['search'].'%" OR paid_out.company like "%'.$t_attr['search'].'%" OR paid_out.payment_method like "%'.$t_attr['search'].'%" OR paid_out.arrival like "%'.$t_attr['search'].'%" OR paid_out.departure like "%'.$t_attr['search'].'%" OR paid_out.room like "%'.$t_attr['search'].'%" OR paid_out.nationality like "%'.$t_attr['search'].'%" OR paid_out.reason like "%'.$t_attr['search'].'%" OR paid_out.remarks like "%'.$t_attr['search'].'%" OR paid_out.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("paid_out");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("paid_out")->result_array();
      }
    }

    public function get_all_paid_outs($uhotels){
      $this->db->select('paid_out.*');
      $this->db->where_in('paid_out.hid',$uhotels);
      $this->db->where('paid_out.deleted', 0);
      return $this->db->count_all_results("paid_out");
    }   

    public function add_paid_out($data){
      $this->db->insert('paid_out', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_paid_out($paid_out_id) {
      $this->db->select('paid_out.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','paid_out.uid = users.id','left');
      $this->db->join('hotels','paid_out.hid = hotels.id','left');
      $this->db->join('status','paid_out.status = status.id','left');
      $this->db->where('paid_out.id', $paid_out_id);
      $this->db->where('paid_out.deleted', 0);   
      $query = $this->db->get('paid_out');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_paid_out($data, $paid_out_id) {
      $this->db->where('paid_out.id', $paid_out_id);   
      $this->db->update('paid_out', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>