<?php

  class Rate_change_model extends CI_Model{

    public function get_rate_changes($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('rate_change.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','rate_change.uid = users.id','left');
      $this->db->join('hotels','rate_change.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','rate_change.role_id = role_group.id','left');
      $this->db->join('status','rate_change.status = status.id','left');
      $this->db->where('rate_change.deleted', 0);
      $this->db->where_in('rate_change.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','rate_change',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(rate_change.id like "%'.$t_attr['search'].'%" OR rate_change.remarks like "%'.$t_attr['search'].'%" OR rate_change.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("rate_change");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("rate_change")->result_array();
      }
    }

    public function get_all_rate_changes($uhotels){
      $this->db->select('rate_change.*');
      $this->db->where_in('rate_change.hid',$uhotels);
      $this->db->where('rate_change.deleted', 0);
      return $this->db->count_all_results("rate_change");
    }   

    public function add_rate_change($data){
      $this->db->insert('rate_change', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_rate_change_item($data){
      $this->db->insert('rate_change_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_rate_change($rate_change_id) {
      $this->db->select('rate_change.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','rate_change.uid = users.id','left');
      $this->db->join('hotels','rate_change.hid = hotels.id','left');
      $this->db->join('status','rate_change.status = status.id','left');
      $this->db->where('rate_change.id', $rate_change_id);
      $this->db->where('rate_change.deleted', 0);   
      $query = $this->db->get('rate_change');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_rate_change_items($rate_change_id) {
      $this->db->select('rate_change_item.*');
      $this->db->where(array('rate_change_item.deleted'=>0,'rate_change_item.rate_change_id'=>$rate_change_id));
      return $this->db->get('rate_change_item')->result_array();
    }

    function edit_rate_change($data, $rate_change_id) {
      $this->db->where('rate_change.id', $rate_change_id);   
      $this->db->update('rate_change', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_rate_change_item($item_id) {
      $this->db->select('rate_change_item.*');
      $this->db->where(array('rate_change_item.deleted'=>0,'rate_change_item.id'=>$item_id));
      return $this->db->get('rate_change_item')->row_array();
    }

    function edit_rate_change_item($data, $item_id) {
      $this->db->where('rate_change_item.id', $item_id);   
      $this->db->update('rate_change_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>