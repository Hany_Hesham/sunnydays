<?php

  class Refund_model extends CI_Model{

    public function get_refunds($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('refund.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','refund.uid = users.id','left');
      $this->db->join('hotels','refund.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','refund.role_id = role_group.id','left');
      $this->db->join('status','refund.status = status.id','left');
      $this->db->where('refund.deleted', 0);
      $this->db->where_in('refund.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','refund',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(refund.id like "%'.$t_attr['search'].'%" OR refund.name like "%'.$t_attr['search'].'%" OR refund.payment_date like "%'.$t_attr['search'].'%" OR refund.company like "%'.$t_attr['search'].'%" OR refund.payment_method like "%'.$t_attr['search'].'%" OR refund.arrival like "%'.$t_attr['search'].'%" OR refund.departure like "%'.$t_attr['search'].'%" OR refund.acc_no like "%'.$t_attr['search'].'%" OR refund.nationality like "%'.$t_attr['search'].'%" OR refund.reason like "%'.$t_attr['search'].'%" OR refund.remarks like "%'.$t_attr['search'].'%" OR refund.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("refund");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("refund")->result_array();
      }
    }

    public function get_all_refunds($uhotels){
      $this->db->select('refund.*');
      $this->db->where_in('refund.hid',$uhotels);
      $this->db->where('refund.deleted', 0);
      return $this->db->count_all_results("refund");
    }   

    public function add_refund($data){
      $this->db->insert('refund', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_refund($refund_id) {
      $this->db->select('refund.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','refund.uid = users.id','left');
      $this->db->join('hotels','refund.hid = hotels.id','left');
      $this->db->join('status','refund.status = status.id','left');
      $this->db->where('refund.id', $refund_id);
      $this->db->where('refund.deleted', 0);   
      $query = $this->db->get('refund');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_refund($data, $refund_id) {
      $this->db->where('refund.id', $refund_id);   
      $this->db->update('refund', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>