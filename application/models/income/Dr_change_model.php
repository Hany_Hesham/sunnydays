<?php

  class Dr_change_model extends CI_Model{

    public function get_dr_changes($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('dr_change.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','dr_change.uid = users.id','left');
      $this->db->join('hotels','dr_change.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','dr_change.role_id = role_group.id','left');
      $this->db->join('status','dr_change.status = status.id','left');
      $this->db->where('dr_change.deleted', 0);
      $this->db->where_in('dr_change.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','dr_change',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(dr_change.id like "%'.$t_attr['search'].'%" OR dr_change.month like "%'.$t_attr['search'].'%" OR dr_change.remarks like "%'.$t_attr['search'].'%" OR dr_change.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("dr_change");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("dr_change")->result_array();
      }
    }

    public function get_all_dr_changes($uhotels){
      $this->db->select('dr_change.*');
      $this->db->where_in('dr_change.hid',$uhotels);
      $this->db->where('dr_change.deleted', 0);
      return $this->db->count_all_results("dr_change");
    }   

    public function add_dr_change($data){
      $this->db->insert('dr_change', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_dr_change_item($data){
      $this->db->insert('dr_change_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_dr_change($dr_change_id) {
      $this->db->select('dr_change.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','dr_change.uid = users.id','left');
      $this->db->join('hotels','dr_change.hid = hotels.id','left');
      $this->db->join('status','dr_change.status = status.id','left');
      $this->db->where('dr_change.id', $dr_change_id);
      $this->db->where('dr_change.deleted', 0);   
      $query = $this->db->get('dr_change');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_dr_change_items($dr_change_id) {
      $this->db->select('dr_change_item.*');
      $this->db->where(array('dr_change_item.deleted'=>0,'dr_change_item.dr_change_id'=>$dr_change_id));
      return $this->db->get('dr_change_item')->result_array();
    }

    function edit_dr_change($data, $dr_change_id) {
      $this->db->where('dr_change.id', $dr_change_id);   
      $this->db->update('dr_change', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_dr_change_item($item_id) {
      $this->db->select('dr_change_item.*');
      $this->db->where(array('dr_change_item.deleted'=>0,'dr_change_item.id'=>$item_id));
      return $this->db->get('dr_change_item')->row_array();
    }

    function edit_dr_change_item($data, $item_id) {
      $this->db->where('dr_change_item.id', $item_id);   
      $this->db->update('dr_change_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>