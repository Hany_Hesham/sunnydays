<?php

  class Late_out_model extends CI_Model{

    public function get_late_outs($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('late_out.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','late_out.uid = users.id','left');
      $this->db->join('hotels','late_out.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','late_out.role_id = role_group.id','left');
      $this->db->join('status','late_out.status = status.id','left');
      $this->db->where('late_out.deleted', 0);
      $this->db->where_in('late_out.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','late_out',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(late_out.id like "%'.$t_attr['search'].'%" OR late_out.remarks like "%'.$t_attr['search'].'%" OR late_out.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("late_out");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("late_out")->result_array();
      }
    }

    public function get_all_late_outs($uhotels){
      $this->db->select('late_out.*');
      $this->db->where_in('late_out.hid',$uhotels);
      $this->db->where('late_out.deleted', 0);
      return $this->db->count_all_results("late_out");
    }   

    public function add_late_out($data){
      $this->db->insert('late_out', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_late_out_item($data){
      $this->db->insert('late_out_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_late_out($late_out_id) {
      $this->db->select('late_out.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','late_out.uid = users.id','left');
      $this->db->join('hotels','late_out.hid = hotels.id','left');
      $this->db->join('status','late_out.status = status.id','left');
      $this->db->where('late_out.id', $late_out_id);
      $this->db->where('late_out.deleted', 0);   
      $query = $this->db->get('late_out');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_late_out_items($late_out_id) {
      $this->db->select('late_out_item.*');
      $this->db->where(array('late_out_item.deleted'=>0,'late_out_item.late_out_id'=>$late_out_id));
      return $this->db->get('late_out_item')->result_array();
    }

    function edit_late_out($data, $late_out_id) {
      $this->db->where('late_out.id', $late_out_id);   
      $this->db->update('late_out', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_late_out_item($item_id) {
      $this->db->select('late_out_item.*');
      $this->db->where(array('late_out_item.deleted'=>0,'late_out_item.id'=>$item_id));
      return $this->db->get('late_out_item')->row_array();
    }

    function edit_late_out_item($data, $item_id) {
      $this->db->where('late_out_item.id', $item_id);   
      $this->db->update('late_out_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>