<?php

  class Remarkes_model extends CI_Model{

    public function get_remarkess($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('remarkes.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','remarkes.uid = users.id','left');
      $this->db->join('hotels','remarkes.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','remarkes.role_id = role_group.id','left');
      $this->db->join('status','remarkes.status = status.id','left');
      $this->db->where('remarkes.deleted', 0);
      $this->db->where_in('remarkes.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','remarkes',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(remarkes.id like "%'.$t_attr['search'].'%" OR remarkes.remarks like "%'.$t_attr['search'].'%" OR remarkes.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("remarkes");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("remarkes")->result_array();
      }
    }

    public function get_all_remarkess($uhotels){
      $this->db->select('remarkes.*');
      $this->db->where_in('remarkes.hid',$uhotels);
      $this->db->where('remarkes.deleted', 0);
      return $this->db->count_all_results("remarkes");
    }   

    public function add_remarkes($data){
      $this->db->insert('remarkes', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_remarkes_item($data){
      $this->db->insert('remarkes_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_remarkes($remarkes_id) {
      $this->db->select('remarkes.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','remarkes.uid = users.id','left');
      $this->db->join('hotels','remarkes.hid = hotels.id','left');
      $this->db->join('status','remarkes.status = status.id','left');
      $this->db->where('remarkes.id', $remarkes_id);
      $this->db->where('remarkes.deleted', 0);   
      $query = $this->db->get('remarkes');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_remarkes_items($remarkes_id) {
      $this->db->select('remarkes_item.*');
      $this->db->where(array('remarkes_item.deleted'=>0,'remarkes_item.remarkes_id'=>$remarkes_id));
      return $this->db->get('remarkes_item')->result_array();
    }

    function edit_remarkes($data, $remarkes_id) {
      $this->db->where('remarkes.id', $remarkes_id);   
      $this->db->update('remarkes', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_remarkes_item($item_id) {
      $this->db->select('remarkes_item.*');
      $this->db->where(array('remarkes_item.deleted'=>0,'remarkes_item.id'=>$item_id));
      return $this->db->get('remarkes_item')->row_array();
    }

    function edit_remarkes_item($data, $item_id) {
      $this->db->where('remarkes_item.id', $item_id);   
      $this->db->update('remarkes_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>