<?php

  class Insurance_model extends CI_Model{

    public function get_insurances($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('insurance.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','insurance.uid = users.id','left');
      $this->db->join('hotels','insurance.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','insurance.role_id = role_group.id','left');
      $this->db->join('status','insurance.status = status.id','left');
      $this->db->where('insurance.deleted', 0);
      $this->db->where_in('insurance.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','insurance',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(insurance.id like "%'.$t_attr['search'].'%" OR insurance.event like "%'.$t_attr['search'].'%" OR insurance.event_date like "%'.$t_attr['search'].'%" OR insurance.location like "%'.$t_attr['search'].'%" OR insurance.name like "%'.$t_attr['search'].'%" OR insurance.payment_date like "%'.$t_attr['search'].'%" OR insurance.reciet like "%'.$t_attr['search'].'%" OR insurance.remarks like "%'.$t_attr['search'].'%" OR insurance.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("insurance");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("insurance")->result_array();
      }
    }

    public function get_all_insurances($uhotels){
      $this->db->select('insurance.*');
      $this->db->where_in('insurance.hid',$uhotels);
      $this->db->where('insurance.deleted', 0);
      return $this->db->count_all_results("insurance");
    }   

    public function add_insurance($data){
      $this->db->insert('insurance', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_insurance($insurance_id) {
      $this->db->select('insurance.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','insurance.uid = users.id','left');
      $this->db->join('hotels','insurance.hid = hotels.id','left');
      $this->db->join('status','insurance.status = status.id','left');
      $this->db->where('insurance.id', $insurance_id);
      $this->db->where('insurance.deleted', 0);   
      $query = $this->db->get('insurance');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_insurance($data, $insurance_id) {
      $this->db->where('insurance.id', $insurance_id);   
      $this->db->update('insurance', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>