<?php

  class Dlr_change_model extends CI_Model{

    public function get_dlr_changes($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('dlr_change.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','dlr_change.uid = users.id','left');
      $this->db->join('hotels','dlr_change.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','dlr_change.role_id = role_group.id','left');
      $this->db->join('status','dlr_change.status = status.id','left');
      $this->db->where('dlr_change.deleted', 0);
      $this->db->where_in('dlr_change.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','dlr_change',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(dlr_change.id like "%'.$t_attr['search'].'%" OR dlr_change.remarks like "%'.$t_attr['search'].'%" OR dlr_change.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("dlr_change");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("dlr_change")->result_array();
      }
    }

    public function get_all_dlr_changes($uhotels){
      $this->db->select('dlr_change.*');
      $this->db->where_in('dlr_change.hid',$uhotels);
      $this->db->where('dlr_change.deleted', 0);
      return $this->db->count_all_results("dlr_change");
    }   

    public function add_dlr_change($data){
      $this->db->insert('dlr_change', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_dlr_change_item($data){
      $this->db->insert('dlr_change_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_dlr_change($dlr_change_id) {
      $this->db->select('dlr_change.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','dlr_change.uid = users.id','left');
      $this->db->join('hotels','dlr_change.hid = hotels.id','left');
      $this->db->join('status','dlr_change.status = status.id','left');
      $this->db->where('dlr_change.id', $dlr_change_id);
      $this->db->where('dlr_change.deleted', 0);   
      $query = $this->db->get('dlr_change');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_dlr_change_items($dlr_change_id) {
      $this->db->select('dlr_change_item.*');
      $this->db->where(array('dlr_change_item.deleted'=>0,'dlr_change_item.dlr_change_id'=>$dlr_change_id));
      return $this->db->get('dlr_change_item')->result_array();
    }

    function edit_dlr_change($data, $dlr_change_id) {
      $this->db->where('dlr_change.id', $dlr_change_id);   
      $this->db->update('dlr_change', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_dlr_change_item($item_id) {
      $this->db->select('dlr_change_item.*');
      $this->db->where(array('dlr_change_item.deleted'=>0,'dlr_change_item.id'=>$item_id));
      return $this->db->get('dlr_change_item')->row_array();
    }

    function edit_dlr_change_item($data, $item_id) {
      $this->db->where('dlr_change_item.id', $item_id);   
      $this->db->update('dlr_change_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>