<?php

  class Discrepancy_model extends CI_Model{

    public function get_discrepancys($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('discrepancy.*, users.fullname, hotels.hotel_name, meta_data.type_name AS day_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','discrepancy.uid = users.id','left');
      $this->db->join('hotels','discrepancy.hid = hotels.id','left');
      $this->db->join('meta_data','discrepancy.part_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','discrepancy.role_id = role_group.id','left');
      $this->db->join('status','discrepancy.status = status.id','left');
      $this->db->where('discrepancy.deleted', 0);
      $this->db->where_in('discrepancy.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','discrepancy',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(discrepancy.id like "%'.$t_attr['search'].'%" OR discrepancy.remarks like "%'.$t_attr['search'].'%" OR discrepancy.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("discrepancy");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("discrepancy")->result_array();
      }
    }

    public function get_all_discrepancys($uhotels){
      $this->db->select('discrepancy.*');
      $this->db->where_in('discrepancy.hid',$uhotels);
      $this->db->where('discrepancy.deleted', 0);
      return $this->db->count_all_results("discrepancy");
    }   

    public function add_discrepancy($data){
      $this->db->insert('discrepancy', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_discrepancy_item($data){
      $this->db->insert('discrepancy_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_discrepancy($discrepancy_id) {
      $this->db->select('discrepancy.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, meta_data.type_name AS day_name, status.status_name');
      $this->db->join('users','discrepancy.uid = users.id','left');
      $this->db->join('hotels','discrepancy.hid = hotels.id','left');
      $this->db->join('meta_data','discrepancy.part_id = meta_data.id','left');
      $this->db->join('status','discrepancy.status = status.id','left');
      $this->db->where('discrepancy.id', $discrepancy_id);
      $this->db->where('discrepancy.deleted', 0);   
      $query = $this->db->get('discrepancy');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_discrepancy_items($discrepancy_id) {
      $this->db->select('discrepancy_item.*');
      $this->db->where(array('discrepancy_item.deleted'=>0,'discrepancy_item.discrepancy_id'=>$discrepancy_id));
      return $this->db->get('discrepancy_item')->result_array();
    }

    function edit_discrepancy($data, $discrepancy_id) {
      $this->db->where('discrepancy.id', $discrepancy_id);   
      $this->db->update('discrepancy', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_discrepancy_item($item_id) {
      $this->db->select('discrepancy_item.*');
      $this->db->where(array('discrepancy_item.deleted'=>0,'discrepancy_item.id'=>$item_id));
      return $this->db->get('discrepancy_item')->row_array();
    }

    function edit_discrepancy_item($data, $item_id) {
      $this->db->where('discrepancy_item.id', $item_id);   
      $this->db->update('discrepancy_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>