<?php

  class Speceial_rate_model extends CI_Model{

    public function get_speceial_rates($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('speceial_rate.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','speceial_rate.uid = users.id','left');
      $this->db->join('hotels','speceial_rate.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','speceial_rate.role_id = role_group.id','left');
      $this->db->join('status','speceial_rate.status = status.id','left');
      $this->db->where('speceial_rate.deleted', 0);
      $this->db->where_in('speceial_rate.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','speceial_rate',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(speceial_rate.id like "%'.$t_attr['search'].'%" OR speceial_rate.remarks like "%'.$t_attr['search'].'%" OR speceial_rate.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("speceial_rate");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("speceial_rate")->result_array();
      }
    }

    public function get_all_speceial_rates($uhotels){
      $this->db->select('speceial_rate.*');
      $this->db->where_in('speceial_rate.hid',$uhotels);
      $this->db->where('speceial_rate.deleted', 0);
      return $this->db->count_all_results("speceial_rate");
    }   

    public function add_speceial_rate($data){
      $this->db->insert('speceial_rate', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_speceial_rate_item($data){
      $this->db->insert('speceial_rate_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_speceial_rate($speceial_rate_id) {
      $this->db->select('speceial_rate.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','speceial_rate.uid = users.id','left');
      $this->db->join('hotels','speceial_rate.hid = hotels.id','left');
      $this->db->join('status','speceial_rate.status = status.id','left');
      $this->db->where('speceial_rate.id', $speceial_rate_id);
      $this->db->where('speceial_rate.deleted', 0);   
      $query = $this->db->get('speceial_rate');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_speceial_rate_items($speceial_rate_id) {
      $this->db->select('speceial_rate_item.*');
      $this->db->where(array('speceial_rate_item.deleted'=>0,'speceial_rate_item.speceial_rate_id'=>$speceial_rate_id));
      return $this->db->get('speceial_rate_item')->result_array();
    }

    function edit_speceial_rate($data, $speceial_rate_id) {
      $this->db->where('speceial_rate.id', $speceial_rate_id);   
      $this->db->update('speceial_rate', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_speceial_rate_item($item_id) {
      $this->db->select('speceial_rate_item.*');
      $this->db->where(array('speceial_rate_item.deleted'=>0,'speceial_rate_item.id'=>$item_id));
      return $this->db->get('speceial_rate_item')->row_array();
    }

    function edit_speceial_rate_item($data, $item_id) {
      $this->db->where('speceial_rate_item.id', $item_id);   
      $this->db->update('speceial_rate_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>