<?php

  class Estimation_model extends CI_Model{

    public function get_estimations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('estimation.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','estimation.uid = users.id','left');
      $this->db->join('hotels','estimation.hid = hotels.id','left');
      $this->db->join('departments','estimation.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','estimation.role_id = role_group.id','left');
      $this->db->join('status','estimation.status = status.id','left');
      $this->db->where('estimation.deleted', 0);
      $this->db->where_in('estimation.hid',$uhotels);
      $this->db->where_in('estimation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','estimation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(estimation.id like "%'.$t_attr['search'].'%" OR estimation.remarks like "%'.$t_attr['search'].'%" OR estimation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("estimation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("estimation")->result_array();
      }
    }

    public function get_all_estimations($uhotels, $udepartments){
      $this->db->select('estimation.*');
      $this->db->where_in('estimation.hid',$uhotels);
      $this->db->where_in('estimation.dep_code',$udepartments);
      $this->db->where('estimation.deleted', 0);
      return $this->db->count_all_results("estimation");
    }   

    public function add_estimation($data){
      $this->db->insert('estimation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_estimation_item($data){
      $this->db->insert('estimation_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_estimation($estimation_id) {
      $this->db->select('estimation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','estimation.uid = users.id','left');
      $this->db->join('hotels','estimation.hid = hotels.id','left');
      $this->db->join('departments','estimation.dep_code = departments.id','left');
      $this->db->join('status','estimation.status = status.id','left');
      $this->db->where('estimation.id', $estimation_id);
      $this->db->where('estimation.deleted', 0);   
      $query = $this->db->get('estimation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_estimation_items($estimation_id) {
      $this->db->select('estimation_item.*');
      $this->db->where(array('estimation_item.deleted'=>0,'estimation_item.estimation_id'=>$estimation_id));
      return $this->db->get('estimation_item')->result_array();
    }

    function edit_estimation($data, $estimation_id) {
      $this->db->where('estimation.id', $estimation_id);   
      $this->db->update('estimation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_estimation_item($item_id) {
      $this->db->select('estimation_item.*');
      $this->db->where(array('estimation_item.deleted'=>0,'estimation_item.id'=>$item_id));
      return $this->db->get('estimation_item')->row_array();
    }

    function edit_estimation_item($data, $item_id) {
      $this->db->where('estimation_item.id', $item_id);   
      $this->db->update('estimation_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>