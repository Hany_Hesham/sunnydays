<?php

  class Po_model extends CI_Model{

    public function get_pos($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('po.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','po.uid = users.id','left');
      $this->db->join('hotels','po.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','po.role_id = role_group.id','left');
      $this->db->join('status','po.status = status.id','left');
      $this->db->where('po.deleted', 0);
      $this->db->where_in('po.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','po',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(po.id like "%'.$t_attr['search'].'%" OR po.supplier like "%'.$t_attr['search'].'%" OR po.store like "%'.$t_attr['search'].'%" OR po.pr_no like "%'.$t_attr['search'].'%" OR po.po_no like "%'.$t_attr['search'].'%" OR po.po_date like "%'.$t_attr['search'].'%" OR po.del_date like "%'.$t_attr['search'].'%" OR po.remarks like "%'.$t_attr['search'].'%" OR po.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("po");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("po")->result_array();
      }
    }

    public function get_all_pos($uhotels){
      $this->db->select('po.*');
      $this->db->where_in('po.hid',$uhotels);
      $this->db->where('po.deleted', 0);
      return $this->db->count_all_results("po");
    }   

    public function add_po($data){
      $this->db->insert('po', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_po_item($data){
      $this->db->insert('po_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_po($po_id) {
      $this->db->select('po.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','po.uid = users.id','left');
      $this->db->join('hotels','po.hid = hotels.id','left');
      $this->db->join('status','po.status = status.id','left');
      $this->db->where('po.id', $po_id);
      $this->db->where('po.deleted', 0);   
      $query = $this->db->get('po');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_po_items($po_id) {
      $this->db->select('po_item.*');
      $this->db->where(array('po_item.deleted'=>0,'po_item.po_id'=>$po_id));
      return $this->db->get('po_item')->result_array();
    }

    function edit_po($data, $po_id) {
      $this->db->where('po.id', $po_id);   
      $this->db->update('po', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_po_item($item_id) {
      $this->db->select('po_item.*');
      $this->db->where(array('po_item.deleted'=>0,'po_item.id'=>$item_id));
      return $this->db->get('po_item')->row_array();
    }

    function edit_po_item($data, $item_id) {
      $this->db->where('po_item.id', $item_id);   
      $this->db->update('po_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>