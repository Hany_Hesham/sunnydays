<?php

  class Pricing_model extends CI_Model{

    public function get_pricings($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('pricing.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','pricing.uid = users.id','left');
      $this->db->join('hotels','pricing.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','pricing.role_id = role_group.id','left');
      $this->db->join('status','pricing.status = status.id','left');
      $this->db->where('pricing.deleted', 0);
      $this->db->where_in('pricing.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','pricing',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(pricing.id like "%'.$t_attr['search'].'%" OR pricing.remarks like "%'.$t_attr['search'].'%" OR pricing.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("pricing");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("pricing")->result_array();
      }
    }

    public function get_all_pricings($uhotels){
      $this->db->select('pricing.*');
      $this->db->where_in('pricing.hid',$uhotels);
      $this->db->where('pricing.deleted', 0);
      return $this->db->count_all_results("pricing");
    }   

    public function add_pricing($data){
      $this->db->insert('pricing', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_pricing_item($data){
      $this->db->insert('pricing_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_pricing($pricing_id) {
      $this->db->select('pricing.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','pricing.uid = users.id','left');
      $this->db->join('hotels','pricing.hid = hotels.id','left');
      $this->db->join('status','pricing.status = status.id','left');
      $this->db->where('pricing.id', $pricing_id);
      $this->db->where('pricing.deleted', 0);   
      $query = $this->db->get('pricing');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_pricing_items($pricing_id) {
      $this->db->select('pricing_item.*, meta_data.type_name AS item_name, meta_data.type_data AS aritem_name');
      $this->db->join('meta_data','pricing_item.item = meta_data.id','left');
      $this->db->where(array('pricing_item.deleted'=>0,'pricing_item.pricing_id'=>$pricing_id));
      return $this->db->get('pricing_item')->result_array();
    }

    function edit_pricing($data, $pricing_id) {
      $this->db->where('pricing.id', $pricing_id);   
      $this->db->update('pricing', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_pricing_item($item_id) {
      $this->db->select('pricing_item.*, meta_data.type_name AS item_name, meta_data.type_data AS item_name');
      $this->db->join('meta_data','pricing_item.item = meta_data.id','left');
      $this->db->where(array('pricing_item.deleted'=>0,'pricing_item.id'=>$item_id));
      return $this->db->get('pricing_item')->row_array();
    }

    function edit_pricing_item($data, $item_id) {
      $this->db->where('pricing_item.id', $item_id);   
      $this->db->update('pricing_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>