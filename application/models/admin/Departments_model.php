<?php

	class Departments_model extends CI_Model{
	
	  	function __contruct(){
			parent::__construct;
	  	}
		
		public function get_departments_ajax($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
		    $this->db->select('departments.*, user_groups.name AS global_role, user_groups1.name AS hotel_role');
			$this->db->join('user_groups','departments.role_id = user_groups.id','left');
			$this->db->join('user_groups AS user_groups1','departments.role_hotel = user_groups1.id','left');
		    if ($search) {
         		$this->db->where('(dep_name like "'.$search.'%" OR code like "'.$search.'%" OR rank like "'.$search.'%" OR user_groups.name like "'.$search.'%" OR user_groups1.name like "'.$search.'%")');
		  	}
		    if ($filter_count == 'count') { 
     			return $this->db->count_all_results("departments");
			}else{
			   	$this->db->order_by($col_name,$order);
      			return $this->db->limit($length,$start)->get("departments")->result_array();
			}    
	    }


	    public function get_all_departments(){
			return $this->db->count_all_results("departments");
	    }  

	    function get_departments(){
	  		$this->db->select('departments.*');
			$this->db->where('departments.deleted', 0);
			return $this->db->get('departments')->result_array();
	  	}

	    public function get_department($id){

			return $this->db->get_where('departments',array('id'=>$id))->row_array();

	    }  

	    public function get_department_by_devision($dev_id){

			return $this->db->get_where('departments',array('devision_id'=>$dev_id))->result_array();

	    }  

	    public function add_department($data){

				$this->db->insert('departments', $data);

				return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

			 }  


		public function update_department($dep_id,$data){

			 	$this->db->where('departments.id', $dep_id);
			 				
				$this->db->update('departments', $data);
				
				 if ($this->db->affected_rows() >= 0) {
	                  
	                   return $this->db->affected_rows();
	              } 
	        
	          }	 	 

}
?>
