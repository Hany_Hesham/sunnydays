<?php

	class Notification_model extends CI_Model{

		public function get_notifications($start,$length,$search=false,$order,$col_name,$filter_count,$module_id=''){
		    $query = 'SELECT notifications.*, modules.name AS module_name, users.fullname
		        FROM notifications
		        LEFT JOIN modules ON notifications.module_id = modules.id
		        LEFT JOIN users ON notifications.uid = users.id
		    ';
		    if ($module_id !='' || $search) {             
	  	       	$query .=' WHERE';
	  	    }
	  	    if ($module_id !='') {
               	$query .=' module_id='.$module_id.'';
            }     
            if ($module_id !='' &&  $search) {
               	$query .=' AND';
            }      
	  	    if ($search) {
	  	       	$query .=' (notifications.head like "%'.$search.'%" OR modules.name like "%'.$search.'%" OR notifications.message like "%'.$search.'%" OR notifications.link like "%'.$search.'%" OR users.fullname like "%'.$search.'%" OR notifications.timestamp like "%'.$search.'%") ';
	  	    }
          	if ($filter_count == 'count') { 
      	     	$records = $this->db->query($query);
                return $records->num_rows();
	        }else{
	           	$query .='  ORDER BY '.$col_name.' '.$order.'';
      	     	$query .=' LIMIT '.$start.','.$length.'';
	           	$records = $this->db->query($query);
	           	return $records->result_array();
	       	}   
	   	}

        public function get_all_notifications($module_id=''){
			if ($module_id !='') {
				return $this->db->get_where('notifications',array('module_id'=>$module_id))->result_array();
			}else{
			   	return $this->db->get('notifications')->result_array();
			}
		}  

		public function get_notification($id){
			$this->db->select('notifications.*, modules.name AS module_name, users.fullname');
			$this->db->join('modules','notifications.module_id = modules.id','left');
			$this->db->join('users','notifications.uid = users.id','left');
			$this->db->where('notifications.id',$id);
			return $this->db->get('notifications')->result_array();
		}  

		function updateRead($id, $statue){
	      	$this->db->update('notifications', array('status'=> $statue, 'deleted'=> $statue, 'readedby'=> $this->data['user_id']), "id = ".$id);
	      	return ($this->db->affected_rows() == 1)? TRUE : FALSE;
	    }

  	}

?>	