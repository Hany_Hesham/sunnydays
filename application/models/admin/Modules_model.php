<?php

  class Modules_model extends CI_Model{

    public function get_modulesz($t_attr, $filter_count){
      $query = 'SELECT modules.*
        FROM modules';
      if ($t_attr['search']) {
        $query .= ' where (modules.id like "%'.$t_attr['search'].'%" OR modules.name like "%'.$t_attr['search'].'%" OR modules.tdatabase like "%'.$t_attr['search'].'%" OR modules.type like "%'.$t_attr['search'].'%" OR modules.menu_group like "%'.$t_attr['search'].'%")';
      }
      $query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'
        LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
      if ($filter_count == 'count') {
        return $this->db->query($query)->num_rows();
      }else{
        return $this->db->query($query)->result_array();
      }
    }

    public function get_all_modules(){
      return $this->db->get_where('modules',array())->result_array();
    }

    public function get_modules_by_ids($ids){
      $this->db->where_in('modules.id', $ids);
      $this->db->select('modules.id, modules.name');  
      return $this->db->get('modules')->result_array();
    } 
    
    public function getAllModules($type = FALSE){
      $this->db->select('modules.*');
      if ($type) {
        $this->db->where('modules.type', $type);
      }
      $query = $this->db->get('modules');
      return ($query->num_rows() > 0 )? $query->result_array() : FALSE;
    }

    public function get_signs($modules_id, $t_attr, $filter_count){
      $query = 'SELECT sign_types.*
        FROM sign_types
        WHERE sign_types.module_id = '.$modules_id;
      if ($t_attr['search']) {
        $query .= ' AND (sign_types.id like "%'.$t_attr['search'].'%" OR sign_types.name like "%'.$t_attr['search'].'%")';
      }
      $query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'
        LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
      if ($filter_count == 'count') {
        return $this->db->query($query)->num_rows();
      }else{
        return $this->db->query($query)->result_array();
      }
    }

    public function get_all_signs($modules_id){
      return $this->db->get_where('sign_types',array('sign_types.module_id'=>$modules_id))->result_array();
    } 

    public function add_module($data){
      $this->db->insert('modules', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    } 

    function get_module($module_id) {
      $this->db->select('modules.*');
      $this->db->where('modules.id', $module_id);
      $query = $this->db->get('modules');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function update_module($module_id, $data) {
      $this->db->where('modules.id', $module_id);   
      $this->db->update('modules', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    public function get_module_by($id='',$name=''){
      if ($id) {
        $this->db->where('modules.id',$id);
      }    
      if ($name) {       
        $this->db->where('modules.name',$name);
      }   
      return $this->db->get('modules')->row_array(); 
    }

    function get_columns($table){
      $query = $this->db->list_fields($table);
      return $query; 
    }

    public function add_signtype($data){
      $this->db->insert('sign_types', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    } 

    function get_signtype($type_id) {
      $this->db->select('sign_types.*');
      $this->db->where('sign_types.id', $type_id);
      $query = $this->db->get('sign_types');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function update_signtype($type_id, $data) {
      $this->db->where('sign_types.id', $type_id);   
      $this->db->update('sign_types', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_roles($type_id) {
      $this->db->select('sign_role.*');
      $this->db->where('sign_role.type', $type_id);
      $query = $this->db->get('sign_role');
      return ($query->num_rows() > 0 )? $query->result_array() : FALSE;
    }

    function get_allroles() {
      $this->db->select('user_groups.*');
      $query = $this->db->get('user_groups');
      return ($query->num_rows() > 0 )? $query->result_array() : FALSE;
    }

    function get_role($role_id) {
      $this->db->select('sign_role.*, user_groups.name AS role_name');
      $this->db->join('user_groups','sign_role.role = user_groups.id','left');
      $this->db->where('sign_role.id', $role_id);
      $query = $this->db->get('sign_role');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    public function add_role($data){
      $this->db->insert('sign_role', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    } 

    function edit_role($data, $role_id) {
      $this->db->where('sign_role.id', $role_id);   
      $this->db->update('sign_role', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
       
       public function get_modules(){
         
         $this->db->order_by('rank','ASC');
         
         return $this->db->get('modules')->result_array();
       
       }

      public function get_user_modules(){
         
         $this->db->order_by('rank','ASC');
         
         return $this->db->get_where('modules',array('type'=>'user'))->result_array();
       
       }


      function getForm($database, $form_id) {
        $this->db->select($database.'.*');
        $this->db->where($database.'.id', $form_id);
        $query = $this->db->get($database);
        return $query->row_array();
      }
   }

?>