<?php

	class User_model extends CI_Model{

		function get_users($uids,$t_attr,$custom_search='',$filter_count=''){
			$this->db->select('users.* ,user_groups.name AS role_name');
			$this->db->join('user_groups','users.role_id = user_groups.id','left');
         $this->db->where('users.deleted',0);
         $this->db->where_in('users.id',$uids);
	      if ($custom_search) {
            if (isset($custom_search['role_id']) && $custom_search['role_id']  !='') {
         		$this->db->where_in('users.role_id',$custom_search['role_id']);
			   } 
            if (isset($custom_search['toDate']) && $custom_search['fromDate'] && $custom_search['toDate']) {
         		$this->db->where('DATE_FORMAT(users.created_at,GET_FORMAT(DATE,"JIS")) BETWEEN ("'.$custom_search['fromDate'].'") AND ("'.$custom_search['toDate'].'")');
			   }
			   if (isset($custom_search['toDate']) && $custom_search['fromDate'] && !$custom_search['toDate']) {
         		$this->db->where('DATE_FORMAT(users.created_at,GET_FORMAT(DATE,"JIS")) BETWEEN ("'.$custom_search['fromDate'].'") AND ("'.$custom_search['toDate'].'")');
			   }
			   if (isset($custom_search['toDate']) && $custom_search['toDate'] && !$custom_search['fromDate']) {
         		$this->db->where('DATE_FORMAT(users.created_at,GET_FORMAT(DATE,"JIS")) <= "'.$custom_search['toDate'].'"');
			   } 
	      }
	      if ($t_attr['search']) {
         	$this->db->where('(users.id like "%'.$t_attr['search'].'%" OR username like "%'.$t_attr['search'].'%" OR fullname like "%'.$t_attr['search'].'%" OR email like "%'.$t_attr['search'].'%")');
	      }
	      if ($filter_count == 'count') {
      		return $this->db->count_all_results("users");
	      }else{
        		$this->db->order_by($t_attr['col_name'],$t_attr['order']);
        		return $this->db->limit($t_attr['length'],$t_attr['start'])->get("users")->result_array();
	      }
	   }

	   public function get_all_users($uids){
         $this->db->where_in('users.id',$uids);
         $this->db->where('users.deleted',0);
      	return $this->db->count_all_results("users");
		}

		public function add_user($data){

			$this->db->insert('users', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		}

		public function add_user_hotels($user_id,$hotel){
	       $data= array(
				'user_id' => $user_id,
				'hotel_id'    => $hotel
			);

			$this->db->insert('user_hotels', $data);

			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;

		}

        public function delete_user_hotels($uid,$hotel_id){

        	$this->db->where(array('user_id'=>$uid,'hotel_id'=>$hotel_id));

        	$this->db->delete('user_hotels');
        }

        public function edit_user_hotels($hotel_id,$user_id){

			$this->db->where(array('user_hotels.hotel_id'=>$hotel_id,'user_hotels.user_id'=>$user_id));

			$this->db->update('user_hotels.hotel_id', $data);

			return true;

		}


		public function get_all_users_by_uids_role_id($uids,$role_id=''){
			if ($role_id) {
				$this->db->where(array('role_id'=>$role_id,'deleted'=>0,'disabled'=>0));
			 }else{
			 	$this->db->where(array('deleted'=>0,'disabled'=>0));
			 }
            $this->db->where_in('users.id',$uids);
			return $this->db->get('users')->result_array();

		}

		public function get_user_hotels($user_id){
			$this->db->select('user_hotels.*,hotels.code AS hotel_code,hotels.hotel_name');
			$this->db->join('hotels','user_hotels.hotel_id = hotels.id','left');
			$this->db->where('user_hotels.user_id',$user_id);
			$query = $this->db->get('user_hotels');
			return $result = $query->result_array();
		}


		public function get_user_roles($hotel_id,$uid){
			
			$this->db->select('user_hotels.role_id');

			return $this->db->get_where('user_hotels',array('user_hotels.hotel_id' => $hotel_id,'user_hotels.user_id' => $uid))->row_array();
			 
		}

		public function update_user_hotels($id,$data){

            $this->db->update('user_hotels', array('role_id' => $data), "id = ".$id);
			
			 if ($this->db->affected_rows() >= 0) {
                  
                   return $this->db->affected_rows();
              } 
		}


		public function get_users_by_granted_hotels($hids){
			 $query = 'SELECT user_hotels.user_id,hotel_id FROM user_hotels';
			 $query .='  WHERE user_hotels.hotel_id IN('.implode(',',$hids).')';
			 return $this->db->query($query)->result_array();
		   }

		public function get_owned_users($uids){
			$this->db->select('users.*');
			$users_ids_chunk = array_chunk($uids, 25);
            foreach ($users_ids_chunk as $users_ids) {
                $this->db->or_where_in('users.id', $users_ids);
            }
	        $this->db->where('users.deleted',0);
        	$this->db->order_by('users.username','ASC');
            return $this->db->get('users')->result_array();
        }


		public function get_user_by_id($id){

			 $this->db->select('users.*,user_groups.name as role_name');

             $this->db->join('user_groups','users.role_id = user_groups.id','left');
		     
		     $this->db->where('users.id',$id);

		     $query = $this->db->get('users');

		     return ($query->num_rows() > 0 )? $query->row_array() : FALSE;

		}

		public function edit_user($data, $id){

			$this->db->where('id', $id);

			$this->db->update('users', $data);

			return true;

		}

		public function count_users(){
           
            return $this->db->get_where("users",array('deleted'=>0,'disabled'=>0))->num_rows();
		  }

  // ------------------------------------------ premission part ------------------------------------------------------------

     public function get_group_permissions($role_id,$module_id){
	 	
	 	$this->db->select('user_groups_permission.*');
	 	
	 	$this->db->where('user_groups_permission.role_id',$role_id);

	 	$this->db->where('user_groups_permission.module_id',$module_id);
	 	
	 	return $this->db->get('user_groups_permission')->row_array();
	 	
	 }


	 public function get_AllGroup_permissions($role_id){
	 	
	 	$this->db->select('user_groups_permission.*');
	 	
	 	$this->db->where('user_groups_permission.role_id',$role_id);
	 	
	 	return $this->db->get('user_groups_permission')->result_array();
	 	
	 }

	 public function get_access_modules($user_id,$module_id){

	 	$this->db->select('user_permissions.*');
	 	
	 	$this->db->where('user_permissions.user_id',$user_id);

	    $this->db->where('user_permissions.module_id',$module_id);
	 	
	 	return $this->db->get('user_permissions')->row_array();

	 }

     public function add_permission($data){

		$this->db->insert('user_permissions', $data);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		}


	 public function update_permission_by_user_id($id, $user_id, $data){

	 	$this->db->where('user_permissions.user_id', $user_id);
	 	
	 	$this->db->where('user_permissions.id', $id);
		
		$this->db->update('user_permissions', $data);
		
		return true;
	  }	

	  public function update_permission_bulk($module_id, $user_id, $data){

	 	$this->db->where('user_permissions.user_id', $user_id);
	 	
	 	$this->db->where('user_permissions.module_id', $module_id);
		
		$this->db->update('user_permissions', $data);
		
		return true;
	  }	
    
     public function get_role($role_id){
     	
     	$this->db->where('user_groups.id',$role_id);
     	
     	return $this->db->get('user_groups')->row_array();
     }

     public function add_role_name($data){

		$this->db->insert('user_groups',['name' => $data]);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		
		}

     public function add_group_permission($data){

		$this->db->insert('user_groups_permission', $data);
	
		return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		
		}

	 public function update_group_permission_by_id($id, $role_id, $data){

	 	$this->db->where('user_groups_permission.role_id', $role_id);
	 	
	 	$this->db->where('user_groups_permission.id', $id);
		
		$this->db->update('user_groups_permission', $data);
		
		return true;
	  }

	public function get_user_access($module_id,$user_id){

	 	$this->db->select('user_permissions.*');
	 	
	 	$this->db->where('user_permissions.user_id',$user_id);

	    $this->db->where('user_permissions.module_id',$module_id);
	 	
	 	return $this->db->get('user_permissions')->row_array();

	 }  		



	}



?>