<?php

	class General_model extends CI_Model{
       
       
         
       public function logger($data) {

			$this->db->insert('log', $data);

			return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		
		}
         
        function get_modules($group = FALSE){
		    $this->db->select('modules.*');
		    if ($group) {
		    	$this->db->like('modules.menu_group', $group);
		    }
		    $this->db->order_by('modules.rank','ASC');
		    return $this->db->get('modules')->result_array();
  	    }

  	    function get_comapnies($ids = FALSE){
		    $this->db->select('companies.*');
		    if ($ids) {
		    	$this->db->where_in('companies.id', $ids);
		    }
		   	$this->db->where('companies.deleted', 0);
		    $this->db->order_by('companies.id','ASC');
		    return $this->db->get('companies')->result_array();
  	    }

  	    function get_menuModules(){
		    $this->db->select('modules.*');
		    $this->db->group_by('menu_group');
		    $this->db->order_by('modules.rank','ASC');
		    return $this->db->get('modules')->result_array();
  	    }

  	    function get_file($module_id,$form_id){

		    $this->db->select('files.*,users.fullname');

		    $this->db->join('users', 'files.user_id = users.id','left');
		   
		    $this->db->where('files.module_id', $module_id);
		   
		    $this->db->where('files.form_id', $form_id);
		   
		    return $this->db->get('files')->result_array();
  	      
  	    }

  	    function get_singleLog($module, $form, $item = FALSE, $action = FALSE){
		    $this->db->select('log.*,users.fullname');
		    $this->db->join('users', 'log.user_id = users.id','left');
		    $this->db->where('log.module_id', $module);
		    $this->db->where('log.target_id', $form);
		    if ($action) {
		    	$this->db->like('log.action', $action);
		    }
		    if ($item) {
			    $this->db->where('log.mini_target_id', $item);
		    }
		    return $this->db->get('log')->result_array();
  	    }

  	    function get_file_from_table($module_id, $form_id, $table, $type=0){
		    $this->db->select(''.$table.'.*,users.fullname');
		    $this->db->join('users', ''.$table.'.user_id = users.id','left');
		    $this->db->where(''.$table.'.module_id', $module_id);
		    $this->db->where(''.$table.'.form_id', $form_id);
		    $this->db->where(''.$table.'.type', $type);
		    return $this->db->get(''.$table.'')->result_array();
  	    }  

  	    function get_last_file($module_id,$form_id){

		    $this->db->select('files.*,users.fullname');

		    $this->db->join('users', 'files.user_id = users.id','left');
		   
		    $this->db->where('files.module_id', $module_id);
		   
		    $this->db->where('files.form_id', $form_id);

		    $this->db->order_by('id','DESC');
		   
		    return $this->db->get('files')->row_array();
  	      
  	      }  

     	function remove_file($id,$table='',$module_id='') {
           if ($table) {
           	  $file = $this->db->get_where(''.$table.'',array('id'=>$id))->row_array();
		      $this->db->query('DELETE FROM '.$table.' WHERE id = '.$id);
		      loger('Delete',$module_id,'Files', $file['form_id'],$id,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'Deleted file No#.' .$id.'from  '.$table);
		    }else{
		      $file = $this->db->get_where('files',array('id'=>$id))->row_array();	
		      $this->db->query('DELETE FROM files WHERE id = '.$id);
		      loger('Delete',$module_id,'Files', $file['form_id'],$id,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'Deleted file No#.' .$id.'from g-files ');
		    }
		  
		  }	


		function add_file($module_id,$form_id, $name, $user_id) {
			
	  		$this->db->query('INSERT INTO files(module_id,form_id, file_name, user_id, timestamp) VALUES("'.$module_id.'","'.$form_id.'","'.$name.'","'.$user_id.'","'.date("Y-m-d H:i:s").'")');
	  		loger('Create',$module_id,'Files',$form_id,0,json_encode(array('table'=>'files','file_name'=>$name), JSON_UNESCAPED_UNICODE),0,0,0,'added file: '.$name.'');
	  	 }

	  	function add_file_intable($module_id,$table,$form_id, $name, $user_id, $type=0) {

	  		$this->db->query('INSERT INTO '.$table.'(module_id,form_id, file_name, user_id, type, timestamp) VALUES("'.$module_id.'","'.$form_id.'","'.$name.'","'.$user_id.'","'.$type.'","'.date("Y-m-d H:i:s").'")');
	  	    loger('Create',$module_id,'Files',$form_id,0,json_encode(array('table'=>$table,'file_name'=>$name), JSON_UNESCAPED_UNICODE),0,0,0,'added file:'.$name.'' );
	  	 } 


	  	function update_files($assumed_id, $module_id,$form_id,$table='') {
	     if ($table) {
	        $file = $this->db->get_where(''.$table.'',array('form_id'=>$assumed_id))->result_array();
	     	 $this->db->query('UPDATE '.$table.' SET form_id = "'.$form_id.'" WHERE form_id = "'.$assumed_id.'"' );
	     	 loger('Update',$module_id,'Files',$form_id,0,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>'files'), JSON_UNESCAPED_UNICODE),0,0,'updated files of form No#. : '.$form_id );
	        }else{  	
	        $file = $this->db->get_where('files',array('form_id'=>$assumed_id))->result_array();
	        $this->db->query('UPDATE files SET form_id = "'.$form_id.'" WHERE form_id = "'.$assumed_id.'" AND module_id ="'.$module_id.'" ' );
	        loger('Update',$module_id,'Files',$form_id,0,json_encode($file, JSON_UNESCAPED_UNICODE),json_encode(array('table'=>$table), JSON_UNESCAPED_UNICODE),0,0,'updated files of form No#. : '.$form_id );
	       }
   		    return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
  	     
  	     } 

		public function get_count($table,$hids='',$del='',$status=''){
							    
			    if ($hids) {
			 
				    $this->db->where_in($table.'.hid',$hids);

			    }
              
               if ($del) {
               	
			    $this->db->where($table.'.deleted',0);

               }

               if ($status) {
               	
			    $this->db->where($table.'.'.$status,$status);

               }

				return $this->db->count_all_results($table);
			
			}

         public function search_in($table,$col,$name,$exist_value=''){
               
               if (!$exist_value) {

				       return $this->db->get_where($table,array($col => $name,'deleted'=> 0))->row_array();

                  }elseif($exist_value){

                  	return $this->db->get_where($table,array($col => $name,'deleted'=> 0,$col.'!=' => str_replace ("%20", " ", $exist_value)))->row_array();
                  
                  }

             }  


         
         public function add_notify($data) {

			  $this->db->insert('notifications', $data);

			  return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		
		 } 

		    public function add_savedForms($user_id,$data) {
		 	  $fdata = [
		 	  	        'uid'         => $user_id,
		 	  	        'module_id'   => $data['formData'][0],
		 	  	        'form_id'     => $data['formData'][1],
		 	  	        'page_title'  => $data['pageTitle'],
		 	  	        'page_info'   => $data['pageInfo'],
		 	  	        'link'        => $data['link'],
		 	  	        'timestamp'   => date("Y-m-d H:i:s"),
		 	           ];

			  $this->db->insert('saved_forms', $fdata);

			  return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		
		   }    

		  public function get_savedForms($uid){
			$this->db->select('*');
			$this->db->where('uid', $uid);
	  	        $this->db->order_by('timestamp', 'DESC');

			return $this->db->get('saved_forms')->result_array();
             	  }   


         public function get_notifications($uid, $uhotels='', $dep_id = ''){
				$this->db->select('*');
				$this->db->where('deleted', 0);
			 	if ($dep_id) {
    				$this->db->where('(to_hid IN ('. implode(', ', $uhotels) . ') AND (to_dep_id IN ('.implode(',',(array)$dep_id).') OR to_uid = '.$uid.'))');	
				}else{
    				$this->db->where('(to_hid IN ('.implode(',',$uhotels).') AND to_uid = '.$uid.')');
				}
				$this->db->order_by('timestamp', 'DESC');
				$this->db->limit(30);
				return $this->db->get('notifications')->result_array();
         }

           	public function get_messages($uid = FALSE, $module_id = FALSE, $form_id = FALSE){

			$this->db->select('messages.*, users.fullname AS username');
			$this->db->join('users', 'messages.uid = users.id', 'left');
			$this->db->where('messages.deleted', 0);


			if ($module_id && $form_id) {
				$this->db->where('module_id', $module_id);
				$this->db->where('form_id', $form_id);
				$this->db->where('messages.private', 0);

	  	     	} else {
				$this->db->where('messages.readed', 0);
				$this->db->where('DATE(timestamp) >=', date('Y-m-d'));
				$this->db->where('DATE(timestamp) <=', date('Y-m-t'));
				$this->db->where('to_uid', $uid);
				$this->db->where('messages.private', 1);
	  	     	}

			$this->db->order_by('timestamp', 'DESC');
			return $this->db->get('messages')->result_array();
           	}

           	public function add_message($data) {
			  	$this->db->insert('messages', $data);
			  	return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
		 	}    


		   	public function get_emails_queue($role_id='', $hid='', $dep_id='', $to_uid=''){
		  	    if ($hid) {
			        $query = 'SELECT user_hotels.*, users.department
			        FROM user_hotels 
			    	LEFT JOIN users ON user_hotels.user_id = users.id
		  	    	WHERE user_hotels.hotel_id = '.$hid;
			    }else{
			      	$query = 'SELECT user_hotels.*, users.department
			        FROM user_hotels 
			    	LEFT JOIN users ON user_hotels.user_id = users.id';
			    }
	            $records = $this->db->query($query);
	            $rows    =  $records->result_array();
				if ($rows) {
	               	if($to_uid){
						$this->db->select('users.email');
	                   	$emails = $this->db->get_where('users',array('id'=>$to_uid))->result_array();
	               	}else{
			            $uids = array();	              
	               		if ($dep_id) {
		                   	foreach ($rows as $row) {
				              	$selected_department = json_decode($row['department']);
				              	if ($selected_department) {
					              	if (in_array($dep_id, $selected_department)) {
					             		array_push($uids, $row['user_id']);
					             	}
					             	
				              	}
				            }
		               	}else{
			               	foreach ($rows as $row) {
				              	$selected_roles = json_decode($row['role_id']);
				              	if ($selected_roles) {
					              	if (in_array($role_id, $selected_roles)) {
					             		array_push($uids, $row['user_id']);
					             	}
					             	
				              	}
				            }
		               	}
				       	if ($uids) {
			                $this->db->select('users.email');
			                $this->db->where_in('users.id',$uids);
			                $emails = $this->db->get('users')->result_array();
				        }
				    }
	               	$qemails = array();
	               	foreach ($emails as $email) {
		               	array_push($qemails, $email['email']);
		            }
		            if ($qemails) {
	              return $qemails;
		            }else{
		          		return false;
		          	}
	        	}else{
	          		return false;
	          	}
           	}        

           	public function get_emails_queue_supplier($supplier_id=''){
	            if($supplier_id){
					$this->db->select('suppliers.cont_email');
	               	$emails = $this->db->get_where('suppliers',array('id'=>$to_uid))->result_array();
	            }else{
		            $this->db->select('suppliers.cont_email');
		            $emails = $this->db->get('suppliers')->result_array();
	            }
	            $qemails = array();
		        foreach ($emails as $email) {
	          		if (isset($email['email'])) {
		          		array_push($qemails, $email['email']);
		         	}
	         	}
	           	return implode(',',$qemails);
           	}        
	

	       public function add_mail($data) {

				  $this->db->insert('mails_queue', $data);

				  return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
			
			 }


		    public function get_code_id($table,$code='',$id='') {
			   	if ($code) {
                  	$this->db->select(''.$table.'.id');
                  	$this->db->where(''.$table.'.code',$code);
				}elseif($id) {
				  	$this->db->select(''.$table.'.code');
                  	$this->db->where(''.$table.'.id',$id);
				}
               	return $this->db->get(''.$table.'')->row_array();
			 }

			public function get_code_role($table,$code) {
                $this->db->select(''.$table.'.role_id, '.$table.'.role_hotel');
                $this->db->where(''.$table.'.code',$code);             
                return $this->db->get(''.$table.'')->row_array();
			}

			public function get_id_role($table,$id) {
                $this->db->select(''.$table.'.role_id, '.$table.'.role_hotel');
                $this->db->where(''.$table.'.id',$id);             
                return $this->db->get(''.$table.'')->row_array();
			}

			public function getdepname($code) {
                $this->db->select('departments.dep_name');
                $this->db->where('departments.id',$code);             
                return $this->db->get('departments')->row_array();
			} 

			public function hotelName($id) {
                $this->db->select('hotels.hotel_name');
                $this->db->where('hotels.id',$id);             
                return $this->db->get('hotels')->row_array();
			}

			public function getHotelName($name) {
                $this->db->select('hotels.id');
                $this->db->like('hotels.hotel_name',$name);             
                return $this->db->get('hotels')->row_array();
			}

			public function metaName($id) {
                $this->db->select('meta_data.type_name');
                $this->db->where('meta_data.id',$id);             
                return $this->db->get('meta_data')->row_array();
			} 

			public function metaID($type_name, $meta_keyword) {
                $this->db->select('meta_data.id');
                $this->db->like('meta_data.type_name',$type_name);             
                $this->db->where('meta_data.meta_keyword',$meta_keyword);             
                return $this->db->get('meta_data')->row_array();
			} 

			public function metadataes($id) {
                $this->db->select('meta_data.*');
                $this->db->where_in('meta_data.id',$id);             
                return $this->db->get('meta_data')->result_array();
			} 

			public function search_metaData($search, $hid, $keyword) {
                $this->db->select('meta_data.*');
                $this->db->like('meta_data.type_name',$search);             
                $this->db->where('meta_data.hid',$hid);             
                $this->db->where('meta_data.meta_keyword',$keyword);             
                return $this->db->get('meta_data')->result_array();
			} 

			public function search_employee($search, $hid) {
                $this->db->select('employee.*, user_groups.name AS position_name');
		    		 $this->db->join('user_groups','employee.position = user_groups.id','left');
                $this->db->where('employee.code',$search);             
                $this->db->where('employee.hid',$hid);             
                return $this->db->get('employee')->row_array();
			} 

			public function search_APIEmployee($code, $hid) {
                $this->db->select('api_employee.*, hotels.hotel_name, api_location.name AS location_name, api_departments.dep_name, api_user_groups.name AS position_name');
				$this->db->join('hotels','api_employee.emp_hid = hotels.id','left');
				$this->db->join('api_location','api_employee.emp_lid = api_location.id','left');
				$this->db->join('api_departments','api_employee.emp_did = api_departments.id','left');
				$this->db->join('api_user_groups','api_employee.emp_pid = api_user_groups.id','left');
                $this->db->where('api_employee.emp_code',$code);             
                $this->db->where('api_employee.emp_hid',$hid);      
				$query = $this->db->get('api_employee');       
                return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
			} 

			public function get_Data_all($table, $hid, $code) {
                $this->db->select(''.$table.'.*, api_location.hid');
				$this->db->join('api_location',''.$table.'.connection = api_location.connection','left');
                $this->db->where('api_location.hid', $hid);      
				$this->db->group_by(''.$table.'.'.$code.''); 
                return $this->db->get(''.$table.'')->result_array();
			}

			public function get_limits($module_id) {
                $this->db->select('payment_setting.limit');
                $this->db->where('payment_setting.module_id', $module_id);             
                $this->db->where('payment_setting.active', 1);             
                $this->db->where('payment_setting.deleted', 0);    
				$this->db->order_by("payment_setting.id", "DESC");  
				$this->db->limit(1);       
                return $this->db->get('payment_setting')->row_array();
			} 

			public function currencyName($id) {
                $this->db->select('currencies.symbol');
                $this->db->where('currencies.id',$id);             
                return $this->db->get('currencies')->row_array();
			} 

			public function userName($id) {
                $this->db->select('users.fullname');
                $this->db->where('users.id',$id);             
                return $this->db->get('users')->row_array();
			} 

			public function get_currency_symbol($currency_name) {
                  $this->db->select('currencies.symbol');
                  $this->db->where('currencies.vname',$currency_name);             
                  $row = $this->db->get('currencies')->row_array();
			      return $row['symbol'];
			 }


		    public function get_delivery_paments($hotel_id,$from,$to){
	            $query = 'SELECT delivery.* FROM delivery
  	    	           WHERE delivery.deleted = 0 AND delivery.hid = '.$hotel_id.' AND delivery_date BETWEEN ("'.$from.'") AND ("'.$to.'")';
	             $records = $this->db->query($query);
	             return $records->result_array();

	          }

	        public function get_delivery_items($delivery_ids){
	          if ($delivery_ids) {
		         $query = 'SELECT sum(delivery_items.net_amount) AS hotel_pos
		          	   	   FROM delivery_items
	  	    	           WHERE delivery_items.delivery_id IN('.implode(',',$delivery_ids).')';
	             $records = $this->db->query($query);
	            
	             return $records->row_array();
	             	

	              }else{

					return 0;
				}
	        }   	       	  


	        public function count_delivery_items($delivery_ids){
	          if ($delivery_ids) {
		         $query = 'SELECT delivery_items.id AS hotel_pos
		          	   	FROM delivery_items
	  	    	        WHERE delivery_items.delivery_id IN('.implode(',',$delivery_ids).') AND delivery_items.type != 4';
	             $records = $this->db->query($query);

	             return $records->num_rows();

              }else{

				return 0;
				}
		     }


		    public function get_users_by_roleId($role_id){

		    	return $this->db->get_where('users',array('role_id'=>$role_id))->result_array();
		    
		    }

		    public function get_module_status($module_id){

		    	return $this->db->get_where('status',array('id !='=>'0'))->result_array();
		    
		    } 

		    public function get_all_roles(){
                $this->db->order_by('id','ASC');
		    	return $this->db->get('user_groups')->result_array();
		    }

		    public function get_meta_data($meta_keyword){
                $this->db->where(array('meta_data.meta_keyword'=>$meta_keyword,'deleted'=>0));
                $this->db->order_by('rank','ASC');
		    	return $this->db->get('meta_data')->result_array();
		    }

		    public function get_meta_id($id){
                $this->db->where(array('meta_data.id'=>$id,'deleted'=>0));
		    	return $this->db->get('meta_data')->row_array();
		    }

		    public function get_meta_keys($type){
                $this->db->where(array('meta_data.type_data'=>$type,'deleted'=>0));
                $this->db->order_by('rank','ASC');
		    	return $this->db->get('meta_data')->result_array();
		    }

		    public function get_meta_keys_data($meta_keyword, $type){
                $this->db->where(array('meta_data.meta_keyword'=>$meta_keyword,'meta_data.type_data'=>$type,'deleted'=>0));
                $this->db->order_by('rank','ASC');
		    	return $this->db->get('meta_data')->result_array();
		    }

		    public function get_forms_to_sign($uhotels,$uroles,$t_attr,$module,$filter_count=''){
		        $query = form_to_sign($uroles,$uhotels,$t_attr,$module,'');
				      if ($filter_count == 'count') {
				      	 // $query .=' GROUP BY form_id';
		                    return $this->db->query($query)->num_rows();
		               }else{
			                    $query .='  ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].' 
			                               LIMIT '.$t_attr['start'].','.$t_attr['length'].'';             
		                    return $this->db->query($query)->result_array();
		             }
		          }


		    public function get_all_forms_to_sign($uhotels,$uroles,$sign_type){
		     
		       return $this->db->query(form_to_sign($this->data['uroles'],$this->data['uhotels'],'',''))->result_array();
		    } 

		    public function get_modules_to_sign($uhotels,$uroles){
		    	$this->db->select('signatures.*,COUNT(module_id) AS module_count,modules.name AS module_name, modules.link AS module_link,modules.view_link AS module_view,modules.tdatabase AS form_table,modules.text_color,modules.background_color,modules.menu_icon');
		    	$this->db->join('modules','signatures.module_id = modules.id','left');
		    	$this->db->where('signatures.deleted', 0);
		    	$this->db->where('signatures.reject', 0);
		    	$this->db->where('signatures.user_id IS NULL');
		    	$this->db->where_in('signatures.role_id', 1);
		    	$this->db->where_in('signatures.hid', $uhotels);
		    	$this->db->group_by('signatures.module_id');
		    	return $this->db->get('signatures')->result_array();
		    } 

   }

?>