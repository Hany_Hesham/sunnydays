<?php

	class Log_model extends CI_Model{

		public function get_logs($start,$length,$search=false,$order='',$col_name='',$filter_count='',$single=''){
			$this->db->select('log.*,modules.name AS module_name,users.fullname');
			$this->db->join('modules','log.module_id = modules.id','left');
			$this->db->join('users','log.user_id = users.id','left');
	  	   	if ($single['module_id'] !='' && $single['form_id'] !='') {
			 	$this->db->where('log.module_id', $single['module_id']);
			 	$this->db->where('log.target_id', $single['form_id']);
         	}     
	  	   	if ($search) {
				$this->db->where('(action like "%'.$search.'%" OR target like "%'.$search.'%" OR modules.name like "%'.$search.'%" OR users.fullname like "%'.$search.'%"  OR comments like "%'.$search.'%"  OR log_time like "%'.$search.'%")');
			}
         	if ($filter_count == 'count') { 
      			return $this->db->count_all_results("log");
	      	}else{
			   $this->db->order_by($col_name,$order);
       			return $this->db->limit($length,$start)->get("log")->result_array();
	      	}   
	   	}

      	public function get_all_logs($single=''){
			$this->db->select('log.*');
			if ($single['module_id'] !='' && $single['form_id'] !='') {
			 	$this->db->where('log.module_id', $single['module_id']);
			 	$this->db->where('log.target_id', $single['form_id']);
         	} 
      		return $this->db->count_all_results("log");
		} 
		
		public function get_loged($start,$length,$search=false,$order='',$col_name='',$filter_count='',$single=''){
			$this->db->select('log.*,modules.name AS module_name,users.fullname');
			$this->db->join('modules','log.module_id = modules.id','left');
			$this->db->join('users','log.user_id = users.id','left');
	  	   	if ($single['user_id'] !='') {
			 	$this->db->where('log.user_id', $single['user_id']);
         	}     
	  	   	if ($search) {
				$this->db->where('(action like "%'.$search.'%" OR target like "%'.$search.'%" OR modules.name like "%'.$search.'%" OR users.fullname like "%'.$search.'%"  OR comments like "%'.$search.'%"  OR log_time like "%'.$search.'%")');
			}
         	if ($filter_count == 'count') { 
      			return $this->db->count_all_results("log");
	      	}else{
			   $this->db->order_by($col_name,$order);
       			return $this->db->limit($length,$start)->get("log")->result_array();
	      	}   
	   	}

     	public function get_all_loged($single=''){
			$this->db->select('log.*');
			if ($single['user_id'] !='') {
				$this->db->where('log.user_id', $single['user_id']);
			}  
      		return $this->db->count_all_results("log");
		} 

		public function get_log($id){
			$this->db->select('log.*,modules.name AS module_name,users.fullname');
			$this->db->join('modules','log.module_id = modules.id','left');
			$this->db->join('users','log.user_id = users.id','left');
			$this->db->where('log.id',$id);
			return $this->db->get('log')->result_array();
		} 

		public function get_logData($module_id, $form_id, $action){
			$this->db->select('log.*,modules.name AS module_name, users.fullname');
			$this->db->join('modules','log.module_id = modules.id','left');
			$this->db->join('users','log.user_id = users.id','left');
			$this->db->where('log.module_id',$module_id);
			$this->db->where('log.target_id',$form_id);
			$this->db->like('log.action',$action);
			return $this->db->get('log')->result_array();
		}  

 	}

?>	