<?php

	class Email_model extends CI_Model{

		public function get_emails($start,$length,$search=false,$order='',$col_name='',$filter_count='',$single=''){
	       	$this->db->select('mails_queue.*, modules.name AS module_name, users.fullname');
			$this->db->join('modules','mails_queue.module_id = modules.id','left');
			$this->db->join('users','mails_queue.uid = users.id','left');
	  	    if ($single['module_id'] !='' && $single['form_id'] !='') {
			 	$this->db->where('mails_queue.module_id', $single['module_id']);
			 	$this->db->where('mails_queue.form_id', $single['form_id']);
            }     
	  	    if ($search) {
				$this->db->where('(type like "%'.$search.'%" OR modules.name like "%'.$search.'%" OR emails like "%'.$search.'%" OR form_id like "%'.$search.'%" OR users.fullname like "%'.$search.'%" OR message like "%'.$search.'%" OR sent_at like "%'.$search.'%" OR timestamp like "%'.$search.'%")');
			}
          	if ($filter_count == 'count') { 
      			return $this->db->count_all_results("mails_queue");
	        }else{
			    $this->db->order_by($col_name,$order);
       			return $this->db->limit($length,$start)->get("mails_queue")->result_array();
	       	}   
	   	}

        public function get_all_emails($single=''){
        	$this->db->select('mails_queue.*');
	  	    if ($single['module_id'] !='' && $single['form_id'] !='') {
			 	$this->db->where('mails_queue.module_id', $single['module_id']);
			 	$this->db->where('mails_queue.form_id', $single['form_id']);
            } 
      		return $this->db->count_all_results("mails_queue");
		}  

		public function get_email($id){
			$this->db->select('mails_queue.*, modules.name AS module_name, users.fullname');
			$this->db->join('modules','mails_queue.module_id = modules.id','left');
			$this->db->join('users','mails_queue.uid = users.id','left');
			$this->db->where('mails_queue.id',$id);
			return $this->db->get('mails_queue')->result_array();
		}  

  	}

?>	