<?php

	class User_groups_model extends CI_Model{
	
  		function __contruct(){
			parent::__construct;
  	  	}

		public function get_user_groups_ajax($start,$length,$search=false,$order=false,$col_name=false,$filter_count=false){
	        $this->db->select('user_groups.*');
	       	if ($search) {
		 		$this->db->like('user_groups.name', $search);
	        }
	        if ($filter_count == 'count') { 
      			return $this->db->count_all_results("user_groups");
		    }else{
	          	$this->db->order_by($col_name,$order);
       			return $this->db->limit($length,$start)->get("user_groups")->result_array();
		    }    
    	}

    	public function get_all_user_groups(){
			return $this->db->count_all_results("user_groups");
    	}  

    	public function get_by_id($id){
			$this->db->where('id', $id);
			return $this->db->get('user_groups')->row_array();
  	  	}

  	  	public function get_user_groups(){
	  		$this->db->select('user_groups.*');
			$this->db->where('user_groups.deleted', 0);
			return $this->db->get('user_groups')->result_array();
  	  	}

    	public function add_user_group($data){
			$this->db->insert('user_groups', $data);
			return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
		 }  


		public function update_user_group($hotel_id,$data){
		 	$this->db->where('user_groups.id', $hotel_id);
			$this->db->update('user_groups', $data);
			if ($this->db->affected_rows() >= 0) {
                return $this->db->affected_rows();
            } 
        }	 	 

	}

?>