<?php

  class Treatment_model extends CI_Model{

    public function get_treatments($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('treatment.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','treatment.uid = users.id','left');
      $this->db->join('hotels','treatment.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','treatment.role_id = role_group.id','left');
      $this->db->join('status','treatment.status = status.id','left');
      $this->db->where('treatment.deleted', 0);
      $this->db->where_in('treatment.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','treatment',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(treatment.id like "%'.$t_attr['search'].'%" OR treatment.remarks like "%'.$t_attr['search'].'%" OR treatment.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("treatment");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("treatment")->result_array();
      }
    }

    public function get_all_treatments($uhotels){
      $this->db->select('treatment.*');
      $this->db->where_in('treatment.hid',$uhotels);
      $this->db->where('treatment.deleted', 0);
      return $this->db->count_all_results("treatment");
    }   

    public function add_treatment($data){
      $this->db->insert('treatment', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_treatment_item($data){
      $this->db->insert('treatment_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_treatment($treatment_id) {
      $this->db->select('treatment.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','treatment.uid = users.id','left');
      $this->db->join('hotels','treatment.hid = hotels.id','left');
      $this->db->join('status','treatment.status = status.id','left');
      $this->db->where('treatment.id', $treatment_id);
      $this->db->where('treatment.deleted', 0);   
      $query = $this->db->get('treatment');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_treatment_items($treatment_id) {
      $this->db->select('treatment_item.*, vip_types.type_name AS vip_type, guest_status.type_name AS guest_status');
      $this->db->join('meta_data AS vip_types','treatment_item.vip_id = vip_types.id','left');
      $this->db->join('meta_data AS guest_status','treatment_item.status_id = guest_status.id','left');
      $this->db->where(array('treatment_item.deleted'=>0,'treatment_item.treatment_id'=>$treatment_id));
      return $this->db->get('treatment_item')->result_array();
    }

    function edit_treatment($data, $treatment_id) {
      $this->db->where('treatment.id', $treatment_id);   
      $this->db->update('treatment', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_treatment_item($item_id) {
      $this->db->select('treatment_item.*, vip_types.type_name AS vip_type, guest_status.type_name AS guest_status');
      $this->db->join('meta_data AS vip_types','treatment_item.vip_id = vip_types.id','left');
      $this->db->join('meta_data AS guest_status','treatment_item.status_id = guest_status.id','left');
      $this->db->where(array('treatment_item.deleted'=>0,'treatment_item.id'=>$item_id));
      return $this->db->get('treatment_item')->row_array();
    }

    function edit_treatment_item($data, $item_id) {
      $this->db->where('treatment_item.id', $item_id);   
      $this->db->update('treatment_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>