<?php

  class Rebate_model extends CI_Model{

    public function get_rebates($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('rebate.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','rebate.uid = users.id','left');
      $this->db->join('hotels','rebate.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','rebate.role_id = role_group.id','left');
      $this->db->join('status','rebate.status = status.id','left');
      $this->db->where('rebate.deleted', 0);
      $this->db->where_in('rebate.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','rebate',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(rebate.id like "%'.$t_attr['search'].'%" OR rebate.room_no like "%'.$t_attr['search'].'%" OR rebate.name like "%'.$t_attr['search'].'%" OR rebate.acc_no like "%'.$t_attr['search'].'%" OR rebate.exp like "%'.$t_attr['search'].'%" OR rebate.reasons like "%'.$t_attr['search'].'%" OR rebate.remarks like "%'.$t_attr['search'].'%" OR rebate.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("rebate");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("rebate")->result_array();
      }
    }

    public function get_all_rebates($uhotels){
      $this->db->select('rebate.*');
      $this->db->where_in('rebate.hid',$uhotels);
      $this->db->where('rebate.deleted', 0);
      return $this->db->count_all_results("rebate");
    }   

    public function add_rebate($data){
      $this->db->insert('rebate', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_rebate($rebate_id) {
      $this->db->select('rebate.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','rebate.uid = users.id','left');
      $this->db->join('hotels','rebate.hid = hotels.id','left');
      $this->db->join('status','rebate.status = status.id','left');
      $this->db->where('rebate.id', $rebate_id);
      $this->db->where('rebate.deleted', 0);   
      $query = $this->db->get('rebate');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_rebate($data, $rebate_id) {
      $this->db->where('rebate.id', $rebate_id);   
      $this->db->update('rebate', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>