<?php

  class Check_out_model extends CI_Model{

    public function get_check_outs($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('check_out.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','check_out.uid = users.id','left');
      $this->db->join('hotels','check_out.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','check_out.role_id = role_group.id','left');
      $this->db->join('status','check_out.status = status.id','left');
      $this->db->where('check_out.deleted', 0);
      $this->db->where_in('check_out.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','check_out',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(check_out.id like "%'.$t_attr['search'].'%" OR check_out.room_no like "%'.$t_attr['search'].'%" OR check_out.name like "%'.$t_attr['search'].'%" OR check_out.arrival like "%'.$t_attr['search'].'%" OR check_out.departure like "%'.$t_attr['search'].'%" OR check_out.travel_agent like "%'.$t_attr['search'].'%" OR check_out.departure_time like "%'.$t_attr['search'].'%" OR check_out.reasons like "%'.$t_attr['search'].'%" OR check_out.remarks like "%'.$t_attr['search'].'%" OR check_out.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("check_out");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("check_out")->result_array();
      }
    }

    public function get_all_check_outs($uhotels){
      $this->db->select('check_out.*');
      $this->db->where_in('check_out.hid',$uhotels);
      $this->db->where('check_out.deleted', 0);
      return $this->db->count_all_results("check_out");
    }   

    public function add_check_out($data){
      $this->db->insert('check_out', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_check_out($check_out_id) {
      $this->db->select('check_out.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','check_out.uid = users.id','left');
      $this->db->join('hotels','check_out.hid = hotels.id','left');
      $this->db->join('status','check_out.status = status.id','left');
      $this->db->where('check_out.id', $check_out_id);
      $this->db->where('check_out.deleted', 0);   
      $query = $this->db->get('check_out');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_check_out($data, $check_out_id) {
      $this->db->where('check_out.id', $check_out_id);   
      $this->db->update('check_out', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>