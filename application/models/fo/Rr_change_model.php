<?php

  class Rr_change_model extends CI_Model{

    public function get_rr_changes($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('rr_change.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','rr_change.uid = users.id','left');
      $this->db->join('hotels','rr_change.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','rr_change.role_id = role_group.id','left');
      $this->db->join('status','rr_change.status = status.id','left');
      $this->db->where('rr_change.deleted', 0);
      $this->db->where_in('rr_change.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','rr_change',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(rr_change.id like "%'.$t_attr['search'].'%" OR rr_change.from_room like "%'.$t_attr['search'].'%" OR rr_change.name like "%'.$t_attr['search'].'%" OR rr_change.to_room like "%'.$t_attr['search'].'%" OR rr_change.from_rate like "%'.$t_attr['search'].'%" OR rr_change.travel_agent like "%'.$t_attr['search'].'%" OR rr_change.departure_time like "%'.$t_attr['search'].'%" OR rr_change.to_rate like "%'.$t_attr['search'].'%" OR rr_change.remarks like "%'.$t_attr['search'].'%" OR rr_change.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("rr_change");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("rr_change")->result_array();
      }
    }

    public function get_all_rr_changes($uhotels){
      $this->db->select('rr_change.*');
      $this->db->where_in('rr_change.hid',$uhotels);
      $this->db->where('rr_change.deleted', 0);
      return $this->db->count_all_results("rr_change");
    }   

    public function add_rr_change($data){
      $this->db->insert('rr_change', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_rr_change($rr_change_id) {
      $this->db->select('rr_change.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','rr_change.uid = users.id','left');
      $this->db->join('hotels','rr_change.hid = hotels.id','left');
      $this->db->join('status','rr_change.status = status.id','left');
      $this->db->where('rr_change.id', $rr_change_id);
      $this->db->where('rr_change.deleted', 0);   
      $query = $this->db->get('rr_change');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_rr_change($data, $rr_change_id) {
      $this->db->where('rr_change.id', $rr_change_id);   
      $this->db->update('rr_change', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>