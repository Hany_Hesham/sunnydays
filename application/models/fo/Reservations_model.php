<?php

  class Reservations_model extends CI_Model{

    public function get_reservationss($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('reservations.*, users.fullname, hotels.hotel_name, meta_data.type_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','reservations.uid = users.id','left');
      $this->db->join('hotels','reservations.hid = hotels.id','left');
      $this->db->join('meta_data','reservations.type_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','reservations.role_id = role_group.id','left');
      $this->db->join('status','reservations.status = status.id','left');
      $this->db->where('reservations.deleted', 0);
      $this->db->where_in('reservations.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','reservations',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(reservations.id like "%'.$t_attr['search'].'%" OR reservations.name like "%'.$t_attr['search'].'%" OR reservations.agent like "%'.$t_attr['search'].'%" OR reservations.contact like "%'.$t_attr['search'].'%" OR reservations.arrival like "%'.$t_attr['search'].'%" OR reservations.departure like "%'.$t_attr['search'].'%" OR reservations.phone like "%'.$t_attr['search'].'%" OR reservations.fax like "%'.$t_attr['search'].'%" OR reservations.payment_method like "%'.$t_attr['search'].'%" OR reservations.instruction like "%'.$t_attr['search'].'%" OR reservations.remarks like "%'.$t_attr['search'].'%" OR reservations.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("reservations");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("reservations")->result_array();
      }
    }

    public function get_all_reservationss($uhotels){
      $this->db->select('reservations.*');
      $this->db->where_in('reservations.hid',$uhotels);
      $this->db->where('reservations.deleted', 0);
      return $this->db->count_all_results("reservations");
    }   

    public function add_reservations($data){
      $this->db->insert('reservations', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_reservations_item($data){
      $this->db->insert('reservations_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_reservations($reservations_id) {
      $this->db->select('reservations.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, meta_data.type_name, status.status_name');
      $this->db->join('users','reservations.uid = users.id','left');
      $this->db->join('hotels','reservations.hid = hotels.id','left');
      $this->db->join('meta_data','reservations.type_id = meta_data.id','left');
      $this->db->join('status','reservations.status = status.id','left');
      $this->db->where('reservations.id', $reservations_id);
      $this->db->where('reservations.deleted', 0);   
      $query = $this->db->get('reservations');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_reservations_items($reservations_id) {
      $this->db->select('reservations_item.*, meta_data.type_name AS room_type');
      $this->db->join('meta_data','reservations_item.type_id = meta_data.id','left');
      $this->db->where(array('reservations_item.deleted'=>0,'reservations_item.reservations_id'=>$reservations_id));
      return $this->db->get('reservations_item')->result_array();
    }

    function edit_reservations($data, $reservations_id) {
      $this->db->where('reservations.id', $reservations_id);   
      $this->db->update('reservations', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_reservations_item($item_id) {
      $this->db->select('reservations_item.*, meta_data.type_name AS room_type');
      $this->db->join('meta_data','reservations_item.type_id = meta_data.id','left');
      $this->db->where(array('reservations_item.deleted'=>0,'reservations_item.id'=>$item_id));
      return $this->db->get('reservations_item')->row_array();
    }

    function edit_reservations_item($data, $item_id) {
      $this->db->where('reservations_item.id', $item_id);   
      $this->db->update('reservations_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>