<?php

  class Activities_model extends CI_Model{

    public function get_activitiess($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('activities.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','activities.uid = users.id','left');
      $this->db->join('hotels','activities.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','activities.role_id = role_group.id','left');
      $this->db->join('status','activities.status = status.id','left');
      $this->db->where('activities.deleted', 0);
      $this->db->where_in('activities.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','activities',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(activities.id like "%'.$t_attr['search'].'%" OR activities.month like "%'.$t_attr['search'].'%" OR activities.remarks like "%'.$t_attr['search'].'%" OR activities.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("activities");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("activities")->result_array();
      }
    }

    public function get_all_activitiess($uhotels){
      $this->db->select('activities.*');
      $this->db->where_in('activities.hid',$uhotels);
      $this->db->where('activities.deleted', 0);
      return $this->db->count_all_results("activities");
    }   

    public function add_activities($data){
      $this->db->insert('activities', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_activities_item($data){
      $this->db->insert('activities_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_activities($activities_id) {
      $this->db->select('activities.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name');
      $this->db->join('users','activities.uid = users.id','left');
      $this->db->join('hotels','activities.hid = hotels.id','left');
      $this->db->join('status','activities.status = status.id','left');
      $this->db->where('activities.id', $activities_id);
      $this->db->where('activities.deleted', 0);   
      $query = $this->db->get('activities');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_activities_items($activities_id) {
      $this->db->select('activities_item.*, meta_data.type_name AS activity_name');
      $this->db->join('meta_data','activities_item.activity = meta_data.id','left');
      $this->db->where(array('activities_item.deleted'=>0,'activities_item.activities_id'=>$activities_id));
      return $this->db->get('activities_item')->result_array();
    }

    function edit_activities($data, $activities_id) {
      $this->db->where('activities.id', $activities_id);   
      $this->db->update('activities', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_activities_item($item_id) {
      $this->db->select('activities_item.*, meta_data.type_name AS activity_name');
      $this->db->join('meta_data','activities_item.activity = meta_data.id','left');
      $this->db->where(array('activities_item.deleted'=>0,'activities_item.id'=>$item_id));
      return $this->db->get('activities_item')->row_array();
    }

    function edit_activities_item($data, $item_id) {
      $this->db->where('activities_item.id', $item_id);   
      $this->db->update('activities_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>