<?php defined('BASEPATH') or exit('No direct script access allowed');

    class Api_model extends CI_Model {

        public function __construct() {
            parent::__construct();
        }

        public function value($value) {
            if($value){
                return $value;
            }else{
                return '';
            }
        }

        public function check_token($token) {
            $this->db->where('token', $token);
            $user = $this->db->get('user_api')->row();
            if(isset($user)){
                return true;
            }
            return false;
        }

        function get_location($connection, $code) {
            $this->db->select('api_location.*');
            $this->db->where(array('api_location.connection'=>$connection,'api_location.code'=>$code));   
            return $this->db->get('api_location')->row_array();
        }

        function get_department($connection, $code) {
            $this->db->select('api_departments.*');
            $this->db->where(array('api_departments.connection'=>$connection,'api_departments.dep_code'=>$code));   
            return $this->db->get('api_departments')->row_array();
        }

        function get_position($connection, $code) {
            $this->db->select('api_user_groups.*');
            $this->db->where(array('api_user_groups.connection'=>$connection,'api_user_groups.code'=>$code));   
            return $this->db->get('api_user_groups')->row_array();
        }

        function get_employee($connection, $code) {
            $this->db->select('api_employee.*');
            $this->db->where(array('api_employee.connection'=>$connection,'api_employee.emp_code'=>$code));   
            return $this->db->get('api_employee')->row_array();
        }

    }
?>