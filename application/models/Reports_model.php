<?php

class Reports_model extends CI_Model{

	public function get_reports(){
		$this->db->order_by('rank','ASC');		
		return $this->db->get_where('reports',array('deleted'=>0))->result_array();

	}  

	public function get_reports_modules(){
		$this->db->select('reports.*,modules.name as module_name');
		$this->db->join('modules','reports.module_id = modules.id','left');
		$this->db->where(array('reports.deleted'=>0));
		$this->db->group_by('module_id');
		$this->db->order_by('rank','ASC');		
		return $this->db->get('reports')->result_array();

	}  

	public function get_reports_by_module($module_id){
		$this->db->order_by('rank','ASC');		
		return $this->db->get_where('reports',array('deleted'=>0,'module_id'=>$module_id))->result_array();

	}


	public function custom_search_query($custom_search,$extra_filters='', $table=''){
		$query='';
		if (isset($custom_search['hotel_id']) && $custom_search['hotel_id'] !='') {
			$query .=' AND '.$table.'.hid IN('.implode(',',$custom_search['hotel_id']).')';
		}
		if (isset($custom_search['dep_code']) && $custom_search['dep_code'] !='') {
			$query .=' AND '.$table.'.dep_code IN("'.implode('","',$custom_search['dep_code']).'")';
		}  
		if (isset($custom_search['toDate']) && $custom_search['fromDate'] && $custom_search['toDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) BETWEEN ("'.$custom_search['fromDate'].'") AND ("'.$custom_search['toDate'].'")';
		}
		if (isset($custom_search['toDate']) && $custom_search['fromDate'] && !$custom_search['toDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) >= "'.$custom_search['fromDate'].'"';
		}
		if (isset($custom_search['toDate']) && $custom_search['toDate'] && !$custom_search['fromDate']) {
			$query .=' AND DATE_FORMAT('.$table.'.timestamp,GET_FORMAT(DATE,"JIS")) <= "'.$custom_search['toDate'].'"';
		} 
		if (isset($custom_search['status']) && $custom_search['status'] !='') {
			$query .=' AND '.$table.'.status IN('.implode(',',$custom_search['status']).')';
		}    

		return $query; 	   	

	} 
	
	/**
     * ***************************
     * General Reports Section   *
     *****************************
     */



    /**
      * 1- Salary Advance Report
    */
	public function get_salary_advance_report($uhotels,$t_attr='',$custom_search='',$filter_count='')
	{
		$query = 'SELECT salary_advance.*, users.fullname, hotels.hotel_name, 
				departments.dep_name, user_groups.name AS position_name, 
				role_group.name AS role_name, status.status_name, status.status_color
			FROM salary_advance
			LEFT JOIN users ON salary_advance.uid = users.id
			LEFT JOIN departments ON salary_advance.dep_code = departments.id
			LEFT JOIN hotels ON salary_advance.hid = hotels.id
			LEFT JOIN user_groups ON salary_advance.position = user_groups.id
			LEFT JOIN user_groups AS role_group ON salary_advance.role_id = role_group.id
			LEFT JOIN status ON salary_advance.status = status.id
			WHERE  salary_advance.deleted = 0 AND salary_advance.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','salary_advance');  
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (salary_advance.id like "%'.$t_attr['search'].'%"
				    OR salary_advance.clock_no like "%'.$t_attr['search'].'%"
					OR salary_advance.name like "%'.$t_attr['search'].'%"
					OR salary_advance.remarks like "%'.$t_attr['search'].'%" 
					OR salary_advance.timestamp like "%'.$t_attr['search'].'%" 
					OR users.fullname like "%'.$t_attr['search'].'%" 
					OR hotels.hotel_name like "%'.$t_attr['search'].'%" 
					OR departments.dep_name like "%'.$t_attr['search'].'%" 
					OR user_groups.name like "%'.$t_attr['search'].'%" 
					OR role_group.name like "%'.$t_attr['search'].'%" 
					OR status.status_name like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_salary_advance($uhotels)
	{
		$this->db->where_in('salary_advance.hid',$uhotels);
		return $this->db->get_where('salary_advance',array('salary_advance.deleted'=>0))->result_array();
	}

	/**
      * 1- complimentary Report
    */
	public function get_complimentary_report($uhotels,$t_attr='',$custom_search='',$filter_count='')
	{
		$query = 'SELECT complimentary.*, users.fullname, hotels.hotel_name, 
				departments.dep_name,role_group.name AS role_name, status.status_name, 
				status.status_color
			FROM complimentary
			LEFT JOIN users ON complimentary.uid = users.id
			LEFT JOIN departments ON complimentary.dep_code = departments.id
			LEFT JOIN hotels ON complimentary.hid = hotels.id
			LEFT JOIN user_groups AS role_group ON complimentary.role_id = role_group.id
			LEFT JOIN status ON complimentary.status = status.id
			WHERE  complimentary.deleted = 0 AND complimentary.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','complimentary');  
			if ($custom_search['complimentary_companies'] !='') {
				$query .=' AND company IN("'.implode('","',$custom_search['complimentary_companies']).'")';
			}
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (complimentary.id like "%'.$t_attr['search'].'%"
				    OR complimentary.company like "%'.$t_attr['search'].'%"
					OR complimentary.conf_no like "%'.$t_attr['search'].'%"
					OR complimentary.name like "%'.$t_attr['search'].'%"
					OR complimentary.room_type like "%'.$t_attr['search'].'%"
					OR complimentary.payment like "%'.$t_attr['search'].'%" 
					OR complimentary.reasons like "%'.$t_attr['search'].'%" 
					OR complimentary.remarks like "%'.$t_attr['search'].'%" 
					OR complimentary.timestamp like "%'.$t_attr['search'].'%" 
					
					OR users.fullname like "%'.$t_attr['search'].'%" 
					OR hotels.hotel_name like "%'.$t_attr['search'].'%" 
					OR departments.dep_name like "%'.$t_attr['search'].'%" 
					OR role_group.name like "%'.$t_attr['search'].'%" 
					OR status.status_name like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_complimentary($uhotels)
	{
		$this->db->where_in('complimentary.hid',$uhotels);
		return $this->db->get_where('complimentary',array('complimentary.deleted'=>0))->result_array();
	}

	public function get_complimentary_companies()
	{
		$this->db->distinct();
		$this->db->select('company');
		return $this->db->get('complimentary')->result_array();	
	}

    /**
     * ***************************
     * Payable Reports Section   *
     *****************************
    */



    /**
      * 1- petty cash Report
    */
	public function get_petty_cash_report($uhotels,$t_attr='',$custom_search='',$filter_count='')
	{
		$query = 'SELECT petty_cash_item.*,petty_cash.hid, petty_cash.code, petty_cash.name,
		        petty_cash.explanation,petty_cash.currency, petty_cash.position_name, 
				users.fullname, hotels.hotel_name, user_groups.name AS role_name, 
				status.status_name, status.status_color
			FROM petty_cash_item
			LEFT JOIN petty_cash ON petty_cash_item.petty_cash_id = petty_cash.id
			LEFT JOIN users ON petty_cash.uid = users.id
			LEFT JOIN hotels ON petty_cash.hid = hotels.id
			LEFT JOIN user_groups ON petty_cash.role_id = user_groups.id
			LEFT JOIN status ON petty_cash.status = status.id
			WHERE  petty_cash_item.deleted = 0 AND petty_cash.deleted = 0 AND petty_cash.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters','petty_cash');  
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND (petty_cash.id like "%'.$t_attr['search'].'%" 
				OR petty_cash.name like "%'.$t_attr['search'].'%" 
				OR petty_cash.explanation like "%'.$t_attr['search'].'%" 
				OR petty_cash.remarks like "%'.$t_attr['search'].'%" 
				OR petty_cash.timestamp like "%'.$t_attr['search'].'%" 
				OR users.fullname like "%'.$t_attr['search'].'%" 
				OR hotels.hotel_name like "%'.$t_attr['search'].'%" 
				OR petty_cash.position_name like "%'.$t_attr['search'].'%" 
				OR status.status_name like "%'.$t_attr['search'].'%"
				OR petty_cash_item.acc_no like "%'.$t_attr['search'].'%"
				OR petty_cash_item.notes like "%'.$t_attr['search'].'%"
				OR petty_cash_item.description like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_petty_cash($uhotels)
	{
		$this->db->select('petty_cash_item.*, petty_cash.code');
		$this->db->join('petty_cash','petty_cash_item.petty_cash_id = petty_cash.id','left');
		$this->db->where_in('petty_cash.hid',$uhotels);
		return $this->db->get_where('petty_cash_item',['petty_cash.deleted'=>0, 'petty_cash_item.deleted'=>0])->result_array();
	}

	/**
      * 1- Payment Voucher Report
    */
	public function get_payment_voucher_report($uhotels,$t_attr='',$custom_search='',$filter_count='')
	{
		$query = 'SELECT  payment_voucher_item.*, payment_voucher.hid, payment_voucher.explanation, 
		        payment_voucher.currency, users.fullname,
				hotels.hotel_name, user_groups.name AS role_name, status.status_name, status.status_color
			FROM  payment_voucher_item
			LEFT JOIN payment_voucher ON  payment_voucher_item. payment_voucher_id =  payment_voucher.id
			LEFT JOIN users ON  payment_voucher.uid = users.id
			LEFT JOIN hotels ON  payment_voucher.hid = hotels.id
			LEFT JOIN user_groups ON  payment_voucher.role_id = user_groups.id
			LEFT JOIN status ON  payment_voucher.status = status.id
			WHERE payment_voucher_item.deleted = 0 AND  payment_voucher.deleted = 0 AND  payment_voucher.hid IN('.implode(',',$uhotels).')';

		if ($custom_search) {
			$query .= $this->custom_search_query($custom_search,'extra_filters',' payment_voucher');  
		} 
		if ($t_attr) {
			if ($t_attr['search']) {
				$query .= ' AND ( payment_voucher.id like "%'.$t_attr['search'].'%"  
				OR  payment_voucher.explanation like "%'.$t_attr['search'].'%" 
				OR  payment_voucher.remarks like "%'.$t_attr['search'].'%" 
				OR  payment_voucher.timestamp like "%'.$t_attr['search'].'%" 
				OR users.fullname like "%'.$t_attr['search'].'%" 
				OR hotels.hotel_name like "%'.$t_attr['search'].'%" 
				OR status.status_name like "%'.$t_attr['search'].'%"
				OR  payment_voucher_item.acc_no like "%'.$t_attr['search'].'%"
				OR  payment_voucher_item.notes like "%'.$t_attr['search'].'%"
				OR  payment_voucher_item.description like "%'.$t_attr['search'].'%")';
			}
			if ($filter_count == 'count') { 
				$records = $this->db->query($query);
				return $records->num_rows();
			}else{
				$query .=' ORDER BY '.$t_attr['col_name'].' '.$t_attr['order'].'';
				$query .=' LIMIT '.$t_attr['start'].','.$t_attr['length'].'';
			}        
		}   
		$records = $this->db->query($query);
		return $records->result_array();
	}

	public function get_all_payment_voucher($uhotels)
	{
		$this->db->select(' payment_voucher_item.*');
		$this->db->join(' payment_voucher',' payment_voucher_item. payment_voucher_id =  payment_voucher.id','left');
		$this->db->where_in(' payment_voucher.hid',$uhotels);
		return $this->db->get_where(' payment_voucher_item',[' payment_voucher.deleted'=>0, ' payment_voucher_item.deleted'=>0])->result_array();
	}
	
}
?>	