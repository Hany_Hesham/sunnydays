<?php

  class Petty_cash_model extends CI_Model{

    public function get_petty_cashs($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('petty_cash.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color, meta_data.type_name');
      $this->db->join('users','petty_cash.uid = users.id','left');
      $this->db->join('hotels','petty_cash.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','petty_cash.role_id = role_group.id','left');
      $this->db->join('status','petty_cash.status = status.id','left');
      $this->db->join('meta_data','petty_cash.type_id = meta_data.id','left');
      $this->db->where('petty_cash.deleted', 0);
      $this->db->where_in('petty_cash.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','petty_cash',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(petty_cash.id like "%'.$t_attr['search'].'%" OR petty_cash.name like "%'.$t_attr['search'].'%" OR petty_cash.explanation like "%'.$t_attr['search'].'%" OR petty_cash.remarks like "%'.$t_attr['search'].'%" OR petty_cash.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("petty_cash");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("petty_cash")->result_array();
      }
    }

    public function get_all_petty_cashs($uhotels){
      $this->db->select('petty_cash.*');
      $this->db->where_in('petty_cash.hid',$uhotels);
      $this->db->where('petty_cash.deleted', 0);
      return $this->db->count_all_results("petty_cash");
    }   

    public function add_petty_cash($data){
      $this->db->insert('petty_cash', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_petty_cash_item($data){
      $this->db->insert('petty_cash_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_petty_cash($petty_cash_id) {
      $this->db->select('petty_cash.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name, meta_data.type_name');
      $this->db->join('users','petty_cash.uid = users.id','left');
      $this->db->join('hotels','petty_cash.hid = hotels.id','left');
      $this->db->join('status','petty_cash.status = status.id','left');
      $this->db->join('meta_data','petty_cash.type_id = meta_data.id','left');
      $this->db->where('petty_cash.id', $petty_cash_id);
      $this->db->where('petty_cash.deleted', 0);   
      $query = $this->db->get('petty_cash');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_petty_cash_items($petty_cash_id) {
      $this->db->select('petty_cash_item.*');
      $this->db->where(array('petty_cash_item.deleted'=>0,'petty_cash_item.petty_cash_id'=>$petty_cash_id));
      return $this->db->get('petty_cash_item')->result_array();
    }

    function edit_petty_cash($data, $petty_cash_id) {
      $this->db->where('petty_cash.id', $petty_cash_id);   
      $this->db->update('petty_cash', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_petty_cash_item($item_id) {
      $this->db->select('petty_cash_item.*');
      $this->db->where(array('petty_cash_item.deleted'=>0,'petty_cash_item.id'=>$item_id));
      return $this->db->get('petty_cash_item')->row_array();
    }

    function edit_petty_cash_item($data, $item_id) {
      $this->db->where('petty_cash_item.id', $item_id);   
      $this->db->update('petty_cash_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>