<?php

  class Bank_transfer_model extends CI_Model{

    public function get_bank_transfers($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('bank_transfer.*, users.fullname, hotels.hotel_name, meta_data.type_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','bank_transfer.uid = users.id','left');
      $this->db->join('hotels','bank_transfer.hid = hotels.id','left');
      $this->db->join('meta_data','bank_transfer.type_id = meta_data.id','left');
      $this->db->join('user_groups AS role_group','bank_transfer.role_id = role_group.id','left');
      $this->db->join('status','bank_transfer.status = status.id','left');
      $this->db->where('bank_transfer.deleted', 0);
      $this->db->where_in('bank_transfer.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','bank_transfer',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(bank_transfer.id like "%'.$t_attr['search'].'%" OR bank_transfer.from_bank like "%'.$t_attr['search'].'%" OR bank_transfer.from_acc_no like "%'.$t_attr['search'].'%" OR bank_transfer.to_name like "%'.$t_attr['search'].'%" OR bank_transfer.to_bank like "%'.$t_attr['search'].'%" OR bank_transfer.to_acc_no like "%'.$t_attr['search'].'%" OR bank_transfer.amount like "%'.$t_attr['search'].'%" OR bank_transfer.currency like "%'.$t_attr['search'].'%" OR bank_transfer.reason like "%'.$t_attr['search'].'%" OR bank_transfer.remarks like "%'.$t_attr['search'].'%" OR bank_transfer.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("bank_transfer");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("bank_transfer")->result_array();
      }
    }

    public function get_all_bank_transfers($uhotels){
      $this->db->select('bank_transfer.*');
      $this->db->where_in('bank_transfer.hid',$uhotels);
      $this->db->where('bank_transfer.deleted', 0);
      return $this->db->count_all_results("bank_transfer");
    }   

    public function add_bank_transfer($data){
      $this->db->insert('bank_transfer', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_bank_transfer($bank_transfer_id) {
      $this->db->select('bank_transfer.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, meta_data.type_name, status.status_name');
      $this->db->join('users','bank_transfer.uid = users.id','left');
      $this->db->join('hotels','bank_transfer.hid = hotels.id','left');
      $this->db->join('meta_data','bank_transfer.type_id = meta_data.id','left');
      $this->db->join('status','bank_transfer.status = status.id','left');
      $this->db->where('bank_transfer.id', $bank_transfer_id);
      $this->db->where('bank_transfer.deleted', 0);   
      $query = $this->db->get('bank_transfer');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_bank_transfer($data, $bank_transfer_id) {
      $this->db->where('bank_transfer.id', $bank_transfer_id);   
      $this->db->update('bank_transfer', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>