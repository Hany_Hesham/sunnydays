<?php

  class Payment_voucher_model extends CI_Model{

    public function get_payment_vouchers($uhotels, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('payment_voucher.*, users.fullname, hotels.hotel_name, role_group.name AS role_name, status.status_name, status.status_color, meta_data.type_name');
      $this->db->join('users','payment_voucher.uid = users.id','left');
      $this->db->join('hotels','payment_voucher.hid = hotels.id','left');
      $this->db->join('user_groups AS role_group','payment_voucher.role_id = role_group.id','left');
      $this->db->join('status','payment_voucher.status = status.id','left');
      $this->db->join('meta_data','payment_voucher.type_id = meta_data.id','left');
      $this->db->where('payment_voucher.deleted', 0);
      $this->db->where_in('payment_voucher.hid',$uhotels);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','payment_voucher',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(payment_voucher.id like "%'.$t_attr['search'].'%" OR payment_voucher.explanation like "%'.$t_attr['search'].'%" OR payment_voucher.remarks like "%'.$t_attr['search'].'%" OR payment_voucher.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%" OR meta_data.type_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("payment_voucher");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("payment_voucher")->result_array();
      }
    }

    public function get_all_payment_vouchers($uhotels){
      $this->db->select('payment_voucher.*');
      $this->db->where_in('payment_voucher.hid',$uhotels);
      $this->db->where('payment_voucher.deleted', 0);
      return $this->db->count_all_results("payment_voucher");
    }   

    public function add_payment_voucher($data){
      $this->db->insert('payment_voucher', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_payment_voucher_item($data){
      $this->db->insert('payment_voucher_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_payment_voucher($payment_voucher_id) {
      $this->db->select('payment_voucher.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, status.status_name, meta_data.type_name');
      $this->db->join('users','payment_voucher.uid = users.id','left');
      $this->db->join('hotels','payment_voucher.hid = hotels.id','left');
      $this->db->join('status','payment_voucher.status = status.id','left');
      $this->db->join('meta_data','payment_voucher.type_id = meta_data.id','left');
      $this->db->where('payment_voucher.id', $payment_voucher_id);
      $this->db->where('payment_voucher.deleted', 0);   
      $query = $this->db->get('payment_voucher');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_payment_voucher_items($payment_voucher_id) {
      $this->db->select('payment_voucher_item.*');
      $this->db->where(array('payment_voucher_item.deleted'=>0,'payment_voucher_item.payment_voucher_id'=>$payment_voucher_id));
      return $this->db->get('payment_voucher_item')->result_array();
    }

    function edit_payment_voucher($data, $payment_voucher_id) {
      $this->db->where('payment_voucher.id', $payment_voucher_id);   
      $this->db->update('payment_voucher', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_payment_voucher_item($item_id) {
      $this->db->select('payment_voucher_item.*');
      $this->db->where(array('payment_voucher_item.deleted'=>0,'payment_voucher_item.id'=>$item_id));
      return $this->db->get('payment_voucher_item')->row_array();
    }

    function edit_payment_voucher_item($data, $item_id) {
      $this->db->where('payment_voucher_item.id', $item_id);   
      $this->db->update('payment_voucher_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>