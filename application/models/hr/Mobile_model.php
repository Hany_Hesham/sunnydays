<?php

  class Mobile_model extends CI_Model{

    public function get_mobiles($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('mobile.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','mobile.uid = users.id','left');
      $this->db->join('hotels','mobile.hid = hotels.id','left');
      $this->db->join('departments','mobile.dep_code = departments.id','left');
      $this->db->join('api_departments','mobile.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','mobile.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups','mobile.position = user_groups.id','left');
      $this->db->join('user_groups AS role_group','mobile.role_id = role_group.id','left');
      $this->db->join('status','mobile.status = status.id','left');
      $this->db->where('mobile.deleted', 0);
      $this->db->where_in('mobile.hid',$uhotels);
      $this->db->where_in('mobile.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','mobile',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(mobile.id like "%'.$t_attr['search'].'%" OR mobile.code like "%'.$t_attr['search'].'%" OR mobile.name like "%'.$t_attr['search'].'%" OR mobile.hire_date like "%'.$t_attr['search'].'%" OR mobile.effective_date like "%'.$t_attr['search'].'%" OR mobile.remarks like "%'.$t_attr['search'].'%" OR mobile.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("mobile");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("mobile")->result_array();
      }
    }

    public function get_all_mobiles($uhotels, $udepartments){
      $this->db->select('mobile.*');
      $this->db->where_in('mobile.hid',$uhotels);
      $this->db->where_in('mobile.dep_code',$udepartments);
      $this->db->where('mobile.deleted', 0);
      return $this->db->count_all_results("mobile");
    }   

    public function add_mobile($data){
      $this->db->insert('mobile', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_mobile($mobile_id) {
      $this->db->select('mobile.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, status.status_name');
      $this->db->join('users','mobile.uid = users.id','left');
      $this->db->join('hotels','mobile.hid = hotels.id','left');
      $this->db->join('departments','mobile.dep_code = departments.id','left');
      $this->db->join('api_departments','mobile.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','mobile.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups','mobile.position = user_groups.id','left');
      $this->db->join('status','mobile.status = status.id','left');
      $this->db->where('mobile.id', $mobile_id);
      $this->db->where('mobile.deleted', 0);   
      $query = $this->db->get('mobile');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_mobile($data, $mobile_id) {
      $this->db->where('mobile.id', $mobile_id);   
      $this->db->update('mobile', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>