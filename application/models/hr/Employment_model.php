<?php

  class Employment_model extends CI_Model{

    public function get_employments($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('employment.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','employment.uid = users.id','left');
      $this->db->join('hotels','employment.hid = hotels.id','left');
      $this->db->join('departments','employment.dep_code = departments.id','left');
      $this->db->join('user_groups','employment.position = user_groups.id','left');
      $this->db->join('user_groups AS role_group','employment.role_id = role_group.id','left');
      $this->db->join('status','employment.status = status.id','left');
      $this->db->where('employment.deleted', 0);
      $this->db->where_in('employment.hid',$uhotels);
      $this->db->where_in('employment.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','employment',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(employment.id like "%'.$t_attr['search'].'%" OR employment.clock_no like "%'.$t_attr['search'].'%" OR employment.name like "%'.$t_attr['search'].'%" OR employment.nationality like "%'.$t_attr['search'].'%" OR employment.birth_date like "%'.$t_attr['search'].'%" OR employment.religion like "%'.$t_attr['search'].'%" OR employment.card_id like "%'.$t_attr['search'].'%" OR employment.issue_date like "%'.$t_attr['search'].'%" OR employment.address like "%'.$t_attr['search'].'%" OR employment.phone like "%'.$t_attr['search'].'%" OR employment.remarks like "%'.$t_attr['search'].'%" OR employment.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("employment");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("employment")->result_array();
      }
    }

    public function get_all_employments($uhotels, $udepartments){
      $this->db->select('employment.*');
      $this->db->where_in('employment.hid',$uhotels);
      $this->db->where_in('employment.dep_code',$udepartments);
      $this->db->where('employment.deleted', 0);
      return $this->db->count_all_results("employment");
    }   

    public function add_employment($data){
      $this->db->insert('employment', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_employment($employment_id) {
      $this->db->select('employment.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, status.status_name');
      $this->db->join('users','employment.uid = users.id','left');
      $this->db->join('hotels','employment.hid = hotels.id','left');
      $this->db->join('departments','employment.dep_code = departments.id','left');
      $this->db->join('user_groups','employment.position = user_groups.id','left');
      $this->db->join('status','employment.status = status.id','left');
      $this->db->where('employment.id', $employment_id);
      $this->db->where('employment.deleted', 0);   
      $query = $this->db->get('employment');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_employment($data, $employment_id) {
      $this->db->where('employment.id', $employment_id);   
      $this->db->update('employment', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>