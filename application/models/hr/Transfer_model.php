<?php

  class Transfer_model extends CI_Model{

    public function get_transfers($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('transfer.*, users.fullname, from_hotels.hotel_name AS from_hotel_name, to_hotels.hotel_name AS to_hotel_name, from_departments.dep_name AS from_dep_name, to_departments.dep_name AS to_dep_name, from_user_groups.name AS from_position_nam, to_user_groups.name AS to_position_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','transfer.uid = users.id','left');
      $this->db->join('hotels AS from_hotels','transfer.from_hid = from_hotels.id','left');
      $this->db->join('departments AS from_departments','transfer.from_dep_code = from_departments.id','left');
      $this->db->join('user_groups AS from_user_groups','transfer.from_position = from_user_groups.id','left');
      $this->db->join('hotels AS to_hotels','transfer.to_hid = to_hotels.id','left');
      $this->db->join('departments AS to_departments','transfer.to_dep_code = to_departments.id','left');
      $this->db->join('user_groups AS to_user_groups','transfer.to_position = to_user_groups.id','left');
      $this->db->join('user_groups AS role_group','transfer.role_id = role_group.id','left');
      $this->db->join('status','transfer.status = status.id','left');
      $this->db->where('transfer.deleted', 0);
      $this->db->where('((transfer.from_hid IN('.implode(',',$uhotels).') AND transfer.from_dep_code IN('.implode(',',$udepartments).')) OR (transfer.to_hid IN('.implode(',',$uhotels).') AND transfer.to_dep_code IN('.implode(',',$udepartments).')))');
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','transfer',$t_attr['module_id'],1);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(transfer.id like "%'.$t_attr['search'].'%" OR transfer.name like "%'.$t_attr['search'].'%" OR transfer.nationality like "%'.$t_attr['search'].'%" OR transfer.hiring_date like "%'.$t_attr['search'].'%" OR transfer.transfer_date like "%'.$t_attr['search'].'%" OR transfer.actual_transfer_date like "%'.$t_attr['search'].'%" OR transfer.remarks like "%'.$t_attr['search'].'%" OR transfer.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR from_hotels.hotel_name like "%'.$t_attr['search'].'%"OR to_hotels.hotel_name like "%'.$t_attr['search'].'%" OR from_departments.dep_name like "%'.$t_attr['search'].'%" OR to_departments.dep_name like "%'.$t_attr['search'].'%" OR from_user_groups.name like "%'.$t_attr['search'].'%" OR to_user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("transfer");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("transfer")->result_array();
      }
    }

    public function get_all_transfers($uhotels, $udepartments){
      $this->db->select('transfer.*');
      $this->db->where('((transfer.from_hid IN('.implode(',',$uhotels).') AND transfer.from_dep_code IN('.implode(',',$udepartments).')) OR (transfer.to_hid IN('.implode(',',$uhotels).') AND transfer.to_dep_code IN('.implode(',',$udepartments).')))');
      $this->db->where('transfer.deleted', 0);
      return $this->db->count_all_results("transfer");
    }   

    public function add_transfer($data){
      $this->db->insert('transfer', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_transfer($transfer_id) {
      $this->db->select('transfer.*, users.fullname, from_hotels.hotel_name AS from_hotel_name, from_hotels.logo as from_h_logo, to_hotels.hotel_name AS to_hotel_name, to_hotels.logo as to_h_logo, from_departments.dep_name AS from_dep_name, to_departments.dep_name AS to_dep_name, from_user_groups.name AS from_position_nam, to_user_groups.name AS to_position_name, role_group.name AS role_name, status.status_name');
      $this->db->join('users','transfer.uid = users.id','left');
      $this->db->join('hotels AS from_hotels','transfer.from_hid = from_hotels.id','left');
      $this->db->join('departments AS from_departments','transfer.from_dep_code = from_departments.id','left');
      $this->db->join('user_groups AS from_user_groups','transfer.from_position = from_user_groups.id','left');
      $this->db->join('hotels AS to_hotels','transfer.to_hid = to_hotels.id','left');
      $this->db->join('departments AS to_departments','transfer.to_dep_code = to_departments.id','left');
      $this->db->join('user_groups AS to_user_groups','transfer.to_position = to_user_groups.id','left');
      $this->db->join('user_groups AS role_group','transfer.role_id = role_group.id','left');
      $this->db->join('status','transfer.status = status.id','left');
      $this->db->where('transfer.id', $transfer_id);
      $this->db->where('transfer.deleted', 0);   
      $query = $this->db->get('transfer');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_transfer($data, $transfer_id) {
      $this->db->where('transfer.id', $transfer_id);   
      $this->db->update('transfer', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>