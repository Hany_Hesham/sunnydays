<?php

  class Settlement_model extends CI_Model{

    public function get_settlements($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('settlement.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','settlement.uid = users.id','left');
      $this->db->join('hotels','settlement.hid = hotels.id','left');
      $this->db->join('departments','settlement.dep_code = departments.id','left');
      $this->db->join('user_groups','settlement.position = user_groups.id','left');
      $this->db->join('user_groups AS role_group','settlement.role_id = role_group.id','left');
      $this->db->join('status','settlement.status = status.id','left');
      $this->db->where('settlement.deleted', 0);
      $this->db->where_in('settlement.hid',$uhotels);
      $this->db->where_in('settlement.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','settlement',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(settlement.id like "%'.$t_attr['search'].'%" OR settlement.clock_no like "%'.$t_attr['search'].'%" OR settlement.name like "%'.$t_attr['search'].'%" OR settlement.hiring_date like "%'.$t_attr['search'].'%" OR settlement.last_date like "%'.$t_attr['search'].'%" OR settlement.remarks like "%'.$t_attr['search'].'%" OR settlement.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("settlement");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("settlement")->result_array();
      }
    }

    public function get_all_settlements($uhotels, $udepartments){
      $this->db->select('settlement.*');
      $this->db->where_in('settlement.hid',$uhotels);
      $this->db->where_in('settlement.dep_code',$udepartments);
      $this->db->where('settlement.deleted', 0);
      return $this->db->count_all_results("settlement");
    }   

    public function add_settlement($data){
      $this->db->insert('settlement', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_settlement($settlement_id) {
      $this->db->select('settlement.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, status.status_name');
      $this->db->join('users','settlement.uid = users.id','left');
      $this->db->join('hotels','settlement.hid = hotels.id','left');
      $this->db->join('departments','settlement.dep_code = departments.id','left');
      $this->db->join('user_groups','settlement.position = user_groups.id','left');
      $this->db->join('status','settlement.status = status.id','left');
      $this->db->where('settlement.id', $settlement_id);
      $this->db->where('settlement.deleted', 0);   
      $query = $this->db->get('settlement');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_settlement($data, $settlement_id) {
      $this->db->where('settlement.id', $settlement_id);   
      $this->db->update('settlement', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>