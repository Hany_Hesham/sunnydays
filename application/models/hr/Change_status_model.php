<?php

  class Change_status_model extends CI_Model{

    public function get_change_statuss($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('change_status.*, users.fullname, hotels.hotel_name, departments.dep_name, new_departments.dep_name AS new_dep_name, api_departments.dep_name AS depart_name, new_api_departments.dep_name AS new_depart_name, user_groups.name AS position_name, new_user_groups.name AS new_position_name, api_user_groups.name AS pos_name, new_api_user_groups.name AS new_pos_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','change_status.uid = users.id','left');
      $this->db->join('hotels','change_status.hid = hotels.id','left');
      $this->db->join('departments','change_status.dep_code = departments.id','left');
      $this->db->join('departments AS new_departments','change_status.new_dep = new_departments.id','left');
      $this->db->join('user_groups','change_status.position = user_groups.id','left');
      $this->db->join('user_groups AS new_user_groups','change_status.new_position = new_user_groups.id','left');
      $this->db->join('api_departments','change_status.dep_id = api_departments.id','left');
      $this->db->join('api_departments AS new_api_departments','change_status.new_deps = new_api_departments.id','left');
      $this->db->join('api_user_groups','change_status.pos_id = api_user_groups.id','left');
      $this->db->join('api_user_groups AS new_api_user_groups','change_status.new_positions = new_api_user_groups.id','left');
      $this->db->join('user_groups AS role_group','change_status.role_id = role_group.id','left');
      $this->db->join('status','change_status.status = status.id','left');
      $this->db->where('change_status.deleted', 0);
      $this->db->where_in('change_status.hid',$uhotels);
      $this->db->where_in('change_status.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','change_status',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(change_status.id like "%'.$t_attr['search'].'%" OR change_status.name like "%'.$t_attr['search'].'%" OR change_status.clock_no like "%'.$t_attr['search'].'%" OR change_status.new_clock like "%'.$t_attr['search'].'%" OR change_status.hire_date like "%'.$t_attr['search'].'%" OR change_status.last_increase like "%'.$t_attr['search'].'%" OR change_status.effective_date like "%'.$t_attr['search'].'%" OR change_status.remarks like "%'.$t_attr['search'].'%" OR change_status.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR new_departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR new_user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("change_status");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("change_status")->result_array();
      }
    }

    public function get_all_change_statuss($uhotels, $udepartments){
      $this->db->select('change_status.*');
      $this->db->where_in('change_status.hid',$uhotels);
      $this->db->where_in('change_status.dep_code',$udepartments);
      $this->db->where('change_status.deleted', 0);
      return $this->db->count_all_results("change_status");
    }   

    public function add_change_status($data){
      $this->db->insert('change_status', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_change_status($change_status_id) {
      $this->db->select('change_status.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, new_departments.dep_name AS new_dep_name, api_departments.dep_name AS depart_name, new_api_departments.dep_name AS new_depart_name, user_groups.name AS position_name, new_user_groups.name AS new_position_name, api_user_groups.name AS pos_name, new_api_user_groups.name AS new_pos_name, status.status_name');
      $this->db->join('users','change_status.uid = users.id','left');
      $this->db->join('hotels','change_status.hid = hotels.id','left');
      $this->db->join('departments','change_status.dep_code = departments.id','left');
      $this->db->join('departments AS new_departments','change_status.new_dep = new_departments.id','left');
      $this->db->join('user_groups','change_status.position = user_groups.id','left');
      $this->db->join('api_departments','change_status.dep_id = api_departments.id','left');
      $this->db->join('api_departments AS new_api_departments','change_status.new_deps = new_api_departments.id','left');
      $this->db->join('api_user_groups','change_status.pos_id = api_user_groups.id','left');
      $this->db->join('api_user_groups AS new_api_user_groups','change_status.new_positions = new_api_user_groups.id','left');
      $this->db->join('user_groups AS new_user_groups','change_status.new_position = new_user_groups.id','left');
      $this->db->join('status','change_status.status = status.id','left');
      $this->db->where('change_status.id', $change_status_id);
      $this->db->where('change_status.deleted', 0);   
      $query = $this->db->get('change_status');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_change_status($data, $change_status_id) {
      $this->db->where('change_status.id', $change_status_id);   
      $this->db->update('change_status', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>