<?php

  class Housing_model extends CI_Model{

    public function get_housings($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('housing.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','housing.uid = users.id','left');
      $this->db->join('hotels','housing.hid = hotels.id','left');
      $this->db->join('departments','housing.dep_code = departments.id','left');
      $this->db->join('user_groups','housing.position = user_groups.id','left');
      $this->db->join('api_departments','housing.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','housing.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups AS role_group','housing.role_id = role_group.id','left');
      $this->db->join('status','housing.status = status.id','left');
      $this->db->where('housing.deleted', 0);
      $this->db->where_in('housing.hid',$uhotels);
      $this->db->where_in('housing.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','housing',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(housing.id like "%'.$t_attr['search'].'%" OR housing.code like "%'.$t_attr['search'].'%" OR housing.name like "%'.$t_attr['search'].'%" OR housing.hire_date like "%'.$t_attr['search'].'%" OR housing.effective_date like "%'.$t_attr['search'].'%" OR housing.remarks like "%'.$t_attr['search'].'%" OR housing.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("housing");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("housing")->result_array();
      }
    }

    public function get_all_housings($uhotels, $udepartments){
      $this->db->select('housing.*');
      $this->db->where_in('housing.hid',$uhotels);
      $this->db->where_in('housing.dep_code',$udepartments);
      $this->db->where('housing.deleted', 0);
      return $this->db->count_all_results("housing");
    }   

    public function add_housing($data){
      $this->db->insert('housing', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_housing($housing_id) {
      $this->db->select('housing.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, status.status_name');
      $this->db->join('users','housing.uid = users.id','left');
      $this->db->join('hotels','housing.hid = hotels.id','left');
      $this->db->join('departments','housing.dep_code = departments.id','left');
      $this->db->join('api_departments','housing.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','housing.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups','housing.position = user_groups.id','left');
      $this->db->join('status','housing.status = status.id','left');
      $this->db->where('housing.id', $housing_id);
      $this->db->where('housing.deleted', 0);   
      $query = $this->db->get('housing');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_housing($data, $housing_id) {
      $this->db->where('housing.id', $housing_id);   
      $this->db->update('housing', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>