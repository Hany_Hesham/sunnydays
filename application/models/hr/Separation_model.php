<?php

  class Separation_model extends CI_Model{

    public function get_separations($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('separation.*, users.fullname, hotels.hotel_name, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','separation.uid = users.id','left');
      $this->db->join('hotels','separation.hid = hotels.id','left');
      $this->db->join('departments','separation.dep_code = departments.id','left');
      $this->db->join('user_groups','separation.position = user_groups.id','left');
      $this->db->join('api_departments','separation.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','separation.pos_id = api_user_groups.id','left');
      $this->db->join('user_groups AS role_group','separation.role_id = role_group.id','left');
      $this->db->join('status','separation.status = status.id','left');
      $this->db->where('separation.deleted', 0);
      $this->db->where_in('separation.hid',$uhotels);
      $this->db->where_in('separation.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','separation',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(separation.id like "%'.$t_attr['search'].'%" OR separation.clock_no like "%'.$t_attr['search'].'%" OR separation.name like "%'.$t_attr['search'].'%" OR separation.hiring_date like "%'.$t_attr['search'].'%" OR separation.last_date like "%'.$t_attr['search'].'%" OR separation.card_id like "%'.$t_attr['search'].'%" OR separation.registration like "%'.$t_attr['search'].'%" OR separation.remarks like "%'.$t_attr['search'].'%" OR separation.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR user_groups.name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("separation");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("separation")->result_array();
      }
    }

    public function get_all_separations($uhotels, $udepartments){
      $this->db->select('separation.*');
      $this->db->where_in('separation.hid',$uhotels);
      $this->db->where_in('separation.dep_code',$udepartments);
      $this->db->where('separation.deleted', 0);
      return $this->db->count_all_results("separation");
    }   

    public function add_separation($data){
      $this->db->insert('separation', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_separation($separation_id) {
      $this->db->select('separation.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, user_groups.name AS position_name, api_departments.dep_name AS depart_name, api_user_groups.name AS pos_name, status.status_name');
      $this->db->join('users','separation.uid = users.id','left');
      $this->db->join('hotels','separation.hid = hotels.id','left');
      $this->db->join('api_departments','separation.dep_id = api_departments.id','left');
      $this->db->join('api_user_groups','separation.pos_id = api_user_groups.id','left');
      $this->db->join('departments','separation.dep_code = departments.id','left');
      $this->db->join('user_groups','separation.position = user_groups.id','left');
      $this->db->join('status','separation.status = status.id','left');
      $this->db->where('separation.id', $separation_id);
      $this->db->where('separation.deleted', 0);   
      $query = $this->db->get('separation');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function edit_separation($data, $separation_id) {
      $this->db->where('separation.id', $separation_id);   
      $this->db->update('separation', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }
    
  }

?>