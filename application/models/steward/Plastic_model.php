<?php

  class Plastic_model extends CI_Model{

    public function get_plastics($uhotels, $udepartments, $t_attr, $custom_search='', $filter_count=''){
      $this->db->select('plastic.*, users.fullname, hotels.hotel_name, departments.dep_name, role_group.name AS role_name, status.status_name, status.status_color');
      $this->db->join('users','plastic.uid = users.id','left');
      $this->db->join('hotels','plastic.hid = hotels.id','left');
      $this->db->join('departments','plastic.dep_code = departments.id','left');
      $this->db->join('user_groups AS role_group','plastic.role_id = role_group.id','left');
      $this->db->join('status','plastic.status = status.id','left');
      $this->db->where('plastic.deleted', 0);
      $this->db->where_in('plastic.hid',$uhotels);
      $this->db->where_in('plastic.dep_code',$udepartments);
      if ($custom_search) {
        custom_search_query($custom_search,'extra_filters','plastic',$t_attr['module_id']);  
      }  
      if ($t_attr['search']) {
        $this->db->where('(plastic.id like "%'.$t_attr['search'].'%" OR plastic.remarks like "%'.$t_attr['search'].'%" OR plastic.timestamp like "%'.$t_attr['search'].'%" OR users.fullname like "%'.$t_attr['search'].'%" OR hotels.hotel_name like "%'.$t_attr['search'].'%" OR departments.dep_name like "%'.$t_attr['search'].'%" OR role_group.name like "%'.$t_attr['search'].'%" OR status.status_name like "%'.$t_attr['search'].'%")');
      }
      if ($filter_count == 'count') {
        return $this->db->count_all_results("plastic");
      }else{
        $this->db->order_by($t_attr['col_name'],$t_attr['order']);
        return $this->db->limit($t_attr['length'],$t_attr['start'])->get("plastic")->result_array();
      }
    }

    public function get_all_plastics($uhotels, $udepartments){
      $this->db->select('plastic.*');
      $this->db->where_in('plastic.hid',$uhotels);
      $this->db->where_in('plastic.dep_code',$udepartments);
      $this->db->where('plastic.deleted', 0);
      return $this->db->count_all_results("plastic");
    }   

    public function add_plastic($data){
      $this->db->insert('plastic', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }

    public function add_plastic_item($data){
      $this->db->insert('plastic_item', $data);
      return($this->db->affected_rows()==1) ? $this->db->insert_id() : FALSE;
    }     

    function get_plastic($plastic_id) {
      $this->db->select('plastic.*, users.fullname, hotels.hotel_name, hotels.logo as h_logo, departments.dep_name, status.status_name');
      $this->db->join('users','plastic.uid = users.id','left');
      $this->db->join('hotels','plastic.hid = hotels.id','left');
      $this->db->join('departments','plastic.dep_code = departments.id','left');
      $this->db->join('status','plastic.status = status.id','left');
      $this->db->where('plastic.id', $plastic_id);
      $this->db->where('plastic.deleted', 0);   
      $query = $this->db->get('plastic');
      return ($query->num_rows() > 0 )? $query->row_array() : FALSE;
    }

    function get_plastic_items($plastic_id) {
      $this->db->select('plastic_item.*');
      $this->db->where(array('plastic_item.deleted'=>0,'plastic_item.plastic_id'=>$plastic_id));
      return $this->db->get('plastic_item')->result_array();
    }

    function edit_plastic($data, $plastic_id) {
      $this->db->where('plastic.id', $plastic_id);   
      $this->db->update('plastic', $data);
      return ($this->db->affected_rows() == 1)? $this->db->insert_id() : FALSE;
    }

    function get_plastic_item($item_id) {
      $this->db->select('plastic_item.*');
      $this->db->where(array('plastic_item.deleted'=>0,'plastic_item.id'=>$item_id));
      return $this->db->get('plastic_item')->row_array();
    }

    function edit_plastic_item($data, $item_id) {
      $this->db->where('plastic_item.id', $item_id);   
      $this->db->update('plastic_item', $data);
      if ($this->db->affected_rows() >= 0) {
        return $this->db->affected_rows();
      } 
    }
    
  }

?>