<?php defined('BASEPATH') or exit('No direct script access allowed');

class App
{    /**
     * CI Instance
     * @deprecated 1.9.8 Use $this->ci instead
     * @var object
     */
    private $_instance;
    /**
     * CI Instance
     * @var object
     */
    private $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
        // @deprecated
        $this->_instance = $this->ci;
    }

    /**
     * Function that will parse table data from the tables folder for amin area
     * @param  string $table  table filename
     * @param  array  $params additional params
     * @return void
     */
    public function get_table_data($table, $params = array())
    {
        $hook_data = array(
            'table' => $table,
            'params' => $params,
        );

        foreach ($hook_data['params'] as $key => $val) {
            $$key = $val;
        }

        $table = $hook_data['table'];

        include_once(VIEWPATH . 'tables/' . $table . '.php');
        die(print_r("IAM HERE"));

        if (file_exists(VIEWPATH . 'admin/tables/my_' . $table . '.php')) {
            include_once(VIEWPATH . 'admin/tables/my_' . $table . '.php');
        } else {
            include_once(VIEWPATH . 'tables/' . $table . '.php');
        }

        die(print_r("IAM HERE"));
        echo json_encode($output);
        die;
    }

}
