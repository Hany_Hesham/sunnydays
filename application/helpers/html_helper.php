<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('initiate_modal')){
   
   function initiate_modal($size='',$head=false,$form_link=false,$body=false,$id=false){

    $ci=get_instance();
    if ($id) {
        $data['id'] = $id;
      }else{
        $data['id'] ='smallmodal';
      }
      if ($size) {
        $data['size'] = $size;
      }else{
        $data['size'] ='';
      }
    $data['head'] = $head;
    $data['form_link'] = $form_link;
    $data['body'] = $body;
    $ci->load->view('admin/html_parts/modal_body',$data);
  }

}

if (!function_exists('initiate_modalmessage')){
  function initiate_modalmessage($private = FALSE, $formLink = FALSE, $formID = FALSE, $moduleID = FALSE){
    $ci=get_instance();
    if ($formID) {
      $data['formID'] = $formID;
    }else{
      $data['formID'] = '';
    }
    if ($moduleID) {
      $data['moduleID'] = $moduleID;
    }else{
      $data['moduleID'] = '';
    }
    if ($moduleID) {
      $data['private'] = 0;
      $data['head'] = 'Comments';
    }else{
      $data['private'] = 1;
      $data['head'] = 'Private Message';
    }
    $data['formLink'] = $formLink;
    $ci->load->view('admin/html_parts/messenger',$data);
  }
}

 if (!function_exists('initiate_alert')){
   
   function initiate_alert(){
   
    $ci=get_instance();

    $ci->load->view('admin/html_parts/alert_body');
  }

}

if (!function_exists('initiate_tour_alert')){
  function initiate_tour_alert(){
    $ci=get_instance();
    $ci->load->view('admin/html_parts/tour_alert_body');
  }
}

if (!function_exists('upfiles_js')){
   
   function upfiles_js($cr_path,$uploads,$module_id,$temp_id,$folder,$table='',$type=''){
   
    $ci=get_instance();
    
    $data['cr_path']   = $cr_path;
    $data['uploads']   = $uploads;
    $data['module_id'] = $module_id;
    $data['temp_id']   = $temp_id;
    $data['folder']    = $folder;
    $data['table']     = $table;
    $data['type']      = $type;
    $ci->load->view('admin/html_parts/upfiles.js.php',$data);
  }

}


if (!function_exists('initiate_filters')){
  function initiate_filters($module_id, $module_name, $uhotels = FALSE, $dep_ids = FALSE, $sign = FALSE, $date = FALSE,$extra_filters=array()){
    $ci=get_instance();
    $ci->load->helper('date_helper');
    $data=array();
    //=========== global filters for all forms indexs
    if ($uhotels) {
        $data['hotels']          = $ci->Hotels_model->get_hotels($uhotels);
    }
    if (!$sign) {
        $data['uroles']          = getUserRoles();
        $data['roles']           = getIfSigner($module_id);
        $data['statuss']         = $ci->General_model->get_module_status($module_id);
    }
    if ($dep_ids) {
        $data['departments']     = get_departments($dep_ids); 
    }
    if (!$date) {
        $datestring              = '%Y-%m-%d';
        $data['from_date']       = date("Y-m-d", strtotime("-1 month"));
        $data['to_date']         = mdate($datestring, time());
    }
    //=========== customized filters
    if (isset($extra_filters['arrival_departure']) && $extra_filters['arrival_departure'] !='') {
        $datestring                = '%Y-%m-%d';
        $data['arrival_date']      = date("Y-m-d", strtotime("-1 month"));
        $data['departure_date']    = mdate($datestring, time());
    }
    if (isset($extra_filters['form_date']) && $extra_filters['form_date'] !='') {
        $datestring                = '%Y-%m-%d';
        $data['from_form_date']    = date("Y-m-d", strtotime("-1 month"));
        $data['to_form_date']      = mdate($datestring, time());
    }
    if (isset($extra_filters['items_search']) && $extra_filters['items_search'] !='') {
        $data['items_search']      = 'create';
    }
    if (isset($extra_filters['shop_upload_types']) && $extra_filters['shop_upload_types'] !='') {
        $data['shop_upload_types']      = 'create';
    }
    if (isset($extra_filters['to_hid']) && $extra_filters['to_hid'] !='') {
        $data['to_hotels']     =  $data['hotels'];
    }
    $ci->load->view('admin/html_parts/index_filters.php',$data);
  }
}


if (!function_exists('initiate_breadcrumb')){
   
  function initiate_breadcrumb($page_name,$form_id='',$module_id='',$log_permission='',$email_permission='',$options='',$locations=''){
      $ci=get_instance();
      $data['page_name']           = $page_name;
      $data['form_id']             = $form_id;
      $data['module_id']           = $module_id;
      $data['log_permission']      = $log_permission; 
      $data['email_permission']    = $email_permission;
      $data['options']             = $options; 
      $data['locations']           = $locations;
      $ci->load->view('admin/html_parts/breadcrumb.php',$data);
   }

}

if (!function_exists('initiate_row_card')) {
    function initiate_row_card($header, $details, $creation, $tools) {
        $main_content='';
        foreach ($details as $detail) {
           $main_content .=  ($detail?'<span class="m-b-15 d-block text-dark">'.$detail.' </span>':'');
          }
        return '<div class="d-flex flex-row comment-row m-t-0 after-hover">
                <div class="comment-text w-100 text-dark">
                  <h6 class="font-medium text-dark">' . $header . '</h6>
                  '.$main_content.'
                    <div class="comment-footer">
                        <h4 class="text-muted font-10">
                        '.(isset($creation['created_by'])?'<i class="text-muted">'.$creation['created_by'].'</i>':'').'
                        '.($creation['timestamp']?'<i class="text-muted float-right">'.$creation['timestamp'].'</i>':'').'
                        </h4>
                    </div>
                    <div class="comment-footer wait-hover">
                      '.(isset($tools['edit_link']) && $tools['edit_link'] !=''?'<a href="'.base_url($tools['edit_link']).'">
                                          <span class="text-dark"><i class="m-r-10 mdi mdi-table-edit font-18"></i></span>
                                          <b class="text-dark">Edit</b>
                                        </a>':'').'
                      '.(isset($tools['delete_link']) && $tools['delete_link'] !=''?'<a  '.$tools['delete_link'].' 
                            href="javascript: void(0);">
                             <span class="text-dark"><i class="m-r-10 mdi mdi-delete-empty font-18 text-danger"></i></span>
                             <b class="text-dark">Delete</b>
                        </a>':'').'
                   </div>
                  </div>
                </div>';
    }
}


?>