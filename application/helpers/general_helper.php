<?php 

if (!function_exists('user_access')){
   
   function user_access($module_id){
     
     $CI = get_instance();
     
     $CI->load->model('admin/User_model');
     
     $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
     
     $user_permissions = $CI->User_model->get_user_access($module_id,$user['id']);
 
     return  $user_permissions;

   }
}


if (!function_exists('access_checker')){
  function access_checker($g_view, $view, $private_view='', $creat='', $edit='', $remove='', $f_change='', $red='', $form='', $hid=''){
    $CI = get_instance();
    if (isset($form) && isset($hid) && isset($form['hid']) && !(in_array($form['hid'], $hid))) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }elseif ( $g_view != 1 && $view != 1 && $private_view != 1 && $creat != 1 && $edit != 1 && $remove != 1 && $f_change != 1) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }
  }
}

if (!function_exists('login_checker')){
  function login_checker(){
    $CI = get_instance();
    if (!$CI->session->has_userdata('is_admin_login')) {
      if (strpos($CI->uri->uri_string(), 'tour/login') === FALSE) {
        $CI->session->set_userdata(array(
          'red_url' => $CI->uri->uri_string()
        ));
      }
      redirect(base_url('tour/login'));
    }
  }
}

if (!function_exists('tour_access_checker')){
  function tour_access_checker(){
    $CI = get_instance();
    $CI->load->model('admin/User_model');
    $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
    if (!is_null($user['travel']) && !$user['is_admin']) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect('tour');
    }
  }
}

if (!function_exists('notour_access_checker')){
  function notour_access_checker(){
    $CI = get_instance();
    $CI->load->model('admin/User_model');
    $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
    if (is_null($user['travel']) && !$user['is_admin']) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission!']);
      redirect('admin/dashboard');
    }else{
      return json_decode($user['travel'], true);
    }
  }
}

if (!function_exists('get_department_bycode')){
    function get_department_bycode($table,$code){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_code_id($table,$code);
      return $req_id['id']; 
    }
  }

if (!function_exists('from_to_access_checker')){
  function from_to_access_checker($g_view, $view, $private_view='', $creat='', $edit='', $remove='', $f_change='', $red='', $form='', $hid=''){
    $CI = get_instance();
    if (isset($form) && isset($hid) && isset($form['from_hid']) && isset($form['to_hid']) && (!(in_array($form['from_hid'], $hid)) && !(in_array($form['to_hid'], $hid)))) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }elseif ( $g_view != 1 && $view != 1 && $private_view != 1 && $creat != 1 && $edit != 1 && $remove != 1 && $f_change != 1) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }
  }
}

if (!function_exists('edit_checker')){ 
  function edit_checker($form,$edit,$red){
    $CI = get_instance();
    if ((!$form) ||($form['status'] == 2 && $_SESSION['is_admin'] != 1) ||(($form['uid'] != $_SESSION['user_id'])&& ($form['status'] != 2 && $edit!=1))) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }
  }
}

if (!function_exists('change_checker')){ 
  function change_checker($form,$edit,$red){
    $CI = get_instance();
    if ((!$form && $_SESSION['is_admin'] != 1) || (($form['uid'] != $_SESSION['user_id']) && $edit!=1)) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }
  }
}

if (!function_exists('editdone_checker')){ 
  function editdone_checker($form,$edit,$red){
    $CI = get_instance();
    if ((!$form) || (($form['uid'] != $_SESSION['user_id']) && ($edit != 1))) {
      $CI->session->set_flashdata(['alert'=>'Warning','msg'=>'Sorry You Dont have a permission !']);
      redirect($red);
    }
  }
}

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('array_assoc_value_exists')){
   
  function array_assoc_value_exists($arr, $index, $search) {
    foreach ($arr as $key => $value) {
      if ($value[$index] == $search) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   if (!function_exists('getMAC')){
       function getMAC(){
        
     $mac = shell_exec("arp -a ".escapeshellarg($_SERVER['REMOTE_ADDR'])." | grep -o -E '(:xdigit:{1,2}:){5}:xdigit:{1,2}'");
     return $mac;
      }
   }

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('loger')){
   
   function loger($action,$module_id,$target,$target_id, $mini_target_id,$data=false,$new_data=false,$items=false,$new_items=false,$comment=false,$print=false){

     $CI = get_instance();
     $CI->load->model('admin/General_model');
      if ($print == 'print') {
        $CI->load->model('admin/User_model');
        $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
      }elseif ($print == 'supplier') {
       $user = array('id' => NULL);
      }else{
        $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);
      }

     $form_data = array(
				         'user_id'          => $user['id'],
				         'action'           => $action,
				         'module_id'        => $module_id,
				         'target'           => $target,
				         'target_id'        => $target_id,
				         'mini_target_id'   => $mini_target_id,
				         'data'             => $data,
				         'new_data'         => $new_data,
				         'items'            => $items,
				         'new_items'        => $new_items,
				         'comments'         => $comment,
				         'ip'               => $CI->input->ip_address().'('.getMAC().')',
                 'log_time'         => date("Y-m-d H:i:s"),
				       );

     $log_id = $CI->General_model->logger($form_data);

   }
}

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 if (!function_exists('exact_data')){
   
   function exact_data($old,$new){ 
     if(is_array($old) && is_array($new)){
   	   $field    = implode(' , ', array_keys( array_diff_assoc(json_decode($old,true), json_decode($new,true))));
     	 return($field);
      }else{
        return($old.' to '.$new) ; 
      }


   }
}

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   if (!function_exists('do_upload')){
     
       function do_upload($field, $folder, $directory='uploads/') {

        $CI = get_instance();
        if ($directory == 1) {
          $directory='';
        }
        $config['upload_path'] = 'assets/'.$directory.''.$folder;

        $config['allowed_types'] = '*';

        $CI->load->library('Upload', $config);

        if ( ! $CI->upload->do_upload($field)) {

              $data['error'] = array('error' => $CI->upload->display_errors());

              return FALSE;

            }else {

              $file = $CI->upload->data();

              return $file['file_name'];

             }
 
         }
     }

  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
         // mailer == 1 just normal notify
         // mailer == 2 just mailer
         // mailer == 3 both notify and mailer public to department
         // mailer == 4 both notify and mailer private to one user
     if (!function_exists('notify')){
       function notify($module_id,$to_uid='',$to_hid='',$to_dep_id='',$head='',$message='',$link='',$form_id='',$type='',$mail_message='',$role_id='',$mailer=''){

         $CI = get_instance();

         $CI->load->model('admin/General_model');

         $CI->load->model('admin/Modules_model');

         $user = $CI->User_model->get_user_by_id($_SESSION['user_id'], TRUE);

         if ($mailer == 1 || $mailer == 3 || $mailer == 4) {
             $form_data = array(
                         'module_id'        => $module_id,
                         'uid'              => $user['id'],
                         'to_uid'           => $to_uid,
                         'to_hid'           => $to_hid,
                         'to_dep_id'        => $to_dep_id,
                         'head'             => $head,
                         'message'          => $message,
                         'link'             => $link,
                         'timestamp'        => date("Y-m-d H:i:s"),
                       );

                 if ($mailer == 4) {
                          unset($form_data['to_dep_id']);
                          //unset($form_data['to_hid']);
                  }

              $notify_id = $CI->General_model->add_notify($form_data);
            }

         if ($mailer == 2 || $mailer == 3 || $mailer == 4) {
          
           //die(print_r($role_id));

              $emails = $CI->General_model->get_emails_queue($role_id,$to_hid,$to_dep_id, $to_uid);
              if ($emails) {
              mailer($user['id'],$module_id,$form_id,$type,$emails,$mail_message);
              //------- get emails and send to mail service 
              $module  = $CI->Modules_model->get_module_by($module_id);
              $url     = base_url($module['view_link'].'/'.$form_id);
              $subject = header_data($module_id,$form_id);
              $body    = $subject.' '.$mail_message.','.'Please use the link below:<br>';

                $body.= '<a href='.$url.' target="_blank">'.$url.'</a>';
              // foreach (explode(',',$emails) as $email) {
              //   //send_to_sendmail_service($email,$subject,$body, $url);
              // }
              }

           }

       }
}

      if (!function_exists('otpNotify')){
        function otpNotify($userId){
          $CI = get_instance();
          $user = $CI->User_model->get_user_by_id($userId, TRUE);
          $OTP   =  $userId;
          $OTP  .=  rand(0,9);
          $OTP  .=  rand(0,9);
          $OTP  .=  rand(0,9);
          $OTP  .=  rand(0,9);
          $CI->db->update('users', array('token' => $OTP), "id = ".$userId);
          $url     = base_url('admin/auth/verify');
          $subject = '"'.$OTP.'" Is The OTP Token';
          $body    = 'Your OTP Is:<br>'.$OTP;
          $body.= '<a href='.$url.' target="_blank">'.$url.'</a>';
          send_to_sendmail_service($user['email'],$subject,$body, $url);
        }
      }

if (!function_exists('header_data')){
  function header_data($module_id,$form_id){ 
    $CI = get_instance();
    $CI->load->model('admin/Modules_model');
    $module   = $CI->Modules_model->get_module_by($module_id);
    $form     = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
    $field    = $module['name'].' NO.# '.$form_id;
    if (isset($form['hid'])) {
      $field    .= ' For '.hotel_name($form['hid']); 
    }
    return($field);
  }
}

if (!function_exists('messaged')){
  function messaged($uid, $message, $private, $to_uid='', $module_id='', $form_id=''){
    $CI = get_instance();
    $CI->load->model('admin/General_model');
    $message_data = array(
      'uid' => $uid,
      'to_uid' => $to_uid,
      'module_id' => $module_id,
      'form_id' => $form_id,
      'message' => $message,
      'private' => $private,
      'timestamp' => date("Y-m-d H:i:s"),
    );
    $message_id = $CI->General_model->add_message($message_data);
  }
}


 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('mailer')){
       
       function mailer($uid='',$module_id='',$form_id='',$type='',$emails='',$message=''){

         $CI = get_instance();

         $CI->load->model('admin/General_model');
foreach ($emails as $email) {

         $form_data = array(
                     'uid'              => $uid,
                     'module_id'        => $module_id,
                     'form_id'          => $form_id,
                     'type'             => $type,
                     'emails'           => $email,
                     'message'          => $message,
                     'timestamp'        => date("Y-m-d H:i:s"),
                   );

         $notify_id = $CI->General_model->add_mail($form_data);
}
       }
  }



  if (!function_exists('get_id_bycode')){
    function get_id_bycode($table,$code='',$id=''){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      if ($code) {
        $req_id = $CI->General_model->get_code_id($table,$code,'');
        return $req_id['id']; 
      }elseif($id){
        $req_id = $CI->General_model->get_code_id($table,'',$id);
        return $req_id['code'];
      }
    }
  }

  if (!function_exists('projectCode')){
    function projectCode($id=''){
      $CI = get_instance();
      $CI->load->model('projects_plans/projects/Projects_model');
      $req_id = $CI->Projects_model->get_project($id);
      return $req_id['code'];
    }
  }

  if (!function_exists('projectHotel')){
    function projectHotel($id=''){
      $CI = get_instance();
      $CI->load->model('projects_plans/projects/Projects_model');
      $req_id = $CI->Projects_model->get_project($id);
      return $req_id['hid']; 
    }
  }

  if (!function_exists('getTypeData')){
    function getTypeData($id=''){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_meta_id($id);
      return $req_id['type_data']; 
    }
  }

  if (!function_exists('get_role_bycode')){
    function get_role_bycode($table,$code){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_code_role($table,$code);
      return $req_id['role_id']; 
    }
  }

  if (!function_exists('get_role_byid')){
    function get_role_byid($table,$id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_id_role($table,$id);
      return $req_id['role_id']; 
    }
  }

  if (!function_exists('get_roleHotel_bycode')){
    function get_roleHotel_bycode($table,$code){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_code_role($table,$code);
      return $req_id['role_hotel']; 
    }
  }

  if (!function_exists('get_roleHotel_byid')){
    function get_roleHotel_byid($table,$id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->get_id_role($table,$id);
      return $req_id['role_hotel']; 
    }
  }

  if (!function_exists('getdepname')){
    function getdepname($code){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->getdepname($code);
      return $req_id['dep_name']; 
    }
  }

  if (!function_exists('userName')){
    function userName($id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->userName($id);
      return $req_id['fullname']; 
    }
  }

  if (!function_exists('hotelName')){
    function hotelName($id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->hotelName($id);
      return $req_id['hotel_name']; 
    }
  }

  if (!function_exists('getHotelName')){
    function getHotelName($name){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->getHotelName($name);
      return $req_id['id']; 
    }
  }

  if (!function_exists('metaName')){
    function metaName($id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->metaName($id);
      return $req_id['type_name']; 
    }
  }

  if (!function_exists('metaID')){
    function metaID($name, $meta_keyword){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->metaID($name, $meta_keyword);
      return $req_id['id']; 
    }
  }

  if (!function_exists('currencyName')){
    function currencyName($id){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $req_id = $CI->General_model->currencyName($id);
      return $req_id['symbol']; 
    }
  }

  if (!function_exists('get_singleLog')){
    function get_singleLog($module, $form, $item = FALSE, $columne = FALSE, $action = FALSE){
      $CI = get_instance();
      $CI->load->model('admin/General_model');
      $logers = $CI->General_model->get_singleLog($module, $form, $item);
      $finalData = array();
      foreach ($logers as $key => $loger) {
        $oldData = json_decode($loger['data']);
        $newData = json_decode($loger['new_data']);
        if (isset($newData->$columne) && isset($oldData->$columne)) {
          if ($newData->$columne != $oldData->$columne) {
            if ($columne == 'hid') {
              $oldHotel = hotelName($oldData->$columne);
              $newHotel = hotelName($newData->$columne);
              $finalData[] = array('oldData' => $oldHotel, 'newData' => $newHotel, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
            }elseif($columne == 'operator_id' || $columne == 'type' || $columne == 'accident' || $columne == 'visited' || $columne == 'cctv' || $columne == 'photographs' || $columne == 'report' || $columne == 'reports' || $columne == 'indemnity' || $columne == 'informed' || $columne == 'added' || $columne == 'accepted' || $columne == 'insurance' || $columne == 'witness1' || $columne == 'paperwork' || $columne == 'cristal' || $columne == 'audits' || $columne == 'logs' || $columne == 'maintenance' || $columne == 'documents' || $columne == 'other' || $columne == 'incident' || $columne == 'complaints' || $columne == 'doctor' || $columne == 'solicitor' || $columne == 'letter' || $columne == 'medical' || $columne == 'Other' || $columne == 'decision' || $columne == 'confirmed' || $columne == 'unconfirmed' || $columne == 'other1' || $columne == 'subject' || $columne == 'symptoms' || $columne == 'ir_type'){
              $oldMeta = metaName($oldData->$columne);
              $newMeta = metaName($newData->$columne);
              if ($oldMeta && $newMeta) {
                $finalData[] = array('oldData' => $oldMeta, 'newData' => $newMeta, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }else{
                $finalData[] = array('oldData' => $oldData->$columne, 'newData' => $newData->$columne, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }
            }elseif($columne == 'curency' || $columne == 'curency1' || $columne == 'curency2'){
              $oldMeta = currencyName($oldData->$columne);
              $newMeta = currencyName($newData->$columne);
              if ($oldMeta && $newMeta) {
                $finalData[] = array('oldData' => $oldMeta, 'newData' => $newMeta, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }else{
                $finalData[] = array('oldData' => $oldData->$columne, 'newData' => $newData->$columne, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }
            }elseif($columne == 'recieved' || $columne == 'avaible' || $columne == 'visit'){
              if ($oldData->$columne == 0) {
                $oldMeta = 'NO';
              }elseif ($oldData->$columne == 1) {
                $oldMeta = 'Yes';
              }
              if ($newData->$columne == 0) {
                $newMeta = 'NO';
              }elseif ($newData->$columne == 1) {
                $newMeta = 'Yes';
              }
              if ($oldMeta && $newMeta) {
                $finalData[] = array('oldData' => $oldMeta, 'newData' => $newMeta, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }else{
                $finalData[] = array('oldData' => $oldData->$columne, 'newData' => $newData->$columne, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
              }
            }else{
              $finalData[] = array('oldData' => $oldData->$columne, 'newData' => $newData->$columne, 'user_name' => $loger['fullname'], 'timestamp' => $loger['log_time']);
            }
          }
        }
      }
      return $finalData; 
    }
  }

   // if ( ! defined('BASEPATH')) exit('No direct script access allowed');

   //   if (!function_exists('money_formater')){
       
   //     function money_formater($money,$currency=''){

   //       $CI = get_instance();

   //       $CI->load->model('admin/General_model');
         
   //       if ($currency) {
   //             $symbol = $CI->General_model->get_currency_symbol($currency);
   //             echo(number_format($money,2,".",",").' '.$symbol);
   //        }else{
   //             echo(number_format($money,2,".",","));
   //        }
         

   //      }
   //  }


    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('money_formater')){
       
       function money_formater($money,$currency=''){

         $CI = get_instance();

         $CI->load->model('admin/General_model');
         
         if ($currency) {
               $symbol = $CI->General_model->get_currency_symbol($currency);
               return(number_format($money,2,".",",").' '.$symbol);
          }else{
               return(number_format($money,2,".",","));
          }
         

        }
    }





  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

     if (!function_exists('getPercentage')){
       
       function getPercentage($value,$total){
        if ($total != 0) {
           return number_format((((float)$value)*100)/(float)$total, 2, '.', '').' %';
         }else{
           return '0.00 %';
         }

       }

    }  

  if (!function_exists('dateTimeFormater')){
    function dateTimeFormater($date) {
      if ($date == '0000-00-00 00:00:00') {
        $dates ='';
      }else{
        $date = explode(" ",$date);
        $date[0] = explode("-",$date[0]);
        $dates = implode("-",$date[0]);
      }
      return $dates;
    }
  }

  if (!function_exists('dateConverter')){
    function dateConverter($date) {
      $dates = '';
      $final_date = '';
      $date1 = explode(" ",$date);
      if (isset($date1[1])) {
        $dates = $date1;
      }else{
        $date2 = explode("/",$date);
        if (isset($date2[1])) {
          $dates = $date2;
        }else{
          $date3 = explode(".",$date);
          if (isset($date3[1])) {
            $dates = $date3;
          }else{
            $date4 = explode("-",$date);
            if (isset($date4[1])) {
              $dates = $date4;
            }
          }
        }
      }
      if (strlen($dates[0]) == 4) {
        $year   = $dates[0];
        $month  = sprintf("%02d", $dates[1]);
        $day    = sprintf("%02d", $dates[2]);
      }else{
        $year = $dates[2];
        $month  = sprintf("%02d", $dates[1]);
        $day    = sprintf("%02d", $dates[0]);
      }
      $final_date = $year.'-'.$month.'-'.$day;
      return $final_date;
    }
  }

  if (!function_exists('getValueArray')){
    function getValueArray($id, $array_name){
      $key = array_search($id, $array_name);
      if (isset($array_name[$key]['name'])) {
        $value = $array_name[$key]['name'];
      }elseif (isset($array_name[$key]['dep_name'])) {
        $value = $array_name[$key]['dep_name'];
      }elseif (isset($array_name[$key]['hotel_name'])) {
        $value = $array_name[$key]['hotel_name'];
      }else{
        $value = $array_name[$key]['type_name'];
      }
      return  $value;
    }
  }

 if (!function_exists('getValueArray_multi')){
     function getValueArray_multi($id, $array_name){
       $key = array_search($id, array_column($array_name, 'id'));
       $value = $array_name[$key]['name'];
       return  $value;
    }
 }

  if (!function_exists('getTaxValue')){
    function getTaxValue($tax, $total){
      $variable = "1.".(float)$tax;
      $value = ((float)$total*(float)$variable);
      return  $value;
    }
  }

  if (!function_exists('getPercentageValue')){
    function getPercentageValue($amount, $total){
      $value = ((float)$amount/(float)$total)*100;
      return  $value;
    }
  }

  if (!function_exists('send_to_sendmail_service')){
    function send_to_sendmail_service($email,$subject,$body, $url) {
      require_once FCPATH. 'vendor/sailing2014/chat-api-sdk-php/vendor/autoload.php';
      $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('instanceId', '83685');
      $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('token', 'oas6j80hai7x1igb');
      $apiInstance = new OpenAPI\Client\Api\Class2MessagesApi(new GuzzleHttp\Client(), $config);
      $mobile = get_userBy_email($email);
      if ($mobile) {
        $send_message_request = [
          'phone' => '2'.$mobile,
          'body' => strip_tags($body),
        ];
        $apiInstance->sendMessage($send_message_request);
        $send_message_request = [
          'phone' => '201068267949',
          'body' => strip_tags($body),
        ];
        $apiInstance->sendMessage($send_message_request);
      }
    }
  }

  function send_Message() {
        require_once FCPATH. 'vendor/sailing2014/chat-api-sdk-php/vendor/autoload.php';
      $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('instanceId', '83685');
      $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('token', 'oas6j80hai7x1igb');
      $apiInstance = new OpenAPI\Client\Api\Class2MessagesApi(new GuzzleHttp\Client(), $config);
      // $mobile = get_userBy_email($email);
      // if ($mobile) {
        // $send_message_request = [
        //   'phone' => '2'.$mobile,
        //   'body' => strip_tags($body),
        // ];
        // $apiInstance->sendMessage($send_message_request);
        $send_message_request = [
          'phone' => '201068267949',
          'body' => strip_tags('Test'),
        ];
        $apiInstance->sendMessage($send_message_request);
      // }
     }
    //     require_once($_SERVER['DOCUMENT_ROOT'].'/application/vendor/mikechip/chatapi/chatapi.class.php');
    //     $api = new ChatApi(
    //     'oas6j80hai7x1igb', // Chat-Api.com token
    //     'https://eu69.chat-api.com/instance83685' // Chat-Api.com API url
    //       );
    //     $mobile = get_userBy_email($email);
    //     if ($mobile) {
    //         //$api->sendPhoneMessage('+2'.$mobile,strip_tags($body));
    //         $previewBase64 = 'data:image/jpeg;base64,/9j/'.base64_encode($url);
    //         $api->sendLink('+2'.$mobile, $url, $previewBase64, $subject);
    //         sleep(30);
    //     }


    // }

    function differenceDates($date, $date1) {
      $date = strtotime($date);
      $date1 = strtotime($date1);
      $dates = $date1 - $date; 
      $result['years'] = floor($dates / (365*24*60*60));
      $result['months'] = floor(($dates - $result['years']*365*24*60*60) / (30*24*60*60));
      $result['days'] = floor(($dates)/ (24*60*60));
      $result['hours'] = floor(($dates - $result['years']*365*24*60*60 - $result['months']*30*24*60*60 - $result['days']*24*60*60)/ (60*60)); 
      $result['minuts']  = floor(($dates - $result['years']*365*24*60*60 - $result['months']*30*24*60*60 - $result['days']*24*60*60 - $result['hours']*60*60)/ 60);
      $result['seconds'] = floor(($dates - $result['years']*365*24*60*60 - $result['months']*30*24*60*60 - $result['days']*24*60*60 - $result['hours']*60*60 - $result['minuts']*60)); 
      return $result;
    }

    if (!function_exists('getYear')){
      function getYear($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("Y");
      }
    }

    if (!function_exists('getMonth')){
      function getMonth($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("m");
      }
    }

    if (!function_exists('getDay')){
      function getDay($pdate) {
        $date = DateTime::createFromFormat("Y-m-d", $pdate);
        return $date->format("d");  
      }
    }


?>