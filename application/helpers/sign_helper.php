<?php

	if (!function_exists('getSigners')){
	    function getSigners($module_id, $form_id, $typed = FALSE){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	      	$CI->load->model('admin/User_model');
	      	$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
	      	$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
	      	$signatures = $CI->Sign_model->getRoleSign($module_id, $form_id, $typed);
	      	$signers = array();
	      	foreach ($signatures as $key => $signature) {
	      		$last = $key - 1;
		        $signers[$key] = array();
		        $signers[$key]['id'] = $signature['id'];
		        $signers[$key]['role'] = $signature['role'];
		        $signers[$key]['role_id'] = $signature['role_id'];
		        $signers[$key]['rank'] = $signature['rank'];
		        $signers[$key]['typed'] = $signature['typed'];
	        	if (!is_null($signature['user_id']) && $signature['user_id'] != 0) {
		          	$signers[$key]['sign'] = array();
		          	$signers[$key]['sign']['id'] = $signature['user_id'];
	            	$signers[$key]['sign']['reject'] = $signature['reject'];
	            	$signers[$key]['sign']['reason'] = $signature['reason'];
		          	$user = $CI->User_model->get_user_by_id($signature['user_id']);
		          	$signers[$key]['sign']['name'] = $user['fullname'];
		          	$signers[$key]['sign']['mail'] = $user['email'];
		          	$signers[$key]['sign']['sign'] = $user['signature'];
		          	$signers[$key]['sign']['timestamp'] = $signature['timestamp'];
		          	if (isset($_SESSION['user_id']) && ($_SESSION['user_id'] == $signature['user_id'])) {
		          		$signers[$key]['sign']['unsign'] = TRUE;
		          	}else{
		          		$signers[$key]['sign']['unsign'] = FALSE;
		          	}
		          	if ((isset($signers[$last]) && $signers[$last])) {
			          	$signers[$last]['sign']['unsign'] = FALSE;
			        }
			        if ($form['status'] == 2) {
			          	$signers[$key]['sign']['unsign'] = FALSE;
			        }
			        if (isset($CI->global_data) && $CI->global_data['sessioned_user']['is_admin']==1) {
		          		$signers[$key]['sign']['unsign'] = TRUE;
			        }
	        	} else {
	         	 	$signers[$key]['queue'] = array();
		        	$signers[$key]['queue']['reason'] = $signature['reason'];
		        	if (isset($form['hid'])) {
				        $users = $CI->Sign_model->getQueue($signature['role_id'], $form['hid']);
		        	}else{
		        		if (isset($form['from_hid']) && $typed == 1) {
				        	$users = $CI->Sign_model->getQueue($signature['role_id'], $form['from_hid']);
				        } elseif (isset($form['to_hid']) && $typed == 2) {
				        	$users = $CI->Sign_model->getQueue($signature['role_id'], $form['to_hid']);
				        } else{
					        $users = $CI->Sign_model->getQueue($signature['role_id']);
				        }
		        	}
			        $user_id = array();
			        foreach ($users as $use) {
			        	$user_id[] = $use['id'];
	              		$signers[$key]['queue'][$use['id']] = array();
	              		$signers[$key]['queue'][$use['id']]['id'] = $use['id'];
	              		$signers[$key]['queue'][$use['id']]['name'] = $use['fullname'];
	              		$signers[$key]['queue'][$use['id']]['mail'] = $use['email'];
	            	}
	            	if (in_array($_SESSION['user_id'], $user_id)) {
			            $signers[$key]['signer'] = TRUE;
			        }else{
			            $signers[$key]['signer'] = FALSE;
			        }
	        	}
	      	}
	      	return $signers;
	    }
	}

	if (!function_exists('getWaitingSigners')){
	    function getWaitingSigners($module_id, $form_id, $typed = FALSE){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	      	$CI->load->model('admin/User_model');
	      	$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
	      	$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
	      	$signatures = $CI->Sign_model->getRoleSign($module_id, $form_id, $typed);
	      	$signers = array();
	      	foreach ($signatures as $signature) {
	        	if ($signature['user_id'] == 0) {
	         	 	$signers[] = $signature['role_id'];
	        	}
	      	}
	      	return $signers;
	    }
	}

	if (!function_exists('signChecker')){
	    function signChecker($module_id, $form_id){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	      	$CI->load->model('admin/Modules_model');
	      	$module = $CI->Modules_model->get_module_by($module_id);
	      	$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
	     	$role_types = $CI->Sign_model->getRoleTypes($module_id);
            $type = 0;
            foreach ($role_types as $role_type) {
              	$found = 0;
              	if (!is_null($role_type['hotel_id'])) {
                	if(isset($form['hid'])){
	                	if ($role_type['hotel_condition'] == '!=') {
	                  		if (($form['hid'] != $role_type['hotel_id'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['hotel_condition'] == '==') {
	                  		if (($form['hid'] == $role_type['hotel_id'])) {
	                    		$found++;
	                  		}
	                	}
	                }
              	}else{
                	$found++;
              	}
              	if (!is_null($role_type['dep_code'])) {
                	if(isset($form['dep_code'])){
	                	if ($role_type['department_condition'] == '!=') {
	                  		if ($form['dep_code'] != $role_type['dep_code']) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['department_condition'] == '==') {
	                  		if ($form['dep_code'] == $role_type['dep_code']) {
	                    		$found++;
	                  		}
	                	}
	                }
              	}else{
                	$found++;
              	}
              	if (!is_null($role_type['special'])) {
                	if(isset($form[$role_type['special']])){
	                	if ($role_type['special_condition'] == '<=') {
	                  		if (($form[$role_type['special']] <= $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == '>=') {
	                  		if (($form[$role_type['special']] >= $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == '>') {
	                  		if (($form[$role_type['special']] > $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == '<') {
	                  		if (($form[$role_type['special']] < $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == '!=') {
	                  		if (($form[$role_type['special']] != $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == '==') {
	                  		if (($form[$role_type['special']] == $role_type['special_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special_condition'] == 'is_not_null') {
	                  		if (!is_null($form[$role_type['special']])) {
	                    		$found++;
	                  		}
	                	}
	                }elseif ($role_type['special_condition'] == 'is_nulls') {
	                  	if (is_null($form[$role_type['special']])) {
	                    	$found++;
	                  	}
	                }
              	}else{
                	$found++;
              	}
              	if (!is_null($role_type['special1'])) {
                	if(isset($form[$role_type['special1']])){
	                	if ($role_type['special1_condition'] == '<=') {
	                  		if (($form[$role_type['special1']] <= $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == '>=') {
	                  		if (($form[$role_type['special1']] >= $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == '>') {
	                  		if (($form[$role_type['special1']] > $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == '<') {
	                  		if (($form[$role_type['special1']] < $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == '!=') {
	                  		if (($form[$role_type['special1']] != $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == '==') {
	                  		if (($form[$role_type['special1']] == $role_type['special1_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special1_condition'] == 'is_not_null') {
	                  		if (!is_null($form[$role_type['special1']])) {
	                    		$found++;
	                  		}
	                	}
	                }elseif ($role_type['special1_condition'] == 'is_nulls') {
	                  	if (is_null($form[$role_type['special1']])) {
	                    	$found++;
	                  	}
	                }
              	}else{
                	$found++;
              	}
              	if (!is_null($role_type['special2'])) {
                	if(isset($form[$role_type['special2']])){
	                	if ($role_type['special2_condition'] == '<=') {
	                  		if (($form[$role_type['special2']] <= $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special2_condition'] == '>=') {
	                  		if (($form[$role_type['special2']] >= $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                  	}elseif ($role_type['special2_condition'] == '>') {
	                  		if (($form[$role_type['special2']] > $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special2_condition'] == '<') {
	                  		if (($form[$role_type['special2']] < $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special2_condition'] == '!=') {
	                  		if (($form[$role_type['special2']] != $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special2_condition'] == '==') {
	                  		if (($form[$role_type['special2']] == $role_type['special2_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special2_condition'] == 'is_not_null') {
	                  		if (!is_null($form[$role_type['special2']])) {
	                    		$found++;
	                  		}
	                	}
	                }elseif ($role_type['special2_condition'] == 'is_nulls') {
	                  	if (is_null($form[$role_type['special2']])) {
	                    	$found++;
	                  	}
	                }
              	}else{
                	$found++;
              	}
              	if (!is_null($role_type['special3'])) {
                	if(isset($form[$role_type['special3']])){
	                	if ($role_type['special3_condition'] == '<=') {
	                  		if (($form[$role_type['special3']] <= $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special3_condition'] == '>=') {
	                  		if (($form[$role_type['special3']] >= $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                  	}elseif ($role_type['special3_condition'] == '>') {
	                  		if (($form[$role_type['special3']] > $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special3_condition'] == '<') {
	                  		if (($form[$role_type['special3']] < $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special3_condition'] == '!=') {
	                  		if (($form[$role_type['special3']] != $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special3_condition'] == '==') {
	                  		if (($form[$role_type['special3']] == $role_type['special3_value'])) {
	                    		$found++;
	                  		}
	                	}elseif ($role_type['special3_condition'] == 'is_not_null') {
	                  		if (!is_null($form[$role_type['special3']])) {
	                    		$found++;
	                  		}
	                	}
	                }elseif ($role_type['special3_condition'] == 'is_nulls') {
	                  	if (is_null($form[$role_type['special3']])) {
	                    	$found++;
	                  	}
	                }
              	}else{
                	$found++;
              	}
              	if ($found == 6) {
                	$type = $role_type['id'];
                	break;
              	}
            }
            return $type;
	    } 
	}

	if (!function_exists('signerRoles')){
	    function signerRoles($type){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	      	$roles = $CI->Sign_model->getSignerRoles($type);
	      	return $roles;
	    }
	}

	if (!function_exists('signRanker')){
	    function signRanker($module_id, $form_id, $rank, $typed = FALSE){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	     	$roles = $CI->Sign_model->signRank($module_id, $form_id, $rank, $_SESSION['user_id'], $typed);
	    }
	}

	if (!function_exists('sign')){
	    function sign($module_id, $form_id, $id){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	     	$CI->load->model('admin/Modules_model');
		    $module = $CI->Modules_model->get_module_by($module_id);
	      	$CI->Sign_model->updateState($module['tdatabase'], $form_id, 1);
	     	$CI->Sign_model->updateRebak($module['tdatabase'], $form_id, '');
	      	$roles = $CI->Sign_model->sign($id, $_SESSION['user_id']);
	    }
	}

	if (!function_exists('signFor')){
	    function signFor($module_id, $form_id, $id, $uid){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	     	$CI->load->model('admin/Modules_model');
		    $module = $CI->Modules_model->get_module_by($module_id);
	      	$CI->Sign_model->updateState($module['tdatabase'], $form_id, 1);
	     	$CI->Sign_model->updateRebak($module['tdatabase'], $form_id, '');
	      	$roles = $CI->Sign_model->sign($id, $uid);
	    }
	}

	if (!function_exists('unSign')){
	    function unSign($module_id, $form_id, $id){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
		    $CI->load->model('admin/Modules_model');
		    $module = $CI->Modules_model->get_module_by($module_id);
	      	$CI->Sign_model->updateState($module['tdatabase'], $form_id, 1);
		   	$CI->Sign_model->updateRebak($module['tdatabase'], $form_id, '');
	      	$roles = $CI->Sign_model->unSign($id);
	    }
	}

	if (!function_exists('reject')){
	    function reject($module_id, $form_id, $id, $reason){
		    $CI = get_instance();
		    $CI->load->model('admin/Sign_model');
		    $CI->load->model('admin/Modules_model');
		    $module = $CI->Modules_model->get_module_by($module_id);
		    $rejectData = array('user_id' => $_SESSION['user_id'], 'reason' => $reason, 'type' => 'Reject');
	      	$rejectData = json_encode($rejectData, JSON_UNESCAPED_UNICODE);
	      	$CI->Sign_model->updateRebak($module['tdatabase'], $form_id, $rejectData);
		    $CI->Sign_model->reject($id, $reason, $_SESSION['user_id']);
		    $CI->Sign_model->updateNextSign($module['tdatabase'], $form_id, 0);
		    $CI->Sign_model->updateState($module['tdatabase'], $form_id, 3);
	    }
	}

	if (!function_exists('resign')){
	    function resign($module_id, $form_id, $reason, $rank, $typed = FALSE){
		    $CI = get_instance();
		    $CI->load->model('admin/Sign_model');
		    $CI->load->model('admin/Modules_model');
		    $module = $CI->Modules_model->get_module_by($module_id);
	      	$form = $CI->Modules_model->getForm($module['tdatabase'], $form_id);
	      	$rebackData = array('user_id' => $_SESSION['user_id'], 'reason' => $reason, 'type' => 'Reback');
	      	$rebackData = json_encode($rebackData, JSON_UNESCAPED_UNICODE);
	      	$CI->Sign_model->updateRebak($module['tdatabase'], $form_id, $rebackData);
			$CI->Sign_model->reset($module_id, $form_id, $rank, $typed);
			$CI->Sign_model->reset_reason($module_id, $form_id, $reason, $rank, $typed);
			$CI->Sign_model->updateState($module['tdatabase'], $form_id, 1);
	    }
	}

	if (!function_exists('getIfSigner')){
	    function getIfSigner($module_id){
	     	$CI = get_instance();
	     	$CI->load->model('admin/Sign_model');
	      	$CI->load->model('admin/User_model');
	      	$CI->load->model('admin/Modules_model');
	      	$roleTypes = $CI->Sign_model->getRoleTypes($module_id);
	      	$types = array();
	      	foreach ($roleTypes as $roleType) {
	      		$types[] = $roleType['id'];
	      	}
	      	$signedRoles = $CI->Sign_model->getSignedRoles($types);
	      	$roled = array();
	      	foreach ($signedRoles as $signedRole) {
	      		$roled[] = $signedRole['role'];
	      	}
	      	$roles = $CI->Sign_model->getRoles($roled);
	      	return $roles;
	    }
	}

	if (!function_exists('getUroles')){
   		function getUroles($user_id){
    		$ci=get_instance();
    		$ci->db->select('user_hotels.role_id');
    		$ci->db->where('user_hotels.user_id',$user_id);
    		$allUserData= $ci->db->get('user_hotels')->result_array();
    		$uroles = array();
        	foreach ($allUserData as $userData) {
        		$roles = json_decode($userData['role_id']);
        		foreach ($roles as $role) {
          			$uroles[] = $role;
          		}
        	}
        	$uroles = array_unique($uroles); 
     		return $uroles;
  		}
	}

	if (!function_exists('getHroles')){
   		function getHroles($hid){
    		$ci=get_instance();
    		$ci->db->select('user_hotels.role_id');
    		$ci->db->where('user_hotels.user_id',$_SESSION['user_id']);
    		$ci->db->where('user_hotels.hotel_id',$hid);
    		$allUserData= $ci->db->get('user_hotels')->result_array();
    		$uroles = array();
        	foreach ($allUserData as $userData) {
        		$roles = json_decode($userData['role_id']);
        		foreach ($roles as $role) {
          			$uroles[] = $role;
          		}
        	}
        	$uroles = array_unique($uroles); 
     		return $uroles;
  		}
	}

	if (!function_exists('addEditSigners')) {
		function addEditSigners ($module_id, $form_id, $typed = FALSE){
    		$CI=get_instance();
			$type = signChecker($module_id, $form_id);
			$module = $CI->Modules_model->get_module_by($module_id);
			$form 	= $CI->Modules_model->getForm($module['tdatabase'], $form_id);
			if ($typed) {
	          	$CI->Sign_model->updateStage($module['tdatabase'], $form_id, $typed);
	        }
	        if (isset($form['hid'])) {
	         	$hid = $form['hid'];
	        }else{
	        	if (isset($form['from_hid']) && $typed == 1) {
	         		$hid = $form['from_hid'];
				} elseif (isset($form['to_hid']) && $typed == 2) {
	         		$hid = $form['to_hid'];
				} else{
		        	$hid = FALSE;
		        }
	        }
           	$signers = signerRoles($type);
           	$signerd = getSigners($module_id, $form_id, $typed);
           	if (count($signerd) > 0 && $form['typed'] != $type) {
				$CI->Sign_model->deleteSign($module_id, $form_id, $typed);
				$CI->Sign_model->updateTyped($module['tdatabase'], $form_id, $type);
				foreach ($signers as $signer) {
					$CI->Sign_model->addSignature($module_id, $form_id, $signer['role'], $signer['rank'], $hid, $typed);
				}
           	} elseif(count($signerd) == 0) {
           		$CI->Sign_model->updateTyped($module['tdatabase'], $form_id, $type);
	           	foreach ($signers as $signer) {
					$CI->Sign_model->addSignature($module_id, $form_id, $signer['role'], $signer['rank'], $hid, $typed);
				}
           	}
		}
	}

?>