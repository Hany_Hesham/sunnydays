<?php

if (!function_exists('pdfStyling')) {
	function pdfStyling(){
		$html ='<html>
					<head>
						<style>
							body {font-family: sans-serif;
							  font-size: 10pt;
							 }
							p { margin: 0pt; }
							table.items {
							  border: 0.1mm solid #000000;
							 }
							td { vertical-align: top; }
							.items td {
							  border-left: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							table thead td { 
							  background-color: #343a40;
							  color:#FFFFFF;
							  text-align: center;
							  border: 0.1mm solid #000000;
							  border-bottom: 0.1mm solid #000000;
							 }
							.items td.blanktotal {
							  background-color: #EEEEEE;
							  border: 0.1mm solid #000000;
							  background-color: #FFFFFF;
							  border: 0mm none #000000;
							  border-top: 0.1mm solid #000000;
							  border-right: 0.1mm solid #000000;
							 }
							.items td.totals {
							  text-align: right;
							  border: 0.1mm solid #000000;
							 }
							.items td.cost{
							  text-align: "." center;
							 }
							.department-row{
							  background-color :#EEEEEE;
							  border: 0 !important;
							 }
							.total-department-row{
							  background-color :#ccc;
							  color:#FFF !important;
							  border: 0 !important;
							  }
							a{ 
							  color: inherit;
							  text-decoration: none; 
							  } 

						</style>
					</head>';
		return $html;			
	  }
 }


if (!function_exists('report_pdf_generator')){
    function report_pdf_generator($data){ 
    	$ci=get_instance();
		ob_start();
		ini_set("memory_limit","1024M");
		ini_set("pcre.backtrack_limit", "50000000000000");
		$html = pdfStyling();
		$html.='
				<body>
				<!--mpdf
					<htmlpagefooter name="myfooter">
					<div style="border-top: 1px solid #000000; font-size: 9pt;padding-top: 3mm; ">
						<table width="100%">
							<tr>
							<td width="35%">'.date("Y-m-d h:i:sa").'</td>
							<td width="25%">'.$data['username'].'</td>
							<td width="45%" style="text-align: right;">Page {PAGENO} of {nb}</td>
							</tr>
						</table>
					</div>
					</htmlpagefooter>
					<sethtmlpagefooter name="myfooter" value="on" />
				mpdf-->
				<table width="100%">
					<tr>
						<td width="50%"><br><br>
							<h2 style="color:#198bbe;">'.$data['hotel']['hotel_name'].'</h2><br>
							<h3><strong>'.$data['report_name'].'</strong></h3>
							<h4><strong>From: '.$data['from_date'].' to: '.$data['to_date'].'</strong></h4>
						</td>
						<td width="50%" style="text-align: right;"> <img style="width:200px;height:130px;" src="'.base_url('assets/uploads/logos/'.$data['hotel']['logo']).'" alt="user"><br></td>
					</tr>
				</table>
				<br />
				<table class="items" width="100%" style="font-size: 9pt; border-collapse: collapse; " cellpadding="8">
					<thead>';  
				
				/**
				 * General Section
				*/
				
				/**
				 * Salary Advance Report
				*/
				if ($data['report_name']=='Salary Advance Report') {	     
						$html .='<tr>';
						$html .='  
							<td width="5%"><strong>#</strong></td>
							<td width="20%"><strong>Salary Advance</strong></td>
							<td width="15%"><strong>Hotel Name</strong></td>
							<td width="15%"><strong>Department Name</strong></td>
							<td width="15%"><strong>Position Name</strong></td>
							<td width="10%"><strong>Clock No#</strong></td>
							<td width="15%"><strong>Employee Name</strong></td>
							<td width="15%"><strong>Advance</strong></td>
							<td width="10%"><strong>Status</strong></td>
							<td width="20%"><strong>Signature On</strong></td>
							</tr>
						</thead>
						<tbody>';
						$i = 1;     
						foreach ($data['tableData'] as $row) {
							$html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
							$html .='   <td>'.$i.'.</td>
										<td>'.$row['salary_advance'].'</td>
										<td>'.$row['hotel_name'].'</td>
										<td>'.$row['dep_name'].'</td>
										<td>'.$row['position_name'].'</td>
										<td>'.$row['clock_no'].'</td>
										<td>'.$row['emp_name'].'</td>
										<td>'.$row['advance'].'</td>
										<td>'.$row['status'].'</td>
										<td>'.$row['signature'].'</td>
									</tr>';
							$i++;
							}
				
				}

				/**
				 * Salary Advance Report
				*/
				if ($data['report_name']=='Complimentary report') {	     
					$html .='<tr>';
					$html .='  
						<td width="5%"><strong>#</strong></td>
						<td width="20%"><strong>Complimentary</strong></td>
						<td width="20%"><strong>Hotel Name</strong></td>
						<td width="15%"><strong>Department Name</strong></td>
						<td width="15%"><strong>Company</strong></td>
						<td width="10%"><strong>Conf.</strong></td>
						<td width="15%"><strong>Name</strong></td>
						<td width="15%"><strong>arrival</strong></td>
						<td width="15%"><strong>departure</strong></td>
						<td width="15%"><strong>Room Type</strong></td>
						<td width="15%"><strong>Method Of Payment</strong></td>
						<td width="20%"><strong>reasons</strong></td>
						<td width="15%"><strong>Status</strong></td>
						<td width="20%"><strong>Signature On</strong></td>
						</tr>
					</thead>
					<tbody>';
					$i = 1;     
					foreach ($data['tableData'] as $row) {
						$html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
						$html .='   <td>'.$i.'.</td>
									<td>'.$row['complimentary'].'</td>
									<td>'.$row['hotel_name'].'</td>
									<td>'.$row['dep_name'].'</td>
									<td>'.$row['company'].'</td>
									<td>'.$row['conf_no'].'</td>
									<td>'.$row['name'].'</td>
									<td>'.$row['arrival'].'</td>
									<td>'.$row['departure'].'</td>
									<td>'.$row['room_type'].'</td>
									<td>'.$row['payment'].'</td>
									<td>'.$row['reasons'].'</td>
									<td>'.$row['status'].'</td>
									<td>'.$row['signature'].'</td>
								</tr>';
						$i++;
					}
			    }


				/**
				 * Payable Section
				*/
				
				/**
				 * Petty Cash Report
				*/
				if ($data['report_name']=='Petty Cash Report') {	     
					$html .='<tr>';
					$html .='  
						<td width="5%"><strong>#</strong></td>
						<td width="25%"><strong>Petty Cash Advance Disbursement</strong></td>
						<td width="20%"><strong>Hotel Name</strong></td>
						<td width="20%"><strong>Name</strong></td>
						<td width="15%"><strong>Position Name</strong></td>
						<td width="15%"><strong>Acc No#</strong></td>
						<td width="20%"><strong>Description</strong></td>
						<td width="20%"><strong>Notes</strong></td>
						<td width="15%"><strong>Amount</strong></td>
						<td width="20%"><strong>Status</strong></td>
						<td width="20%"><strong>Signature On</strong></td>
						</tr>
					</thead>
					<tbody>';
					$i = 1;     
					foreach ($data['tableData'] as $row) {
						$html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
						$html .='   <td>'.$i.'.</td>
									<td>'.$row['petty_cash'].'</td>
									<td>'.$row['hotel_name'].'</td>
									<td>'.$row['name'].'</td>
									<td>'.$row['position_name'].'</td>
									<td>'.$row['acc_no'].'</td>
									<td>'.$row['description'].'</td>
									<td>'.$row['notes'].'</td>
									<td>'.$row['amount'].'</td>
									<td>'.$row['status'].'</td>
									<td>'.$row['signature'].'</td>
								</tr>';
						$i++;
						}
			
			}

			/**
			 * Payment Voucher Report
			*/
			if ($data['report_name']=='Payment Voucher Report') {	     
				$html .='<tr>';
				$html .='  
					<td width="5%"><strong>#</strong></td>
					<td width="25%"><strong>Petty Cash Advance Disbursement</strong></td>
					<td width="20%"><strong>Hotel Name</strong></td>
					<td width="15%"><strong>Explanation</strong></td>
					<td width="15%"><strong>Acc No#</strong></td>
					<td width="20%"><strong>Description</strong></td>
					<td width="20%"><strong>Notes</strong></td>
					<td width="15%"><strong>Amount</strong></td>
					<td width="20%"><strong>Status</strong></td>
					<td width="20%"><strong>Signature On</strong></td>
					</tr>
				</thead>
				<tbody>';
				$i = 1;     
				foreach ($data['tableData'] as $row) {
					$html .='<tr  class="'.($i % 2 == 0?'department-row':'').'">';
					$html .='   <td>'.$i.'.</td>
								<td>'.$row['payment_voucher'].'</td>
								<td>'.$row['hotel_name'].'</td>
								<td>'.$row['explanation'].'</td>
								<td>'.$row['acc_no'].'</td>
								<td>'.$row['description'].'</td>
								<td>'.$row['notes'].'</td>
								<td>'.$row['amount'].'</td>
								<td>'.$row['status'].'</td>
								<td>'.$row['signature'].'</td>
							</tr>';
					$i++;
				}
		    }
	
		// end of pdf 		
		$html .= ' </tbody></table></body></html>';   
		ob_end_clean();
		return($html);
			
	}
}

              


?>