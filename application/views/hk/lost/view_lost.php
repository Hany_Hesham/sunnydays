<?php initiate_breadcrumb(
  'Gifts and Findings Exit Permit #'.$lost['id'].'',
  $lost['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Gifts and Findings Exit Permit Edit','url'=>'hk/lost/edit/'.$lost['id']),
    array('name'=>'Gifts and Findings Exit Permit Copy','url'=>'hk/lost/copy/'.$lost['id'].'/1')
  ),//options
  array(array('name'=>'Gifts and Findings Exit Permit','url'=>'hk/lost'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$lost['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Gifts and Findings Exit Permit #<?php echo $lost['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'lost'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $lost['hotel_name']?>, 
                <br/><strong>Department: </strong> <?php echo  $lost['dep_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $lost['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $lost['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $lost['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($lost['reback']) && $lost['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($lost['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hk/lost','viewrates','<?php echo $lost['id']?>','mainPageBody');
  getViewAjax('hk/lost','signers_items','<?php echo ''.$lost['id']?>','signersItems');
</script>
