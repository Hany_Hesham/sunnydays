<?php initiate_breadcrumb(
  'Maintenance Request #'.$maintenance['id'].'',
  $maintenance['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Maintenance Request Edit','url'=>'hk/maintenance/edit/'.$maintenance['id']),
    array('name'=>'Maintenance Request Copy','url'=>'hk/maintenance/copy/'.$maintenance['id'].'/1')
  ),//options
  array(array('name'=>'Maintenance Request','url'=>'hk/maintenance'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$maintenance['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Maintenance Request #<?php echo $maintenance['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'maintenance'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $maintenance['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo $maintenance['dep_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $maintenance['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $maintenance['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $maintenance['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($maintenance['reback']) && $maintenance['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($maintenance['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hk/maintenance','viewrates','<?php echo $maintenance['id']?>','mainPageBody');
  getViewAjax('hk/maintenance','signers_items','<?php echo ''.$maintenance['id']?>','signersItems');
</script>
