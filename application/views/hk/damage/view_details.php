<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Room</strong>
              </td>
              <td>
                <?php echo $damage['room']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Area</strong>
              </td>
              <td>
                <?php echo $damage['area']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Item</strong>
              </td>
              <td>
                <?php echo $damage['item']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Cost</strong>
              </td>
              <td>
                <?php echo $damage['cost']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Damage</strong>
              </td>
              <td colspan="3">
                <?php echo $damage['damage']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $damage['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>