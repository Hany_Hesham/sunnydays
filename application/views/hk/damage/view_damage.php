<?php initiate_breadcrumb(
  'Damage Report #'.$damage['id'].'',
  $damage['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Damage Report Edit','url'=>'hk/damage/edit/'.$damage['id']),
    array('name'=>'Damage Report Copy','url'=>'hk/damage/copy/'.$damage['id'].'/1')
  ),//options
  array(array('name'=>'Damage Report','url'=>'hk/damage'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$damage['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Damage Report #<?php echo $damage['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'damage'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $damage['hotel_name']?>, 
                <br/><strong>Department: </strong> <?php echo  $damage['dep_name']; ?>,
                <br/><strong>Date: </strong> <?php echo  $damage['date']; ?>,
                <br/><strong>Status: </strong> <?php echo  $damage['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $damage['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $damage['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($damage['reback']) && $damage['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($damage['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hk/damage','viewrates','<?php echo $damage['id']?>','mainPageBody');
  getViewAjax('hk/damage','signers_items','<?php echo ''.$damage['id']?>','signersItems');
</script>
