<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">To</strong>
              </td>
              <td>
                <?php echo $bank_transfer['to_name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Amount</strong>
              </td>
              <td>
                <?php echo number_format($bank_transfer['amount'],2)?> <?php echo $bank_transfer['currency']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">From Bank</strong>
              </td>
              <td>
                <?php echo $bank_transfer['from_bank']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">From ACC NO.</strong>
              </td>
              <td>
                <?php echo $bank_transfer['from_acc_no']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">To Bank</strong>
              </td>
              <td>
                <?php echo $bank_transfer['to_bank']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">To ACC NO.</strong>
              </td>
              <td>
                <?php echo $bank_transfer['to_acc_no']?>
              </td>
            </tr>
            <tr>
              <tr>
              <td>
                <strong style="font-size: 11pt;">Reason</strong>
              </td>
              <td colspan="3">
                <?php echo $bank_transfer['reason']?>
              </td>
            </tr>
            <tr>
              <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $bank_transfer['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>