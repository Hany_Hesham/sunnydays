<?php initiate_breadcrumb(
  'Bank Transfer #'.$bank_transfer['id'].'',
  $bank_transfer['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Bank Transfer Edit','url'=>'payable/bank_transfer/edit/'.$bank_transfer['id']),
    array('name'=>'Bank Transfer Copy','url'=>'payable/bank_transfer/copy/'.$bank_transfer['id'].'/1')
  ),//options
  array(array('name'=>'Bank Transfer','url'=>'payable/bank_transfer'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$bank_transfer['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Bank Transfer #<?php echo $bank_transfer['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'bank_transfer'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $bank_transfer['hotel_name']?>, 
                <br/><strong>Type: </strong> <?php echo  $bank_transfer['type_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $bank_transfer['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $bank_transfer['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $bank_transfer['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($bank_transfer['reback']) && $bank_transfer['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($bank_transfer['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('payable/bank_transfer','viewrates','<?php echo $bank_transfer['id']?>','mainPageBody');
  getViewAjax('payable/bank_transfer','signers_items','<?php echo ''.$bank_transfer['id']?>','signersItems');
</script>