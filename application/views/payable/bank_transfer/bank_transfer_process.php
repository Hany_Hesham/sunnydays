<?php 
  $back_name = (isset($bank_transfer)? 'Bank Transfer #'.$bank_transfer['id'].'': 'Bank Transfer');
  $back_url  = (isset($bank_transfer)? 'payable/bank_transfer/view/'.$bank_transfer['id'].'':'payable/bank_transfer');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($bank_transfer)) {
      if (isset($copy)) {
        echo form_open(base_url('payable/bank_transfer/copy/'.$bank_transfer['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('payable/bank_transfer/edit/'.$bank_transfer['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('payable/bank_transfer/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($bank_transfer) && ($bank_transfer['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Bank</label>
                <input type="text" class="form-control" id="from_bank" name="from_bank" placeholder="From Bank" value="<?php echo (isset($bank_transfer))? $bank_transfer['from_bank']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From ACC NO.</label>
                <input type="text" class="form-control" id="from_acc_no" name="from_acc_no" placeholder="From ACC NO." value="<?php echo (isset($bank_transfer))? $bank_transfer['from_acc_no']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To</label>
                <input type="text" class="form-control" id="to_name" name="to_name" placeholder="To" value="<?php echo (isset($bank_transfer))? $bank_transfer['to_name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Bank</label>
                <input type="text" class="form-control" id="to_bank" name="to_bank" placeholder="To Bank" value="<?php echo (isset($bank_transfer))? $bank_transfer['to_bank']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To ACC NO.</label>
                <input type="text" class="form-control" id="to_acc_no" name="to_acc_no" placeholder="To ACC NO." value="<?php echo (isset($bank_transfer))? $bank_transfer['to_acc_no']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                <select  id="type_id" name="type_id" class="selectpicker type-select form-control"data-container="body" data-live-search="true" title="Select Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($types as $type){?>
                    <option value="<?php echo $type['id']?>" <?php echo (isset($bank_transfer) && ($bank_transfer['type_id'] == $type['id']))? 'selected="selected"' : set_select('type_id',$type['id'] ); ?>><?php echo $type['type_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Amount</label>
                <input type="number" step="0.01" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?php echo (isset($bank_transfer))? $bank_transfer['amount']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Currency</label>
                <select  id="currency" name="currency" class="selectpicker currency-select form-control"data-container="body" data-live-search="true" title="Select Currency" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($currencies as $currency){?>
                    <option value="<?php echo $currency['symbol']?>" data-rate="<?php echo $currency['rate']?>" <?php echo (isset($bank_transfer) && ($bank_transfer['currency'] == $currency['symbol']))? 'selected="selected"' : set_select('currency',$currency['symbol'] ); ?>><?php echo $currency['symbol']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Reason</label>
                <textarea name="reason" class="form-control" rows="3"><?php echo (isset($bank_transfer))? $bank_transfer['reason']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($bank_transfer))? $bank_transfer['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('payable/bank_transfer',$uploads,$this->data['module']['id'],$gen_id,'bank_transfer','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('payable/bank_transfer/bank_transfer.js.php');?>