<script type="text/javascript">
  
  let limit    = '<?php echo $this->data['limit']?>';

  $('#submitRS').show();

  $('#warningMessage').hide();

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  $('#currency').on('change', function () {
    checkLimit();
  });

  $('#amount').on( 'keyup', function() {
    checkLimit();
  });

  function checkLimit(){
    let rate      = $('option:selected', '#currency').attr('data-rate');
    let total     = $("#amount").val();
    let total_rated = total * rate ;
    // console.log(rate);
    // console.log(total_rated);
    // console.log(limit);
    if ((limit > 0) && (total_rated > limit)) {
      alert('The Maximum Total Amount Must be Less or Equal Limit '+limit+' The Amount Now is '+total_rated);
      $('#submitRS').hide();
    }else{
      $('#submitRS').show();
    }
  }
  
</script>