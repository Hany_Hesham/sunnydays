<?php $f = new NumberFormatter("ar", NumberFormatter::SPELLOUT);?>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Amount</strong>
              </td>
              <td>
                <span><?php echo number_format($payment_voucher['amount'],2)?> <?php echo $payment_voucher['currency']?></span>
              </td>
              <td>
                <strong style="font-size: 11pt;">Amount in Letters</strong>
              </td>
              <td>
                <span><?php echo $f->format($payment_voucher['amount'])?>  <?php echo $payment_voucher['currency']?></span>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Explanation</strong>
              </td>
              <td colspan="3">
                <span><?php echo $payment_voucher['explanation']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="14%"><strong style="font-size: 11pt;">Acc. NO</strong></th>
              <th width="30%"><strong style="font-size: 11pt;">Description</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">Amount</strong></th>
              <th width="40%"><strong style="font-size: 11pt;">Notes</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; $total = 0; foreach($payment_voucher_items as $item){ $total += $item['amount']; ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['acc_no']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['description']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['amount'],2)?> <?php echo $payment_voucher['currency']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['notes']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
            <tr>
              <td colspan="3">
                <strong style="font-size: 16pt;">Total</strong>
              </td>
              <td colspan="2">
                <strong style="font-size: 11pt;"><?php echo number_format($total,2)?> <?php echo $payment_voucher['currency']?></strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $payment_voucher['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>