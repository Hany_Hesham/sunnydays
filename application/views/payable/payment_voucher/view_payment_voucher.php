<?php initiate_breadcrumb(
  'Payment Voucher #'.$payment_voucher['id'].'',
  $payment_voucher['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Payment Voucher Edit','url'=>'payable/payment_voucher/edit/'.$payment_voucher['id']),
    array('name'=>'Payment Voucher Copy','url'=>'payable/payment_voucher/copy/'.$payment_voucher['id'].'/1')
  ),//options
  array(array('name'=>'Payment Voucher','url'=>'payable/payment_voucher'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$payment_voucher['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Payment Voucher #<?php echo $payment_voucher['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'payment_voucher'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $payment_voucher['hotel_name']?>, 
                <br/><strong>Type Name: </strong> <?php echo  $payment_voucher['type_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $payment_voucher['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $payment_voucher['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $payment_voucher['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($payment_voucher['reback']) && $payment_voucher['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($payment_voucher['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Received by</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('payable/payment_voucher','viewrates','<?php echo $payment_voucher['id']?>','mainPageBody');
  getViewAjax('payable/payment_voucher','signers_items','<?php echo ''.$payment_voucher['id']?>','signersItems');
</script>
