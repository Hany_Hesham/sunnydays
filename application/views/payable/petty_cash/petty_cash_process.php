<?php 
  $back_name = (isset($petty_cash)? 'Petty Cash Disbursement #'.$petty_cash['id'].'': 'Petty Cash Disbursement');
  $back_url  = (isset($petty_cash)? 'payable/petty_cash/view/'.$petty_cash['id'].'':'payable/petty_cash');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($petty_cash)) {
      if (isset($copy)) {
        echo form_open(base_url('payable/petty_cash/copy/'.$petty_cash['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('payable/petty_cash/edit/'.$petty_cash['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('payable/petty_cash/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($petty_cash) && ($petty_cash['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label for="items" class="text-right control-label col-form-label" style="font-size:12px;">*Items</label>
                <select  id="item_id" name="itemslist" class="selectpicker items-select form-control" data-live-search="true" title="Select Items" style="height: 100% !important;width:100%;">
                </select>
                <div class="hidden" id="selectedValues"></div>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type Name</label>
                <select  id="type_id" name="type_id" class="selectpicker type-select form-control" data-container="body" data-live-search="true" title="Select Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($types as $type){?>
                    <option value="<?php echo $type['id']?>" <?php echo (isset($petty_cash) && ($petty_cash['type_id'] == $type['id']))? 'selected="selected"':set_select('type_id',$type['id'] ); ?>><?php echo $type['type_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Explanation</label>
                <textarea name="explanation" class="form-control" rows="3"><?php echo (isset($petty_cash))? $petty_cash['explanation']: '';?></textarea>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Currency</label>
                <select  id="currency" name="currency" class="selectpicker currency-select form-control"data-container="body" data-live-search="true" title="Select Currency" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($currencies as $currency){?>
                    <option value="<?php echo $currency['symbol']?>" data-rate="<?php echo $currency['rate']?>" <?php echo (isset($petty_cash) && ($petty_cash['currency'] == $currency['symbol']))? 'selected="selected"':set_select('currency',$currency['symbol'] ); ?>><?php echo $currency['symbol']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Code</label>
                <input type="text" class="form-control" id="code" name="code" placeholder="Code" value="<?php echo (isset($petty_cash))? $petty_cash['code']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label for="items" class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($petty_cash))? $petty_cash['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label for="items" class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <input type="text" class="form-control" id="position_name" name="position_name" placeholder="Position" value="<?php echo (isset($petty_cash))? $petty_cash['position_name']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="petty_cashAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="15%">Acc. NO</th>
                        <th width="30%">Description</th>
                        <th width="14%">Amount</th>
                        <th width="40%">Notes</th>
                      </tr>
                    </thead>
                    <tbody id="petty_cash-data">
                      <?php if(isset($petty_cash)){$i=1;$ids = '';foreach($petty_cash_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="acc_no<?php echo $i?>" type="text" name="items[<?php echo $i?>][acc_no]" class="form-control" value="<?php echo $row['acc_no']?>"/>
                          </td>
                          <td class="text-center">
                            <input id="description<?php echo $i?>" type="text" name="items[<?php echo $i?>][description]" class="form-control" value="<?php echo $row['description']?>"/>
                          </td>           
                          <td>
                            <input id="amount<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][amount]" class="form-control amount" value="<?php echo $row['amount']?>"/>
                          </td> 
                          <td>
                            <textarea id="notes<?php echo $i?>" name="items[<?php echo $i?>][notes]" class="form-control" rows="3"><?php echo $row['notes']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($petty_cash))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($petty_cash))? $petty_cash['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('payable/petty_cash',$uploads,$this->data['module']['id'],$gen_id,'petty_cash','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" name="submit" onclick="remvesubmit()" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('payable/petty_cash/petty_cash.js.php');?>