<script type="text/javascript">
    
  let limit    = '<?php echo $this->data['limit']?>';

  $('#submitRS').show();

  $('#warningMessage').hide();

  $(document).ready(function(){
    $( ".items-select .bs-searchbox input" ).attr('id','items-search-box');
    $('#items-search-box').keyup(function(e) {
      var code = e.key;
      if(code==="Enter"){
        itemsSearch();
      }
    });
  });

  $('#currency').on('change', function () {
    checkLimit();
  });

  $( "#selected-Items-list-table" ).on( "keyup", "input", function( event ) {
    checkLimit();
  });

  $('#item_id').change(function () {
    storeSelected();
  });

  $('#hid').on('change', function () {
    $("#selected-Items-list-table .rowIds").remove(); 
    $("#item_id").empty();
    $("#item_id").selectpicker("refresh");
  });

  $('#code').change(function () {
    employeeSearch();
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function checkLimit(){
    let rate = $('option:selected', '#currency').attr('data-rate');
    let total = 0 ;
    $('.amount').map(function(){
      total = Number($(this).val())+Number(total);
    }).get(); 
    let total_rated = total * rate ;
    // console.log(rate);
    // console.log(total_rated);
    // console.log(limit);
    if ((limit > 0) && (total_rated > limit)) {
      alert('The Maximum Total Amount Must be Less or Equal Limit '+limit+' The Amount Now is '+total_rated);
      $('#submitRS').hide();
    }else{
      $('#submitRS').show();
    }
  }

  function itemsSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    var search  = $('#items-search-box').val();
    var hid     = $('#hid').val();
    var keyword = 'chart_account';
    var url="<?php echo base_url('payable/petty_cash/search_metaData/');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{search:search, hid:hid, keyword:keyword},
      success: function(data){
        $("#item_id").empty();
        $("#selectedValues").empty();
        $.each(data, function(i,item){
          if (data[i].type_name == undefined) {
            $("#item_id").append('<option value="'+ data[i].id +'">'+ data[i].key +'</option>');
          }else{
            $("#item_id").append('<option value="'+ data[i].id+'" data-subtext="'+data[i].type_data+'">'+ data[i].type_name +'</option>');
            $("#selectedValues").append('<span id="selected-type_data'+data[i].id+'">'+data[i].type_data+'</span><span id="selected-type_name'+data[i].id+'">'+data[i].type_name+'</span>');
          }
          $('.selectpicker').selectpicker('refresh');            
        });       
      }
    })
  }

  function employeeSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    let hid   = $('#hid').val();
    let empNo = $('#code').val();
    if (hid != '' && empNo !='') {
      var url =getUrl()+'payable/payment_voucher/search_employee/'+hid+'/'+empNo;      
      $.post(url).done(function(response) {
        response = JSON.parse(response);
        if(response, $.each(response, function() {
          $('#name').val(response.name);
          $('#position_name').val(response.position_name);
          $('#name').prop('readonly', true);
          $('#position_name').prop('readonly', true);
        }));
      });
    }
  }

  function storeSelected(){
    let  item_id     = $("#item_id").val();
    let  description = $("#selected-type_data"+item_id+"").text();
    let  code        = $("#selected-type_name"+item_id+"").text();
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#petty_cash-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="acc_no'+rowId+'" type="text" name="items['+rowId+'][acc_no]" class="form-control" value="'+code+'" readonly/></td>     <td><input id="description'+rowId+'" type="text" name="items['+rowId+'][description]" class="form-control" value="'+description+'" readonly/></td>     <td><input id="amount'+rowId+'" type="number" step="0.01" name="items['+rowId+'][amount]" class="form-control amount" value=""/> </td>     <td><textarea id="notes'+rowId+'" name="items['+rowId+'][notes]" class="form-control" rows="3"></textarea></td>     </tr>');
    $("#item_id").val('default');
    $("#item_id").selectpicker("refresh");
  }

  function petty_cashAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#petty_cash-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="acc_no'+rowId+'" type="text" name="items['+rowId+'][acc_no]" class="form-control" value=""/></td>     <td><input id="description'+rowId+'" type="text" name="items['+rowId+'][description]" class="form-control" value=""/></td>     <td><input id="amount'+rowId+'" type="number" step="0.01" name="items['+rowId+'][amount]" class="form-control amount" value=""/> </td>     <td><textarea id="notes'+rowId+'" name="items['+rowId+'][notes]" class="form-control" rows="3"></textarea></td>     </tr>');
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'payable/petty_cash/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>