<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Reason</strong>
              </td>
              <td colspan="3">
                <span><?php echo $condemnation['reason']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%" ><strong style="font-size: 11pt;">#</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Name</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Qty</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Price</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Total Current Price</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Purchase Date</strong></th>
              <th width="40%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; $total=0; $priceTotal=0; foreach($condemnation_items as $item){ $total+=$item['price'];$priceTotal+=$item['total_price'];?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quantity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['price'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['total_price'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['purchase_date']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
            <tr>
              <td colspan="4">
                <strong style="font-size: 11pt;">Total</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total,2)?> EGP</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($priceTotal,2)?> EGP</strong>
              </td>
              <td colspan="2">
                <strong style="font-size: 11pt;"></strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $condemnation['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>