<?php initiate_breadcrumb(
  'Condemnation Record #'.$condemnation['id'].'',
  $condemnation['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Condemnation Record Edit','url'=>'general/condemnation/edit/'.$condemnation['id']),
    array('name'=>'Condemnation Record Copy','url'=>'general/condemnation/copy/'.$condemnation['id'].'/1')
  ),//options
  array(array('name'=>'Condemnation Record','url'=>'general/condemnation'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$condemnation['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Condemnation Record #<?php echo $condemnation['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'condemnation'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $condemnation['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo $condemnation['dep_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $condemnation['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $condemnation['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $condemnation['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($condemnation['reback']) && $condemnation['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($condemnation['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <div class="container-fluid">
        <h3 style="text-align: right;">بناءً على التوقيعات بالاعلى يتم تكوين لجنه التكهين للتاكد من جميع الاصناف وتتكون من الاتى </h3>
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Cost Control</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Security Manager</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Owning Company Auditing Manager</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Owning Company Chief Engineer</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Projects Manager</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('general/condemnation','viewrates','<?php echo $condemnation['id']?>','mainPageBody');
  getViewAjax('general/condemnation','signers_items','<?php echo ''.$condemnation['id']?>','signersItems');
</script>
