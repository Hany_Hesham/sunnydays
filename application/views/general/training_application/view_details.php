<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Name</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['name']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">الاسم</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Gender</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['gender_name']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">النوع </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Date of Birth</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['birth_date']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">تاريخ الميلاد</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Place of Birth</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['birth_location']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">محل الميلاد </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Nationality</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['nationality']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">الجنسية </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Religion</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['religion']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">الديانة </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Identity Card</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['card_id']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">رقم البطاقة الشخصية </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Status of Military</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['military']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">الموقف من التجنيد</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Marital Status</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['marital_name']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">الحالة الاجتماعية</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Present Address</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['address']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">العنوان الحالي </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Telephone Number</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['telephone']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">تليفون رقم</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Emergency Address</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['address2']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">عنوان  الطوارىء</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Starting Date</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['starting_date']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">تاريخ التعيين</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Training Period</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['period']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">فترة التدريب</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Institute Name</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['institute']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">اسم المعهد او الكلية</strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Reward</strong>
              </td>
              <td style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['reward']?></span>
              </td>
              <td style="font-size: 11pt; text-align: right;">
                <strong style="font-size: 11pt; text-align: right;">مكافأة  </strong>
              </td>
            </tr>
            <tr>
              <td style="font-size: 11pt; text-align: left;">
                <strong style="font-size: 11pt; text-align: left;">Remarks</strong>
              <td colspan="2" style="text-align: center;">
                <span style="text-align: center;" ><?php echo $training_application['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>