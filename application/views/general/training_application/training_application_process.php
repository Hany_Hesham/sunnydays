<?php 
  $back_name = (isset($training_application)? 'Application For Training #'.$training_application['id'].'': 'Application For Training');
  $back_url  = (isset($training_application)? 'general/training_application/view/'.$training_application['id'].'':'general/training_application');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($training_application)) {
      if (isset($copy)) {
        echo form_open(base_url('general/training_application/copy/'.$training_application['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/training_application/edit/'.$training_application['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/training_application/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($training_application) && ($training_application['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($training_application) && ($training_application['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($training_application))? $training_application['name']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Gender</label>
                <select  id="gender" name="gender" class="selectpicker gender-select form-control"data-container="body" data-live-search="true" title="Select Gender" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($genders as $gender){?>
                    <option value="<?php echo $gender['id']?>" <?php echo (isset($training_application) && ($training_application['gender'] == $gender['id']))? 'selected="selected"' : set_select('gender',$gender['id'] ); ?>><?php echo $gender['type_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date of Birth </label>
                <input type="text" class="form-control" id="birth_date" data-toggle="datepicker" name="birth_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($training_application))? $training_application['birth_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Place of Birth</label>
                <input type="text" class="form-control" id="birth_location" name="birth_location" placeholder="Place of Birth" value="<?php echo (isset($training_application))? $training_application['birth_location']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Nationality</label>
                <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" value="<?php echo (isset($training_application))? $training_application['nationality']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Religion</label>
                <input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" value="<?php echo (isset($training_application))? $training_application['religion']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Identity Card</label>
                <input type="text" class="form-control" id="card_id" name="card_id" placeholder="Identity Card" value="<?php echo (isset($training_application))? $training_application['card_id']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Status of Military</label>
                <input type="text" class="form-control" id="military" name="military" placeholder="Status of Military" value="<?php echo (isset($training_application))? $training_application['military']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Marital Status</label>
                <select  id="marital" name="marital" class="selectpicker marital-select form-control"data-container="body" data-live-search="true" title="Select Marital Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($marital_statuses as $marital_status){?>
                    <option value="<?php echo $marital_status['id']?>" <?php echo (isset($training_application) && ($training_application['marital'] == $marital_status['id']))? 'selected="selected"' : set_select('marital',$marital_status['id'] ); ?>><?php echo $marital_status['type_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Present Address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Present Address" value="<?php echo (isset($training_application))? $training_application['address']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Telephone Number</label>
                <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Telephone Number" value="<?php echo (isset($training_application))? $training_application['telephone']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Emergency Address</label>
                <input type="text" class="form-control" id="address2" name="address2" placeholder="Emergency Address" value="<?php echo (isset($training_application))? $training_application['address2']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Starting date</label>
                <input type="text" class="form-control" id="starting_date" data-toggle="datepicker" name="starting_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($training_application))? $training_application['starting_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Training Period</label>
                <input type="text" class="form-control" id="period" name="period" placeholder="Training Period" value="<?php echo (isset($training_application))? $training_application['period']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Institute Name</label>
                <input type="text" class="form-control" id="institute" name="institute" placeholder="Institute Name" value="<?php echo (isset($training_application))? $training_application['institute']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Reward</label>
                <input type="number" class="form-control" id="reward" step="0.01" name="reward" placeholder="Reward" value="<?php echo (isset($training_application))? $training_application['reward']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($training_application))? $training_application['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/training_application',$uploads,$this->data['module']['id'],$gen_id,'training_application','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/training_application/training_application.js.php');?>