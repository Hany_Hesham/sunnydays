<?php initiate_breadcrumb(
  'Performance Evaluation #'.$evaluation['id'].'',
  $evaluation['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Performance Evaluation Edit','url'=>'general/evaluation/edit/'.$evaluation['id']),
    array('name'=>'Performance Evaluation Copy','url'=>'general/evaluation/copy/'.$evaluation['id'].'/1')
  ),//options
  array(array('name'=>'Performance Evaluation','url'=>'general/evaluation'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$evaluation['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Performance Evaluation #<?php echo $evaluation['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'evaluation'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $evaluation['hotel_name']?>, 
                <br/><strong>Type: </strong> <?php echo  $evaluation['evaluation_type']; ?>,
                <br/><strong>Department: </strong> <?php echo  $evaluation['dep_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $evaluation['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $evaluation['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $evaluation['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($evaluation['reback']) && $evaluation['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($evaluation['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('general/evaluation','viewrates','<?php echo $evaluation['id']?>','mainPageBody');
  getViewAjax('general/evaluation','signers_items','<?php echo ''.$evaluation['id']?>','signersItems');
</script>
