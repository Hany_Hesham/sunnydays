<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $evaluation['name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Position</strong>
              </td>
              <td>
                <?php echo $evaluation['position_name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Hiring Date</strong>
              </td>
              <td>
                <?php echo $evaluation['hire_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Last Promotion</strong>
              </td>
              <td>
                <?php echo $evaluation['promotion_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Section</strong>
              </td>
              <td>
                <?php echo $evaluation['section']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Age</strong>
              </td>
              <td>
                <?php echo $evaluation['age']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Education</strong>
              </td>
              <td>
                <?php echo $evaluation['education']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Career Recommendations</strong>
              </td>
              <td>
                <?php echo $evaluation['recommendation_name']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="20%"><strong style="font-size: 11pt;">Areas</strong></th>
              <th width="20%"><strong style="font-size: 11pt;">Evaluation</strong></th>
              <th width="60%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($evaluation_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['area_name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['evaluation_name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">What is the employee s best performance?</strong>
              </td>
              <td colspan="3">
                <?php echo $evaluation['best_performance']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">What Are the employee s areas of improvement?</strong>
              </td>
              <td colspan="3">
                <?php echo $evaluation['improvement']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Training Required</strong>
              </td>
              <td colspan="3">
                <?php echo $evaluation['training']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Name</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Present Position</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Qualified For</strong></th>
              <th width="24%"><strong style="font-size: 11pt;">When Qualified</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($evaluation_itemz as $item){ ?>
              <tr id="rowz<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['position']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['qualified']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['month']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $evaluation['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>