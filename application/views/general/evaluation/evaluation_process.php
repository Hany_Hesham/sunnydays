<?php 
  $back_name = (isset($evaluation)? 'Performance Evaluation #'.$evaluation['id'].'': 'Performance Evaluation');
  $back_url  = (isset($evaluation)? 'general/evaluation/view/'.$evaluation['id'].'':'general/evaluation');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($evaluation)) {
      if (isset($copy)) {
        echo form_open(base_url('general/evaluation/copy/'.$evaluation['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/evaluation/edit/'.$evaluation['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/evaluation/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($evaluation) && ($evaluation['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($evaluation) && ($evaluation['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <select  id="position" name="position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($evaluation) && ($evaluation['position'] == $user_group['id']))? 'selected="selected"' : set_select('position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                <select  id="type" name="type" class="selectpicker type-select form-control"data-container="body" data-live-search="true" title="Select Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($types as $type){?>
                    <option value="<?php echo $type['id']?>" <?php echo (isset($evaluation) && ($evaluation['type'] == $type['id']))? 'selected="selected"' : set_select('type',$type['id'] ); ?>><?php echo $type['type_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($evaluation))? $evaluation['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date Of Hiring</label>
                <input type="text" class="form-control" id="hire_date" data-toggle="datepicker" name="hire_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($evaluation))? $evaluation['hire_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Section</label>
                <input type="text" class="form-control" id="section" name="section" placeholder="Section" value="<?php echo (isset($evaluation))? $evaluation['section']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Last Promotion</label>
                <input type="text" class="form-control" id="promotion_date" data-toggle="datepicker" name="promotion_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($evaluation))? $evaluation['promotion_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Age</label>
                <input type="number" class="form-control" id="age" name="age" placeholder="Age" value="<?php echo (isset($evaluation))? $evaluation['age']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Education</label>
                <input type="text" class="form-control" id="education" name="education" placeholder="Education" value="<?php echo (isset($evaluation))? $evaluation['education']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Itemz-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="20%">Areas</th>
                        <th width="20%">Evaluation</th>
                        <th width="60%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php if(isset($evaluation)):?>
                        <?php $i=1;foreach($evaluation_items as $row){?>
                          <tr class="rowIdz">                                      
                            <td class="text-center">
                              <span><?php echo $row['area_name']?></span>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                              <input id="areaid<?php echo $i?>" type="hidden" name="items[<?php echo $i?>][areaid]" value="<?php echo $row['areaid']?>"/>
                            </td>          
                            <td>
                              <select id="evaluation<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Evaluation" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][evaluation]" required>
                                <?php foreach ($evaluations as $evaluationd): ?>
                                  <option value="<?php echo $evaluationd['id']?>" <?php echo ($row['evaluation'] == $evaluationd['id'])? 'selected="selected"':''?>><?php echo $evaluationd['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>
                            <td>
                              <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                            </td> 
                          </tr>
                        <?php $i++;}?>  
                      <?php else:?>  
                        <?php $i=1;foreach($evaluation_areas as $row){?>
                          <tr class="rowIdz">                                      
                            <td class="text-center">
                              <span><?php echo $row['type_name']?></span>
                              <input id="areaid<?php echo $i?>" type="hidden" name="items[<?php echo $i?>][areaid]" value="<?php echo $row['id']?>"/>
                            </td>          
                            <td>
                              <select id="evaluation<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Evaluation" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][evaluation]" required>
                                <?php foreach ($evaluations as $evaluationd): ?>
                                  <option value="<?php echo $evaluationd['id']?>"><?php echo $evaluationd['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>
                            <td>
                              <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"></textarea>
                            </td> 
                          </tr>
                        <?php $i++;}?>  
                      <?php endif;?>  
                    </tbody>
                  </table>
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">What is the employee s best performance?</label>
                <textarea name="best_performance" class="form-control" rows="3"><?php echo (isset($evaluation))? $evaluation['best_performance']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">What Are the employee s areas of improvement?</label>
                <textarea name="improvement" class="form-control" rows="3"><?php echo (isset($evaluation))? $evaluation['improvement']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Training Required</label>
                <textarea name="training" class="form-control" rows="3"><?php echo (isset($evaluation))? $evaluation['training']: '';?></textarea>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Career Recommendations</label>
                <select  id="recommendation" name="recommendation" class="selectpicker recommendation-select form-control"data-container="body" data-live-search="true" title="Select Recommendation" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($recommendations as $recommendation){?>
                    <option value="<?php echo $recommendation['id']?>" <?php echo (isset($evaluation) && ($evaluation['recommendation'] == $recommendation['id']))? 'selected="selected"' : set_select('recommendation',$recommendation['id'] ); ?>><?php echo $recommendation['type_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="evaluationAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="25%">Name</th>
                        <th width="25%">Present Position</th>
                        <th width="25%">Qualified For</th>
                        <th width="24%">When Qualified</th>
                      </tr>
                    </thead>
                    <tbody id="evaluation-data">
                      <?php if(isset($evaluation)){$i=1;$ids = '';foreach($evaluation_itemz as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="itemz[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="name<?php echo $i?>" type="text" name="itemz[<?php echo $i?>][name]" class="form-control" value="<?php echo $row['name']?>"/>
                          </td>          
                          <td>
                            <input id="position<?php echo $i?>" type="text" name="itemz[<?php echo $i?>][position]" class="form-control" value="<?php echo $row['position']?>"/>
                          </td>
                          <td>
                            <input id="qualified<?php echo $i?>" type="text" name="itemz[<?php echo $i?>][qualified]" class="form-control" value="<?php echo $row['qualified']?>"/>
                          </td> 
                          <td>
                            <input id="month<?php echo $i?>" type="text" class="form-control" data-toggle="datepicker1" name="itemz[<?php echo $i?>][month]" placeholder="yyyy-mm" value="<?php echo $row['month']?>">
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($evaluation))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($evaluation))? $evaluation['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/evaluation',$uploads,$this->data['module']['id'],$gen_id,'evaluation','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/evaluation/evaluation.js.php');?>