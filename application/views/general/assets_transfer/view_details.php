<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Reason</strong>
              </td>
              <td colspan="3">
                <?php echo $assets_transfer['reason']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%" ><strong style="font-size: 11pt;">#</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">name</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Qty</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="24%"><strong style="font-size: 11pt;">Location</strong></th>
              <th width="40%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($assets_transfer_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quantity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['location']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $assets_transfer['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>