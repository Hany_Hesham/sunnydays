<?php initiate_breadcrumb(
  'Assets Transfer Request #'.$assets_transfer['id'].'',
  $assets_transfer['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Assets Transfer Request Edit','url'=>'general/assets_transfer/edit/'.$assets_transfer['id']),
    array('name'=>'Assets Transfer Request Copy','url'=>'general/assets_transfer/copy/'.$assets_transfer['id'].'/1')
  ),//options
  array(array('name'=>'Assets Transfer Request','url'=>'general/assets_transfer'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Assets Transfer Request #<?php echo $assets_transfer['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'assets_transfer'));?> 
              </h4>
              <p class="font-bold">
                <strong>Type: </strong> <?php echo  $assets_transfer['type_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $assets_transfer['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $assets_transfer['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $assets_transfer['timestamp']; ?>,
              </p>
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <p class="font-bold">
                <strong>From Hotel: </strong> <?php echo $assets_transfer['from_hotel_name']?>, 
                <br/><strong>To Hotel: </strong> <?php echo  $assets_transfer['to_hotel_name']; ?>,
                <br/><strong>From Department: </strong> <?php echo  $assets_transfer['from_dep_name']; ?>,
                <br/><strong>To Department: </strong> <?php echo  $assets_transfer['to_dep_name']; ?>,
                <br/><strong>Date Of Transfer: </strong> <?php echo  $assets_transfer['transfer_date']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($assets_transfer['reback']) && $assets_transfer['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($assets_transfer['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br><div id="signerzItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('general/assets_transfer','viewrates','<?php echo $assets_transfer['id']?>','mainPageBody');
  getViewAjax('general/assets_transfer','signers_items','<?php echo '1/'.$assets_transfer['id'].'/'.$assets_transfer['stage'].'/'.$assets_transfer['status']?>','signersItems');
  getViewAjax('general/assets_transfer','signers_items','<?php echo '2/'.$assets_transfer['id'].'/'.$assets_transfer['stage'].'/'.$assets_transfer['status']?>','signerzItems');
</script>
