<?php 
  $back_name = (isset($assets_transfer)? 'Assets Transfer Request #'.$assets_transfer['id'].'': 'Assets Transfer Request');
  $back_url  = (isset($assets_transfer)? 'general/assets_transfer/view/'.$assets_transfer['id'].'':'general/assets_transfer');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($assets_transfer)) {
      if (isset($copy)) {
        echo form_open(base_url('general/assets_transfer/copy/'.$assets_transfer['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/assets_transfer/edit/'.$assets_transfer['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/assets_transfer/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Hotel</label>
                <select  id="hotel_id" name="from_hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($assets_transfer) && ($assets_transfer['from_hid'] == $hotel['id']))? 'selected="selected"' : set_select('from_hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Hotel</label>
                <select  id="hotels_id" name="to_hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($assets_transfer) && ($assets_transfer['to_hid'] == $hotel['id']))? 'selected="selected"' : set_select('to_hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                <select  id="type_id" name="type_id" class="selectpicker type-select form-control"data-container="body" data-live-search="true" title="Select Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($types as $type){?>
                    <option value="<?php echo $type['id']?>" <?php echo (isset($assets_transfer) && ($assets_transfer['type_id'] == $type['id']))? 'selected="selected"' : set_select('type_id',$type['id'] ); ?>><?php echo $type['type_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
            <div class="col-sm-4">
              <label class="text-right control-label col-form-label" style="font-size:12px;">*From Department</label>
                <select  id="from_dep_code" name="from_dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($assets_transfer) && ($assets_transfer['from_dep_code'] == $department['id']))? 'selected="selected"' : set_select('from_dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Department</label>
                <select  id="to_dep_code" name="to_dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($assets_transfer) && ($assets_transfer['to_dep_code'] == $department['id']))? 'selected="selected"' : set_select('to_dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date Of Transfer</label>
                <input type="text" class="form-control" id="transfer_date" data-toggle="datepicker" name="transfer_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($assets_transfer))? $assets_transfer['transfer_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Reason</label>
                <textarea name="reason" class="form-control" rows="3"><?php echo (isset($assets_transfer))? $assets_transfer['reason']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($assets_transfer))? $assets_transfer['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="assets_transferAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="15%">Name</th>
                        <th width="10%">Qty</th>
                        <th width="10%">Unit</th>
                        <th width="24%">Location</th>
                        <th width="40%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="assets_transfer-data">
                      <?php if(isset($assets_transfer)){$i=1;$ids = '';foreach($assets_transfer_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>    
                          <td>
                            <input id="name<?php echo $i?>" type="text" name="items[<?php echo $i?>][name]" class="form-control" value="<?php echo $row['name']?>"/>
                          </td>                        
                          <td class="text-center">
                            <input id="quantity<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][quantity]" class="form-control" value="<?php echo $row['quantity']?>"/>
                          </td>          
                          <td>
                            <input id="unit<?php echo $i?>" type="text" name="items[<?php echo $i?>][unit]" class="form-control" value="<?php echo $row['unit']?>"/>
                          </td>
                          <td>
                            <input id="location<?php echo $i?>" type="text" name="items[<?php echo $i?>][location]" class="form-control" value="<?php echo $row['location']?>"/>
                          </td> 
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($assets_transfer))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/assets_transfer',$uploads,$this->data['module']['id'],$gen_id,'assets_transfer','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/assets_transfer/assets_transfer.js.php');?>