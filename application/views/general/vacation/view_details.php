<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Department Name</strong>
              </td>
              <td>
                <?php echo  ($vacation['dep_id'])? $vacation['depart_name']:$vacation['dep_name'] ; ?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Position Name</strong>
              </td>
              <td>
                <?php echo  ($vacation['pos_id'])? $vacation['pos_name']:$vacation['position_name'] ; ?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Clock No#</strong>
              </td>
              <td>
                <?php echo $vacation['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $vacation['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Hiring Date</strong>
              </td>
              <td>
                <?php echo $vacation['hire_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Address</strong>
              </td>
              <td>
                <?php echo $vacation['address']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">From Date</strong>
              </td>
              <td>
                <?php echo $vacation['from_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">To Date</strong>
              </td>
              <td>
                <?php echo $vacation['to_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Working Date</strong>
              </td>
              <td>
                <span><?php echo date('Y-m-d',strtotime($vacation['to_date'] . "+1 days"));?> </span>
              </td>
              <td>
                <strong style="font-size: 11pt;">Total Leave</strong>
              </td>
              <td>
                <?php $diff = differenceDates($vacation['from_date'], $vacation['to_date']) ?>
                <?php $diffrentes = $diff['days'] + 1 ?>
                <span><?php echo  $diffrentes ?></span> Days
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $vacation['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>