<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Conf.</strong>
              </td>
              <td>
                <?php echo $complimentary['conf_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $complimentary['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Arrival Date</strong>
              </td>
              <td>
                <?php echo $complimentary['arrival']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Departure Date</strong>
              </td>
              <td>
                <?php echo $complimentary['departure']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Company</strong>
              </td>
              <td>
                <?php echo $complimentary['company']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Room Type</strong>
              </td>
              <td>
                <?php echo $complimentary['room_type']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Method Of Payment</strong>
              </td>
              <td>
                <?php echo $complimentary['payment']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Reasons</strong>
              </td>
              <td>
                <?php echo $complimentary['reasons']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $complimentary['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>