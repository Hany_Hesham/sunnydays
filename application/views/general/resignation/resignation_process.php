<?php 
  $back_name = (isset($resignation)? 'Resignation Request #'.$resignation['id'].'': 'Resignation Request');
  $back_url  = (isset($resignation)? 'general/resignation/view/'.$resignation['id'].'':'general/resignation');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($resignation)) {
      if (isset($copy)) {
        echo form_open(base_url('general/resignation/copy/'.$resignation['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/resignation/edit/'.$resignation['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/resignation/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($resignation) && ($resignation['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Clock No#</label>
                <input type="text" class="form-control" id="clock_no" name="clock_no" placeholder="Clock No#" value="<?php echo (isset($resignation))? $resignation['clock_no']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($resignation))? $resignation['name']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($resignation) && ($resignation['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <select  id="position" name="position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($resignation) && ($resignation['position'] == $user_group['id']))? 'selected="selected"' : set_select('position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hiring Date</label>
                <input type="text" class="form-control" id="hiring_date" data-toggle="datepicker" name="hiring_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($resignation))? $resignation['hiring_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Last Date</label>
                <input type="text" class="form-control" id="last_date" data-toggle="datepicker" name="last_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($resignation))? $resignation['last_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
            <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Department Head Opinion</label>
                <textarea name="opinion" class="form-control" rows="3"><?php echo (isset($resignation))? $resignation['opinion']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($resignation))? $resignation['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/resignation',$uploads,$this->data['module']['id'],$gen_id,'resignation','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/resignation/resignation.js.php');?>