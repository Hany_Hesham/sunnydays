<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Clock No#</strong>
              </td>
              <td>
                <?php echo $resignation['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $resignation['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Hiring Date</strong>
              </td>
              <td>
                <?php echo $resignation['hiring_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Last Date</strong>
              </td>
              <td>
              <?php echo $resignation['last_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Department Head Opinion</strong>
              </td>
              <td colspan="3">
                <?php echo $resignation['opinion']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $resignation['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>