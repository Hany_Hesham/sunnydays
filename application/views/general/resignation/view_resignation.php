<?php initiate_breadcrumb(
  'Resignation Request #'.$resignation['id'].'',
  $resignation['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Resignation Request Edit','url'=>'general/resignation/edit/'.$resignation['id']),
    array('name'=>'Resignation Request Copy','url'=>'general/resignation/copy/'.$resignation['id'].'/1')
  ),//options
  array(array('name'=>'Resignation Request','url'=>'general/resignation'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$resignation['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Resignation Request #<?php echo $resignation['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'resignation'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $resignation['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo  $resignation['dep_name']; ?>,
                <br/><strong>Position Name: </strong> <?php echo  $resignation['position_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $resignation['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $resignation['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $resignation['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($resignation['reback']) && $resignation['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($resignation['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left"><?php echo  $resignation['name']; ?></h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered">Signature</h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered">Fingerprint</h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('general/resignation','viewrates','<?php echo $resignation['id']?>','mainPageBody');
  getViewAjax('general/resignation','signers_items','<?php echo ''.$resignation['id']?>','signersItems');
</script>
