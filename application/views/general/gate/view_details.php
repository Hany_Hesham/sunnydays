<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Out Date</strong>
              </td>
              <td>
                <?php echo $gate['out_date']?>
              </td>
              <td>
              <strong style="font-size: 11pt;">Address</strong>
              </td>
              <td>
              <?php echo $gate['address']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%" ><strong style="font-size: 11pt;">#</strong></th>
              <th width="12%"><strong style="font-size: 11pt;">Qty</strong></th>
              <th width="13%"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Description</strong></th>
              <th width="45%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($gate_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quantity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['description']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan="3">
                <span><?php echo $gate['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>