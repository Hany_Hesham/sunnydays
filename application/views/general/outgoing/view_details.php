<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Out Date</strong>
              </td>
              <td>
                <?php echo $outgoing['out_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Estimated Return Date</strong>
              </td>
              <td>
                <?php echo $outgoing['return_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Address</strong>
              </td>
              <td>
                <?php echo $outgoing['address']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Reasons</strong>
              </td>
              <td>
                <?php echo get_meta_datas($outgoing['resson_id'])?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Reason</strong>
              </td>
              <td colspan="3">
                <?php echo $outgoing['reason']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <span class="hidden" id="storedIds"></span>  
        <?php echo form_open(base_url('general/del_outgoing/add/'.$outgoing['id']), 'class="form-horizontal,",method="get"');  ?> 
          <span>
            <?php if($this->data['del_permission'] && $this->data['del_permission']['creat'] == 1 && $outgoing['status']==2) {?>
              <input id="itemsIds" type="hidden" name="mids" value="" />
              <button id="editButton" onclick="getCheck_v()" type="submit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Recieving Items" class="btn btn-light btn-lg btn-circle hidden"><i class="far fa-edit"></i></button>
            <?php }?>   
          </span>
        <?php echo form_close(); ?> 
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <?php if($this->data['del_permission'] && $this->data['del_permission']['creat'] == 1 && $outgoing['status']==2) {?>
                <th width="5%" class="centered no-print">
                  <label class="customcheckbox m-b-40" style="margin-bottom:20px;">
                    <input type="checkbox" id="mainCheckbox" class="listCheckbox">
                    <span class="checkmark"></span>
                  </label>
                </th>
              <?php }?>
              <th width="5%" ><strong style="font-size: 11pt;">#</strong></th>
              <th width="12%"><strong style="font-size: 11pt;">Qty</strong></th>
              <th width="13%"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Description</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Recieved</strong></th>
              <th width="45%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($outgoing_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <?php if($this->data['del_permission'] && $this->data['del_permission']['creat'] == 1 && $outgoing['status']==2) {?>
                  <td class="no-print">
                    <label class="customcheckbox">
                      <?php if (((float)$item['quantity']-(float)$item['recieved']) > 0 && $item['delivered']==0){?>
                        <input id="check<?php echo $item['id']?>" type="checkbox" class="listCheckbox" value="<?php echo $item['id']?>">
                        <span class="checkmark"></span>
                      <?php }elseif(((float)$item['quantity']-(float)$item['recieved'])== 0 || $item['delivered']==1){?>
                        <input type="checkbox" class="listCheckbox" checked disabled>
                        <span class="checkmark" style="background-color:#cf0505;"></span>
                      <?php }?>  
                    </label>
                  </td>
                <?php }?>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quantity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['description']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['recieved']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $outgoing['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>