<script type="text/javascript">
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  $(document).ready(function(){
    $('input:checkbox.listCheckbox').click(function(){
      var check = document.getElementById('mainCheckbox').checked;
      if(check == true){
        $('#editButton').show();
      } else{ 
        var boxes = $("input:checkbox").length-1;
        var all = $("input:checkbox:checked").length;
        if (all > 0 && all != boxes ){
          $('#editButton').show();
        }else if(all == 0 || (all == boxes )){
          $('#editButton').hide();
        }
      }
    });
  }); 

  function outgoingAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#outgoing-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="quantity'+rowId+'" type="number" step="0.01" name="items['+rowId+'][quantity]" class="form-control" value=""/></td>     <td><input id="unit'+rowId+'" type="text" name="items['+rowId+'][unit]" class="form-control" value=""/></td>     <td><input id="description'+rowId+'" type="text" name="items['+rowId+'][description]" class="form-control" value=""/></td>     <td><textarea id="remarks'+rowId+'" name="items['+rowId+'][remarks]" class="form-control" rows="3"></textarea></td>     </tr>');
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'general/outgoing/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

  function getCheck_v(){  
    $("#storedIds").empty();
    $('input:checkbox.listCheckbox').each(function () {
      var sThisVal = (this.checked ? $(this).val() : "");
      if(sThisVal !=''){
        $('#storedIds').append(''+sThisVal+',');
      }
    });
    $('#itemsIds').val(processIds());
  }

  function processIds(){
    var ids       = $("#storedIds").text();
    var nids      = ids.split(",");
    var extracted = [];
    for (var i = 0; i < nids.length; i++) {
      if (nids[i] >= 1) {
        extracted[i] = nids[i];
      }
    }
    return extracted;
  } 

</script>