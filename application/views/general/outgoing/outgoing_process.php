<?php 
  $back_name = (isset($outgoing)? 'Outgoing Record #'.$outgoing['id'].'': 'Outgoing Record');
  $back_url  = (isset($outgoing)? 'general/outgoing/view/'.$outgoing['id'].'':'general/outgoing');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($outgoing)) {
      if (isset($copy)) {
        echo form_open(base_url('general/outgoing/copy/'.$outgoing['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/outgoing/edit/'.$outgoing['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/outgoing/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($outgoing) && ($outgoing['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($outgoing) && ($outgoing['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Out Date</label>
                <input type="text" class="form-control" id="out_date" data-toggle="datepicker" name="out_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($outgoing))? $outgoing['out_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Estimated Return Date</label>
                <input type="text" class="form-control" id="return_date" data-toggle="datepicker" name="return_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($outgoing))? $outgoing['return_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo (isset($outgoing))? $outgoing['address']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Reasons</label>
                <select  id="resson_id" name="resson_id[]" class="selectpicker reason-select form-control"data-container="body" data-live-search="true" title="Select Reasons" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple required>
                  <?php foreach($out_reasons as $out_reason){?>
                    <option value="<?php echo $out_reason['id']?>" data-subtext="<?php echo $out_reason['type_data']?>" <?php echo (isset($outgoing) && (in_array($out_reason['id'], json_decode($outgoing['resson_id']))))? 'selected="selected"' : set_select('resson_id[]',$out_reason['id'] ); ?>><?php echo $out_reason['type_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Reason</label>
                <textarea name="reason" class="form-control" rows="3"><?php echo (isset($outgoing))? $outgoing['reason']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="5%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="outgoingAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="12%">Qty</th>
                        <th width="13%">Unit</th>
                        <th width="25%">Description</th>
                        <th width="45%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="outgoing-data">
                      <?php if(isset($outgoing)){$i=1;$ids = '';foreach($outgoing_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="quantity<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][quantity]" class="form-control" value="<?php echo $row['quantity']?>"/>
                          </td>          
                          <td>
                            <input id="unit<?php echo $i?>" type="text" name="items[<?php echo $i?>][unit]" class="form-control" value="<?php echo $row['unit']?>"/>
                          </td>
                          <td>
                            <input id="description<?php echo $i?>" type="text" name="items[<?php echo $i?>][description]" class="form-control" value="<?php echo $row['description']?>"/>
                          </td> 
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($outgoing))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($outgoing))? $outgoing['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/outgoing',$uploads,$this->data['module']['id'],$gen_id,'outgoing','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/outgoing/outgoing.js.php');?>