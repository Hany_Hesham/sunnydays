<?php 
  $back_name = (isset($del_outgoing)? 'Outgoing Delivery Record #'.$del_outgoing['id'].'': 'Outgoing Delivery Record');
  $back_url  = (isset($del_outgoing)? 'general/del_outgoing/view/'.$del_outgoing['id'].'':'general/del_outgoing');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($del_outgoing)) {
      echo form_open(base_url('general/del_outgoing/edit/'.$del_outgoing['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
    }else{
      echo form_open(base_url('general/del_outgoing/add/'.$outgoing['id']), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="12%">Qty</th>
                        <th width="13%">Unit</th>
                        <th width="25%">Description</th>
                        <th width="5%">Recieved</th>
                        <th width="5%">Report</th>
                        <th width="45%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="del_outgoing-data">
                      <?php if(isset($del_outgoing_items)){$i=1;$ids = '';foreach($del_outgoing_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">                                     
                          <td class="text-center">
                            <?php if(isset($row['id'])){?>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id'];?>"/>
                            <?php }?>
                            <input type="hidden" name="items[<?php echo $i?>][item_id]" value="<?php echo $row['item_id'];?>"/> 
                            <input id="quantity<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][quantity]" class="form-control" value="<?php echo $row['quantity']?>" disabled/>
                          </td>          
                          <td>
                            <input id="unit<?php echo $i?>" type="text" name="items[<?php echo $i?>][unit]" class="form-control" value="<?php echo $row['unit']?>" disabled/>
                          </td>
                          <td>
                            <input id="description<?php echo $i?>" type="text" name="items[<?php echo $i?>][description]" class="form-control" value="<?php echo $row['description']?>" disabled/>
                          </td> 
                          <td>
                            <?php if(isset($outgoing)){?>
                              <input type="number" step="0.01" name="items[<?php echo $i?>][recieved]" class="form-control" max="<?php echo ((float)$row['quantity']-(float)$row['recieved'])?>" required/>
                            <?php }else{?>
                              <input type="number" step="0.01" name="items[<?php echo $i?>][recieved]" class="form-control" value="<?php echo $row['recieved']?>" max="<?php echo $row['recieved']?>" required/>
                            <?php }?> 
                          </td>
                          <td>
                            <input type="text" name="items[<?php echo $i?>][report]" class="form-control" value="<?php if(isset($row['report'])){ echo $row['report'];}?>" required/>
                          </td>
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($del_outgoing))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($del_outgoing))? $del_outgoing['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/del_outgoing',$uploads,$this->data['module']['id'],$gen_id,'del_outgoing','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/del_outgoing/del_outgoing.js.php');?>