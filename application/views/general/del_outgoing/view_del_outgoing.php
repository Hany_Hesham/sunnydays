<?php initiate_breadcrumb(
  'Outgoing Delivery Record #'.$del_outgoing['id'].'',
  $del_outgoing['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Outgoing Delivery Record Edit','url'=>'general/del_outgoing/edit/'.$del_outgoing['id']),
    array('name'=>'Outgoing Delivery Record Copy','url'=>'general/del_outgoing/copy/'.$del_outgoing['id'].'/1')
  ),//options
  array(array('name'=>'Outgoing Delivery Record','url'=>'general/del_outgoing'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$del_outgoing['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Outgoing Delivery Record #<?php echo $del_outgoing['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'del_outgoing'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $del_outgoing['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo $del_outgoing['dep_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $del_outgoing['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $del_outgoing['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $del_outgoing['timestamp']; ?>,
                <br><a class="text-danger" target="blank" href="<?php echo base_url('general/outgoing/view/'.$del_outgoing['outgoing_id']);?>">
                  <strong>Outgoing No: </strong> <?php echo $del_outgoing['outgoing_id']?>,
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($del_outgoing['reback']) && $del_outgoing['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($del_outgoing['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">Out With</h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>      
</div>
<?php $this->load->view('general/del_outgoing/del_outgoing.js.php');?>
<script type="text/javascript">
  getViewAjax('general/del_outgoing','viewrates','<?php echo $del_outgoing['id']?>','mainPageBody');
  getViewAjax('general/del_outgoing','signers_items','<?php echo ''.$del_outgoing['id']?>','signersItems');
</script>
