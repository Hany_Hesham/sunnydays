<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Clock No#</strong>
              </td>
              <td>
                <?php echo $salary_advance['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $salary_advance['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Position Name</strong>
              </td>
              <td>
                <?php echo  ($salary_advance['pos_id'])? $salary_advance['pos_name']:$salary_advance['position_name'] ; ?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Vacation Balance</strong>
              </td>
              <td>
                <?php echo $salary_advance['balance']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Basic Salary</strong>
              </td>
              <td>
                <?php echo number_format($salary_advance['salary'],2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Advance</strong>
              </td>
              <td>
                <?php echo number_format($salary_advance['advance'],2)?> EGP
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $salary_advance['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>