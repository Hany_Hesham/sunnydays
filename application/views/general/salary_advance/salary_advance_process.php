<?php 
  $back_name = (isset($salary_advance)? 'Salary Advance Request #'.$salary_advance['id'].'': 'Salary Advance Request');
  $back_url  = (isset($salary_advance)? 'general/salary_advance/view/'.$salary_advance['id'].'':'general/salary_advance');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($salary_advance)) {
      if (isset($copy)) {
        echo form_open(base_url('general/salary_advance/copy/'.$salary_advance['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('general/salary_advance/edit/'.$salary_advance['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('general/salary_advance/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($salary_advance) && ($salary_advance['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($salary_advance) && ($salary_advance['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Clock No#</label>
                <input type="text" class="form-control" id="clock_no" name="clock_no" placeholder="Clock No#" value="<?php echo (isset($salary_advance))? $salary_advance['clock_no']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($salary_advance))? $salary_advance['name']: '';?>" required readonly>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <input type="text" class="form-control" id="department_name" placeholder="Department" value="<?php echo (isset($salary_advance))? $salary_advance['dep_name']: '';?>" required readonly>
                <input type="hidden" id="dep_id" name="dep_id" value="<?php echo (isset($salary_advance))? $salary_advance['dep_id']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <input type="text" class="form-control" id="position_name" placeholder="Position" value="<?php echo (isset($salary_advance))? $salary_advance['position_name']: '';?>" required readonly>
                <input type="hidden" id="pos_id" name="pos_id" value="<?php echo (isset($salary_advance))? $salary_advance['pos_id']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Vacation Balance</label>
                <input type="number" step="0.01" class="form-control" id="balance" name="balance" placeholder="Vacation Balance" value="<?php echo (isset($salary_advance))? $salary_advance['balance']: '';?>" required readonly>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Basic Salary</label>
                <input type="number" step="0.01" class="form-control" id="salary" name="salary" placeholder="Basic Salary" value="<?php echo (isset($salary_advance))? $salary_advance['salary']: '';?>" required readonly> EGP
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Advance</label>
                <input type="number" step="0.01" class="form-control" id="advance" name="advance" placeholder="Advance" value="<?php echo (isset($salary_advance))? $salary_advance['advance']: '';?>" required> EGP
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($salary_advance))? $salary_advance['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('general/salary_advance',$uploads,$this->data['module']['id'],$gen_id,'salary_advance','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('general/salary_advance/salary_advance.js.php');?>