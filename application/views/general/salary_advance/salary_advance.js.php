<script type="text/javascript">
  
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('#clock_no').change(function () {
    employeeSearch();
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  <?php if (!isset($salary_advance)):?>
    $('#clock_no, #hid').on('change',function () {
      $('#name').val('');
      $('#department_name').val('');
      $('#dep_id').val('');
      $('#position_name').val('');
      $('#pos_id').val('');
      $('#balance').val('');
      $('#salary').val('');
      employeeSearch();
    });
  <?php endif; ?>

  function employeeSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    let hid   = $('#hid').val();
    let empNo = $('#clock_no').val();
    if (hid != '' && empNo !='') {
      var url =getUrl()+'general/salary_advance/search_APIEmployee/'+hid+'/'+empNo;      
      $.post(url).done(function(response) {
        response = JSON.parse(response);
        if(response, $.each(response, function() {
          $('#name').val(response.emp_ename);
          $('#department_name').val(response.dep_name);
          $('#dep_id').val(response.emp_did);
          $('#position_name').val(response.position_name);
          $('#pos_id').val(response.emp_pid);
          let date = new Date();
          let months = (date.getMonth()) + 1;
          let balance = ((Number(response.emp_balncemonth) * Number(months)) + Number(response.emp_balanceopen) - Number(response.emp_balancecon));
          $('#balance').val(balance);
          $('#salary').val(response.emp_grosssalary);
          $('#name').prop('readonly', true);
          $('#department_name').prop('readonly', true);
          $('#position_name').prop('readonly', true);
          $('#balance').prop('readonly', true);
          $('#salary').prop('readonly', true);
        }));
      });
    }
  }

</script>