<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Modules</h4>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Modules</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-6">
      <div class="card">
        <div class="card-body">
          <?php if($this->data['permission']['creat'] == 1){?>
            <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary">
              <i class="fa fa-cloud"></i>&nbsp; <strong>Add Modules</strong>
            </button> <br> 
            <div class="dropdown-divider"></div><br>
          <?php }?>
          <h4 class="page-title">Modules</h4>
          <table id="modules-list-table" class="table table-hover"  style="width:100% !important;">
            <thead class="thead-light">
              <tr>
                <th scope="col" width="10%"><strong>#</strong></th>
                <th scope="col" width="40%"><strong>Module Name</strong></th>
                <th scope="col" width="40%"><strong>Group</strong></th>
                <th scope="col" width="10%"><strong>Deleted</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-lg-6" id="signatureTable">
    </div>
  </div>
</div>
<?php $this->load->view('admin/modules/modules.js.php');?>