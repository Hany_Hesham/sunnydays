<?php
    foreach ($this->data['menuModules'] as $key => $module) {
        $this->data['menuModules'][$key]['moduled'] = get_menuModules($module['menu_group']);
        $this->data['menuModules'][$key]['showd'] = 0;
        foreach ($this->data['menuModules'][$key]['moduled'] as $keys => $modulez) {
            $this->data['menuModules'][$key]['moduled'][$keys]['permission'] = user_access($modulez['id']);
            if (isset($this->data['menuModules'][$key]['moduled'][$keys]['permission']['view']) && $this->data['menuModules'][$key]['moduled'][$keys]['permission']['view'] == 1) {
                $this->data['menuModules'][$key]['showd']++;
            }
        }
    }
?>
<aside class="left-sidebar no-print" data-sidebarbg="skin">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> 
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/dashboard');?>" aria-expanded="false">
                               <i class="mdi mdi-view-dashboard text-dark"></i><strong class="hide-menu text-dark">Dashboard</strong>
                           </a>
                        </li>
                        <?php foreach ($this->data['menuModules'] as $moduled):?>
                            <?php if($moduled['showd'] > 0){?>  
                                <li class="sidebar-item"> 
                                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                                        <i class="<?php echo $moduled['menu_group_icon'] ?> text-dark"></i><strong class="hide-menu text-dark"><?php echo $moduled['menu_group'] ?> </strong>
                                    </a>
                                    <ul aria-expanded="false" class="collapse  first-level">
                                            <?php foreach ($moduled['moduled'] as $module):?>
                                                <?php if( isset($module['permission']['view']) && $module['permission']['view'] == 1){?>  
                                                    <li class="sidebar-item">
                                                        <a href="<?php echo base_url($module['link']);?>" class="sidebar-link text-dark">
                                                            <i class="<?php echo $module['menu_icon'];?> text-dark"></i>
                                                            <strong class="hide-menu text-dark"><?php echo $module['name'] ?></strong>
                                                        </a>
                                                    </li>
                                                <?php }?>  
                                            <?php endforeach ?>
                                    </ul>
                                </li>
                            <?php }?>  
                        <?php endforeach ?>
                        <?php if ($this->global_data['sessioned_user']['is_admin']==1):?>
                            <li class="sidebar-item"> 
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('cron/backup_me');?>" aria-expanded="false">
                                    <i class="fas fa-database text-dark"></i><strong class="hide-menu text-dark">DB Backup</strong>
                                </a>
                            </li>  
                        <?php endif; ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>