<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<?php   
        if ($this->input->is_ajax_request()) {
            $this->load->view('admin/includes/popup_layout');
            return true;
        }
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon --> 
    <link rel="icon" type="image/png" sizes="20x20" href="<?php echo base_url('assets/images/favicon2.png');?>">
    <title>WorkFlow</title>
    <!-- Custom CSS -->

  <script src="<?php echo base_url('assets/editor/jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/custom/main_scripts.js')?>"></script>
 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/extra-libs/multicheck/multicheck.css')?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/select2/dist/css/select2.min.css')?>">
    <link href="<?php echo base_url('assets/libs/flot/css/float-chart.css');?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets/css/style.min.css');?>" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Buttons/css/buttons.dataTables.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Buttons/css/buttons.bootstrap4.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css')?>"> 
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/extensions/Responsive/css/responsive.bootstrap.min.css')?>">    
    <link rel="stylesheet" href="<?php echo base_url('assets/libs/datatables/media/css/dataTables.bootstrap4.min.css')?>">

    <link rel="stylesheet" href="<?= base_url() ?>assets/css/fileinput.min.css">
    
    <script src="<?= base_url() ?>assets/js/fileinput.min.js"></script>

    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom.css')?>">
 
     <link href="<?php echo base_url('assets/libs/magnific-popup/dist/magnific-popup.css')?>" rel="stylesheet">


     <!---------------------------------------------Selec picker -------------------------------->
    <link  href="<?php echo base_url('assets/extra-libs/selectpicker/dist/css/bootstrap-select.min.css')?>" rel="stylesheet">
     <!-- <link  href="<?php echo base_url('assets/extra-libs/selectpicker/dist/css/ajax-bootstrap-select.css')?>" rel="stylesheet"> -->
     
     <!---------------------------------------------datepiker -------------------------------->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')?>">
    <script src="<?php echo base_url('assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datetimepicker.css')?>">
    <link rel="manifest" href="<?php echo base_url('manifest.webmanifest')?>">
    <link href="<?php echo base_url('assets/editor/css/summernote.css')?>" rel="stylesheet">
      <link href="<?php echo base_url('assets/editor/css/summernote-bs4.css')?>" rel="stylesheet">

</head>
<body>

   <!--  <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div> -->
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-sidebartype="mini-sidebar" class="mini-sidebar">
    <!--  <div id="main-wrapper">   -->  
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar no-print" data-navbarbg="skin5">
            <input type="hidden" id="baseurl" name="baseurl" value="<?php echo base_url(); ?>" />
           <?php $this->load->view('admin/includes/navbar');?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view('admin/includes/sidebar');?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
         

        <div class="rotate btn-cat-con">
            <?php 
                  $CI =& get_instance();
                  $all_modules_count=0; 
                  $forms_to_sign    = array();
                  $form_modules     = form_modules();
                  foreach ($form_modules as $module){
                        $module_count = total_rows($module['tdatabase'],$module['id']);
                      $all_modules_count += $module_count;
                        $module_links = total_module_data($module['tdatabase']);
                      if ($module_links > 0) {
                          foreach ($module_links as $link){
                              $forms_to_sign []= $link;

                           }
                        }
                    }
                  if ($this->router->fetch_method() == "index") {
                      $current_url  = $this->router->fetch_directory().$this->router->fetch_class();
                   }else{
                      $current_url  = $this->router->fetch_directory().$this->router->fetch_class().'/'.$this->router->fetch_method();
                   }
                  $module_row          = get_current_module($current_url); 
                  
                  if ($module_row ) {
                      $next_form  = get_next_form_to_sign($module_row,$this->uri->segment(4));
                  }
                  $linkto_go       = '';
                  if (isset($next_form['id'])) {
                      $linkto_go = base_url().$module_row['view_link'].'/'.$next_form['id'];
                   }elseif (!isset($next_form) && $all_modules_count>0) {
                        if ($module_row && $all_modules_count>1) {
                              $view_link            = $forms_to_sign[1]['view_link'];
                              $next_module_row      = get_current_module($view_link ); 
                          }else{
                              $view_link            = $forms_to_sign[0]['view_link'];
                              $next_module_row      = get_current_module($view_link ); 
                        }
                          $next_form            = get_next_form_to_sign($next_module_row);
                        if ($next_form) {
                              $linkto_go = base_url().$view_link.'/'.$next_form['id'];
                          }else{
                              $linkto_go = base_url().'admin/dashboard';
                       }
                    }else{
                      $linkto_go = base_url().'admin/dashboard';
                    }
                  echo "<a href=". base_url('admin/dashboard') ." class='btn btn-danger open-subcategory no-print' tabindex='-1'> $all_modules_count Total</a>
                      <a href=".$linkto_go." class='btn btn-dark open-category no-print' tabindex='-1'>Next Form</a>";
         ?>
        </div>

         <div class="page-wrapper">   
            
          <?php $this->load->view($view);?>
          <?php if(isset($this->data['module']['id']) && isset($this->data['form_id'])){?>
            <a class="btn-circle btn-lg btn-cyan float-right text-white no-print" data-toggle="modal" data-target="#messanger" style="bottom:5%;display:block;position:fixed;right:5%;max-width:100px;z-index:99999;"><i class="fas fa-paper-plane"></i>
              <?php if(isset($this->data['commentsCount']) && $this->data['commentsCount'] != 0){ ?>
                <span id="commentsCount" class="badge badge-pill badge-danger" style="margin-left:20px;margin-bottom:0px;">
                  <?php echo $this->data['commentsCount']; ?>
                </span>
              <?php } ?>
            </a>
          <?php } ?>
            <?php if (isset($messaged)) :?>
              <div class="col-lg-12">
                <div class="accordion" id="accordionExample">
                  <div class="card m-b-0">
                    <div class="card-header" id="headingOne">
                      <h5 class="mb-0">
                        <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          <i class="m-r-5 fas fa-comments" aria-hidden="true"></i>
                          <span>Comments</span>
                        </a>
                      </h5>
                    </div>
                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                      <div class="card-body">
                        <div class="card">
                          <div class="card-body">
                            <h4 class="card-title m-b-0">Comments</h4>
                          </div>
                          <ul class="list-style-none">
                            <?php foreach ($messaged as $comment): ?>
                              <li class="d-flex no-block card-body">
                                <i class="fas fa-comment-dots w-30px m-t-5"></i>
                                <div>
                                  <a href="#" class="m-b-0 font-medium p-0"><?php echo $comment['username'] ?></a>
                                  <span class="text-muted"><?php echo $comment['message'] ?></span>
                                </div>
                                <div class="ml-auto">
                                  <div class="tetx-right">
                                    <h5 class="text-muted m-b-0"><?php echo date('d', strtotime($comment['timestamp'])) ?></h5>
                                    <span class="text-muted font-16"><?php echo date('M', strtotime($comment['timestamp'])) ?> <?php echo date('y', strtotime($comment['timestamp'])) ?></span>
                                  </div>
                                </div>
                              </li>
                            <?php endforeach ?>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
            <?php endif ?>
           <footer class="footer text-center no-print">
                <strong>Copyright © 2019 <a href="#">HTM3</a></strong> All rights reserved.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->

    <?php initiate_modal(
                       '','Change Password','admin/users/change_pwd',
                        ' 
                            <input id="PasswordChange" type="password" name="password" class="form-control" placeholder="New password" required><br>
                            <input id="PasswordChangeConfirmation" type="password" name="" class="form-control" placeholder="Confirm password" required>
                            <br>
                          ','passwordChangeBody'
                       )?>        
    </div>
    <?php if(isset($this->data['module']['id']) && isset($this->data['form_id'])){
        initiate_modalmessage(0,'messageComment', $this->data['form_id'], $this->data['module']['id']);
    }?>
    <script src="<?php echo base_url('assets/libs/flot/excanvas.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.pie.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.time.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.stack.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/flot/jquery.flot.crosshair.js')?>"></script>
    <!-- <script src="<?php echo base_url('assets/js/pages/chart/chart-page-init.js')?>"></script> -->
    <script src="<?php echo base_url('assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js')?>"></script> 

     <script src="<?php echo base_url('assets/libs/datatables/media/js/jquery.js')?>"></script> 
<!----------------------------------------- data tables js files ------------------------------------------------------------------->
     <script src="<?php echo base_url('assets/libs/datatables/media/js/jquery.dataTables.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/dataTables.buttons.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.bootstrap4.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.print.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.flash.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.colVis.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/buttons.html5.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/pdfmake.min.js')?>"></script> 
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/jszip.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Buttons/js/vfs_fonts.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/media/js/dataTables.bootstrap4.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/ColReorder/js/colReorder.dataTables.min.js')?>"></script>
     <script src="<?php echo base_url('assets/libs/datatables/extensions/Responsive/js/dataTables.responsive.min.js')?>"></script>
<!-- ---------------------------------------------------------------------------------------------------------------------------------->
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url('assets/libs/popper.js/dist/umd/popper.min.js');?>"></script>
    <script src="<?php echo base_url('assets/libs/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/sparkline/sparkline.js')?>"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url('assets/js/waves.js')?>"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url('assets/js/sidebarmenu.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-datetimepicker.js')?>"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url('assets/js/custom.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/toastr/build/toastr.min.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/datatable-checkbox-init.js')?>"></script>
    <script src="<?php echo base_url('assets/extra-libs/multicheck/jquery.multicheck.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.full.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/select2/dist/js/select2.min.js')?>"></script>

    <script src="<?php echo base_url('assets/libs/magnific-popup/dist/jquery.magnific-popup.min.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/magnific-popup/meg.init.js')?>"></script>

     <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/bootstrap-select.min.js')?>"src="/path/to/datepicker.js"></script>
    <!-- <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/ajax-bootstrap-select.js')?>"></script> -->
    <script src="<?php echo base_url('assets/extra-libs/selectpicker/dist/js/i18n/defaults-en_US.min.js')?>"src="/path/to/datepicker.js"></script>
    <script src="<?php echo base_url('assets/js/custom/printThis.js')?>"></script>
    <script src="<?php echo base_url('assets/js/custom/notifications.js')?>"></script>
    <script src="<?php echo base_url('assets/libs/tableReorder/sortable.js')?>"></script>
    <script type="text/javascript">
        $(document).ready(function(){
           $('.selectpicker').selectpicker();
           $('.selectpicker').selectpicker('setStyle', 'btn btn-outline-dark'); 
        });        
        let adminAcc = <?php echo $sessioned_user['is_admin']?>;
        if (adminAcc != 1 ) {
            preventBrowserBack();
            //preventBrowserInspect();
          }

    </script>
    <script type="text/javascript">
      $(".select2").select2();
      getNotifications();
      getMessaages();
    </script>
      <script src="<?php echo base_url('assets/editor/js/summernote.js')?>"></script>
      <script src="<?php echo base_url('assets/editor/js/summernote-bs4.js')?>"></script>
</body>
</html>