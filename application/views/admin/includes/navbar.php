<nav class="no-print navbar top-navbar navbar-expand-md navbar-dark">
    <div class="navbar-header" data-logobg="skin" style="background-color:#FFFFFF !important;">
        <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close" style="color:#000000;"></i></a>
        <a class="navbar-brand" href="<?php echo base_url('admin/dashboard');?>">
            <b class="logo-icon p-l-10" style="font-size:30px;color:#c41313;">
                <img  style="width:50px;height:40px;" src="<?php echo base_url('assets/images/favicon2.png');?>" alt="homepage" class="light-logo" />
            </b>
            <span class="logo-text">
                 <strong style="color:#000000;font-size:28px;">Work</strong>
                 <strong style="color:#f58442;font-size:28px;">Flow</strong>
               <!--  <img style="width:50px;height:150px;" src="<?php echo base_url('assets/images/logo-mc-new.png')?>" alt="homepage" class="light-logo" /> -->
            </span>
        </a>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Toggle which is visible on mobile only -->
        <!-- ============================================================== -->
        <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more text-dark"></i></a>
    </div>
    <!-- ============================================================== -->
    <!-- End Logo -->
    <!-- ============================================================== -->
    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5" style="background-color:#524e4c !important;">
        <!-- ============================================================== -->
        <!-- toggle and nav items -->
        <!-- ============================================================== -->
        <ul class="navbar-nav float-left mr-auto">
            <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24" style="color:#FFFFFF;"></i></a></li>
            <!-- ============================================================== -->
            <!-- create new -->
            <!-- ============================================================== -->
            <li class="nav-item dropdown">
                <!-- <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="d-none d-md-block">Create New <i class="fa fa-angle-down"></i></span>
                    <span class="d-block d-md-none"><i class="fa fa-plus"></i></span>   
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#">Action</a>
                    <a class="dropdown-item" href="#">Another action</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Something else here</a>
                </div> -->
            </li>
            <!-- ============================================================== -->
            <!-- Search -->
            <!-- ============================================================== -->
           <!--  <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search" style="color:#FFFFFF;"></i></a>
                <form class="app-search position-absolute">
                    <input type="text" id="main_search" class="form-control main-search" placeholder="Search &amp; enter"><div id="result" class="form-control"></div> <a class="srh-btn"><i class="ti-close" style="color:#29a1d7;"></i></a>
                </form>
            </li> -->
        </ul>
        <!-- ============================================================== -->
        <!-- Right side toggle and nav items -->
        <!-- ============================================================== -->
        <ul class="navbar-nav float-right">
            <!-- ============================================================== -->
            <!-- Comment -->
            <!-- ============================================================== -->
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="m-r-10 mdi mdi-database font-24" style="color:#FFFFFF;"></i><span id="savedFormsCount" class="badge  badge-warning" style="margin-left:0px;margin-bottom:20px;"></span>
                </a>
                <div id="savedForms-div" class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown notifications-div" aria-labelledby="2">
                    <ul class="list-style-none">
                        <li>
                            <div class="" id="savedForms">
                                <!-- Notification -->
                                <div class="dropdown-divider"></div>
                                <h5 class="m-b-0 centered">Saved Froms</h5>
                                <div class="dropdown-divider"></div><h5 class="m-b-0 centered">No Saved Forms Found</h5><br>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="mdi mdi-bell font-24" style="color:#FFFFFF;"></i><span id="notifiCount" class="badge  badge-danger" style="margin-left:0px;margin-bottom:20px;"></span>
                </a>
                <div id="notifications-div" class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown notifications-div" aria-labelledby="2">
                    <ul class="list-style-none">
                        <li>
                            <div class="" id="notifications">
                                <!-- Notification -->
                                <div class="dropdown-divider"></div>
                                <h5 class="m-b-0 centered">Notifications</h5>
                                <div class="dropdown-divider"></div><h5 class="m-b-0 centered">No notifications found</h5><br>
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <!-- ============================================================== -->
            <!-- End Comment -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Messages -->
            <!-- ============================================================== -->
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="mdi mdi-comment-processing font-24" style="color:#FFFFFF;"></i><span id="messgCount" class="badge  badge-success" style="margin-left:0px;margin-bottom:20px;"></span>
                </a>
                <div id="messages-div" class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown messages-div" aria-labelledby="2">
                    <ul id="messadgedata" class="list-style-none">
                        <li>
                            <div class="" id="messages">
                                <div class="dropdown-divider"></div>
                                <h5 class="m-b-0 centered">Messages</h5>
                                <div class="dropdown-divider"></div><h5 class="m-b-0 centered">No messages found</h5><br>
                            </div>
                        </li>
                    </ul>
                </div>
            </li> -->
            <!-- ============================================================== -->
            <!-- End Messages -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><strong class="btn-rounded" style="font-size:18px;background-color:#4a4944;color:#FFF">
                <?php echo strtoupper($this->global_data['sessioned_user']['fullname'][0])?></strong></a>
                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                    <a class="dropdown-item" href="javascript:void(0)">
                        <i class="ti-user m-r-5 m-l-5"></i> <?php echo $this->global_data['sessioned_user']['fullname']?>
                    </a>
                    <a class="dropdown-item" id="PasswordChangeModalInitiate" href="javascript:void(0)"><i class="fas fa-exchange-alt m-r-5 m-l-5 text-warning"></i> Change Password</a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/auth/logout');?>"><i class="fa fa-power-off m-r-5 m-l-5 text-danger"></i> Logout</a>
                </div>
            </li>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
        </ul>
    </div>
</nav>
<script type="text/javascript">
    $('#messadgedata li').click(function(ev) {
        if($(this).children('ul').is(':visible'))
            return true;
        if($(this).children('ul').toggle("slow").length)
            return false;
        ev.stopPropagation();
    });
    $(document).ready(function(){
        $("#PasswordChangeModalInitiate").click(function() {
            $('#passwordChangeBody').modal('show');
            $('#passwordChangeBody .modal-footer #modalSubmit-passwordChangeBody').attr('disabled',true);
        });
        $('#PasswordChangeConfirmation , #PasswordChange').keyup(function(){
            let password             = $('#PasswordChange').val();
            let passwordConfirmation = $('#PasswordChangeConfirmation').val();
            if (passwordConfirmation == password) {
                $('#passwordChangeBody .modal-footer #modalSubmit-passwordChangeBody').attr('disabled',false);
            }else{
                $('#passwordChangeBody .modal-footer #modalSubmit-passwordChangeBody').attr('disabled',true);
            }
        });
    });
</script>