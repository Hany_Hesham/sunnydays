<script type="module" src="https://unpkg.com/@progressivewebcomponents/pwa-install/pwa-install.js"></script>
<script type="module" src="https://unpkg.com/@polymer/paper-toast/paper-toast.js?module"></script>
<script src="<?php echo base_url('assets/js/custom/app-sw.js')?>"></script>

<div class="container-fluid">
  <?php initiate_alert();?> 
  <span class="hidden" id="selected-module"></span>
  <div class="row" style="zoom:85%">
    <div class="col-md-6 col-lg-3 col-xlg-3">
          <a  href="javascript: void(0);">
            <div class="card card-hover">
              <div class="box">
                <div class="row">
                  <div class="col-5 m-t-15">
                    <i class="fas fa-calculator" style="font-size:570%;color:#000000 !important;"></i>
                  </div>
                  <div class="col-7 m-t-15 text-dark">
                    <strong class="font-weight-bold float-right">Total Forms</strong><br>
                    <strong class="font-weight-bold float-right" style="font-size:200%;color:#000000 !important;" id="all-forms-count">

                    </strong>              
                  </div>
                </div>
                 <div class="dropdown-divider" style="background-color:#000 !important;"></div>
                <div class="row">
                  <div class="col-6 m-t-15 text-dark">
                   
                  </div>
                  <div class="col-6 m-t-15 text-dark">
                   
                  </div>
                </div>  
              </div>
            </div>
          </a>
        </div>
    <?php $all_modules_count=0; foreach ($modules_counter as $module):?>
      <?php if ($module['id'] == 61 || $module['id'] == 76) {
        $module_count = fromto_total_rows($module['tdatabase'],$module['id']);
      }elseif ($module['id'] == 64 || $module['id'] == 70 || $module['id'] == 72 || $module['id'] == 73 || $module['id'] == 74) {
        $module_count = totalempty_rows($module['tdatabase'],$module['id']);
      }else{
        $module_count = total_rows($module['tdatabase'],$module['id']);
      }?>
      <?php if($module_count > 0) { ?>
        <div class="col-md-6 col-lg-3 col-xlg-3">
          <a  href="javascript: void(0);" onclick="filterForms('<?php echo $module['id']?>',' <?php echo $module_count; ?>')">
            <div class="card card-hover">
              <div class="box" style="background-color:<?php echo $module['background_color'] ?>">
                <div class="row">
                  <div class="col-5 m-t-15" style="color:<?php echo $module['text_color'] ?>;">
                    <i class="<?php echo $module['menu_icon'] ?>" style="font-size:300%;"></i>
                  </div>
                  <div class="col-7 m-t-15 text-dark">
                    <strong class="font-weight-bold float-right"><?php echo $module['name'] ?></strong><br>
                    <strong class="font-weight-bold float-right" style="text-align:right;font-size:200%;color:<?php echo $module['text_color'] ?>;">
                        <?php echo $module_count; ?>
                    </strong>              
                  </div>
                </div>
                 <div class="dropdown-divider" style="background-color:#000 !important;"></div>
                <div class="row">
                  <div class="col-6 m-t-15 text-dark">
                    <?php //if($module['permission']['creat'] == 1){?>         
                     <a class="waves-light" href="<?php echo base_url($module['link'].'/add');?>" data-sidebartype="mini-sidebar">
                      <strong class="text-dark" style="font-size:12px;">Add New &nbsp</strong>
                      <i class="mdi mdi-bell-plus" style="color:<?php echo $module['text_color'] ?>;font-size:12px;" ></i></a>
                    <?php //}?> 
                  </div>
                  <div class="col-6 m-t-15 text-dark">
                    <a class="waves-light float-right" href="<?php echo base_url($module['link']);?>" data-sidebartype="mini-sidebar">
                      <strong class="text-dark" style="font-size:12px;"><?php echo $module['name'] ?></strong>
                      <i class="mdi mdi-arrow-expand-all" style="color:<?php echo $module['text_color'] ?>;font-size:12px;"></i></a>
                  </div>
                </div>  
              </div>
            </div>
          </a>
        </div>
    <?php } ?>
    <?php 
          $all_modules_count += $module_count;
          endforeach; 
    ?>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <strong class="text-dark">Please sign this</strong>
          </div>
          <div class="card-body">
            <div class="table-responsive m-t-40" style="clear: both;margin-top:30px;">
              <table id="forms-toSign-list-table" class="table table-hover indexTable" style="width:100% !important;">
                <thead >
                  <tr>
                    <th class="tableCol" width="35%" id="id"><strong>Form Name</strong></th>
                    <th class="tableCol" width="20%" id="id"><strong>Hotel Name</strong></th>
                    <th class="tableCol" width="25%" id="module_name"><strong>Options</strong></th>
                    <th class="tableCol" width="20%" id="timestamp"><strong>Created At</strong></th>
                  </tr>
                </thead>
                <tbody>
                  <tbody>
                    <tr id="total-tosign">
                      <td colspan="3">
                         <strong class="text-cyan">Dear Sir You Have <?php echo $all_modules_count?> Form To Sign
                          <br><strong class="text-danger">Please Select From Above</strong>
                        </strong>
                      </td>
                    </tr>
                  </tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php initiate_modal('lg','Form view','','','summaryViewModal')?>
<?php $this->load->view('admin/html_parts/loader_div');?>
<script type="text/javascript">
  $('#all-forms-count').text('<?php echo $all_modules_count?>');
  
  function toggleChartWidth(chartDiv){
    $("#"+chartDiv+"").toggleClass("col-lg-12");
    $("#"+chartDiv+"").toggleClass("col-lg-6");
    chartsLoader();
  }

  function filterForms(module_id,num_row){
      document.getElementById('forms-toSign-list-table').scrollIntoView();
      $('#forms-toSign-list-table #total-tosign').empty();
      $('#forms-toSign-list-table').DataTable().destroy();
      $('#selected-module').text(num_row);
       getData(module_id);
     }

  function getData(module_id='') {
    $(document).ready(function(){
      let num_row = $('#selected-module').text();
      var datatable = $('#forms-toSign-list-table').DataTable({
                        "processing"  : true,
                        "serverSide" : true,
                        "searching": true,
                        "draw" :true,
                        "pageLength" :25,
                        "lengthMenu": [10, 25, 50,100,500],
                        "responsive": true,
                        "order": [[0, "desc" ]],
                        "columns": [
                                     {"name":"id"},
                                     {"name":"id"},
                                     {"name":"id"},
                                     {"name":"timestamp"}                 
                                    ],                                 
                        "ajax": {
                            url  : "<?php echo base_url("admin/dashboard/to_sign_ajax") ?>",
                            type : 'POST',
                            data :{module_id:module_id,num_row:num_row}
                        },
                      });

          });
       }
   
    function autoSign(module_id,form_id,sign_id){
      $('#loader').show();
      url = getUrl()+'admin/dashboard/sign/'+module_id+'/'+form_id+'/'+sign_id;
      var toSignTable = $('#forms-toSign-list-table').DataTable();
      $.ajax({
          type: 'post',
          url: url,
          success: function() {
            toSignTable.ajax.reload();
            $('#loader').hide(); 
          },
        });
       $('#summaryViewModal').modal('hide');
    }

  function get_formView(link,id,module_id,sign_id) {
     $('#summaryViewModal').modal('show');
     $('#summaryViewModal .modal-body').empty();
     $('#summaryViewModal .modal-footer').empty();
     $('#summaryViewModal .modal-footer').html('<a href="javascript: void(0);" style="width:50%;margin-left:25%;margin-right:25%;" onclick="autoSign('+module_id+','+id+','+sign_id+')" titel="Sign" class="btn btn-info btn-lg signature-button-from-summary-view"><strong> <i class="fas fa-pencil-alt"></i>&nbsp;Sign Here</strong></a>');
     getViewAjax(link,'',id,'summaryViewModal .modal-body');
   }
   
  
// <a href="javascript: void(0);"
//                                  onclick="autoSign('.$module['id'].','.$row['id'].','.$row['sign_id'].')" titel="Sign" class="btn btn-outline-cyan"><strong>
//                                  <i class="fas fa-pencil-alt"></i>&nbsp;Sign Here</strong>
//                                     </a>&nbsp;                                  
</script>