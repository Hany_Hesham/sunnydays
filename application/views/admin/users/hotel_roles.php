<div class="card-body card-block">
       <?php echo form_open(base_url('admin/users/hotel_roles'), 'class="form-horizontal"');  ?> 
       <table id="hotel_roles-list-table" class="table table-hover" style="margin-top:2%;width:100%;">
         <thead>
             <tr>
                <th width="30"><strong style="font-size:18px;">Hotel Name</strong></th>
                <th width="70"><strong style="font-size:18px;">Selected Roles</strong></th>
             </tr>
          </thead>
          <tbody>
            <?php if (isset($selected_hotels)){ foreach ($selected_hotels as $key => $hotel) {?> 
              <tr>
                  <td width="">
                     <?php if($hotel['id']) :?>
                        <input  type="hidden" name="hotels[<?php echo $hotel['id']?>][id]" value="<?php echo $selected_hotels[$key]['id']?>">
                      <?php endif;?>
                         <strong><?php echo $hotel['hotel_name']?></strong>  
                  </td>
                  <td width="">
                      <select name="hotels[<?php echo $hotel['id']?>][role_id][]" multiple="multiple" class="selectpicker form-control" id="number-multiple" data-container="body" data-live-search="true" title="Select a number" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false"  style="height: 100% !important;width:100%;">
                           <?php foreach($roles as $role){?>
                             <option value="<?php echo $role['id']?>" <?php if ($hotel['selected_roles']) {
                              if (in_array($role['id'], $hotel['selected_roles'])) {
                                echo 'selected="selected"';
                             } 
                             }?> ><?php echo $role['name']?></option>
                          <?php }?>
                      </select>
                  </td>
              </tr>
            <?php } }?>
          </tbody>
         </table>
         <button type="submit" name="submit" class="btn btn-cyan btn-lg" style="margin-top:5%;width:15%">
                  <strong><i class=" fas fa-dot-circle"></i>&nbsp; Save</strong>
         </button>
       <?php echo form_close( ); ?>
    </div>           
