    
    <?php 
      initiate_breadcrumb(
                          (isset($user['id'])?'Edit User'.$user['id']:'Add User'),
                          (isset($user['id'])?$user['id']:''),//form_id
                          $this->data['module']['id'],//module_id
                          //$this->data['log_permission']['view'],
                          //$this->data['email_permission']['view'],
                          '',
                          '',
                          array(
                            (isset($user['id'])? array('name'=>'User Log','url'=>'admin/log_activity/indexed/'.$user['id']):'')
                          ),//options
                          array(array('name'=>'Users','url'=>'admin/users/'))//locations
                         );
    ?>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                     <div class="card-body">
                        <div class="custom-tab">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="custom-nav-home-tab" data-toggle="tab" href="#custom-nav-home" role="tab" aria-controls="custom-nav-home" aria-selected="true">Profile</a>
                                    <a class="nav-item nav-link" id="custom-nav-hotels-tab" data-toggle="tab" href="#custom-nav-hotel" role="tab" aria-controls="custom-nav-hotel" aria-selected="false">Hotels Roles</a>
                                    
                                      <a class="nav-item nav-link" id="custom-nav-profile-tab" data-toggle="tab" href="#custom-nav-profile" role="tab" aria-controls="custom-nav-profile" aria-selected="false">Permissions</a>
                                    
                                </div>
                            </nav>
                            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                              
                                <div class="tab-pane fade show active" id="custom-nav-home" role="tabpanel" aria-labelledby="custom-nav-home-tab">
                                 <?php echo form_open(base_url('admin/users/add'), 'class="form-horizontal" enctype="multipart/form-data"');  ?> 
                                      <div class="card-body card-block">
                                        <input type="hidden" class="form-control" name="id" value="<?php if(isset($user['id'])){ echo $user['id'];}?>" />
                                        <div class="form-group row">
                                             <label for="username" class=" col-sm-3 text-right control-label col-form-label">Username</label>
                                           <div class="col-sm-6">
                                                <input type="text" id="username" name="username" placeholder="Enter your username" id="inputWarning2i" class="form-control-warning form-control" value="<?php if(isset($user['id'])){echo $user['username'];}?>" required>
                                           </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="fullname" class=" col-sm-3 text-right control-label col-form-label">Fullname</label>
                                          <div class="col-sm-6">
                                              <input type="text" id="fullname" name="fullname" placeholder="Enter your fullname" class="form-control" value="<?php if(isset($user['id'])){ echo $user['fullname'];}?>" required>
                                           </div>
                                        </div>

                                        <div class="form-group row">
                                          <label for="email" class=" col-sm-3 text-right control-label col-form-label">Email</label>
                                          <div class="col-sm-6">
                                              <input type="text" id="email" name="email" placeholder="Enter email" class="form-control" value="<?php if(isset($user['id'])){ echo $user['email']; }?>">
                                          </div> 
                                        </div>
                                        <div class="form-group row">
                                          <label for="mobile_no" class=" col-sm-3 text-right control-label col-form-label">Mobile NO</label>
                                          <div class="col-sm-6">
                                              <input type="text" id="mobile_no" name="mobile_no" placeholder="Enter Mobile NO" class="form-control" value="<?php if(isset($user['id'])){ echo $user['mobile_no']; }?>">
                                          </div> 
                                        </div>
                                        <div class="form-group row">
                                          <label for="fullname" class=" col-sm-3 text-right control-label col-form-label">Signature</label>
                                          <div class="col-sm-6">
                                              <input type="file"  name="signature" class="form-control" accept="image/*"/>
                                           </div>
                                        </div>
                                         <div class="form-group row">
                                          <label for="hotel" class=" col-sm-3 text-right control-label col-form-label">Hotels</label>
                                           <div class="col-sm-6">
                                            <select id="hotel_id" name="hotels[]" multiple class="selectpicker form-control" id="number-multiple" data-container="body" data-live-search="true" title="Select a number" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false"  style="height: 100% !important;width:100%;" required>
                                                   <?php foreach($hotels as $hotel){?>
                                                    <option value="<?php echo $hotel['id']?>" 
                                                      <?php if(isset($user['id'])){
                                                         echo (array_assoc_value_exists($selected_hotels, 'hotel_id', $hotel['id']))? 'selected="selected"' : set_select('hotel[]',$hotel['id'] );}?>>
                                                         <?php echo $hotel['code']?></option>
                                                  <?php }?>
                                              </select>
                                           </div>   
                                        </div>
                                        <div class="form-group row">
                                         <label for="role" class=" col-sm-3 text-right control-label col-form-label">Roles</label>
                                          <div class="col-sm-6">
                                             <select id="role_id" name="role_id" class="selectpicker form-control" data-container="body" data-live-search="true" required>
                                                    <option value=""></option>
                                                   <?php foreach($roles as $role){?>
                                                    <option value="<?php echo $role['id']?>"
                                                     <?php if(isset($user['id'])){
                                                      echo ($user['role_id'] == $role['id'])? 'selected="selected"' : set_select('role_id',$role['id'] );}?>><?php echo $role['name']?></option>
                                                  <?php }?>
                                                </select>
                                          </div>
                                        </div>
                                        <div class="form-group row">
                                          <label for="role" class=" col-sm-3 text-right control-label col-form-label">Department</label>
                                          <div class="col-sm-6">
                                             <select id="department_id" name="departments[]" multiple class="selectpicker form-control" id="number-multiple" data-container="body" data-live-search="true" title="Select a number" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false"  style="height: 100% !important;width:100%;">
                                                   <?php foreach($departments as $department){?>
                                                    <option value="<?php echo $department['id']?>" 
                                                      <?php if(isset($user['id']) && is_array(json_decode($user['department']))){
                                                        echo (in_array($department['id'], json_decode($user['department'])))? 'selected="selected"' : set_select('departments[]',$department['id'] );
                                                         }?>><?php echo $department['dep_name']?></option>
                                                  <?php }?>
                                                </select>
                                          </div>
                                        </div>
                                        <div class="row form-group row">
                                              <label for="password" class=" col-sm-3 text-right control-label col-form-label">Password</label>
                                              <div class="col-sm-6">
                                                  <input type="password" id="password" name="password" placeholder="Enter password" class=" form-control">
                                              </div>
                                        </div>                        
                                        </div>
                                        <?php if ((isset($user) && (($this->global_data['sessioned_user']['is_admin'] == 1 && $user['is_admin'] == 1) || ($user['is_admin'] != 1))) || !isset($user)):?>
                                      <button type="submit" name="submit" class="btn btn-cyan btn-lg" style="margin-top:5%;width:15%">
                                              <strong><i class=" fas fa-dot-circle"></i>&nbsp; Save</strong>
                                     </button>
                                   <?php endif; ?>
                                    <?php echo form_close( ); ?>
                                  </div>
                               
                                <div class="tab-pane fade" id="custom-nav-profile" role="tabpanel" aria-labelledby="custom-nav-profile-tab">
                                   <?php $this->load->view('admin/users/permission_tab')?>
                                </div>
                                <div class="tab-pane fade" id="custom-nav-hotel" role="tabpanel" aria-labelledby="custom-nav-hotels-tab">
                                   <?php $this->load->view('admin/users/hotel_roles')?>
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
                </div>
             </div>
         </div>   
<script type="text/javascript">
initDTable_normal('permission-list-table','1',
                   [
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                   ],100
          );

initDTable_normal('hotel_roles-list-table','1',
                   [
                    {"mod":"mod"},
                    {"mod":"mod"},
                   ],100
          );

</script>