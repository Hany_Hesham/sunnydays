<div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Add group Permissions</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url('admin/users');?>">User List</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Users</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
          <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="card">
                     <div class="card-body">
                       <div class="row">
                          <div class="col-5">
                             <?php echo form_open(base_url('admin/users/group'), 'class="form-horizontal"');  ?> 
                                 <div class="form-group">
                                   <select id="role_id" name="role_id" class="selectpicker form-control custom-select" style="width: 100%; height:36px;" title="Select Role" data-live-search="true" onchange="this.form.submit()">
                                     
                                       <?php foreach($roles as $row){?>
                                         <option value="<?php echo $row['id']?>" <?php if(isset($role)&& ($role['id'] == $row['id'])){echo 'selected';}?>><?php echo $row['name']?></option>
                                       <?php }?>
                                  </select>
                                 </div>
                             <?php echo form_close( ); ?>
                             </div> 
                             <!-- <?php if($sessioned_user['is_admin']==1){?>
                               <div class="col-7">
                                  <button data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-cyan" style="float:right;">
                                  <i class="fa fa-star"></i>&nbsp; Add Role</button>  
                              </div>
                            <?php }?> -->
                           </div> 
                             <hr><br>
                              <div class="card-body card-block">
                                <?php echo form_open(base_url('admin/users/add_group'), 'class="form-horizontal"');  ?> 
                                   <input  type="hidden" name="role" value="<?php echo (isset($role))? $role['id']: ''?>">
                                   <table id="permission-list-table" class="table" style="margin-top:2%;width:100%;">
                                     <thead>
                                         <tr>
                                            <th>Module</th>
                                            <th width="10%">View(global)</th>
                                            <th width="10%">View</th>
                                            <th width="15%">Private View</th>
                                            <th width="10%">Create</th>
                                            <th width="10%">Edit</th>
                                            <th width="10%">Remove</th>
                                            <th width="10%">Print</th>
                                         </tr>
                                      </thead>
                                      <tbody>
                                         <?php foreach ($modules as $key => $module) {?> 
                                          <tr>
                                              <td>
                                                 <?php if(isset($role)&& $role['permission'] == 1) :?>
                                                    <input  type="hidden" name="accs[<?php echo $module['id']?>][id]" value="<?php echo $modules[$key]['permission']['id']?>">
                                                  <?php endif;?>
                                                   <input  type="hidden" name="accs[<?php echo $module['id']?>][module_id]" value="<?php echo $modules[$key]['id']?>" style="width:25px;height: 25px;">
                                                     <strong><?php echo $module['name']?></strong>  
                                              </td>
                                              <td style="text-align: center;">
                                                <?php if($modules[$key]['g_view'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][g_view]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][g_view]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['g_view']) && $modules[$key]['permission']['g_view'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                              <td style="text-align: center;">
                                                <?php if($modules[$key]['view'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][view]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][view]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['view']) && $modules[$key]['permission']['view'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                               <td style="text-align: center;">
                                                <?php if($modules[$key]['private_view'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][private_view]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][private_view]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['private_view']) && $modules[$key]['permission']['private_view'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                               <td style="text-align: center;">
                                                <?php if($modules[$key]['creat'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][creat]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][creat]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['creat']) && $modules[$key]['permission']['creat'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                              <td style="text-align: center;">
                                                <?php if($modules[$key]['edit'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][edit]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][edit]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['edit']) && $modules[$key]['permission']['edit'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                              <td style="text-align: center;">
                                                <?php if($modules[$key]['remove'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][remove]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][remove]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['remove']) && $modules[$key]['permission']['remove'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                              <td style="text-align: center;">
                                                <?php if($modules[$key]['f_change'] == 1) :?>
                                                    <label class="customcheckbox">
                                                      <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][f_change]'>
                                                        <input type="checkbox" name="accs[<?php echo $module['id']?>][f_change]"
                                                            value="1"  class="listCheckbox" 
                                                           <?php echo (isset($modules[$key]['permission']['f_change']) && $modules[$key]['permission']['f_change'] =='1')?"checked":"" ;?>
                                                             <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                                                        <span class="checkmark"></span>
                                                     </label>
                                                 <?php endif?>
                                              </td>
                                          </tr>
                                          <?php }?>
                                      </tbody>
                                     </table>
                                      <?php if($sessioned_user['is_admin']==1){?>
                                       <button type="submit" name="submit" class="btn btn-cyan btn-lg" style="margin-top:5%;width:15%">
                                                <strong><i class=" fas fa-dot-circle"></i>&nbsp; Save</strong>
                                       </button>
                                      <?php }?>
                                   <?php echo form_close( ); ?>
                                </div>
                            </div>
                     </div>
                  </div>
                </div>
             </div>

<script type="text/javascript">
initDTable_normal('permission-list-table','1',
                   [
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                    {"mod":"mod"},
                   ],100
          );
</script>
<?php initiate_modal('lg','Add Role Name',
                     'admin/users/add_role_name',
                     '<div class="form-group">
                        <input type="text" id="Role Name" name="role_name" placeholder="Enter Role Name" class="form-control" required>
                      </div>'
                     )?>