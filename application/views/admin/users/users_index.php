    <?php initiate_breadcrumb(
                                'Users List',
                                '',//form_id
                                $this->data['module']['id'],//module_id
                                $this->data['log_permission']['view'],
                                $this->data['email_permission']['view'],
                                'empty',//options
                                array(array('name'=>'Userss','url'=>'empty'))//locations
                             );
    ?>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                      <a class="nav-link dropdown-toggle btn btn-cyan col-md-2" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><strong style="font-size:14px;">Action Menu</strong></a>
                       <div class="dropdown-menu" x-placement="top-start" style="position: absolute; transform: translate3d(0px, -2px, 0px); top: 0px; left: 0px; will-change: transform;">
                              <a class="dropdown-item" href="<?php echo base_url('admin/users/user');?>">Add User</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="<?php echo base_url('admin/users/group');?>">add Group</a>
                            <?php if($sessioned_user['is_admin']==1){?>
                              <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?php echo base_url('admin/users/bulk');?>">Bulk Action</a>
                              <?php }?>
                         </div>
                       <div class="dropdown-divider"></div><br><br>
                       <div class="row">
                         <div class="col-sm-3">
                            <label for="hotel" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Hotels</label>
                            <select id="hotel_id" name="hid[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
                              <?php foreach($hotels as $hotel){?>
                                <option value="<?php echo $hotel['id']?>"><?php echo $hotel['hotel_name']?></option>
                              <?php }?>
                            </select>
                          </div>
                          <div class="col-sm-3" id="rolesFilter">
                            <label for="roles" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Roles</label>
                            <select id="role_id" name="roles[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Signatures" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
                               <?php foreach($roles as $role){?>
                                <option value="<?php echo $role['id']?>"><?php echo $role['name']?></option>
                              <?php }?>
                            </select>
                          </div>  
                          <div class="col-sm-3">
                            <label class="text-right control-label col-form-label" style="font-size:12px;">From Date:</label>
                            <div class="input-group">
                              <input type="text" name="from_date" id="fromDate" class="form-control Delivery-updater" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
                              <div class="input-group-addon">
                                <span><i class="fa fa-calendar"></i></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <label class="text-right control-label col-form-label" style="font-size:12px;">To Date:</label>
                            <div class="input-group">
                              <input type="text" name="to_date" id="toDate" class="form-control Delivery-updater" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
                              <div class="input-group-addon">
                                <span><i class="fa fa-calendar"></i></span>
                              </div>
                            </div>
                          </div>  
                       </div><br>
                        <h4 class="hidden" id="indexName">Users List</h4>
                        <h4 class="card-title" id="totalCount"></h4><br>
                         <table id="complimentary-list-table" class="table table-hover indexTable" style="width:100% !important;">
                           <span id="tableFun" class="hidden">admin/users/users_ajax</span>
                             <thead >
                                <tr>
                                  <th class="tableCol" width="5%"  id="id"><strong>#</strong></th>
                                  <th class="tableCol" width="15%" id="username"><strong>Username</strong></th>
                                  <th class="tableCol" width="15%" id="fullname"><strong>Fullname</strong></th>
                                  <th class="tableCol" width="25%" id="email"><strong>Email</strong></th>
                                  <th class="tableCol" width="15%" id="role_name"><strong>Role</strong></th>
                                  <th class="tableCol" width="8%" id="is_admin"><strong>Type</strong></th>
                                  <th class="tableCol" width="8%" id="disabled"><strong>Disabled</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
    
    <script type="text/javascript">

      $('[data-toggle="datepicker"]').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });


      function dateInitial(){
        this.fromDate = '<?php echo (isset($from_date))? $from_date : ''?>'; 
        this.toDate   = '<?php echo (isset($from_date))? $to_date : ''?>'; 
       }

      function tableConstructor(){
       this.indexTable   = $('.indexTable').attr('id'); 
       this.index_fun    = $('#tableFun').text(); 
       this.tablCols     = $('.tableCol').map(function(){
                             return {name:$(this).attr('id')};
                           }).get();
       this.indexName     = $('#indexName').text();
       this.total         = $('#totalCount').text();
       this.hotel_id      = $('#hotel_id').val();
       this.dep_code      = $('#dep_code').val();
       this.fromDate      = $('#fromDate').val();
       this.toDate        = $('#toDate').val();
       this.role_id       = $('#role_id').val();
         if (this.fromDate == '' || this.toDate == '') {
             let intiDate = new dateInitial();
             if (this.fromDate == ''){this.fromDate = intiDate.fromDate}
             if (this.toDate == ''){this.toDate = intiDate.toDate}   
          }
      $('#fromDate').val(this.fromDate);
      $('#toDate').val(this.toDate);  
      };
    
    $('.filter-indexs').change(function () {
        indexsFilter();
      });
    
  $('[data-toggle="datepicker"]').datepicker().on('changeDate', function(e) {
       indexsFilter();
      });

    function indexsFilter(){
       let filters    = new tableConstructor();
       let table      = $('#'+filters.indexTable+'').DataTable();
       let indexData  = {reportName:filters.indexName,total:filters.total};
       let data       = {searchBy:{hotel_id:filters.hotel_id,
                         dep_code:filters.dep_code,
                         fromDate:filters.fromDate,
                         toDate:filters.toDate,
                         role_id:filters.role_id}};
         if (filters.hotel_id !='' || filters.dep_code != '' || filters.fromDate != ''|| filters.toDate != '' || filters.role_id != '' ) {
           table.destroy();
           initDTableButtons(
                              ""+filters.indexTable+"",
                              filters.tablCols,
                              "<?php echo base_url("")?>"+filters.index_fun,
                              data,'',indexData
                            );
             }
      
       }
      let totalRecords = '';
      $('#totalCount').append(''+totalRecords+'');
      let index      =  new tableConstructor(); 
      let indexDater =  {searchBy:{fromDate:index.fromDate,toDate:index.toDate,dep_code:index.dep_code}};
      let indexData  =  {reportName:index.indexName,total:index.total};
      initDTableButtons(
                          ""+index.indexTable+"",
                            index.tablCols,
                            "<?php echo base_url()?>"+index.index_fun,
                            indexDater,'',indexData
                        ); 

    </script>