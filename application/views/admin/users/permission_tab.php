     <div class="card-body card-block">
       <?php echo form_open(base_url('admin/users/add_permission/'.$user['id']), 'class="form-horizontal"');  ?> 
       <table id="permission-list-table" class="table" style="margin-top:2%;width:100%;">
         <thead>
             <tr>
                <th>Module</th>
                <th width="5%">View(global)</th>
                <th width="5%">View</th>
                <th width="5%">Private View</th>
                <th width="5%">Create</th>
                <th width="5%">Edit</th>
                <th width="5%">Remove</th>
                <th width="5%">Print</th>
             </tr>
          </thead>
          <tbody>
             <?php foreach ($modules as $key => $module) {?> 
              <tr>
                  <td width="10%">
                    <!--  <?php //if($user['permission'] == 1) :?> -->
                        <input  type="hidden" name="accs[<?php echo $module['id']?>][id]" value="<?php echo (isset($modules[$key]['permission']['id']) ? $modules[$key]['permission']['id'] :'') ?>">
                     <!--  <?php //endif;?> -->
                       <input  type="hidden" name="accs[<?php echo $module['id']?>][module_id]" value="<?php echo $modules[$key]['id']?>" style="width:25px;height: 25px;">
                         <strong><?php echo $module['name']?></strong>  
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['g_view'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][g_view]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][g_view]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['g_view']) && $modules[$key]['permission']['g_view'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['view'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][view]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][view]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['view']) && $modules[$key]['permission']['view'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['private_view'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][private_view]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][private_view]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['private_view']) && $modules[$key]['permission']['private_view'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                   <td width="5%">
                    <?php if($modules[$key]['creat'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][creat]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][creat]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['creat']) && $modules[$key]['permission']['creat'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['edit'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][edit]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][edit]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['edit']) && $modules[$key]['permission']['edit'] =='1')?"checked":"" ;?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['remove'] == 1) :?>
                        <label class="customcheckbox switch-info">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][remove]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][remove]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['remove']) && $modules[$key]['permission']['remove'] =='1')?"checked":"" ;?> 
                               <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
                  <td width="5%">
                    <?php if($modules[$key]['f_change'] == 1) :?>
                        <label class="customcheckbox">
                          <input type='hidden' value='0' name='accs[<?php echo $module['id']?>][f_change]'>
                            <input type="checkbox" name="accs[<?php echo $module['id']?>][f_change]"
                                value="1" class="listCheckbox" 
                               <?php echo (isset($modules[$key]['permission']['f_change']) && $modules[$key]['permission']['f_change'] =='1')?"checked":"" ;?>
                               <?php if($sessioned_user['is_admin']!=1){echo "disabled";}?>>
                            <span class="checkmark"></span>
                         </label>
                     <?php endif?>
                  </td>
              </tr>
              <?php }?>
          </tbody>
         </table>
         <button type="submit" name="submit" class="btn btn-cyan btn-lg" style="margin-top:5%;width:15%">
                  <strong><i class=" fas fa-dot-circle"></i>&nbsp; Save</strong>
         </button>
       <?php echo form_close( ); ?>
    </div>