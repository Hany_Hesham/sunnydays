<div class="page-breadcrumb">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-sm-4">
            <div class="float-left">
              <h4 class="page-title">Email List
                <a  class="nav-link waves-effect waves-dark no-print" data-original-title="Run Cron" onclick="runAll()" href="javascript: void(0);">  
                  <i class="fas fa-envelope"></i>
                </a>
              </h4>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Emails</li>
                  <li class="breadcrumb-item"><a href="<?php echo($this->agent->referrer());?>">Back</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <table id="emails-list-table" class="table table-hover" style="width:100% !important;">
            <thead >
              <tr>
                <th scope="col" width="5%"><strong>#</strong></th>
                <th scope="col" width="10%"><strong>Type</strong></th>
                <th scope="col" width="15%"><strong>Action Time</strong></th>
                <th scope="col" width="10%"><strong>Made On</strong></th>
                <th scope="col" width="10%"><strong>Form Id</strong></th>
                <th scope="col" width="7%"><strong>UserName</strong></th>
                <th scope="col" width="10%"><strong>Message</strong></th>
                <th scope="col" width="10%"><strong>Status</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php initiate_modal(
  'lg', 'Email Detailed Data', '',
  '<div class="table-responsive">
    <table id="email-table" class="table table-hover">
      <thead class="thead-light">
        <tr>
          <th scope="col" width="40%"><strong>Description</strong></th>
          <th scope="col" width="60%"><strong>Value</strong></th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>'
)?>
<script type="text/javascript">

  initDTable(
    "emails-list-table",
    [{"name":"id"},
    {"name":"type"},
    {"name":"timestamp"},
    {"name":"module_id"},
    {"name":"form_id"},
    {"name":"fullname"},
    {"name":"message"},
    {"name":"sent_at"},],
    "<?php echo base_url("admin/email/email_ajax/".$module_id.'/'.$form_id) ?>"
  );

  $('.modal-footer').hide();

  function getEmailData(email_id){
    var url="<?php echo base_url('admin/email/get_submenu/email_model/get_email/');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{search:email_id},
      success: function(data){
        $('.modal-body #email-table tbody').empty();
        $.each(data, function(i,item){
          if (data[i].id == undefined) {
            $('.modal-body').append('<option value="'+ data[i].id +'">'+ data[i].key +'</option>');
          }else{
            $('.modal-body #email-table tbody').append('<tr><td><strong style="font-size:16px;">Email Id</strong></td><td class="text-success">'+data[i].id+'</td></tr> <tr><td><strong style="font-size:16px;">Action Time</strong></td><td class="text-success">'+data[i].timestamp+'</td></tr> <tr><td><strong style="font-size:16px;">UserName</strong></td><td class="text-success">'+data[i].fullname+'</td></tr> <tr><td> <strong style="font-size:16px;">Type</strong></td><td class="text-success">'+data[i].type+'</td></tr>  <tr><td><strong style="font-size:16px;">Made ON</strong></td><td class="text-cyan">'+data[i].module_name+' No #.'+data[i].form_id+'</td></tr> <tr><td><strong style="font-size:16px;">Message</strong></td> <td class="text-success">'+data[i].message+'</td></tr> <tr><td><strong style="font-size:20px;">To</strong></td> <td class="text-success">'+data[i].emails+'</td></tr>');
          }           
        });
      }
    });
  }

  function sendEmail(email_id){
    var url="<?php echo base_url('admin/email/sendEmail/');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{email_id:email_id},
      success: function(data){
        return true;
      }
    });
  }

  function runAll(){
    var url="<?php echo base_url('cron/all');?>";      
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{},
      success: function(data){
        return true;
      }
    });
  }

</script>