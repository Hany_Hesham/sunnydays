<?php $signing_module = ( isset($module) ? $module : $this->data['module']) ;
  if (isset($signatures)) :?>
  <div class="container-fluid">
   <?php initiate_alert();?> 
    <div class="row">
      <?php $first = TRUE; ?>
      <?php foreach($signatures as $signature):?> 
        <div class="col-md-3 signature-buttons-container" style="width:25% !important;">
          <div class="card"> 
            <div class="card-header badge-dark after-hover">
              	<div class="row">
	          		<div class="col-sm-2">
              			<ul class="navbar-nav float-left mr-auto">
			                <li class="nav-item dropdown">
			                  	<span class="wait-hover">
                     				<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-placement="top" data-original-title="Mail To" target="signer<?php echo $signature['id']?>-div" href="#" class="nav-link dropdown-toggle waves-effect waves-dark card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
	                  				<div id="signer<?php echo $signature['id']?>-div" class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="2" style="width: 200px;">
				                    	<?php if (isset($signature['queue'])) : ?>
				                      		<?php foreach($signature['queue'] as $key => $queue):?>
				                        		<?php if ($key != 'reason') { ?>
				                        			<div class="row">
				                        				<div class="col-sm-8">
				                      						<a class="dropdown-item" href="#"><?php echo $queue['name']  ?></a>
					                        			</div>
					                        			<?php if ($this->global_data['sessioned_user']['is_admin']==1): ?>
						                        			<div class="col-sm-4">
							                        			<a id="signFor" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sign For <?php echo $queue['name']  ?>" href="<?php echo base_url();?><?php echo $signing_module['link'] ?>/sign_for/<?php echo $signing_module['id']?>/<?php echo $form_id?>/<?php echo $signature['id']?>/<?php echo $queue['id'] ?>/<?php echo $signature['typed']?>" class="card-hover btn btn-light btn-lg btn-circle" style="float:right;"><i style="text-align: center; font-size: 20px;" class="fas fa-calendar-alt text-success"></i></a>
							                        		</div>
							                        	<?php endif ?>
					                        		</div>
				                        			<div class="dropdown-divider"></div>
				                      			<?php } ?>
				                      		<?php endforeach; ?>
				                    	<?php elseif (isset($signature['sign'])) : ?>
				                      		<a class="dropdown-item" href="#"><?php echo $signature['sign']['name'] ?></a>
				                      		<div class="dropdown-divider"></div>
				                    	<?php endif ?>
	                  				</div>
                  				</span>
                			</li>
              			</ul>
          	  		</div>
	          		<div class="col-sm-10">
              			<h6 class="card-title float-left"><?php echo $signature['role']?></h6>
          			</div>
     			</div>
            </div>
            <div class="card-body centered">
              <?php if (isset($signature['sign'])) :?>
               <h5 class="card-title centered"><?php echo $signature['sign']['name']?></h5>
                <div>
                  <img src="
                    <?php if($signature['sign']['reject'] != 0){ 
                      echo $signature_path.'rejected.png';
                    } else {
                      echo $signature_path.''.$signature['sign']['sign'];
                    }?>
                  " alt="<?php echo $signature['sign']['name']; ?>" style="width: 160px; height: 45px;">
                  <?php if ($signature['sign']['unsign'] && $doned == 0){?>
                   <a id="unsign" data-toggle="tooltip" data-placement="top" title="" data-original-title="UnSign" href="<?php echo base_url();?><?php echo $signing_module['link'] ?>/unSign/<?php echo $signing_module['id']?>/<?php echo $form_id?>/<?php echo $signature['id']?>/<?php echo $signature['typed']?>" class="card-hover btn btn-light btn-lg btn-circle" style="float:right;"><i style="text-align: center; font-size: 20px;" class="fas fa-calendar-alt text-danger"></i></a>
                  <?php } ?>
                </div>
              <h5><?php echo $signature['sign']['reason']?> </h5>
              <h6><?php echo $signature['sign']['timestamp']?> </h6>
              <?php elseif (isset($signature['queue'])) : ?>
                <h5><?php echo $signature['queue']['reason']?> </h5>
                <?php if ($signature['signer'] && $first && $doned == 0) :?>
                  &nbsp;&nbsp;&nbsp;
                  <a id="sign" data-toggle="tooltip" data-placement="top" title="" data-original-title="Sign" href="<?php echo base_url();?><?php echo $signing_module['link'] ?>/sign/<?php echo $signing_module['id']?>/<?php echo $form_id?>/<?php echo $signature['id']?>/<?php echo $signature['typed']?>" class="card-hover btn btn-light btn-lg btn-circle signature-button no-print"><i style="text-align: center; font-size: 20px;" class="fas fa-calendar-check text-success "></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Reject" value="<?php echo $signature['id']?>" dataID="<?php echo $signature['typed']?>" class="card-hover btn btn-light btn-lg btn-circle signature-button reject"><i style="text-align: center; font-size: 20px;" class="fas fa-calendar-times text-danger no-print"></i></button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <button data-toggle="tooltip" data-placement="top" title="" data-original-title="Reback" value="<?php echo $signature['id']?>" dataID="<?php echo $signature['typed']?>" class="card-hover btn btn-light btn-lg btn-circle signature-button resign"><i style="text-align: center; font-size: 20px;" class="fas fa-calendar-minus text-warning no-print"></i></button>
                  <?php $first = FALSE; ?>
                <?php else: ?>
                  <?php $first = FALSE; ?>
                <?php endif; ?>              
              <?php endif ?>
            </div>
          </div>
        </div>
        <?php if (isset($signature['sign']) && $signature['sign']['reject'] != 0){break;}?>
      <?php endforeach; ?>
    </div>
  </div>
  <?php
    	$select = '<select name="rank" class="selectpicker form-control custom-select rank"  data-container="body" data-live-search="true" title="Select Role" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="width: 100%; height:36px;" required>';
    	$select .= '<option value="">Select Role</option>';     
    	foreach ($signatures as $siner) {
        	$select .= '<option value="'.$siner['rank'].'">'.$siner['role'].'</option>';
    	}
    	$select .= '</select><br>';
  ?>
  <?php initiate_modal('','','','<input type="hidden" name="id" class="form-control id" value=""><br>     
    	<input type="text" name="reason" class="form-control reason" placeholder="Reason" required><br> 
    	'. $select.'<br>','reject'
  )?>
  <?php initiate_modal('','','','<input type="hidden" name="id" class="form-control id" value=""><br>     
    	<input type="text" name="reason" class="form-control reason" placeholder="Reason" required><br> 
    	'. $select.'<br>','resign'
  )?>
  <script type="text/javascript">
  	var module = '<?php echo $signing_module['link']?>';
    	$(document).ready(function(){
  	    $(".reject").click(function() {
            var dataReject   = $(this).attr('dataID').match(/\d/g).join("");
            console.log(dataReject);
            $('#modalSubmit-reject').prop('id', 'modalSubmit-reject'+dataReject);
  	      	$('#reject').modal('show');
  	      	$('.modal-body .rank').hide();
  	      	$('.modal-body .rank').removeAttr('required');
  	      	$('form').attr('action', getUrl()+module+'/reject/'+<?php echo $signing_module['id'] ?>+'/'+<?php echo $form_id ?>+'/'+dataReject);
  	      	$('.modal-title').empty();
  	      	$('.modal-title').append('Reject');
  	      	var id = $(this , '.reject').val();
  	      	$('.modal-body .id').val(id);
  	    });
  	    $(".resign").click(function() {
            var dataResign   = $(this).attr('dataID').match(/\d/g).join("");
            $('#modalSubmit-resign').prop('id', 'modalSubmit-resign'+dataResign);
  	      	$('#resign').modal('show');
  	      	$('.modal-body .rank').show();
  	      	$('form').attr('action', ''+getUrl()+module+'/resign/'+<?php echo $signing_module['id'] ?>+'/'+<?php echo $form_id ?>+'/'+dataResign);
  	      	$('.modal-title').empty();
  	      	$('.modal-title').append('Resign');
  	    });
    	});
  </script>
<?php endif; ?>
