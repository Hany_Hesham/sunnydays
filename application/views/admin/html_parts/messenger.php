<div class="modal fad scrollablee" id="messanger" tabindex="-1" role="dialog" aria-labelledby="smallmodalLabel" aria-hidden="true">
  <div class="modal-dialog modal-50" role="document">
    <div class="modal-content">
      <div class="modal-header badge-dark">
        <h5 class="modal-title" id="smallmodalLabel"><?php echo $head ?></h5>
        <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php if ($private == 0):?>
        <div class="modal-body">
          <div class="chat-box" style="height:330px;">
            <ul class="chat-list">
              <?php if(isset($messaged)):?>
                <?php foreach ($messaged as $message):?>
                  <?php if ($this->data['user_id'] == $message['uid']):?>
                    <li class="odd chat-item">
                      <div class="chat-content">
                        <div class="box bg-light-inverse">
                          <?php echo $message['message'] ?>
                        </div>
                        <div class="chat-time"><?php echo $message['timestamp'] ?></div>
                      </div>
                    </li>
                  <?php else: ?>
                    <li class="chat-item">
                      <div class="chat-content">
                        <h6 class="font-medium"><?php echo $message['username'] ?></h6>
                        <div class="box bg-light-info">
                          <?php echo $message['message'] ?>
                        </div>
                        <div class="chat-time"><?php echo $message['timestamp'] ?></div>
                      </div>
                     </li>
                  <?php endif;?>
                <?php endforeach; ?>
              <?php endif;?>
            </ul>
          </div>
        </div>
      <?php endif;?>
      <?php echo form_open(base_url('admin/dashboard/'.$formLink), 'class="form-horizontal"');?> 
        <div class="card-body border-top">
          <div class="row">
            <?php if ($private != 0): ?>
              <div class="col-12">
                <div class="input-field m-t-0 m-b-0">
                  <select id="to_uid" name="to_uid" class="selectpicker form-control"  data-container="body" data-live-search="true" title="Select User" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" >
                    <?php foreach($this->data['users'] as $user){?>
                      <option value="<?php echo $user['id']?>"> <?php echo $user['username']?> </option>
                    <?php }?>
                  </select>
                </div>
              </div>
            <?php else: ?>
              <input type="hidden" name="form_id" value="<?php echo $formID ?>" />
              <input type="hidden" name="module_id" value="<?php echo $moduleID ?>" />
            <?php endif; ?>
            <div class="col-12">
              <div class="input-field m-t-0 m-b-0">
                <textarea id="textarea1" name="message" placeholder="Type & enter" class="form-control border-0" required></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" type="submit" name="submit" class="btn btn-primary">Confirm</button>
            </div>
          </div>
        </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>