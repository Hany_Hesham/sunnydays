<?php   
  if ($this->input->is_ajax_request()) {
    return true;
  }
?>
<div class="page-breadcrumb no-print" style="zoom:90%;">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <div class="col-md-12">
            <div class="float-left">
              <h5 class="page-title">
                <span class="page-title-toSave"><?php echo $page_name?></span>
                <a class="nav-link waves-effect waves-dark no-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-h font-24"></i></a>
                <div class="dropdown-menu">
                  <div style="margin-top: 15px;">
                    <?php if ($options != 'empty') {?>
                      <a style="margin-left:5px;" class="savedFormsButton" id="<?php echo $module_id.','.$form_id?>" href="javascript: void(0);">
                        <span class="text-danger"><i id="favouritIcon" class="far fa-star font-18"></i></span>  
                        <b class="text-dark">Favourite</b>
                      </a>
                      <div class="dropdown-divider"></div>
                      <?php foreach($options as $option){ ?>
                        <?php if(isset($option['print']) && $option['print']=='jsPrinter'){?>
                          <?php if($this->data['permission']['f_change'] == 1){?>
                            <a style="margin-left:5px;"  href="javascript: void(0);" onclick="printDiv()">
                              <span class="text-danger"><i class="fas fa-file-pdf font-18"></i></span>  
                              <b class="text-dark">Print</b>
                            </a>
                            <div class="dropdown-divider"></div>
                          <?php } ?>
                        <?php }elseif(isset($option['print']) && $option['print']!=''){?>
                          <a style="margin-left:5px;"  href="<?php echo base_url('pdf_generator/'.$option['print'].'/'.$form_id);?>">
                            <span class="text-danger"><i class="fas fa-file-pdf font-18"></i></span>  
                            <b class="text-dark">Print</b>
                          </a>
                          <div class="dropdown-divider"></div>
                        <?php }elseif ($option['name'] != ''){?>  
                          <a style="margin-left:5px;" href="<?php echo base_url($option['url']);?>"> 
                            <span class="text-dark"><i class="fas fa-circle-notch"></i></span> 
                            <b class="text-dark"><?php echo $option['name']?></b> 
                          </a> 
                          <div class="dropdown-divider"></div>
                        <?php } ?>
                      <?php } ?>
                    <?php } ?>  
                    <?php if($this->data['log_permission']['view']==1 || $log_permission==1){?>
                      <a style="margin-left:5px;" href="<?php echo base_url('admin/log_activity/index/'.$module_id.'/'.$form_id);?>"> 
                        <span class="text-dark"><i class="fas fa-shield-alt"></i></span> 
                        <b class="text-dark">View Log Activity</b> 
                      </a>
                      <div class="dropdown-divider"></div>
                    <?php }?>  
                    <?php if($this->data['email_permission']['view']==1){?>
                      <a style="margin-left:5px;" href="<?php echo base_url('admin/email/index/'.$this->data['module']['id'].'/'.$form_id);?>"> 
                        <span class="text-dark"><i class="fas fa-envelope-open"></i></span> 
                        <b class="text-dark">View Email Activity</b> 
                      </a>
                      <div class="dropdown-divider"></div>
                    <?php }?>  
                  </div>   
                </div>  
              </h5>
            </div>
          </div>
          <div class="col-md-12">
            <div class="ml-auto float-right">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                  <?php if ($locations != 'empty') {
                    foreach($locations as $location){ ?>
                      <?php if($location['url'] != 'empty') {?>
                        <li class="breadcrumb-item">
                          <a href="<?php echo base_url($location['url']);?>"><?php echo $location['name']?></a>
                        </li>
                      <?php }else{ ?>  
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $location['name']?></li>
                      <?php }?>  
                    <?php } ?>
                  <?php } ?>  
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>