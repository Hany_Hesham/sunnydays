<div class="row">
  <?php if(isset($hotels)){?>
    <div class="col-sm-3">
      <label for="hotel" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Hotels</label>
      <select id="hotel_id" name="hid[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
        <?php foreach($hotels as $hotel){?>
          <option value="<?php echo $hotel['id']?>"><?php echo $hotel['hotel_name']?></option>
        <?php }?>
      </select>
    </div>
  <?php }?>
  <?php if( isset($to_hotels) ){?>
    <div class="col-sm-3">
      <label for="to_hotel" class="text-right control-label col-form-label" style="font-size:12px;">Filter By To Hotels</label>
      <select id="to_hotel_id" name="to_hid[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
        <?php foreach($to_hotels as $hotel){?>
          <option value="<?php echo $hotel['id']?>"><?php echo $hotel['hotel_name']?></option>
        <?php }?>
      </select>
    </div>
  <?php }?>
  <?php if(isset($departments)){?>
    <div class="col-sm-3">
      <label for="department" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Department</label>
      <select id="dep_code" name="dep_code[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
        <?php foreach($departments as $department){?>
          <option value="<?php echo $department['id']?>"><?php echo $department['dep_name']?></option>
        <?php }?>
      </select>
    </div>
  <?php }?>
  <?php if(isset($from_date)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">From Creation Date:</label>
      <div class="input-group">
        <input type="text" name="from_date" id="fromDate" class="form-control Delivery-updater" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">To Creation Date:</label>
      <div class="input-group">
        <input type="text" name="to_date" id="toDate" class="form-control Delivery-updater" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>  
  <?php }?>  
  <?php if(isset($statuss)){?>
    <div class="col-sm-3" id="statusFilter">
      <label for="statuss" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Status</label>
      <select id="status_id" name="statuss[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
        <?php foreach($statuss as $status){?>
          <option value="<?php echo $status['id']?>"><?php echo $status['status_name']?></option>
        <?php }?>
      </select>
    </div>   
  <?php }?>
  <?php if(isset($roles)){?>
    <div class="col-sm-3" id="rolesFilter">
      <label for="roles" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Signatures</label>
      <select id="role_id" name="roles[]" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Signatures" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
        <?php foreach($roles as $role){?>
          <option value="<?php echo $role['id']?>" <?php if (in_array($role['id'], $uroles)) {echo 'selected';}?>><?php echo $role['name']?></option>
        <?php }?>
      </select>
    </div>  
  <?php }?>
  <?php if(isset($arrival_date)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">Arrival Date:</label>
      <div class="input-group">
        <input type="text" name="arrival_date" id="arrivalDate" class="form-control" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>
  <?php }?> 
  <?php if(isset($departure_date)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">Departure Date:</label>
      <div class="input-group">
        <input type="text" name="departure_date" id="departureDate" class="form-control" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>  
  <?php }?>
  <?php if(isset($from_form_date)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">From Date:</label>
      <div class="input-group">
        <input type="text" name="from_form_date" id="fromFormDate" class="form-control" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>
  <?php }?> 
  <?php if(isset($to_form_date)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">TO Date:</label>
      <div class="input-group">
        <input type="text" name="departure_date" id="toFormDate" class="form-control" data-toggle="datepicker"  placeholder="yyyy-mm-dd" required>
        <div class="input-group-addon">
          <span><i class="fa fa-calendar"></i></span>
        </div>
      </div>
    </div>  
  <?php }?>    
  <?php if(isset($items_search)){?>
    <div class="col-sm-3">
      <label class="text-right control-label col-form-label" style="font-size:12px;">Search In Items</label>
      <div class="input-group">
        <input type="text" name="items_search" id="items_search" class="form-control filter-indexs" placeholder="Write / Enter" required>
      </div>
    </div>  
  <?php }?>   
  <?php if(isset($shop_upload_types)){?>
    <div class="col-sm-3" id="shop_upload_typesFilter">
      <label class="text-right control-label col-form-label" style="font-size:12px;">Filter By Waiting Uploads</label>
      <select id="shop_upload_types" name="shop_upload_types" class="selectpicker show-menu-arrow form-control filter-indexs"  data-container="body" data-live-search="true" title="Select Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;">
          <option value="credit_demo">Contract Demo</option>
          <option value="lawyer_final">Final Contract</option>
          <option value="credit_final">Final Signed Contract</option>
          <option value="credit_demo_change">Change Contract Demo</option>
          <option value="lawyer_final_change">Change Final Contract</option>
          <option value="credit_final_change">Change Final Signed Contract</option>
      </select>
    </div>   
  <?php }?> 
</div>
<br>
<div class="row">
  <div class="col-lg-12"> 
    <input type="submit" name="submit" class="btn btn-cyan btn-lg" onclick="indexsFilter()" value="Generate" style="float:right;width:12%;">
  </div>
</div>
<br>
<div class="border-top"></div>
<br>
<br>
<?php $this->load->view('admin/html_parts/index_filters.js.php');?>
<?php if(isset($roles)){?>
<script type="text/javascript">
  let uroles = '<?php echo implode(',', $uroles) ?>'.split(",");
  $(document).ready(function(){
    indexsFilter();
    resetSelect('role_id',uroles);
    function resetSelect(select_id,value){
      $('.filter-indexs #'+select_id+'').selectpicker('val', value);
    }
  });  
</script>
<?php }?>