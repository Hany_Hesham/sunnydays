<?php 
  if (count($uploads) > 0) {
    foreach($uploads as $upload){
      $date = $upload['timestamp'];
      $exist = 1;
    }
  }else{
    $date = 'No Attachment';
      $exist = 0;
  }
?>
  <button type="button" class="btn btn-light btn-lg btn-circle no-print" 
          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="<?php echo $date?>">
          <?php if ($exist == 1):?>
            <i style="color:#008502;" class="fas fa-file-pdf"></i>
          <?php else: ?>
            <i style="color:#da542e;" class="fas fa-file-pdf"></i>
          <?php endif; ?>
  </button>
  <div class="dropdown-menu no-print">
    <?php foreach($uploads as $upload){ 
      $ext = pathinfo($upload['file_name'], PATHINFO_EXTENSION);?>
      <div>
        <?php if($ext=="tif"||$ext=="jpg"||$ext=="jpg"||$ext=="gif"||$ext=="PNG"||$ext=="png") {?>
          <a style="color:#212529" class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo base_url('assets/uploads/'.$folder.'/'.$upload['file_name']);?>"> 
            <i style="color:blue" class="fas fa-image"></i>&nbsp;<span><?php echo $upload['file_name']?></span>
          </a>
        <?php }else{?>
          <a style="color:#212529" class="btn default btn-outline" href="<?php echo base_url('assets/uploads/'.$folder.'/'.$upload['file_name']);?>" target="_heopenit"> 
            <i style="color:#28b779" class="fas fa-file-powerpoint"></i>&nbsp;<span><?php echo $upload['file_name']?></span>
          </a>
        <?php }?>  
        <div class="dropdown-divider"></div>
      </div>
    <?php }?>   
  </div>