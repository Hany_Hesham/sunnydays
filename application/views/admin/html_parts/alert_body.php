 <?php if($this->session->flashdata('msg') != ''): ?>
      <div class="row">
       <div class="col-6" style="float:right">
         <div class="sufee-alert alert with-close alert-<?php if($this->session->flashdata('alert')=='succsess'){echo'success';}else{echo 'danger';}?> alert-dismissible fade show">
              <span class="badge badge-pill badge-<?php if($this->session->flashdata('alert')=='succsess'){echo'success';}else{echo 'danger';}?>"><?= $this->session->flashdata('alert'); ?></span>
                <?= $this->session->flashdata('msg'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
       </div> 
      </div>
  <?php endif; ?>

  