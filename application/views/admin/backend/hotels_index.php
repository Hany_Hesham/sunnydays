    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Hotels</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Hotels</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>Add Hotel</strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="hotels-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="5%"><strong>#</strong></th>
                                  <th scope="col" width="15%"><strong>Hotel Name</strong></th>
                                  <th scope="col" width="15%"><strong>Group</strong></th>
                                  <th scope="col" width="10%"><strong>Code</strong></th>
                                  <th scope="col" width="10%"><strong>Disabled</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <?php 
         $select = '<select id="group_id" name="group_id" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Hotel Group" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="width: 100%; height:36px;" required>'; 
              foreach ($groups as $group) {
                  $select .= '<option value="'.$group['id'].'">'.$group['hotel_group'].'</option>';
                }
         $select .= '</select><br>';
         initiate_modal(
                         '','Hotel','admin/hotels/hotel_process',
                          '<input id="hotel_id" type="hidden" name="id">
                           <input  id="hotelName" type="text" name="hotel_name" class="form-control " placeholder="Hotel Name" required><br>
                            '. $select.'<br>
                           <input id="code" type="text" name="code" class="form-control" placeholder="Code" required><br><input id="logo" type="file"  name="logo" class="form-control" accept="image/*"/>
                           <br>'
                         )
  ?>

<script type="text/javascript">
      initDTable(
                 "hotels-list-table",
                    [{"name":"id"},
                     {"name":"hotel_name"},
                     {"name":"hotel_group"},
                     {"name":"code"},
                     {"name":"deleted"},],
                  "<?php echo base_url("admin/hotels/hotels_ajax/") ?>"
           );

      $(document).ready(function(){
          
          $(".edit-hotel").click(function() {
              var $row       = $(this).closest("tr");
              var hotel_id   = $row.find("#hotelId").text();
              var hotel_name = $row.find("#hotelName").text();
              var group_id   = $row.find("#group_id").text();
              var code       = $row.find("#hotelCode").text();
                 $(".modal-body #hotel_id").val(hotel_id );
                 $(".modal-body #hotelName").val( hotel_name );
                 $(".modal-body #group_id").val( group_id );
                 $('.modal-body #group_id').selectpicker('refresh');
                 $(".modal-body #code").val( code );
                 $(".modal-body #logo").val('');

              });

          $("#add-new").click(function() {
                 $(".modal-body #hotel_id").val('');
                 $(".modal-body #hotelName").val('');
                 $(".modal-body #group_id").val('');
                 $('.modal-body #group_id').selectpicker('refresh');
                 $(".modal-body #code").val('');
                 $(".modal-body #logo").val('');
            });

       });
      
</script>