    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">User Groups</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">User Groups</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <?php if($this->data['permission']['creat'] == 1){?>
                         <button id="add-new" data-toggle="modal" data-target="#smallmodal" class="btn btn-outline-primary btn-lg">
                                 <i class="fa fa-cloud"></i>&nbsp; <strong>Add User Group</strong></button> <br> 
                         <div class="dropdown-divider"></div><br><br>
                       <?php }?>
                         <table id="user_groups-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="5%"><strong>#</strong></th>
                                  <th scope="col" width="15%"><strong>User Group Name</strong></th>
                                  <th scope="col" width="10%"><strong>Disabled</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
<?php 
    initiate_modal(
        '','User Group','admin/user_groups/user_group_process',
        '<input id="userGroup_id" type="hidden" name="id"><input  id="userGroupName" type="text" name="name" class="form-control " placeholder="User Group Name" required><br><br>'
    )
?>
<script type="text/javascript">
    
    initDTable(
        "user_groups-list-table",
        [
            {"name":"id"},
            {"name":"name"},
            {"name":"deleted"},
        ],
        "<?php echo base_url("admin/user_groups/user_groups_ajax/") ?>"
    );

    $(document).ready(function(){      
        
        $(".edit-user_group").click(function() {
            var $row              = $(this).closest("tr");
            var user_groupId      = $row.find("#user_groupId").text();
            var user_groupName    = $row.find("#user_groupName").text();
            $(".modal-body #userGroup_id").val(user_groupId );
            $(".modal-body #userGroupName").val( user_groupName );
        });

        $("#add-new").click(function() {
            $(".modal-body #hotel_id").val('');
            $(".modal-body #hotelName").val('');
            $(".modal-body #group_id").val('');
            $('.modal-body #group_id').selectpicker('refresh');
            $(".modal-body #code").val('');
            $(".modal-body #logo").val('');
        });

    });
      
</script>