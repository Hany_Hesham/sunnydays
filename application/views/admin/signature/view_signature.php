<?php 
  initiate_breadcrumb(
    'Signature signature #'.$signature['id'].'',
    $signature['id'],//form_id
    $this->data['module']['id'],//module_id
    '',
    '',
    array(
      array('print' =>'jsPrinter'),
    ),//options
    array(array('name'=>'Signature signature','url'=>'admin/signature'))//locations
  );
?>
<div class="container-fluid">
  <?php initiate_alert();?> 
    <div class="row"  id="DivIdToPrint">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <div style="">
                <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/SR-Master.png');?>" alt="user">
              </div>
              <div class="" style="margin-top:30px;margin-left:50%;width:25% !important;">
                <h4 class="font-bold"><?php echo 'Signature signature';?>
                  <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'signature'));?> 
                </h4>
                <p class="text-muted m-l-30">
                  <strong>Form: </strong> <?php echo $signature['id']?>,
                  <br/><strong>Status: </strong> <?php echo  $signature['status_name']; ?>,
                  <br><strong>Created By: </strong> <?php echo $signature['fullname']?>,
                  <br><strong>Created At: </strong> <?php echo $signature['timestamp']?>,
                </p>
              </div>
            </div>
          </div>
        </div>
      <div id="mainPageBody"></div>
      <div id="signersItems"></div>
  </div>
</div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<?php $this->load->view('admin/signature/signature.js.php');?> 
<script type="text/javascript">
  getViewAjax('admin/signature','view_body_signature','<?php echo $signature['id']?>','mainPageBody');
  getViewAjax('admin/signature','signers_items','<?php echo ''.$signature['id']?>','signersItems');
</script>