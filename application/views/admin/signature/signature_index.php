<?php initiate_breadcrumb(
  'Signature signature',
  '',//form_id
  $this->data['module']['id'],//module_id
  $this->data['log_permission']['view'],
  $this->data['email_permission']['view'],
  'empty',//options
  array(array('name'=>'Signature signature','url'=>'empty'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <?php if($this->data['permission']['creat'] == 1){?>
            <a id="add-new" href="<?php echo base_url('admin/signature/add');?>" class="btn btn-outline-cyan btn-lg float-left">
            <strong><i class="fa fa-cloud"></i>&nbsp; Add Signature signature</strong></a> <br> 
            <br><div class="dropdown-divider"></div>
          <?php }?>
          <?php initiate_filters($this->data['module']['id'], $this->data['module']['name'], FALSE, FALSE, 1, 1)?>
          <h4 class="hidden" id="indexName">Signature signature</h4>
          <h4 class="card-title" id="totalCount"></h4><br>
          <table id="signature-list-table" class="table table-hover indexTable" style="width:100% !important;">
            <span id="tableFun" class="hidden">admin/signature/signature_ajax</span>
            <thead>
              <tr>
                <th class="tableCol" width="10%"  id="id"><strong>#</strong></th>
                <th class="tableCol" width="10%" id="name"><strong>Name</strong></th>
                <th class="tableCol" width="20%" id="module_id"><strong>Module</strong></th>
                <th class="tableCol" width="20%" id="hotel_id"><strong>Hotel</strong></th>
                <th class="tableCol" width="20%" id="dep_code"><strong>Department</strong></th>
                <th class="tableCol" width="20%" id="special"><strong>Special Column</strong></th>
                <th class="tableCol" width="20%" id="limitation"><strong>Cost</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  let totalRecords = '';
  $('#totalCount').append(''+totalRecords+'');
  let index      =  new tableConstructor(); 
  let indexDater =  {searchBy:{fromDate:index.fromDate,toDate:index.toDate,status_id:index.status_id}};
  let indexData  =  {reportName:index.indexName,total:index.total};
  initDTableButtons(
    ""+index.indexTable+"",
    index.tablCols,
    "<?php echo base_url()?>"+index.index_fun,
    indexDater,'',indexData
  );
  $(document).ready(function(){
    function resetSelect(select_id,value){
      $('.filter-indexs #'+select_id+'').selectpicker('val', value);
    }
  });  
</script>