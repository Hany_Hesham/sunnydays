
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Log Activity</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Log</li>
                               <li class="breadcrumb-item"><a href="<?php echo($this->agent->referrer());?>">Back</a></li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                         <table id="logs-list-table" class="table table-hover" style="width:100% !important;">
                             <thead >
                                <tr>
                                  <th scope="col" width="5%"><strong>#</strong></th>
                                  <th scope="col" width="10%"><strong>Action</strong></th>
                                  <th scope="col" width="15%"><strong>Log Time</strong></th>
                                  <th scope="col" width="10%"><strong>Made On</strong></th>
                                  <th scope="col" width="10%"><strong>Form Id</strong></th>
                                  <th scope="col" width="7%"><strong>UserName</strong></th>
                                  <th scope="col" width="10%"><strong>Comments</strong></th>
                                </tr>
                              </thead>
                              <tbody>
                      
                              </tbody>
                         </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
   <?php initiate_modal(
                         'lg','Log Detailed Data','',
                          ' <div class="table-responsive">
                               <table id="log-table" class="table table-hover">
                                <thead class="thead-light">
                                   <tr>
                                    <th scope="col" width="40%"><strong>Description</strong></th>
                                    <th scope="col" width="60%"><strong>Value</strong></th>
                                   </tr>
                                </thead>
                                <tbody>
                                 
                                </tbody>
                               </table>
                            </div>
                            '
                         )?>
    
<script type="text/javascript">
  initDTable(
                 "logs-list-table",
                    [{"name":"id"},
                     {"name":"action"},
                     {"name":"log_time"},
                     {"name":"target"},
                     {"name":"target_id"},
                     {"name":"fullname"},
                     {"name":"comments"},],
                  "<?php echo base_url("admin/log_activity/loged_ajax/".$user_id) ?>"
           );
  

  $('.modal-footer').hide();
    function getLogData(log_id){
      var url="<?php echo base_url('admin/log_activity/get_submenu/log_model/get_log/');?>";      
      $.ajax({
          url: url,
          dataType: "json",
          type : 'POST',
          data:{search:log_id},
            success: function(data){
               $('.modal-body #log-table tbody').empty();
                $.each(data, function(i,item){
                  if (data[i].id == undefined) {
                    $('.modal-body').append('<option value="'+ data[i].id +'">'+ data[i].key +'</option>');
                   }else{
                    $('.modal-body #log-table tbody').append('<tr><td><strong style="font-size:16px;">Log Id</strong></td><td class="text-success">'+data[i].id+'</td></tr> <tr><td><strong style="font-size:16px;">Log Time</strong></td><td class="text-success">'+data[i].log_time+'</td></tr> <tr><td><strong style="font-size:16px;">UserName</strong></td><td class="text-success">'+data[i].fullname+'</td></tr> <tr><td> <strong style="font-size:16px;">Transaction</strong></td><td class="text-success">'+data[i].action+'</td></tr>  <tr><td><strong style="font-size:16px;">Updated</strong></td><td class="text-cyan">'+data[i].module_name+' No #.'+data[i].target_id+' Item '+data[i].mini_target_id+'</td></tr> <tr><td><strong style="font-size:16px;">System Comment</strong></td> <td class="text-success">'+data[i].comments+'</td></tr> <tr><td><strong style="font-size:16px;">IP Address</strong></td> <td class="text-success">'+data[i].ip+'</td></tr> <tr><td><strong style="font-size:20px;">Old Data</strong></td><td><strong style="font-size:20px;">New Data</strong></td></tr>  <tr><td  id="oldData"></td><td id="newData"></td></tr>');
                     let allOldData = JSON.parse(data[i].data),allNewData = JSON.parse(data[i].new_data);
                        for(let row of Object.entries(allOldData)){
                            $('.modal-body #log-table tbody #oldData').append(''+row.toString().replace(',',': ')+'<br>');
                         }
                         for(let row of Object.entries(allNewData)){
                            $('.modal-body #log-table tbody #newData').append(''+row.toString().replace(',',': ')+'<br>');
                        }
                    }           
                });
              }
          });
        }
        
    </script>