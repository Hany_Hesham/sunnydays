<?php 
  $back_name = (isset($payment_types)? 'Payment Type #'.$payment_types['id'].'': 'Payment Type');
  $back_url  = (isset($payment_types)? 'setting/payment_types/view/'.$payment_types['id'].'':'setting/payment_types');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($payment_types)) {
      if (isset($copy)) {
        echo form_open(base_url('setting/payment_types/copy/'.$payment_types['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('setting/payment_types/edit/'.$payment_types['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('setting/payment_types/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type Name</label>
                <input type="text" class="form-control" id="type_name" name="type_name" placeholder="Type Name" value="<?php echo (isset($payment_types))? $payment_types['type_name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Extra Data</label>
                <input type="text" class="form-control" id="type_data" name="type_data" placeholder="Extra Data" value="<?php echo (isset($payment_types))? $payment_types['type_data']: '';?>">
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Rank</label>
                <input type="number" class="form-control" id="rank" name="rank" placeholder="Rank" value="<?php echo (isset($payment_types))? $payment_types['rank']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('setting/payment_types',$uploads,$this->data['module']['id'],$gen_id,'payment_types','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('setting/payment_types/payment_types.js.php');?>