<?php initiate_breadcrumb(
  'Payment Type #'.$payment_types['id'].'',
  $payment_types['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Payment Type Edit','url'=>'setting/payment_types/edit/'.$payment_types['id']),
    array('name'=>'Payment Type Copy','url'=>'setting/payment_types/copy/'.$payment_types['id'].'/1')
  ),//options
  array(array('name'=>'Payment Type','url'=>'setting/payment_types'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Payment Type #<?php echo $payment_types['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'payment_types'));?> 
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div id="mainPageBody"> </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('setting/payment_types','viewrates','<?php echo $payment_types['id']?>','mainPageBody');
</script>
