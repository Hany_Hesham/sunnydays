<?php initiate_breadcrumb('Payment Type',
  '',//form_id
  $this->data['module']['id'],//module_id
  $this->data['log_permission']['view'],
  $this->data['email_permission']['view'],
  'empty',//options
  array(array('name'=>'Payment Type','url'=>'empty'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <?php if($this->data['permission']['creat'] == 1){?>
            <a href="<?php echo base_url('setting/payment_types/add');?>" class="btn btn-outline-cyan btn-lg float-left">
              <strong><i class="fa fa-cloud"></i>&nbsp; Add Payment Type</strong>
            </a> <br><br>
            <div class="dropdown-divider"></div>
          <?php }?>
          <?php initiate_filters(
            $this->data['module']['id'],
            $this->data['module']['name'],
            FALSE,
            FALSE,
            True,
            True
          ); ?>
          <h4 class="hidden" id="indexName">Payment Type</h4>
          <h4 class="card-title" id="totalCount"></h4><br>
          <table id="payment_types-list-table" class="table table-hover indexTable" style="width:100% !important;">
            <span id="tableFun" class="hidden">setting/payment_types/payment_types_ajax</span>
            <thead>
              <tr>
                <th class="tableCol" width="5%"  style="font-size:13px;" id="id"><strong>#</strong></th>
                <th class="tableCol" width="20%" style="font-size:13px;" id="id"><strong>Form</strong></th>
                <th class="tableCol" width="20%" style="font-size:13px;" id="type_name"><strong>Type Name</strong></th>
                <th class="tableCol" width="20%" style="font-size:13px;" id="type_data"><strong>Extra Data</strong></th>
                <th class="tableCol" width="15%" style="font-size:13px;" id="rank"><strong>Rank</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  let totalRecords = '';
  $('#totalCount').append(''+totalRecords+'');
  let index      =  new tableConstructor(); 
  let indexDater =  {searchBy:{fromDate:index.fromDate,toDate:index.toDate}};
  let indexData  =  {reportName:index.indexName,total:index.total};
  initDTableButtons(
    ""+index.indexTable+"",
    index.tablCols,
    "<?php echo base_url()?>"+index.index_fun,
    indexDater,'',indexData
  );
</script>