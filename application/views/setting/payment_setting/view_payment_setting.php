<?php initiate_breadcrumb(
  'Payment Setting #'.$payment_setting['id'].'',
  $payment_setting['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Payment Setting Edit','url'=>'setting/payment_setting/edit/'.$payment_setting['id']),
    array('name'=>'Payment Setting Copy','url'=>'setting/payment_setting/copy/'.$payment_setting['id'].'/1')
  ),//options
  array(array('name'=>'Payment Setting','url'=>'setting/payment_setting'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
            <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Payment Setting #<?php echo $payment_setting['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'payment_setting'));?> 
              </h4>
              <p class="font-bold">
                <strong>Module Name: </strong> <?php echo $payment_setting['module_name']?>, 
                <br/><strong>Name: </strong> <?php echo $payment_setting['name']?>, 
                <br/><strong>Limit: </strong> <?php echo  $payment_setting['limit']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <div id="mainPageBody"> </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
</script>