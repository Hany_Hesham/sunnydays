<?php 
  $back_name = (isset($payment_setting)? 'Payment Setting #'.$payment_setting['id'].'': 'Payment Setting');
  $back_url  = (isset($payment_setting)? 'setting/payment_setting/view/'.$payment_setting['id'].'':'setting/payment_setting');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($payment_setting)) {
      if (isset($copy)) {
        echo form_open(base_url('setting/payment_setting/copy/'.$payment_setting['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('setting/payment_setting/edit/'.$payment_setting['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('setting/payment_setting/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Modules</label>
                <select  id="module_id" name="module_id" class="selectpicker module-select form-control"data-container="body" data-live-search="true" title="Select Module" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($modules as $module){?>
                    <option value="<?php echo $module['id']?>" <?php echo (isset($payment_setting) && ($payment_setting['module_id'] == $module['id']))? 'selected="selected"' : set_select('module_id',$module['id'] ); ?>><?php echo $module['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($payment_setting))? $payment_setting['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Limit</label>
                <input type="number" step="0.01" class="form-control" id="limit" name="limit" placeholder="Limit" value="<?php echo (isset($payment_setting))? $payment_setting['limit']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('setting/payment_setting',$uploads,$this->data['module']['id'],$gen_id,'payment_setting','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('setting/payment_setting/payment_setting.js.php');?>