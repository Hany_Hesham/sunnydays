<?php initiate_breadcrumb(
  'Employee Form #'.$employee['id'].'',
  $employee['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Employee Form Edit','url'=>'setting/employee/edit/'.$employee['id']),
    array('name'=>'Employee Form Copy','url'=>'setting/employee/copy/'.$employee['id'].'/1')
  ),//options
  array(array('name'=>'Employee Form','url'=>'setting/employee'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$employee['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Employee Form #<?php echo $employee['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'employee'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $employee['hotel_name']?>, 
                <br/><strong>Position Name: </strong> <?php echo $employee['position_name']?>, 
                <br/><strong>Created By: </strong> <?php echo  $employee['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $employee['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <div id="mainPageBody"> </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('setting/employee','viewrates','<?php echo $employee['id']?>','mainPageBody');
</script>
