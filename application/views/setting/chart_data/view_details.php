<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Code</strong>
              </td>
              <td>
                <?php echo $chart_data['type_name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $chart_data['type_data']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Rank</strong>
              </td>
              <td>
                <?php echo $chart_data['rank']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>