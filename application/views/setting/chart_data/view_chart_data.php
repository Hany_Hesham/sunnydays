<?php initiate_breadcrumb(
  'Chart Form #'.$chart_data['id'].'',
  $chart_data['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Chart Form Edit','url'=>'setting/chart_data/edit/'.$chart_data['id']),
    array('name'=>'Chart Form Copy','url'=>'setting/chart_data/copy/'.$chart_data['id'].'/1')
  ),//options
  array(array('name'=>'Chart Form','url'=>'setting/chart_data'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$chart_data['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Chart Form #<?php echo $chart_data['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'chart_data'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $chart_data['hotel_name']?>, 
              </p>
            </div>
          </div>
        </div>
      </div>
      <div id="mainPageBody"> </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('setting/chart_data','viewrates','<?php echo $chart_data['id']?>','mainPageBody');
</script>
