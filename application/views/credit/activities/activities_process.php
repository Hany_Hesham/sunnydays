<?php 
  $back_name = (isset($activities)? 'Monthly Activities Rent #'.$activities['id'].'': 'Monthly Activities Rent');
  $back_url  = (isset($activities)? 'credit/activities/view/'.$activities['id'].'':'credit/activities');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($activities)) {
      if (isset($copy)) {
        echo form_open(base_url('credit/activities/copy/'.$activities['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('credit/activities/edit/'.$activities['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('credit/activities/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($activities) && ($activities['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Month</label>
                <input type="text" class="form-control" id="month" data-toggle="datepicker" name="month" placeholder="yyyy-mm" value="<?php echo (isset($activities))? $activities['month']: '';?>" required>
              </div>
              <?php if (!isset($activities)) {?>
                <div class="col-sm-4 hidden" id="file_uploader">
                  <label class="text-right control-label col-form-label" style="font-size:12px;">
                    *File
                    <button type="button" class="btn btn-light btn-lg btn-circle no-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Download Sample">
                      <i style="color:#008502;" class="fas fa-file-excel"></i>
                    </button>
                    <div class="dropdown-menu no-print">
                      <div>
                        <a style="color:#212529" class="btn default btn-outline" href="<?php echo base_url('assets/uploads/activities/activities_item.csv');?>" download="activities_item.csv" target="_heopenit"> 
                          <i style="color:#28b779" class="fas fa-file-excel"></i>&nbsp;<span>Sample</span>
                        </a>
                        <div class="dropdown-divider"></div>
                      </div>
                    </div>
                  </label>
                  <input type="file"  name="file" class="form-control" accept="*"/>
                </div> 
                <div class="col-sm-4" id="uploaders">
                  <button type="button" class="btn btn-light btn-lg btn-circle no-print" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Download Sample" id="uploader"><i class="fas fa-file-excel"></i></button>
                  <label class="text-right control-label col-form-label" for="policy">Upload Items From a File</label>
                </div>
              <?php } ?>
            </div>
            <br>
            <br>
            <div class="row" id="item_table">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="5%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="activityAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="15%">Activity</th>
                        <th width="15%">Rent Percentage</th>
                        <th width="15%">Discount Percentage</th>
                        <th width="50%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="activity-data">
                      <?php if(isset($activities)){$i=1;$ids = '';foreach($activities_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <select id="activity<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Activity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][activity]" required>
                              <?php foreach ($activitiez as $activity): ?>
                                <option value="<?php echo $activity['id']?>" <?php echo ($row['activity'] == $activity['id'])? 'selected="selected"':''?>><?php echo $activity['type_name']?></option>
                              <?php endforeach; ?>
                            </select>
                          </td>          
                          <td>
                            <input id="rent<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][rent]" class="form-control" value="<?php echo $row['rent']?>"/>%
                          </td>
                          <td>
                            <input id="discount<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][discount]" class="form-control" value="<?php echo $row['discount']?>"/>%
                          </td> 
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($activities))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($activities))? $activities['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('credit/activities',$uploads,$this->data['module']['id'],$gen_id,'activities','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('credit/activities/activities.js.php');?>