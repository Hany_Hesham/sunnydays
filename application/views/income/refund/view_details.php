<?php $f = new NumberFormatter("ar", NumberFormatter::SPELLOUT);?>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Guest Name</strong>
              </td>
              <td>
                <?php echo $refund['name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Date of Payment</strong>
              </td>
              <td>
                <?php echo $refund['payment_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Payment</strong>
              </td>
              <td>
                <?php echo number_format($refund['payment'],2)?> <?php echo $refund['currency']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Payment Method</strong>
              </td>
              <td>
                <?php echo $refund['payment_method']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Acc No.</strong>
              </td>
              <td>
                <?php echo $refund['acc_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Total Refund Amount</strong>
              </td>
              <td>
                <?php echo number_format($refund['refund'],2)?> <?php echo $refund['currency']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Amount To Be Refund</strong>
              </td>
              <td>
                <?php echo number_format($refund['to_refund'],2)?> <?php echo $refund['currency']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Amount To Be Refund in Written</strong>
              </td>
              <td>
                <span><?php echo $f->format($refund['to_refund'])?> <?php echo $refund['currency']?></span>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Arrival Date</strong>
              </td>
              <td>
                <?php echo $refund['arrival']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Departure Date</strong>
              </td>
              <td>
                <?php echo $refund['departure']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Company/Travel Agent</strong>
              </td>
              <td>
                <?php echo $refund['company']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Nationality</strong>
              </td>
              <td>
                <?php echo $refund['nationality']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Explanation Reason Of Leave our Hotel</strong>
              </td>
              <td colspan="3">
                <?php echo $refund['reason']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $refund['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40">
        <div class="row">
          <div class="col-sm-12">
            <label class="text-right control-label col-form-label" style="font-size:12px;">Documents must be Attached</label>
            <ul>
              <li>Guest Folio</li>
              <li>Copy Of ID</li>
              <li>Copy of RC</li>
              <li>Booking Form or any other correspondence</li>
              <li>In case of online bookings >> the evidence of cancelation & booking is refundable</li>
            </ul>
         </div>
        </div>
      </div>
    </div>
  </div>
</div>