<?php 
  $back_name = (isset($late_out)? 'Late Check Out #'.$late_out['id'].'': 'Late Check Out');
  $back_url  = (isset($late_out)? 'income/late_out/view/'.$late_out['id'].'':'income/late_out');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($late_out)) {
      if (isset($copy)) {
        echo form_open(base_url('income/late_out/copy/'.$late_out['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('income/late_out/edit/'.$late_out['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('income/late_out/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($late_out) && ($late_out['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table id="selected-Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="5%" rowspan="2" colspan="1">
                            <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="late_outAdd()">
                              <i class="mdi mdi-database-plus"></i>
                            </button>
                          </th>
                          <th width="15%">Room</th>
                          <th width="15%">Time C/Out</th>
                          <th width="15%">Company</th>
                          <th width="50%">Remarks</th>
                        </tr>
                      </thead>
                      <tbody id="late_out-data">
                        <?php if(isset($late_out)){$i=1;$ids = '';foreach($late_out_items as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">          
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                            </td>        
                            <td class="text-center">
                              <input id="room<?php echo $i?>" type="number" name="items[<?php echo $i?>][room]" class="form-control" value="<?php echo $row['room']?>"/>
                            </td>   
                            <td>
                              <input id="check_out<?php echo $i?>" type="time" name="items[<?php echo $i?>][check_out]" class="form-control" value="<?php echo $row['check_out']?>"/>
                            </td>    
                            <td>
                              <input id="company<?php echo $i?>" type="text" name="items[<?php echo $i?>][company]" class="form-control" value="<?php echo $row['company']?>"/>
                            </td>    
                            <td>
                              <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                            </td>  
                          </tr>
                        <?php $i++;}}?>  
                        <tr class="hidden">
                          <td class="hidden" id="all-items-ids"></td>
                        </tr>
                      </tbody>
                    </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($late_out))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($late_out))? $late_out['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('income/late_out',$uploads,$this->data['module']['id'],$gen_id,'late_out','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('income/late_out/late_out.js.php');?>