<?php initiate_breadcrumb(
  'Speceial Rate #'.$speceial_rate['id'].'',
  $speceial_rate['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Speceial Rate Edit','url'=>'income/speceial_rate/edit/'.$speceial_rate['id']),
    array('name'=>'Speceial Rate Copy','url'=>'income/speceial_rate/copy/'.$speceial_rate['id'].'/1')
  ),//options
  array(array('name'=>'Speceial Rate','url'=>'income/speceial_rate'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$speceial_rate['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Speceial Rate #<?php echo $speceial_rate['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'speceial_rate'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $speceial_rate['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $speceial_rate['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $speceial_rate['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $speceial_rate['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($speceial_rate['reback']) && $speceial_rate['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($speceial_rate['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('income/speceial_rate','viewrates','<?php echo $speceial_rate['id']?>','mainPageBody');
  getViewAjax('income/speceial_rate','signers_items','<?php echo ''.$speceial_rate['id']?>','signersItems');
</script>
