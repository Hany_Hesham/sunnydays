<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">#</strong></th>
              <th width="9%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Room</strong></th>
              <th width="30%" rowspan="1" colspan="2"><strong style="font-size: 11pt;">Date</strong></th>
              <th width="20%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Travel Agent</strong></th>
              <th width="10%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Rate</strong></th>
              <th width="30%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
            <tr>
              <th width="15%" rowspan="1" colspan="1"><strong style="font-size: 11pt;">Checkin</strong></th>
              <th width="15%" rowspan="1" colspan="1"><strong style="font-size: 11pt;">Checkout</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($speceial_rate_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['checkin']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['checkout']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['travel_agent']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['rate'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan="3">
                <span><?php echo $speceial_rate['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>