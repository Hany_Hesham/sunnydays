<?php 
  $back_name = (isset($discrepancy)? 'Room Discrepancy #'.$discrepancy['id'].'': 'Room Discrepancy');
  $back_url  = (isset($discrepancy)? 'income/discrepancy/view/'.$discrepancy['id'].'':'income/discrepancy');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($discrepancy)) {
      if (isset($copy)) {
        echo form_open(base_url('income/discrepancy/copy/'.$discrepancy['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('income/discrepancy/edit/'.$discrepancy['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('income/discrepancy/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($discrepancy) && ($discrepancy['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*AM/PM</label>
                <select  id="part_id" name="part_id" class="selectpicker day-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($day_parts as $day_part){?>
                    <option value="<?php echo $day_part['id']?>" <?php echo (isset($discrepancy) && ($discrepancy['part_id'] == $day_part['id']))? 'selected="selected"':set_select('part_id',$day_part['id'] ); ?>><?php echo $day_part['type_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%" rowspan="2" colspan="1">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="discrepancyAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="10%">Room</th>
                        <th width="10%">F.O</th>
                        <th width="10%">H.K</th>
                        <th width="10%">F.O Check</th>
                        <th width="10%">H.K Check</th>
                        <th width="50%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="discrepancy-data">
                      <?php if(isset($discrepancy)){$i=1;$ids = '';foreach($discrepancy_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="room<?php echo $i?>" type="number" name="items[<?php echo $i?>][room]" class="form-control" value="<?php echo $row['room']?>"/>
                          </td>          
                          <td>
                            <input id="fo_data<?php echo $i?>" type="text" name="items[<?php echo $i?>][fo_data]" class="form-control" value="<?php echo $row['fo_data']?>"/>
                          </td>
                          <td>
                            <input id="hk_data<?php echo $i?>" type="text" name="items[<?php echo $i?>][hk_data]" class="form-control" value="<?php echo $row['hk_data']?>"/>
                          </td>
                          <td>
                            <input id="fo_check<?php echo $i?>" type="text" name="items[<?php echo $i?>][fo_check]" class="form-control" value="<?php echo $row['fo_check']?>"/>
                          </td> 
                          <td>
                            <input id="hk_check<?php echo $i?>" type="text" name="items[<?php echo $i?>][hk_check]" class="form-control" value="<?php echo $row['hk_check']?>"/>
                          </td> 
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($discrepancy))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($discrepancy))? $discrepancy['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('income/discrepancy',$uploads,$this->data['module']['id'],$gen_id,'discrepancy','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('income/discrepancy/discrepancy.js.php');?>