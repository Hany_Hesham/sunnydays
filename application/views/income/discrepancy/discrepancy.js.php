<script type="text/javascript">
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function discrepancyAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#discrepancy-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="room'+rowId+'" type="number" name="items['+rowId+'][room]" class="form-control" value=""/></td>     <td><input id="fo_data'+rowId+'" type="text" name="items['+rowId+'][fo_data]" class="form-control" value=""/></td>     <td><input id="hk_data'+rowId+'" type="text" name="items['+rowId+'][hk_data]" class="form-control" value=""/></td>     <td><input id="fo_check'+rowId+'" type="text" name="items['+rowId+'][fo_check]" class="form-control" value=""/></td>     <td><input id="hk_check'+rowId+'" type="text" name="items['+rowId+'][hk_check]" class="form-control" value=""/></td>     <td><textarea id="remarks'+rowId+'" name="items['+rowId+'][remarks]" class="form-control" rows="3"></textarea></td>     </tr>');
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'income/discrepancy/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>