<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">Room</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">F.o</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">H.K</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">F.O Check</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">H.K Check</strong></th>
              <th width="50%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; if (count($discrepancy_items) > 0){ foreach($discrepancy_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['fo_data']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['hk_data']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['fo_check']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['hk_check']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } } else{ ?> 
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td colspan="6">
                  <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo base_url('assets/images/Discrepancy.png');?>">
                    <img style="width:150%;height:60px;" src="<?php echo base_url('assets/images/Discrepancy.png');?>" alt="No Attach">
                  </a>
                </td>
              </tr>
            <?php } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan>Remarks</td>
              <td colspan="3">
                <span><?php echo $discrepancy['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>