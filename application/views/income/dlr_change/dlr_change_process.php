<?php 
  $back_name = (isset($dlr_change)? 'Daily Room Change #'.$dlr_change['id'].'': 'Daily Room Change');
  $back_url  = (isset($dlr_change)? 'income/dlr_change/view/'.$dlr_change['id'].'':'income/dlr_change');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($dlr_change)) {
      if (isset($copy)) {
        echo form_open(base_url('income/dlr_change/copy/'.$dlr_change['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('income/dlr_change/edit/'.$dlr_change['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('income/dlr_change/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($dlr_change) && ($dlr_change['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:200%;scroll-behavior:auto;">
                    <table id="selected-Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="1%" rowspan="2" colspan="1">
                            <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="dlr_changeAdd()">
                              <i class="mdi mdi-database-plus"></i>
                            </button>
                          </th>
                          <th width="6%">Arrival Date</th>
                          <th width="6%">Departure Date</th>
                          <th width="5%">Old Room</th>
                          <th width="8%">Old Room Type</th>
                          <th width="13%">Old Room Type Descreption</th>
                          <th width="5%">New Room</th>
                          <th width="8%">New Room Type</th>
                          <th width="13%">New Room Type Descreption</th>
                          <th width="17%">Notes</th>
                          <th width="18%">Remarks</th>
                        </tr>
                      </thead>
                      <tbody id="dlr_change-data">
                        <?php if(isset($dlr_change)){$i=1;$ids = '';foreach($dlr_change_items as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">          
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                            </td>        
                            <td>
                              <input id="arrival<?php echo $i?>" type="text" name="items[<?php echo $i?>][arrival]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value="<?php echo $row['arrival']?>">
                            </td>
                            <td>
                              <input id="departure<?php echo $i?>" type="text" name="items[<?php echo $i?>][departure]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value="<?php echo $row['departure']?>">
                            </td>                    
                            <td class="text-center">
                              <input id="old_room<?php echo $i?>" type="number" name="items[<?php echo $i?>][old_room]" class="form-control" value="<?php echo $row['old_room']?>"/>
                            </td>   
                            <td>
                              <input id="old_room_type<?php echo $i?>" type="text" name="items[<?php echo $i?>][old_room_type]" class="form-control" value="<?php echo $row['old_room_type']?>"/>
                            </td>    
                            <td>
                              <textarea id="old_room_descreption<?php echo $i?>" name="items[<?php echo $i?>][old_room_descreption]" class="form-control" rows="3"><?php echo $row['old_room_descreption']?></textarea>
                            </td>     
                            <td>
                              <input id="new_room<?php echo $i?>" type="number" name="items[<?php echo $i?>][new_room]" class="form-control" value="<?php echo $row['new_room']?>"/>
                            </td>
                            <td>
                              <input id="new_room_type<?php echo $i?>" type="text" name="items[<?php echo $i?>][new_room_type]" class="form-control" value="<?php echo $row['new_room_type']?>"/>
                            </td>    
                            <td>
                              <textarea id="new_room_descreption<?php echo $i?>" name="items[<?php echo $i?>][new_room_descreption]" class="form-control" rows="3"><?php echo $row['new_room_descreption']?></textarea>
                            </td> 
                            <td>
                              <textarea id="notes<?php echo $i?>" name="items[<?php echo $i?>][notes]" class="form-control" rows="3"><?php echo $row['notes']?></textarea>
                            </td> 
                            <td>
                              <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                            </td>  
                          </tr>
                        <?php $i++;}}?>  
                        <tr class="hidden">
                          <td class="hidden" id="all-items-ids"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($dlr_change))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($dlr_change))? $dlr_change['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('income/dlr_change',$uploads,$this->data['module']['id'],$gen_id,'dlr_change','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('income/dlr_change/dlr_change.js.php');?>