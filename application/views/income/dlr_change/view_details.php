<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Arrival Date</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Departure Date</strong></th>
              <th width="3%"><strong style="font-size: 11pt;">Old Room</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Old Room Type</strong></th>
              <th width="13%"><strong style="font-size: 11pt;">Old Room Type Descreption</strong></th>
              <th width="3%"><strong style="font-size: 11pt;">New Room</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">New Room Type</strong></th>
              <th width="13%"><strong style="font-size: 11pt;">New Room Type Descreption</strong></th>
              <th width="17%"><strong style="font-size: 11pt;">Notes</strong></th>
              <th width="18%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; if (count($dlr_change_items) > 0){ foreach($dlr_change_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['arrival']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['departure']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['old_room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['old_room_type']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['old_room_descreption']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['new_room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['new_room_type']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['new_room_descreption']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['notes']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } } else{ ?> 
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td colspan="10">
                  <a class="btn default btn-outline image-popup-vertical-fit el-link" href="<?php echo base_url('assets/images/Rrchange.png');?>">
                    <img style="width:150%;height:60px;" src="<?php echo base_url('assets/images/Rrchange.png');?>" alt="No Attach">
                  </a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan>Remarks</td>
              <td colspan="3">
                <span><?php echo $dlr_change['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>