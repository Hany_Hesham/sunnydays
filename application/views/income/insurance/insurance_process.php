<?php 
  $back_name = (isset($insurance)? 'Insurance insurance #'.$insurance['id'].'': 'Insurance insurance');
  $back_url  = (isset($insurance)? 'income/insurance/view/'.$insurance['id'].'':'income/insurance');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($insurance)) {
      if (isset($copy)) {
        echo form_open(base_url('income/insurance/copy/'.$insurance['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('income/insurance/edit/'.$insurance['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('income/insurance/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($insurance) && ($insurance['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Event</label>
                <input type="text" class="form-control" id="event" name="event" placeholder="Event" value="<?php echo (isset($insurance))? $insurance['event']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Event Date</label>
                <input type="text" class="form-control" id="event_date" data-toggle="datepicker" name="event_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($insurance))? $insurance['event_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Location</label>
                <input type="text" class="form-control" id="location" name="location" placeholder="Location" value="<?php echo (isset($insurance))? $insurance['location']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($insurance))? $insurance['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Insurance</label>
                <input type="number" step="0.01" class="form-control" id="insurance" name="insurance" placeholder="Insurance" value="<?php echo (isset($insurance))? $insurance['insurance']: '';?>" required> EGP
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*No. Of Guest</label>
                <input type="number" class="form-control" id="no_guest" name="no_guest" placeholder="No. Of Guest" value="<?php echo (isset($insurance))? $insurance['no_guest']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Reciet</label>
                <input type="text" class="form-control" id="reciet" name="reciet" placeholder="Reciet" value="<?php echo (isset($insurance))? $insurance['reciet']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Price per Person</label>
                <input type="number" step="0.01" class="form-control" id="price_person" name="price_person" placeholder="Price per Person" value="<?php echo (isset($insurance))? $insurance['price_person']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Payment Date</label>
                <input type="text" class="form-control" id="payment_date" data-toggle="datepicker" name="payment_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($insurance))? $insurance['payment_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Rent</label>
                <input type="number" step="0.01" class="form-control" id="rent" name="rent" placeholder="Rent" value="<?php echo (isset($insurance))? $insurance['rent']: '';?>" required> EGP
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Extra Persons</label>
                <input type="number" class="form-control" id="extra" name="extra" placeholder="Extra Persons" value="<?php echo (isset($insurance))? $insurance['extra']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Price for Extra person</label>
                <input type="number" step="0.01" class="form-control" id="extra_price" name="extra_price" placeholder="Price for Extra person" value="<?php echo (isset($insurance))? $insurance['extra_price']: '';?>" required> EGP
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Discount %</label>
                <input type="number" step="0.01" class="form-control" id="discount" name="discount" placeholder="Discount %" value="<?php echo (isset($insurance))? $insurance['discount']: '';?>" required> %
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($insurance))? $insurance['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('income/insurance',$uploads,$this->data['module']['id'],$gen_id,'insurance','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('income/insurance/insurance.js.php');?>