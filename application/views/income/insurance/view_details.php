<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Event</strong>
              </td>
              <td>
                <?php echo $insurance['event']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Event Date</strong>
              </td>
              <td>
                <?php echo $insurance['event_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $insurance['name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Insurance</strong>
              </td>
              <td>
                <?php echo number_format($insurance['insurance'],2)?> EGP
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Location</strong>
              </td>
              <td>
                <?php echo $insurance['location']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">No. Of Guest</strong>
              </td>
              <td>
                <?php echo $insurance['no_guest']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Reciet</strong>
              </td>
              <td>
                <?php echo $insurance['reciet']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Payment Date</strong>
              </td>
              <td>
                <?php echo $insurance['payment_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Price per Person</strong>
              </td>
              <td>
                <?php echo number_format($insurance['price_person'],2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Total Person Price</strong>
              </td>
              <td>
                <?php echo number_format(($insurance['price_person']*$insurance['no_guest']),2)?> EGP
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Rent</strong>
              </td>
              <td>
                <?php echo number_format($insurance['rent'],2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Total Price</strong>
              </td>
              <td>
                <?php echo number_format((($insurance['price_person']*$insurance['no_guest'])+$insurance['rent']),2)?> EGP
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Extra Persons</strong>
              </td>
              <td>
                <?php echo $insurance['extra']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Price for Extra person</strong>
              </td>
              <td>
                <?php echo $insurance['extra_price']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Total Extra Persons Price</strong>
              </td>
              <td>
                <?php echo number_format(($insurance['extra']*$insurance['extra_price']),2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Discount %</strong>
              </td>
              <td>
                <?php echo $insurance['discount']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Total Discount</strong>
              </td>
              <td>
                <?php echo number_format((($insurance['extra']*$insurance['extra_price'])*($insurance['discount']/100)),2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Remained Insurance</strong>
              </td>
              <td>
                <?php echo number_format(($insurance['insurance']-(($insurance['extra']*$insurance['extra_price'])*($insurance['discount']/100))),2)?> EGP
              </td>
            </tr>
            <tr>
              <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $insurance['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>