<?php initiate_breadcrumb('Insurance Refund',
  '',//form_id
  $this->data['module']['id'],//module_id
  $this->data['log_permission']['view'],
  $this->data['email_permission']['view'],
  'empty',//options
  array(array('name'=>'Insurance Refund','url'=>'empty'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <?php if($this->data['permission']['creat'] == 1){?>
            <a href="<?php echo base_url('income/insurance/add');?>" class="btn btn-outline-cyan btn-lg float-left">
              <strong><i class="fa fa-cloud"></i>&nbsp; Add Insurance Refund</strong>
            </a> <br><br>
            <div class="dropdown-divider"></div>
          <?php }?>
          <?php initiate_filters(
            $this->data['module']['id'],
            $this->data['module']['name'],
            $this->data['uhotels']
          ); ?>
          <h4 class="hidden" id="indexName">Insurance Refund</h4>
          <h4 class="card-title" id="totalCount"></h4><br>
          <table id="allowance-list-table" class="table table-hover indexTable" style="width:100% !important;">
            <span id="tableFun" class="hidden">income/insurance/insurance_ajax</span>
            <thead>
              <tr>
                <th class="tableCol" width="1%" style="font-size:13px;" id="id"><strong>#</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="id"><strong>Form</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="hid"><strong>Hotel Name</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="name"><strong>Guest Name</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="payment_date"><strong>Date of Payment</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="insurance"><strong>Insurance Amount</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="event"><strong>Event</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="event_date"><strong>Event Date</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="status"><strong>Status</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="role_id"><strong>Signature On</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="uid"><strong>Created By</strong></th>
                <th class="tableCol" width="9%" style="font-size:13px;" id="timestamp"><strong>Created At</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>