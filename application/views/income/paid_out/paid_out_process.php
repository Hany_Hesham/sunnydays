<?php 
  $back_name = (isset($paid_out)? 'Paid Out #'.$paid_out['id'].'': 'Paid Out');
  $back_url  = (isset($paid_out)? 'income/paid_out/view/'.$paid_out['id'].'':'income/paid_out');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($paid_out)) {
      if (isset($copy)) {
        echo form_open(base_url('income/paid_out/copy/'.$paid_out['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('income/paid_out/edit/'.$paid_out['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('income/paid_out/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($paid_out) && ($paid_out['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Guest Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Guest Name" value="<?php echo (isset($paid_out))? $paid_out['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date of Payment</label>
                <input type="text" class="form-control" id="payment_date" data-toggle="datepicker" name="payment_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($paid_out))? $paid_out['payment_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Total Paid Amount</label>
                <input type="number" step="0.01" class="form-control" id="payment" name="payment" placeholder="Total Paid Amount" value="<?php echo (isset($paid_out))? $paid_out['payment']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Currency</label>
                <select  id="currency" name="currency" class="selectpicker currency-select form-control"data-container="body" data-live-search="true" title="Select Currency" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($currencies as $currency){?>
                    <option value="<?php echo $currency['symbol']?>" <?php echo (isset($paid_out) && ($paid_out['currency'] == $currency['symbol']))? 'selected="selected"':set_select('currency',$currency['symbol'] ); ?>><?php echo $currency['symbol']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Payment Method</label>
                <input type="text" class="form-control" id="payment_method" name="payment_method" placeholder="Payment Method" value="<?php echo (isset($paid_out))? $paid_out['payment_method']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Room</label>
                <input type="number" class="form-control" id="room" name="room" placeholder="Room" value="<?php echo (isset($paid_out))? $paid_out['room']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Amount To Be Refund</label>
                <input type="number" step="0.01" class="form-control" id="to_refund" name="to_refund" placeholder="" value="<?php echo (isset($paid_out))? $paid_out['to_refund']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Amount To Be Refund in Written</label>
                <input type="text" class="form-control" id="written_to_refund" name="written_to_refund" placeholder="Amount To Be Refund in Written" value="<?php echo (isset($paid_out))? $paid_out['written_to_refund']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Company/Travel Agent</label>
                <input type="text" class="form-control" id="company" name="company" placeholder="Company/Travel Agent" value="<?php echo (isset($paid_out))? $paid_out['company']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Arrival Date</label>
                <input type="text" class="form-control" id="arrival" data-toggle="datepicker" name="arrival" placeholder="yyyy-mm-dd" value="<?php echo (isset($paid_out))? $paid_out['arrival']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Departure Date</label>
                <input type="text" class="form-control" id="departure" data-toggle="datepicker" name="departure" placeholder="yyyy-mm-dd" value="<?php echo (isset($paid_out))? $paid_out['departure']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Nationality</label>
                <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" value="<?php echo (isset($paid_out))? $paid_out['nationality']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Explanation Reason Of Leave our Hotel</label>
                <textarea name="reason" class="form-control" rows="3"><?php echo (isset($paid_out))? $paid_out['reason']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($paid_out))? $paid_out['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Documents must be Attached</label>
                <ul>
                  <li>Guest Folio</li>
                  <li>Copy Of ID</li>
                  <li>Copy of RC</li>
                  <li>Booking Form or any other correspondence</li>
                  <li>In case of online bookings >> the evidence of cancelation & booking is refundable</li>
                </ul>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('income/paid_out',$uploads,$this->data['module']['id'],$gen_id,'paid_out','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('income/paid_out/paid_out.js.php');?>