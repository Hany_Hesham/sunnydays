<?php initiate_breadcrumb(
  'Paid Out #'.$paid_out['id'].'',
  $paid_out['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Paid Out Edit','url'=>'income/paid_out/edit/'.$paid_out['id']),
    array('name'=>'Paid Out Copy','url'=>'income/paid_out/copy/'.$paid_out['id'].'/1')
  ),//options
  array(array('name'=>'Paid Out','url'=>'income/paid_out'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$paid_out['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Paid Out #<?php echo $paid_out['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'paid_out'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $paid_out['hotel_name']?>, 
                <br/><strong>Guest Name: </strong> <?php echo  $paid_out['name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $paid_out['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $paid_out['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $paid_out['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($paid_out['reback']) && $paid_out['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($paid_out['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('income/paid_out','viewrates','<?php echo $paid_out['id']?>','mainPageBody');
  getViewAjax('income/paid_out','signers_items','<?php echo ''.$paid_out['id']?>','signersItems');
</script>