<?php initiate_breadcrumb('Arrival Remarkes',
  '',//form_id
  $this->data['module']['id'],//module_id
  $this->data['log_permission']['view'],
  $this->data['email_permission']['view'],
  'empty',//options
  array(array('name'=>'Arrival Remarkes','url'=>'empty'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-body">
          <?php if($this->data['permission']['creat'] == 1){?>
            <a href="<?php echo base_url('income/remarkes/add');?>" class="btn btn-outline-cyan btn-lg float-left">
              <strong><i class="fa fa-cloud"></i>&nbsp; Add Arrival Remarkes</strong>
            </a> <br><br>
            <div class="dropdown-divider"></div>
          <?php }?>
          <?php initiate_filters(
            $this->data['module']['id'],
            $this->data['module']['name'],
            $this->data['uhotels']
          ); ?>
          <h4 class="hidden" id="indexName">Arrival Remarkes</h4>
          <h4 class="card-title" id="totalCount"></h4><br>
          <table id="allowance-list-table" class="table table-hover indexTable" style="width:100% !important;">
            <span id="tableFun" class="hidden">income/remarkes/remarkes_ajax</span>
            <thead>
              <tr>
                <th class="tableCol" width="2%"  style="font-size:13px;" id="id"><strong>#</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="id"><strong>Form</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="hid"><strong>Hotel Name</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="status"><strong>Status</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="role_id"><strong>Signature On</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="uid"><strong>Created By</strong></th>
                <th class="tableCol" width="14%" style="font-size:13px;" id="timestamp"><strong>Created At</strong></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>