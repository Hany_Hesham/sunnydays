<?php initiate_breadcrumb(
  'Arrival Remarkes #'.$remarkes['id'].'',
  $remarkes['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Arrival Remarkes Edit','url'=>'income/remarkes/edit/'.$remarkes['id']),
    array('name'=>'Arrival Remarkes Copy','url'=>'income/remarkes/copy/'.$remarkes['id'].'/1')
  ),//options
  array(array('name'=>'Arrival Remarkes','url'=>'income/remarkes'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$remarkes['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Arrival Remarkes #<?php echo $remarkes['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'remarkes'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $remarkes['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $remarkes['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $remarkes['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $remarkes['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($remarkes['reback']) && $remarkes['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($remarkes['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('income/remarkes','viewrates','<?php echo $remarkes['id']?>','mainPageBody');
  getViewAjax('income/remarkes','signers_items','<?php echo ''.$remarkes['id']?>','signersItems');
</script>
