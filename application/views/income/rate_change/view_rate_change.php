<?php initiate_breadcrumb(
  'Rate Change #'.$rate_change['id'].'',
  $rate_change['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Rate Change Edit','url'=>'income/rate_change/edit/'.$rate_change['id']),
    array('name'=>'Rate Change Copy','url'=>'income/rate_change/copy/'.$rate_change['id'].'/1')
  ),//options
  array(array('name'=>'Rate Change','url'=>'income/rate_change'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$rate_change['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Rate Change #<?php echo $rate_change['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'rate_change'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $rate_change['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $rate_change['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $rate_change['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $rate_change['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($rate_change['reback']) && $rate_change['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($rate_change['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('income/rate_change','viewrates','<?php echo $rate_change['id']?>','mainPageBody');
  getViewAjax('income/rate_change','signers_items','<?php echo ''.$rate_change['id']?>','signersItems');
</script>
