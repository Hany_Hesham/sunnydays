<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="2%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="24%"><strong style="font-size: 11pt;">Old Room</strong></th>
              <th width="24%"><strong style="font-size: 11pt;">New Room</strong></th>
              <th width="50%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($dr_change_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['old_room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['new_room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan>Remarks</td>
              <td colspan="3">
                <span><?php echo $dr_change['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>