<script type="text/javascript">
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm',
    viewMode: "months", 
    minViewMode: "months",
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function dr_changeAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#dr_change-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="old_room'+rowId+'" type="number" name="items['+rowId+'][old_room]" class="form-control" value=""/></td>     <td><input id="new_room'+rowId+'" type="number" name="items['+rowId+'][new_room]" class="form-control" value=""/></td>     <td><textarea id="remarks'+rowId+'" name="items['+rowId+'][remarks]" class="form-control" rows="3"></textarea></td>     </tr>');
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'income/dr_change/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>