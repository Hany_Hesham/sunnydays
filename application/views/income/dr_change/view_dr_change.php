<?php initiate_breadcrumb(
  'Room Change #'.$dr_change['id'].'',
  $dr_change['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Room Change Edit','url'=>'income/dr_change/edit/'.$dr_change['id']),
    array('name'=>'Room Change Copy','url'=>'income/dr_change/copy/'.$dr_change['id'].'/1')
  ),//options
  array(array('name'=>'Room Change','url'=>'income/dr_change'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$dr_change['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Room Change #<?php echo $dr_change['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'dr_change'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $dr_change['hotel_name']?>, 
                <br/><strong>Month: </strong> <?php echo  $dr_change['month']; ?>,
                <br/><strong>Status: </strong> <?php echo  $dr_change['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $dr_change['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $dr_change['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($dr_change['reback']) && $dr_change['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($dr_change['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('income/dr_change','viewrates','<?php echo $dr_change['id']?>','mainPageBody');
  getViewAjax('income/dr_change','signers_items','<?php echo ''.$dr_change['id']?>','signersItems');
</script>
