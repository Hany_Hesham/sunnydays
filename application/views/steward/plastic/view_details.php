<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="69%"><strong style="font-size: 11pt;">Item</strong></th>
              <th width="30%"><strong style="font-size: 11pt;">Quntity</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; $total=0; foreach($plastic_items as $item){ $total+=$item['quntity']; ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['item']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quntity']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
            <tr>
              <td colspan="2">
                  <strong style="font-size: 16pt;">Total</strong>
              </td>
              <td>
                  <strong style="font-size: 11pt;"><?php echo $total?></strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan="3">
                <span><?php echo $plastic['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>