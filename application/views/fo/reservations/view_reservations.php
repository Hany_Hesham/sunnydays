<?php initiate_breadcrumb(
  'Reservation Form #'.$reservations['id'].'',
  $reservations['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Reservation Form Edit','url'=>'fo/reservations/edit/'.$reservations['id']),
    array('name'=>'Reservation Form Copy','url'=>'fo/reservations/copy/'.$reservations['id'].'/1')
  ),//options
  array(array('name'=>'Reservation Form','url'=>'fo/reservations'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$reservations['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Reservation Form #<?php echo $reservations['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'reservations'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $reservations['hotel_name']?>, 
                <br/><strong>Name: </strong> <?php echo  $reservations['name']; ?>,
                <br/><strong>Type: </strong> <?php echo  $reservations['type_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $reservations['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $reservations['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $reservations['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($reservations['reback']) && $reservations['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($reservations['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('fo/reservations','viewrates','<?php echo $reservations['id']?>','mainPageBody');
  getViewAjax('fo/reservations','signers_items','<?php echo ''.$reservations['id']?>','signersItems');
</script>
