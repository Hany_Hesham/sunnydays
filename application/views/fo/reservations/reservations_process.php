<?php 
  $back_name = (isset($reservations)? 'Reservation Form #'.$reservations['id'].'': 'Reservation Form');
  $back_url  = (isset($reservations)? 'fo/reservations/view/'.$reservations['id'].'':'fo/reservations');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($reservations)) {
      if (isset($copy)) {
        echo form_open(base_url('fo/reservations/copy/'.$reservations['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('fo/reservations/edit/'.$reservations['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('fo/reservations/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($reservations) && ($reservations['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Type</label>
                <select  id="type_id" name="type_id" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($types as $type){?>
                    <option value="<?php echo $type['id']?>" <?php echo (isset($reservations) && ($reservations['type_id'] == $type['id']))? 'selected="selected"':set_select('hid',$type['id'] ); ?>><?php echo $type['type_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($reservations))? $reservations['name']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Contact Person</label>
                <input type="text" class="form-control" id="contact" name="contact" placeholder="Contact Person" value="<?php echo (isset($reservations))? $reservations['contact']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Tel</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Tel" value="<?php echo (isset($reservations))? $reservations['phone']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Fax</label>
                <input type="text" class="form-control" id="fax" name="fax" placeholder="Fax" value="<?php echo (isset($reservations))? $reservations['fax']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Arrival Date</label>
                <input type="text" class="form-control" id="arrival" data-toggle="datepicker" name="arrival" placeholder="yyyy-mm-dd" value="<?php echo (isset($reservations))? $reservations['arrival']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Departure Date</label>
                <input type="text" class="form-control" id="departure" data-toggle="datepicker" name="departure" placeholder="yyyy-mm-dd" value="<?php echo (isset($reservations))? $reservations['departure']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Co./Agent</label>
                <input type="text" class="form-control" id="agent" name="agent" placeholder="Co./Agent" value="<?php echo (isset($reservations))? $reservations['agent']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="reservationsAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="9%">No. of Rooms</th>
                        <th width="30%">Room Type</th>
                        <th width="10%">Rate</th>
                        <th width="50%">Remarks</th>
                      </tr>
                    </thead>
                    <tbody id="reservations-data">
                      <?php if(isset($reservations)){$i=1;$ids = '';foreach($reservations_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="room_no<?php echo $i?>" type="number" name="items[<?php echo $i?>][room_no]" class="form-control" value="<?php echo $row['room_no']?>"/>
                          </td>          
                          <td>
                            <select id="type_id<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Room Type" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][type_id]" required>
                              <?php foreach ($room_types as $room_type): ?>
                                <option value="<?php echo $room_type['id']?>" <?php echo ($row['type_id'] == $room_type['id'])? 'selected="selected"':''?>><?php echo $room_type['type_name']?></option>
                              <?php endforeach; ?>
                            </select>
                          </td> 
                          <td>
                            <input id="rate<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][rate]" class="form-control" value="<?php echo $row['rate']?>"/> EGP
                          </td> 
                          <td>
                            <textarea id="remarks<?php echo $i?>" name="items[<?php echo $i?>][remarks]" class="form-control" rows="3"><?php echo $row['remarks']?></textarea>
                          </td>  
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($reservations))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Method of Payment</label>
                <input type="text" class="form-control" id="payment_method" name="payment_method" placeholder="Method of Payment" value="<?php echo (isset($reservations))? $reservations['payment_method']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Credit Card NO.</label>
                <input type="text" class="form-control" id="credit_card" name="credit_card" placeholder="Credit Card NO." value="<?php echo (isset($reservations))? $reservations['credit_card']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Voucher NO.</label>
                <input type="text" class="form-control" id="voucher" name="voucher" placeholder="Voucher NO." value="<?php echo (isset($reservations))? $reservations['voucher']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Cash Receipt</label>
                <input type="text" class="form-control" id="receipt" name="receipt" placeholder="Cash Receipt" value="<?php echo (isset($reservations))? $reservations['receipt']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Special Instruction</label>
                <textarea name="instruction" class="form-control" rows="3"><?php echo (isset($reservations))? $reservations['instruction']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($reservations))? $reservations['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('fo/reservations',$uploads,$this->data['module']['id'],$gen_id,'reservations','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('fo/reservations/reservations.js.php');?>