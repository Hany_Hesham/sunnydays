<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Co./Agent</strong>
              </td>
              <td>
                <?php echo $reservations['agent']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Contact Person</strong>
              </td>
              <td>
                <?php echo $reservations['contact']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Tel</strong>
              </td>
              <td>
                <?php echo $reservations['phone']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Fax</strong>
              </td>
              <td>
                <?php echo $reservations['fax']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Arrival Date</strong>
              </td>
              <td>
                <?php echo $reservations['arrival']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Departure Date</strong>
              </td>
              <td>
                <?php echo $reservations['departure']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">No. of Rooms</strong></th>
              <th width="30%"><strong style="font-size: 11pt;">Room Type</strong></th>
              <th width="10%"><strong style="font-size: 11pt;">Rate</strong></th>
              <th width="50%"><strong style="font-size: 11pt;">Remarks</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($reservations_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['room_no']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['room_type']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['rate'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['remarks']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Method of Payment</strong>
              </td>
              <td>
                <?php echo $reservations['payment_method']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Credit Card NO.</strong>
              </td>
              <td>
                <?php echo $reservations['credit_card']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Voucher NO.</strong>
              </td>
              <td>
                <?php echo $reservations['voucher']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Cash Receipt</strong>
              </td>
              <td>
                <?php echo $reservations['receipt']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Special Instruction</strong>
              </td>
              <td colspan="3">
                <span><?php echo $reservations['instruction']?></span>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $reservations['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>