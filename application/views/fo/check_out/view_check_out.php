<?php initiate_breadcrumb(
  'Free Late Check Out authorization Form #'.$check_out['id'].'',
  $check_out['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Free Late Check Out authorization Form Edit','url'=>'fo/check_out/edit/'.$check_out['id']),
    array('name'=>'Free Late Check Out authorization Form Copy','url'=>'fo/check_out/copy/'.$check_out['id'].'/1')
  ),//options
  array(array('name'=>'Free Late Check Out authorization Form','url'=>'fo/check_out'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$check_out['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Free Late Check Out authorization Form #<?php echo $check_out['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'check_out'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $check_out['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $check_out['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $check_out['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $check_out['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($check_out['reback']) && $check_out['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($check_out['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('fo/check_out','viewrates','<?php echo $check_out['id']?>','mainPageBody');
  getViewAjax('fo/check_out','signers_items','<?php echo ''.$check_out['id']?>','signersItems');
</script>
