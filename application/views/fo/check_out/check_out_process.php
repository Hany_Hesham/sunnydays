<?php 
  $back_name = (isset($check_out)? 'Free Late Check Out authorization Form #'.$check_out['id'].'': 'Free Late Check Out authorization Form');
  $back_url  = (isset($check_out)? 'fo/check_out/view/'.$check_out['id'].'':'fo/check_out');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($check_out)) {
      if (isset($copy)) {
        echo form_open(base_url('fo/check_out/copy/'.$check_out['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('fo/check_out/edit/'.$check_out['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('fo/check_out/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($check_out) && ($check_out['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Room.</label>
                <input type="text" class="form-control" id="room_no" name="room_no" placeholder="Room." value="<?php echo (isset($check_out))? $check_out['room_no']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($check_out))? $check_out['name']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Travel Agent</label>
                <input type="text" class="form-control" id="travel_agent" name="travel_agent" placeholder="Travel Agent" value="<?php echo (isset($check_out))? $check_out['travel_agent']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Arrival Date</label>
                <input type="text" class="form-control" id="arrival" data-toggle="datepicker" name="arrival" placeholder="yyyy-mm-dd" value="<?php echo (isset($check_out))? $check_out['arrival']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Departure Date</label>
                <input type="text" class="form-control" id="departure" data-toggle="datepicker" name="departure" placeholder="yyyy-mm-dd" value="<?php echo (isset($check_out))? $check_out['departure']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Departure Time</label>
                <input type="time" class="form-control" id="departure_time" name="departure_time" style="height: 43% !important;width:100%;" placeholder="Departure Time" value="<?php echo (isset($check_out))? $check_out['departure_time']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Reasons</label>
                <textarea name="reasons" class="form-control" rows="3"><?php echo (isset($check_out))? $check_out['reasons']: '';?></textarea>
              </div>
              <div class="col-sm-6">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($check_out))? $check_out['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('fo/check_out',$uploads,$this->data['module']['id'],$gen_id,'check_out','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('fo/check_out/check_out.js.php');?>