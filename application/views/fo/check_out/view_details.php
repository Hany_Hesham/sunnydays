<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Room.</strong>
              </td>
              <td>
                <?php echo $check_out['room_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $check_out['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Arrival Date</strong>
              </td>
              <td>
                <?php echo $check_out['arrival']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Departure Date</strong>
              </td>
              <td>
                <?php echo $check_out['departure']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Travel Agent</strong>
              </td>
              <td>
                <?php echo $check_out['travel_agent']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Departure Time</strong>
              </td>
              <td>
                <?php echo $check_out['departure_time']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Reasons</strong>
              </td>
              <td colspan="3">
                <?php echo $check_out['reasons']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $check_out['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>