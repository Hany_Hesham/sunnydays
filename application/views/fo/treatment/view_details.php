<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Room</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">Name</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">Travel Agent</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Pax</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Arrival</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Departure</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Status</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">VIP</strong></th>
              <th width="9%"><strong style="font-size: 11pt;">Reason</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Time</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Amenities</strong></th>
              <th width="8%"><strong style="font-size: 11pt;">Breakfast</strong></th>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($treatment_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['room']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['travel_agent']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['pax']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['arrival']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['departure']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['guest_status']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['vip_type']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['reason']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['timing']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo get_meta_datas($item['amenity'])?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['breakfast']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td colspan>Remarks</td>
              <td colspan="3">
                <span><?php echo $treatment['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>