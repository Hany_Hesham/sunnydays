<?php 
  $select = '';
  foreach ($amenitys as $amenity) {
    $select .= '<option value="'.$amenity['id'].'">'.$amenity['type_name'].'</option>';
  }

  $select1 = '';
  foreach ($vip_types as $vip_type) {
    $select1 .= '<option value="'.$vip_type['id'].'">'.$vip_type['type_name'].'</option>';
  }

  $select2 = '';
  foreach ($guest_statuss as $guest_status) {
    $select2 .= '<option value="'.$guest_status['id'].'">'.$guest_status['type_name'].'</option>';
  }
?>
<script type="text/javascript">
  let select    = '<?php echo $select?>';
  let select1   = '<?php echo $select1?>';
  let select2   = '<?php echo $select2?>';
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function treatmentAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#treatment-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="room'+rowId+'" type="number" name="items['+rowId+'][room]" class="form-control" value=""/></td>     <td><input id="name'+rowId+'" type="text" name="items['+rowId+'][name]" class="form-control" value=""/></td>     <td><input id="travel_agent'+rowId+'" type="text" name="items['+rowId+'][travel_agent]" class="form-control" value=""/></td>     <td><input id="pax'+rowId+'" type="number" name="items['+rowId+'][pax]" class="form-control" value=""/></td>     <td><input id="arrival'+rowId+'" type="date" name="items['+rowId+'][arrival]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value=""></td>     <td><input id="departure'+rowId+'" type="date" name="items['+rowId+'][departure]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value=""></td>     <td><select id="status_id'+rowId+'" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items['+rowId+'][status_id]" required>'+select2+'</select></td>     <td><select id="vip_id'+rowId+'" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select VIP" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items['+rowId+'][vip_id]" required>'+select1+'</select></td>     <td><textarea id="reason'+rowId+'" name="items['+rowId+'][reason]" class="form-control" rows="3"></textarea></td>     <td><input id="timing'+rowId+'" type="time" name="items['+rowId+'][timing]" class="form-control" value=""/></td>     <td><select id="amenity'+rowId+'" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Amenity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items['+rowId+'][amenity][]" multiple required>'+select+'</select></td>     <td><input id="breakfast'+rowId+'" type="text" name="items['+rowId+'][breakfast]" class="form-control" value=""/></td>     </tr>');
    $(document).ready(function(){
      $('.selectpicker').selectpicker('render');
      $('.selectpicker').selectpicker('setStyle','btn btn-dark');
    });
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'fo/treatment/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>