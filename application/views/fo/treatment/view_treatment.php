<?php initiate_breadcrumb(
  'Treatment Order #'.$treatment['id'].'',
  $treatment['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Treatment Order Edit','url'=>'fo/treatment/edit/'.$treatment['id']),
    array('name'=>'Treatment Order Copy','url'=>'fo/treatment/copy/'.$treatment['id'].'/1')
  ),//options
  array(array('name'=>'Treatment Order','url'=>'fo/treatment'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$treatment['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Treatment Order #<?php echo $treatment['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'treatment'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $treatment['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $treatment['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $treatment['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $treatment['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($treatment['reback']) && $treatment['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($treatment['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('fo/treatment','viewrates','<?php echo $treatment['id']?>','mainPageBody');
  getViewAjax('fo/treatment','signers_items','<?php echo ''.$treatment['id']?>','signersItems');
</script>
