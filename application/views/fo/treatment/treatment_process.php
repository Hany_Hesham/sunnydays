<?php 
  $back_name = (isset($treatment)? 'Treatment Order #'.$treatment['id'].'': 'Treatment Order');
  $back_url  = (isset($treatment)? 'fo/treatment/view/'.$treatment['id'].'':'fo/treatment');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($treatment)) {
      if (isset($copy)) {
        echo form_open(base_url('fo/treatment/copy/'.$treatment['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('fo/treatment/edit/'.$treatment['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('fo/treatment/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($treatment) && ($treatment['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:200%;scroll-behavior:auto;">
                    <table id="selected-Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="1%">
                            <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="treatmentAdd()">
                              <i class="mdi mdi-database-plus"></i>
                            </button>
                          </th>
                          <th width="8%">Room</th>
                          <th width="9%">Name</th>
                          <th width="9%">Travel Agent</th>
                          <th width="8%">Pax</th>
                          <th width="8%">Arrival</th>
                          <th width="8%">Departure</th>
                          <th width="8%">Status</th>
                          <th width="8%">VIP</th>
                          <th width="9%">Reason</th>
                          <th width="8%">Time</th>
                          <th width="8%">Amenities</th>
                          <th width="8%">Breakfast</th>
                        </tr>
                      </thead>
                      <tbody id="treatment-data">
                        <?php if(isset($treatment)){$i=1;$ids = '';foreach($treatment_items as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">          
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                            </td>                            
                            <td class="text-center">
                              <input id="room<?php echo $i?>" type="number" name="items[<?php echo $i?>][room]" class="form-control" value="<?php echo $row['room']?>"/>
                            </td>          
                            <td>
                              <input id="name<?php echo $i?>" type="text" name="items[<?php echo $i?>][name]" class="form-control" value="<?php echo $row['name']?>"/>
                            </td>
                            <td>
                              <input id="travel_agent<?php echo $i?>" type="text" name="items[<?php echo $i?>][travel_agent]" class="form-control" value="<?php echo $row['travel_agent']?>"/>
                            </td>
                            <td>
                              <input id="pax<?php echo $i?>" type="number" name="items[<?php echo $i?>][pax]" class="form-control" value="<?php echo $row['pax']?>"/>
                            </td>
                            <td>
                              <input id="arrival<?php echo $i?>" type="text" name="items[<?php echo $i?>][arrival]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value="<?php echo $row['arrival']?>">
                            </td>
                            <td>
                              <input id="departure<?php echo $i?>" type="text" name="items[<?php echo $i?>][departure]" class="form-control" data-toggle="datepicker" placeholder="yyyy-mm-dd" value="<?php echo $row['departure']?>">
                            </td>
                            <td>
                              <select id="status_id<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][status_id]" required>
                                <?php foreach ($guest_statuss as $guest_status): ?>
                                  <option value="<?php echo $guest_status['id']?>" <?php echo ($row['status_id'] == $guest_status['id'])? 'selected="selected"':''?>><?php echo $guest_status['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>
                            <td>
                              <select id="vip_id<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select VIP" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][vip_id]" required>
                                <?php foreach ($vip_types as $vip_type): ?>
                                  <option value="<?php echo $vip_type['id']?>" <?php echo ($row['vip_id'] == $vip_type['id'])? 'selected="selected"':''?>><?php echo $vip_type['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>
                            <td>
                              <textarea id="reason<?php echo $i?>" name="items[<?php echo $i?>][reason]" class="form-control" rows="3"><?php echo $row['reason']?></textarea>
                            </td>
                            <td>
                              <input id="timing<?php echo $i?>" type="time" name="items[<?php echo $i?>][timing]" class="form-control" value="<?php echo $row['timing']?>"/>
                            </td>
                            <td>
                              <select id="amenity<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Amenity" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][amenity][]" multiple required>
                                <?php foreach ($amenitys as $amenity): ?>
                                  <option value="<?php echo $amenity['id']?>"  <?php echo (in_array($amenity['id'], json_decode($row['amenity'])))? 'selected="selected"' : set_select('amenity[]',$amenity['id'] ); ?>><?php echo $amenity['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>
                            <td>
                              <input id="breakfast<?php echo $i?>" type="text" name="items[<?php echo $i?>][breakfast]" class="form-control" value="<?php echo $row['breakfast']?>"/>
                            </td>  
                          </tr>
                        <?php $i++;}}?>  
                        <tr class="hidden">
                          <td class="hidden" id="all-items-ids"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($treatment))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($treatment))? $treatment['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('fo/treatment',$uploads,$this->data['module']['id'],$gen_id,'treatment','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('fo/treatment/treatment.js.php');?>