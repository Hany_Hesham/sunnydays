<?php initiate_breadcrumb(
  'Room/Rate Change #'.$rr_change['id'].'',
  $rr_change['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Room/Rate Change Edit','url'=>'fo/rr_change/edit/'.$rr_change['id']),
    array('name'=>'Room/Rate Change Copy','url'=>'fo/rr_change/copy/'.$rr_change['id'].'/1')
  ),//options
  array(array('name'=>'Room/Rate Change','url'=>'fo/rr_change'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$rr_change['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Room/Rate Change #<?php echo $rr_change['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'rr_change'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $rr_change['hotel_name']?>, 
                <br/><strong>Name: </strong> <?php echo  $rr_change['name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $rr_change['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $rr_change['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $rr_change['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($rr_change['reback']) && $rr_change['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($rr_change['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('fo/rr_change','viewrates','<?php echo $rr_change['id']?>','mainPageBody');
  getViewAjax('fo/rr_change','signers_items','<?php echo ''.$rr_change['id']?>','signersItems');
</script>
