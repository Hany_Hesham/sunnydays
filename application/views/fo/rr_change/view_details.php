<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">From Room</strong>
              </td>
              <td>
                <?php echo $rr_change['from_room']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">To Room</strong>
              </td>
              <td>
                <?php echo $rr_change['to_room']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">From Rate</strong>
              </td>
              <td>
                <?php echo number_format($rr_change['from_rate'],2)?>
              </td>
              <td>
                <strong style="font-size: 11pt;">To Rate</strong>
              </td>
              <td>
                <?php echo number_format($rr_change['to_rate'],2)?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $rr_change['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>