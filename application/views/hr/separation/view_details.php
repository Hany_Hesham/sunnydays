<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Department</strong>
              </td>
              <td>
                <?php echo  ($separation['dep_id'])? $separation['depart_name']:$separation['dep_name'] ; ?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Position Name</strong>
              </td>
              <td>
                <?php echo  ($separation['pos_id'])? $separation['pos_name']:$separation['position_name'] ; ?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Clock No#</strong>
              </td>
              <td>
                <?php echo $separation['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $separation['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Date Of Hiring</strong>
              </td>
              <td>
                <?php echo $separation['hiring_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Last day of work</strong>
              </td>
              <td>
                <?php echo $separation['last_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Identity Card No</strong>
              </td>
              <td>
                <?php echo $separation['card_id']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Registration Office</strong>
              </td>
              <td>
                <?php echo $separation['registration']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $separation['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold" dir="rtl">
          <tbody>
            <tr>
              <td style="text-align: center;">
                <strong style="font-size: 11pt;">إقـــــــرار مخالصــــة نهائـــــــية</strong>
              </td>
            </tr>
            <tr>
              <td>
                <p style="text-align: right;">أقر انا الموقع ادناه  <?php echo $separation['name']?> بطاقة ع/ش رقم  <?php echo $separation['card_id']?> سجل مدنى   <?php echo $separation['registration']?></p> 
                <br>
                <p style="text-align: right;"> اقر بموجب هذا بأنه بناء على انتهاء خدمتى لدى فندق  <?php echo $separation['hotel_name']?> بتاريخ   <?php echo $separation['last_date']?></p>
                <br>
                <p style="text-align: right;">فإننى قد استلمت جميع مستحقاتى القانونية لدى الفندق وليس لى أى مستحقات أخرى من  أى نوع  كان ، وبذلك أعطى مخالصة تامة ونهائية عن جميع مستحقاتى وأتعهد بعدم الرجوع على الفندق بأى مطالبة أو أى إدعاء مهما كان نوعه بمناسبة عملى أو بمناسبة إنتهاء هذا العمل .</p>
                <br>
                <p style="text-align: right;">تحررت هذه المخالصة النهائية والشاملة .</p>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-3" style="width: 25% !important;">
      <div class="card">
        <div class="card-header badge-dark after-hover">
          <h6 class="card-title float-left">المقر بما فيه </h6>
          <ul class="navbar-nav float-right mr-auto">
            <li class="nav-item dropdown">
              <span class="wait-hover">
                <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
              </span>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#"></a>
                <div class="dropdown-divider"></div>
              </div>
            </li>
          </ul>
        </div>
        <div class="card-body centered">
          <h5 class="card-title centered"></h5>
          <div>
            <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
          </div>
          <h5></h5>
          <h6></h6>
        </div>
      </div>
    </div>
  </div>
</div>