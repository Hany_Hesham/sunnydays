<script type="text/javascript">
  
  $('#submitRS').show();

  $('#warningMessage').hide();

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  <?php if (!isset($separation)):?>
    $('#clock_no, #hid').on('change',function () {
      $('#name').val('');
      $('#department_name').val('');
      $('#dep_id').val('');
      $('#position_name').val('');
      $('#pos_id').val('');
      $('#hiring_date').val('');
      $('#card_id').val('');
      $('#last_date').val('');
      employeeSearch();
    });
  <?php endif; ?>

  function employeeSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    let hid   = $('#hid').val();
    let empNo = $('#clock_no').val();
    if (hid != '' && empNo !='') {
      var url =getUrl()+'hr/separation/search_APIEmployee/'+hid+'/'+empNo;      
      $.post(url).done(function(response) {
        response = JSON.parse(response);
        if(response, $.each(response, function() {
          $('#name').val(response.emp_ename);
          $('#department_name').val(response.dep_name);
          $('#dep_id').val(response.emp_did);
          $('#position_name').val(response.position_name);
          $('#pos_id').val(response.emp_pid);
          let result = response.emp_hiringdate.split(' ');
          $('#hiring_date').val(result[0]);
          let result1 = response.emp_contractdate.split(' ');
          $('#last_date').val(result1[0]);
          $('#card_id').val(response.emp_id);
          $('#name').prop('readonly', true);
          $('#department_name').prop('readonly', true);
          $('#position_name').prop('readonly', true);
          $('#hiring_date').prop('readonly', true);
          $('#last_date').prop('readonly', true);
        }));
      });
    }
  }
  
</script>