<?php initiate_breadcrumb(
  'Application For Employment #'.$employment['id'].'',
  $employment['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Application For Employment Edit','url'=>'hr/employment/edit/'.$employment['id']),
    array('name'=>'Application For Employment Copy','url'=>'hr/employment/copy/'.$employment['id'].'/1')
  ),//options
  array(array('name'=>'Application For Employment','url'=>'hr/employment'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$employment['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Application For Employment #<?php echo $employment['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'employment'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $employment['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo  $employment['dep_name']; ?>,
                <br/><strong>Position Name: </strong> <?php echo  $employment['position_name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $employment['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $employment['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $employment['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($employment['reback']) && $employment['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($employment['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/employment','viewrates','<?php echo $employment['id']?>','mainPageBody');
  getViewAjax('hr/employment','signers_items','<?php echo ''.$employment['id']?>','signersItems');
</script>
