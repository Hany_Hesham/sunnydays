<?php 
  $back_name = (isset($employment)? 'Application For Employment #'.$employment['id'].'': 'Application For Employment');
  $back_url  = (isset($employment)? 'hr/employment/view/'.$employment['id'].'':'hr/employment');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($employment)) {
      if (isset($copy)) {
        echo form_open(base_url('hr/employment/copy/'.$employment['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('hr/employment/edit/'.$employment['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('hr/employment/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($employment) && ($employment['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($employment) && ($employment['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <select  id="position" name="position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($employment) && ($employment['position'] == $user_group['id']))? 'selected="selected"' : set_select('position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($employment))? $employment['name']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Serial#</label>
                <input type="text" class="form-control" id="clock_no" name="clock_no" placeholder="Serial#" value="<?php echo (isset($employment))? $employment['clock_no']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Nationality</label>
                <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality" value="<?php echo (isset($employment))? $employment['nationality']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date Of Birth</label>
                <input type="text" class="form-control" id="birth_date" data-toggle="datepicker" name="birth_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($employment))? $employment['birth_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Religion</label>
                <input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" value="<?php echo (isset($employment))? $employment['religion']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Military Service</label>
                <input type="text" class="form-control" id="military" name="military" placeholder="Military Service" value="<?php echo (isset($employment))? $employment['military']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Identity Card No</label>
                <input type="text" class="form-control" id="card_id" name="card_id" placeholder="Identity Card No" value="<?php echo (isset($employment))? $employment['card_id']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Issue Date</label>
                <input type="text" class="form-control" id="issue_date" data-toggle="datepicker" name="issue_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($employment))? $employment['issue_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="<?php echo (isset($employment))? $employment['address']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Phone</label>
                <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php echo (isset($employment))? $employment['phone']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Marital Status</label>
                <input type="text" class="form-control" id="marital" name="marital" placeholder="Marital Status" value="<?php echo (isset($employment))? $employment['marital']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Basic Salary</label>
                <input type="number" step="0.01" class="form-control" id="salary" name="salary" placeholder="Basic Salary" value="<?php echo (isset($employment))? $employment['salary']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($employment))? $employment['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('hr/employment',$uploads,$this->data['module']['id'],$gen_id,'employment','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('hr/employment/employment.js.php');?>