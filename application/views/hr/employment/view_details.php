<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Serial#</strong>
              </td>
              <td>
                <?php echo $employment['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $employment['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Basic Salary</strong>
              </td>
              <td>
                <?php echo number_format($employment['salary'],2)?> EGP
              </td>
              <td>
                <strong style="font-size: 11pt;">Nationality</strong>
              </td>
              <td>
                <?php echo $employment['nationality']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Date Of Birth</strong>
              </td>
              <td>
                <?php echo $employment['birth_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Religion</strong>
              </td>
              <td>
                <?php echo $employment['religion']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Military Service</strong>
              </td>
              <td>
                <?php echo $employment['military']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Marital Status</strong>
              </td>
              <td>
                <?php echo $employment['marital']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Identity Card No#</strong>
              </td>
              <td>
                <?php echo $employment['card_id']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Issue Date</strong>
              </td>
              <td>
                <?php echo $employment['issue_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Address</strong>
              </td>
              <td>
                <?php echo $employment['address']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Phone</strong>
              </td>
              <td>
                <?php echo $employment['phone']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $employment['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>