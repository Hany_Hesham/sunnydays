<script type="text/javascript">
  
  $('#submitRS').show();

  $('#warningMessage').hide();
  
  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });
  
  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  <?php if (!isset($housing)):?>
    $('#code, #hid').on('change',function () {
      $('#name').val('');
      $('#department_name').val('');
      $('#dep_id').val('');
      $('#position_name').val('');
      $('#pos_id').val('');
      $('#hire_date').val('');
      $('#level').val('');
      employeeSearch();
    });
  <?php endif; ?>

  function employeeSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    let hid   = $('#hid').val();
    let empNo = $('#code').val();
    if (hid != '' && empNo !='') {
      var url =getUrl()+'hr/housing/search_APIEmployee/'+hid+'/'+empNo;      
      $.post(url).done(function(response) {
        response = JSON.parse(response);
        if(response, $.each(response, function() {
          $('#name').val(response.emp_ename);
          $('#department_name').val(response.dep_name);
          $('#dep_id').val(response.emp_did);
          $('#position_name').val(response.position_name);
          $('#pos_id').val(response.emp_pid);
          let result = response.emp_hiringdate.split(' ');
          $('#hire_date').val(result[0]);
          $('#level').val(response.emp_level);
          $('#name').prop('readonly', true);
          $('#department_name').prop('readonly', true);
          $('#position_name').prop('readonly', true);
          $('#hiring_date').prop('readonly', true);
          $('#level').prop('readonly', true);
        }));
      });
    }
  }

</script>