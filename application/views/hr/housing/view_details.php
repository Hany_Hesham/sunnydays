<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">To</strong>
              </td>
              <td>
                General Manager
              </td>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $housing['name']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Hiring Date</strong>
              </td>
              <td>
                <?php echo $housing['hire_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Level</strong>
              </td>
              <td>
                <?php echo $housing['level']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Housing Allowance</strong>
              </td>
              <td>
                <?php echo $housing['allowance']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Effective Date</strong>
              </td>
              <td>
                <?php echo $housing['effective_date']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $housing['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>