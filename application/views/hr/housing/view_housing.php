<?php initiate_breadcrumb(
  'Housing Allowance #'.$housing['id'].'',
  $housing['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Housing Allowance Edit','url'=>'hr/housing/edit/'.$housing['id'])
  ),//options
  array(array('name'=>'Housing Allowance','url'=>'hr/housing'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$housing['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Housing Allowance #<?php echo $housing['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'housing'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $housing['hotel_name']?>, 
                <br/><strong>Code: </strong> <?php echo  $housing['code']; ?>,
                <br/><strong>Department: </strong> <?php echo  ($housing['dep_id'])? $housing['depart_name']:$housing['dep_name'] ; ?>,
                <br/><strong>Position: </strong> <?php echo  ($housing['pos_id'])? $housing['pos_name']:$housing['position_name'] ; ?>,
                <br/><strong>Status: </strong> <?php echo  $housing['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $housing['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $housing['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($housing['reback']) && $housing['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($housing['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/housing','viewrates','<?php echo $housing['id']?>','mainPageBody');
  getViewAjax('hr/housing','signers_items','<?php echo ''.$housing['id']?>','signersItems');
</script>
