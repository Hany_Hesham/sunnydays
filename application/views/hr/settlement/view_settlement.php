<?php initiate_breadcrumb(
  'Settlement Form #'.$settlement['id'].'',
  $settlement['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Settlement Form Edit','url'=>'hr/settlement/edit/'.$settlement['id']),
    array('name'=>'Settlement Form Copy','url'=>'hr/settlement/copy/'.$settlement['id'].'/1')
  ),//options
  array(array('name'=>'Settlement Form','url'=>'hr/settlement'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$settlement['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Settlement Form #<?php echo $settlement['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'settlement'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $settlement['hotel_name']?>, 
                <br/><strong>Status: </strong> <?php echo  $settlement['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $settlement['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $settlement['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($settlement['reback']) && $settlement['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($settlement['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3" style="width: 25% !important;">
            <div class="card">
              <div class="card-header badge-dark after-hover">
                <h6 class="card-title float-left">توقيع الموظف </h6>
                <ul class="navbar-nav float-right mr-auto">
                  <li class="nav-item dropdown">
                    <span class="wait-hover">
                      <a data-toggle="dropdown" data-placement="top" data-original-title="Mail To" href="#" class="nav-link card-hover btn btn-light btn-sm btn-circle"><i class="fas fa-envelope"></i></a>
                    </span>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item" href="#"></a>
                      <div class="dropdown-divider"></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="card-body centered">
                <h5 class="card-title centered"></h5>
                <div>
                  <img src="" alt="" style="width: 200px; height: 55px;" class="d-print-none">
                </div>
                <h5></h5>
                <h6></h6>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/settlement','viewrates','<?php echo $settlement['id']?>','mainPageBody');
  getViewAjax('hr/settlement','signers_items','<?php echo ''.$settlement['id']?>','signersItems');
</script>
