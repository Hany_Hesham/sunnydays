<?php 
  $back_name = (isset($settlement)? 'Settlement Form #'.$settlement['id'].'': 'Settlement Form');
  $back_url  = (isset($settlement)? 'hr/settlement/view/'.$settlement['id'].'':'hr/settlement');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($settlement)) {
      if (isset($copy)) {
        echo form_open(base_url('hr/settlement/copy/'.$settlement['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('hr/settlement/edit/'.$settlement['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('hr/settlement/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hotels_id" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($settlement) && ($settlement['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($settlement) && ($settlement['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <select  id="position" name="position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($settlement) && ($settlement['position'] == $user_group['id']))? 'selected="selected"' : set_select('position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Clock No#</label>
                <input type="text" class="form-control" id="clock_no" name="clock_no" placeholder="Clock No#" value="<?php echo (isset($settlement))? $settlement['clock_no']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($settlement))? $settlement['name']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date Of Hiring</label>
                <input type="text" class="form-control" id="hiring_date" data-toggle="datepicker" name="hiring_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($settlement))? $settlement['hiring_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Last day of work</label>
                <input type="text" class="form-control" id="last_date" data-toggle="datepicker" name="last_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($settlement))? $settlement['last_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Salary</label>
                <input type="number" step="0.01" class="form-control" id="salary" name="salary" placeholder="Salary" value="<?php echo (isset($settlement))? $settlement['salary']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($settlement))? $settlement['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <h3 style="text-align: center;">Dues</h3>
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="50%" style="font-size:13px;">Statement</th>
                          <th width="50%" style="font-size:13px;">No. Of Days</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="rowIds">     
                          <td>
                            <strong>Working Days</strong>
                          </td>     
                          <td>
                            <input type="number" name="working_days" placeholder="Working Days" class="form-control" value="<?php echo (isset($settlement))? $settlement['working_days']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Balance</strong>
                          </td>     
                          <td>
                            <input type="number" name="balance" placeholder="Balance" class="form-control" value="<?php echo (isset($settlement))? $settlement['balance']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Remaining Days</strong>
                          </td>     
                          <td>
                            <input type="number" name="remaining" placeholder="Remaining Days" class="form-control" value="<?php echo (isset($settlement))? $settlement['remaining']: '';?>" required="required"/>
                          </td>        
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <h3 style="text-align: center;">Deductions</h3>
                <div class="table-responsive m-t-40" style="clear: both;">
                  <div style="width:100%;">
                    <table id="Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="50%" style="font-size:13px;">Statement</th>
                          <th width="50%" style="font-size:13px;">No. Of Days</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="rowIds">     
                          <td>
                            <strong>Sick Days</strong>
                          </td>     
                          <td>
                            <input type="number" name="sick_days" placeholder="Sick Days" class="form-control" value="<?php echo (isset($settlement))? $settlement['sick_days']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Penalty</strong>
                          </td>     
                          <td>
                            <input type="number" name="penalty" placeholder="Penalty" class="form-control" value="<?php echo (isset($settlement))? $settlement['penalty']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Total Leave</strong>
                          </td>     
                          <td>
                            <input type="number" name="leaves" placeholder="Total Leave" class="form-control" value="<?php echo (isset($settlement))? $settlement['leaves']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Unpaid Days</strong>
                          </td>     
                          <td>
                            <input type="number" name="unpaid" placeholder="Unpaid Days" class="form-control" value="<?php echo (isset($settlement))? $settlement['unpaid']: '';?>" required="required"/>
                          </td>        
                        </tr>
                        <tr class="rowIds">     
                          <td>
                            <strong>Others</strong>
                          </td>     
                          <td>
                            <input type="number" name="others" placeholder="Others" class="form-control" value="<?php echo (isset($settlement))? $settlement['others']: '';?>" required="required"/>
                          </td>        
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <br>
                </div>
              </div>  
            </div> 
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('hr/settlement',$uploads,$this->data['module']['id'],$gen_id,'settlement','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('hr/settlement/settlement.js.php');?>