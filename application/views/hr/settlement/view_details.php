<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Position Name</strong>
              </td>
              <td>
                <?php echo $settlement['position_name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Clock No#</strong>
              </td>
              <td>
                <?php echo $settlement['clock_no']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Name</strong>
              </td>
              <td>
                <?php echo $settlement['name']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Salary</strong>
              </td>
              <td>
                <?php echo number_format($settlement['salary'],2)?> EGP
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Date Of Hiring</strong>
              </td>
              <td>
                <?php echo $settlement['hiring_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Last day of work</strong>
              </td>
              <td>
                <?php echo $settlement['last_date']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <h3 style="text-align: center;">Dues</h3>
    <div class="row" style="margin-top:20px;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <thead style="background-color:#0f597a;color:#FFF">
            <tr>
              <th width="50%" style="font-size:13px;">Statement</th>
              <th width="50%" style="font-size:13px;">No. Of Days</th>
            </tr>
          </thead>
          <tbody>
            <tr class="rowIds">     
              <td>
                <strong>Working Days</strong>
              </td>     
              <td>
                <?php echo $settlement['working_days']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Balance</strong>
              </td>     
              <td>
                <?php echo $settlement['balance']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Remaining Days</strong>
              </td>     
              <td>
                <?php echo $settlement['remaining']?>
              </td>        
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <h3 style="text-align: center;">Deductions</h3>
    <div class="row" style="margin-top:20px;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <thead style="background-color:#0f597a;color:#FFF">
            <tr>
              <th width="50%" style="font-size:13px;">Statement</th>
              <th width="50%" style="font-size:13px;">No. Of Days</th>
            </tr>
          </thead>
          <tbody>
            <tr class="rowIds">     
              <td>
                <strong>Sick Days</strong>
              </td>     
              <td>
                <?php echo $settlement['sick_days']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Penalty</strong>
              </td>     
              <td>
                <?php echo $settlement['penalty']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Total Leave</strong>
              </td>     
              <td>
                <?php echo $settlement['leaves']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Unpaid Days</strong>
              </td>     
              <td>
                <?php echo $settlement['unpaid']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Others</strong>
              </td>     
              <td>
                <?php echo $settlement['others']?>
              </td>        
            </tr>
            <tr class="rowIds">     
              <td>
                <strong>Total</strong>
              </td>     
              <td>
                <?php $total = $settlement['sick_days'] + $settlement['penalty'] + $settlement['leaves'] + $settlement['unpaid'] + $settlement['others'];  ?>
                <?php echo $total?>
              </td>        
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $settlement['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>