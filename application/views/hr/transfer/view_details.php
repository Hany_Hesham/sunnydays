<?php if ($hotel_type == 1) :?>
  <div class="card">
    <div class="card-body"> 
      <div class="row">
        <div class="col-md-6">
          <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$transfer['from_h_logo']);?>" alt="user">
        </div>
        <div class="col-md-6">
          <p class="font-bold">
            <strong>Current Hotel: </strong> <?php echo  $transfer['from_hotel_name']; ?>,
            <br/><strong>Current Department: </strong> <?php echo  $transfer['from_dep_name']; ?>,
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="card devisionsDivs">
    <div class="card-body">
      <div class="row" style="margin-top:20px;zoom:80%;">
        <div class="table-responsive m-t-40" style="clear: both;">
          <table id="Items-list-table" class="custom-table table table-striped font-bold">
            <tbody>
              <tr>
                <td>
                  <strong style="font-size: 11pt;">Current Position</strong>
                </td>
                <td>
                  <?php echo $transfer['from_position']?>
                </td>
                <td>
                  <strong style="font-size: 11pt;">Current Salary</strong>
                </td>
                <td>
                  <?php echo $transfer['from_salary']?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php elseif ($hotel_type == 2) : ?>
  <div class="card">
    <div class="card-body"> 
      <div class="row">
        <div class="col-md-6">
          <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$transfer['to_h_logo']);?>" alt="user">
        </div>
        <div class="col-md-6">
          <p class="font-bold">
            <strong>To Hotel: </strong> <?php echo  $transfer['to_hotel_name']; ?>,
            <br/><strong>To Department: </strong> <?php echo  $transfer['to_dep_name']; ?>,
          </p>
        </div>
      </div>
    </div>
  </div>
  <div class="card devisionsDivs">
    <div class="card-body">
      <div class="row" style="margin-top:20px;zoom:80%;">
        <div class="table-responsive m-t-40" style="clear: both;">
          <table id="Items-list-table" class="custom-table table table-striped font-bold">
            <tbody>
              <tr>
                <td colspan="1">
                  <strong style="font-size: 11pt;">New Position</strong>
                </td>
                <td colspan="2">
                  <?php echo $transfer['to_position_name']?>
                </td>
                <td colspan="1">
                  <strong style="font-size: 11pt;">New Salary</strong>
                </td>
                <td colspan="2">
                  <?php echo $transfer['to_salary']?>
                </td>
              </tr>
              <tr>
                <td colspan="6">
                  <strong style="font-size: 11pt;"><label class="text-right control-label col-form-label text-danger">Salary Scale</label></strong>
                </td>
              </tr>
              <tr>
                <td colspan="1">
                  <strong style="font-size: 11pt;">MIN Salary</strong>
                </td>
                <td colspan="1">
                  <?php echo $transfer['salary_min']?>
                </td>
                <td colspan="1">
                  <strong style="font-size: 11pt;">MED Salary</strong>
                </td>
                <td colspan="1">
                  <?php echo $transfer['salary_med']?>
                </td>
                <td colspan="1">
                  <strong style="font-size: 11pt;">MAX Salary</strong>
                </td>
                <td colspan="1">
                  <?php echo $transfer['salary_max']?>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <strong style="font-size: 11pt;">Remarks</strong>
                </td>
                <td colspan="4">
                  <?php echo $transfer['remarks']?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>