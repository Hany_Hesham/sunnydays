<?php initiate_breadcrumb(
  'Transfer Request #'.$transfer['id'].'',
  $transfer['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Transfer Request Edit','url'=>'hr/transfer/edit/'.$transfer['id']),
    array('name'=>'Transfer Request Copy','url'=>'hr/transfer/copy/'.$transfer['id'].'/1')
  ),//options
  array(array('name'=>'Transfer Request','url'=>'hr/transfer'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Transfer Request #<?php echo $transfer['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'transfer'));?> 
              </h4>
              <p class="font-bold">
                <br/><strong>Status: </strong> <?php echo  $transfer['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $transfer['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $transfer['timestamp']; ?>,
              </p>
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <p class="font-bold">
                <strong>Name: </strong> <?php echo $transfer['name']?>, 
                <br/><strong>Nationality: </strong> <?php echo  $transfer['nationality']; ?>,
                <br/><strong>Date Of Hiring: </strong> <?php echo  $transfer['hiring_date']; ?>,
                <br/><strong>Transfer Date: </strong> <?php echo  $transfer['transfer_date']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($transfer['reback']) && $transfer['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($transfer['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br><div id="mainPagesBody"></div>
      <br><div id="signerzItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/transfer','viewrates','<?php echo '1/'.$transfer['id']?>','mainPageBody');
  getViewAjax('hr/transfer','viewrates','<?php echo '2/'.$transfer['id']?>','mainPagesBody');
  getViewAjax('hr/transfer','signers_items','<?php echo '1/'.$transfer['id'].'/'.$transfer['stage'].'/'.$transfer['status']?>','signersItems');
  getViewAjax('hr/transfer','signers_items','<?php echo '2/'.$transfer['id'].'/'.$transfer['stage'].'/'.$transfer['status']?>','signerzItems');
</script>
