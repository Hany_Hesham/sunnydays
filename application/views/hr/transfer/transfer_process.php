<?php 
  $back_name = (isset($transfer)? 'Transfer Request #'.$transfer['id'].'': 'Transfer Request');
  $back_url  = (isset($transfer)? 'hr/transfer/view/'.$transfer['id'].'':'hr/transfer');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($transfer)) {
      if (isset($copy)) {
        echo form_open(base_url('hr/transfer/copy/'.$transfer['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('hr/transfer/edit/'.$transfer['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('hr/transfer/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Hotel</label>
                <select  id="hotel_id" name="from_hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($transfer) && ($transfer['from_hid'] == $hotel['id']))? 'selected="selected"' : set_select('from_hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Hotel</label>
                <select  id="hotels_id" name="to_hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($transfer) && ($transfer['to_hid'] == $hotel['id']))? 'selected="selected"' : set_select('to_hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Department</label>
                <select  id="from_dep_code" name="from_dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($transfer) && ($transfer['from_dep_code'] == $department['id']))? 'selected="selected"' : set_select('from_dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Department</label>
                <select  id="to_dep_code" name="to_dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($transfer) && ($transfer['to_dep_code'] == $department['id']))? 'selected="selected"' : set_select('to_dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Position</label>
                <select  id="from_position" name="from_position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($transfer) && ($transfer['from_position'] == $user_group['id']))? 'selected="selected"' : set_select('from_position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Position</label>
                <select  id="to_position" name="to_position" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($transfer) && ($transfer['to_position'] == $user_group['id']))? 'selected="selected"' : set_select('to_position',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($transfer))? $transfer['name']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Nationality#</label>
                <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality#" value="<?php echo (isset($transfer))? $transfer['nationality']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Date Of Hiring</label>
                <input type="text" class="form-control" id="hiring_date" data-toggle="datepicker" name="hiring_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($transfer))? $transfer['hiring_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Transfer Date</label>
                <input type="text" class="form-control" id="transfer_date" data-toggle="datepicker" name="transfer_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($transfer))? $transfer['transfer_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Current Salary</label>
                <input type="number" step="0.01" class="form-control" id="from_salary" name="from_salary" placeholder="Current Salary" value="<?php echo (isset($transfer))? $transfer['from_salary']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*New Salary</label>
                <input type="number" step="0.01" class="form-control" id="to_salary" name="to_salary" placeholder="Current Salary" value="<?php echo (isset($transfer))? $transfer['to_salary']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <label class="text-right control-label col-form-label text-danger">Salary Scale</label>
                <div class="row">
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Min Salary</label>
                    <input type="number" step="0.01" class="form-control" id="salary_min" name="salary_min" placeholder="Current Salary" value="<?php echo (isset($transfer))? $transfer['salary_min']: '';?>" required>
                  </div>
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Med Salary</label>
                    <input type="number" step="0.01" class="form-control" id="salary_med" name="salary_med" placeholder="Current Salary" value="<?php echo (isset($transfer))? $transfer['salary_med']: '';?>" required>
                  </div>
                  <div class="col-sm-4">
                    <label class="text-right control-label col-form-label" style="font-size:12px;">*Max Salary</label>
                    <input type="number" step="0.01" class="form-control" id="salary_max" name="salary_max" placeholder="Current Salary" value="<?php echo (isset($transfer))? $transfer['salary_max']: '';?>" required>
                  </div>
                </div>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($transfer))? $transfer['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('hr/transfer',$uploads,$this->data['module']['id'],$gen_id,'transfer','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('hr/transfer/transfer.js.php');?>