<?php initiate_breadcrumb(
  'Mobile Allowance #'.$mobile['id'].'',
  $mobile['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Mobile Allowance Edit','url'=>'hr/mobile/edit/'.$mobile['id'])
  ),//options
  array(array('name'=>'Mobile Allowance','url'=>'hr/mobile'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$mobile['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Mobile Allowance #<?php echo $mobile['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'mobile'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $mobile['hotel_name']?>, 
                <br/><strong>Code: </strong> <?php echo  $mobile['code']; ?>,
                <br/><strong>Department: </strong> <?php echo  ($mobile['dep_id'])? $mobile['depart_name']:$mobile['dep_name'] ; ?>,
                <br/><strong>Position: </strong> <?php echo  ($mobile['pos_id'])? $mobile['pos_name']:$mobile['position_name'] ; ?>,
                <br/><strong>Status: </strong> <?php echo  $mobile['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $mobile['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $mobile['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($mobile['reback']) && $mobile['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($mobile['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/mobile','viewrates','<?php echo $mobile['id']?>','mainPageBody');
  getViewAjax('hr/mobile','signers_items','<?php echo ''.$mobile['id']?>','signersItems');
</script>
