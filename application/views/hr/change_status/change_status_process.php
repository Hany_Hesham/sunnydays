<?php 
  $back_name = (isset($change_status)? 'Change Of Status #'.$change_status['id'].'': 'Change Of Status');
  $back_url  = (isset($change_status)? 'hr/change_status/view/'.$change_status['id'].'':'hr/change_status');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($change_status)) {
      if (isset($copy)) {
        echo form_open(base_url('hr/change_status/copy/'.$change_status['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('hr/change_status/edit/'.$change_status['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('hr/change_status/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($change_status) && ($change_status['hid'] == $hotel['id']))? 'selected="selected"' : set_select('hotels',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departmentz as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($change_status) && ($change_status['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Clock No</label>
                <input type="text" class="form-control" id="clock_no" name="clock_no" placeholder="Clock No" value="<?php echo (isset($change_status))? $change_status['clock_no']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
            <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*New Clock No</label>
                <input type="text" class="form-control" id="new_clock" name="new_clock" placeholder="Clock No" value="<?php echo (isset($change_status))? $change_status['new_clock']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo (isset($change_status))? $change_status['name']: '';?>" required readonly>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hiring Date</label>
                <input type="text" class="form-control" id="hire_date" name="hire_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($change_status))? $change_status['hire_date']: '';?>" required readonly>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <input type="text" class="form-control" id="department_name" placeholder="Department" value="<?php echo (isset($change_status))? $change_status['dep_name']: '';?>" required readonly>
                <input type="hidden" id="dep_id" name="dep_id" value="<?php echo (isset($change_status))? $change_status['dep_id']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Position</label>
                <input type="text" class="form-control" id="position_name" placeholder="Position" value="<?php echo (isset($change_status))? $change_status['position_name']: '';?>" required readonly>
                <input type="hidden" id="pos_id" name="pos_id" value="<?php echo (isset($change_status))? $change_status['pos_id']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*New Department</label>
                <select  id="new_dep" name="new_deps" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php if(isset($change_status)){ foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($change_status) && ($change_status['new_deps'] == $department['id']))? 'selected="selected"' : set_select('new_deps',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php } }?>
                </select>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*New Position</label>
                <select  id="new_position" name="new_positions" class="selectpicker position-select form-control"data-container="body" data-live-search="true" title="Select Position" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php if(isset($change_status)){ foreach($user_groups as $user_group){?>
                    <option value="<?php echo $user_group['id']?>" <?php echo (isset($change_status) && ($change_status['new_positions'] == $user_group['id']))? 'selected="selected"' : set_select('new_positions',$user_group['id'] ); ?>><?php echo $user_group['name']?></option>
                  <?php } }?>
                </select>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Level</label>
                <input type="number" class="form-control" id="level" name="level" placeholder="Level" value="<?php echo (isset($change_status))? $change_status['level']: '';?>" required readonly>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Basic Salary</label>
                <input type="number" class="form-control" id="basic_salary" step="0.01" name="basic_salary" placeholder="Salary" value="<?php echo (isset($change_status))? $change_status['basic_salary']: '';?>" required readonly>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*New Salary</label>
                <input type="number" class="form-control" id="new_salary" step="0.01" name="new_salary" placeholder="Salary" value="<?php echo (isset($change_status))? $change_status['new_salary']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Last Date of Increase</label>
                <input type="text" class="form-control" id="last_increase" data-toggle="datepicker" name="last_increase" placeholder="yyyy-mm-dd" value="<?php echo (isset($change_status))? $change_status['last_increase']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Effective Date</label>
                <input type="text" class="form-control" id="effective_date" data-toggle="datepicker" name="effective_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($change_status))? $change_status['effective_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($change_status))? $change_status['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('hr/change_status',$uploads,$this->data['module']['id'],$gen_id,'change_status','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('hr/change_status/change_status.js.php');?>