<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Clock No</strong>
              </td>
              <td>
                <?php echo $change_status['clock_no']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">New Clock No</strong>
              </td>
              <td>
                <?php echo $change_status['new_clock']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Department</strong>
              </td>
              <td>
                <?php echo  ($change_status['dep_id'])? $change_status['depart_name']:$change_status['dep_name'] ; ?>
              </td>
              <td>
                <strong style="font-size: 11pt;">New Department</strong>
              </td>
              <td>
                <?php echo  ($change_status['new_deps'])? $change_status['new_depart_name']:$change_status['new_dep_name'] ; ?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Position</strong>
              </td>
              <td>
                <?php echo  ($change_status['pos_id'])? $change_status['pos_name']:$change_status['position_name'] ; ?>
              </td>
              <td>
                <strong style="font-size: 11pt;">New Position</strong>
              </td>
              <td>
                <?php echo  ($change_status['new_positions'])? $change_status['new_pos_name']:$change_status['new_position_name'] ; ?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Hiring Date</strong>
              </td>
              <td>
                <?php echo $change_status['hire_date']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Level</strong>
              </td>
              <td>
                <?php echo $change_status['level']?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Basic Salary</strong>
              </td>
              <td>
                <?php echo number_format($change_status['basic_salary'],2)?>
              </td>
              <td>
                <strong style="font-size: 11pt;">New Salary</strong>
              </td>
              <td>
                <?php echo number_format($change_status['new_salary'],2)?>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Last Date of Increase</strong>
              </td>
              <td>
                <?php echo $change_status['last_increase']?>
              </td>
              <td>
                <strong style="font-size: 11pt;">Effective Date</strong>
              </td>
              <td>
                <?php echo $change_status['effective_date']?>
              </td>
            </tr>

            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <?php echo $change_status['remarks']?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>