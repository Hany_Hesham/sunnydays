<script type="text/javascript">
  
  $('#submitRS').show();

  $('#warningMessage').hide();
  
  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }
  
  <?php if (!isset($change_status)):?>
    $('#clock_no, #hid').on('change',function () {
      $('#name').val('');
      $('#department_name').val('');
      $('#dep_id').val('');
      $('#position_name').val('');
      $('#pos_id').val('');
      $('#hire_date').val('');
      $('#level').val('');
      $('#basic_salary').val('');
      employeeSearch();
    });
  <?php endif; ?>

  $('#hid').on('change',function () {
    itemsDepartment();
    itemsPosition();
  });

  function employeeSearch(){
    $(window).on('load.bs.select.data-api').error(function() {})
    let hid   = $('#hid').val();
    let empNo = $('#clock_no').val();
    if (hid != '' && empNo !='') {
      var url =getUrl()+'hr/change_status/search_APIEmployee/'+hid+'/'+empNo;      
      $.post(url).done(function(response) {
        response = JSON.parse(response);
        if(response, $.each(response, function() {
          $('#name').val(response.emp_ename);
          $('#department_name').val(response.dep_name);
          $('#dep_id').val(response.emp_did);
          $('#position_name').val(response.position_name);
          $('#pos_id').val(response.emp_pid);
          let result = response.emp_hiringdate.split(' ');
          $('#hire_date').val(result[0]);
          $('#level').val(response.emp_level);
          $('#basic_salary').val(response.emp_grosssalary);
          $('#name').prop('readonly', true);
          $('#department_name').prop('readonly', true);
          $('#position_name').prop('readonly', true);
          $('#hiring_date').prop('readonly', true);
          $('#level').prop('readonly', true);
          $('#basic_salary').prop('readonly', true);
        }));
      });
    }
  }

  function itemsDepartment(){
    var hid     = $('#hid').val();
    var table   = 'api_departments';
    var code    = 'dep_code';
    if (hid != '') {
      var url = "<?php echo base_url('hr/change_status/get_allData');?>";
      $.ajax({
        url: url,
        dataType: "json",
        type : 'POST',
        data:{hid:hid, table:table, code:code},
        success: function(data){
          $("#new_dep").empty();
          $.each(data, function(i,item){
            if (data[i].id == undefined) {
              $("#new_dep").append('<option value="'+ data[i].id +'">'+ data[i].key +'</option>');
            }else{
              $("#new_dep").append('<option value="'+ data[i].id+'">'+ data[i].dep_name +'</option>');
              $('#new_dep').selectpicker('refresh');
            }
          });
        }
      });
    }
  }

  function itemsPosition(){
    var hid     = $('#hid').val();
    var table   = 'api_user_groups';
    var code    = 'code';
    if (hid != '') {
      var url = "<?php echo base_url('hr/change_status/get_allData');?>";
      $.ajax({
        url: url,
        dataType: "json",
        type : 'POST',
        data:{hid:hid, table:table, code:code},
        success: function(data){
          $("#new_position").empty();
          $.each(data, function(i,item){
            if (data[i].id == undefined) {
              $("#new_position").append('<option value="'+ data[i].id +'">'+ data[i].key +'</option>');
            }else{
              $("#new_position").append('<option value="'+ data[i].id+'">'+ data[i].name +'</option>');
              $('#new_position').selectpicker('refresh');
            }
          });
        }
      });
    }
  }

</script>