<?php initiate_breadcrumb(
  'Change Of Status #'.$change_status['id'].'',
  $change_status['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Change Of Status Edit','url'=>'hr/change_status/edit/'.$change_status['id'])
  ),//options
  array(array('name'=>'Change Of Status','url'=>'hr/change_status'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$change_status['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Change Of Status #<?php echo $change_status['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'change_status'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $change_status['hotel_name']?>, 
                <br/><strong>Name: </strong> <?php echo  $change_status['name']; ?>,
                <br/><strong>Status: </strong> <?php echo  $change_status['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $change_status['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $change_status['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($change_status['reback']) && $change_status['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($change_status['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('hr/change_status','viewrates','<?php echo $change_status['id']?>','mainPageBody');
  getViewAjax('hr/change_status','signers_items','<?php echo ''.$change_status['id']?>','signersItems');
</script>
