<?php 
  $back_name = (isset($po)? 'Purchase Order #'.$po['id'].'': 'Purchase Order');
  $back_url  = (isset($po)? 'cost/po/view/'.$po['id'].'':'cost/po');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($po)) {
      if (isset($copy)) {
        echo form_open(base_url('cost/po/copy/'.$po['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('cost/po/edit/'.$po['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('cost/po/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($po) && ($po['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Supplier</label>
                <input type="text" class="form-control" id="supplier" name="supplier" placeholder="Supplier" value="<?php echo (isset($po))? $po['supplier']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Store</label>
                <input type="text" class="form-control" id="store" name="store" placeholder="Store" value="<?php echo (isset($po))? $po['store']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*PR NO.</label>
                <input type="text" class="form-control" id="pr_no" name="pr_no" placeholder="PR NO." value="<?php echo (isset($po))? $po['pr_no']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*PO NO.</label>
                <input type="text" class="form-control" id="po_no" name="po_no" placeholder="PO NO." value="<?php echo (isset($po))? $po['po_no']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*PO Date</label>
                <input type="text" class="form-control" id="po_date" data-toggle="datepicker" name="po_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($po))? $po['po_date']: '';?>" required>
              </div>
              <div class="col-sm-3">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Delivery Date</label>
                <input type="text" class="form-control" id="del_date" data-toggle="datepicker" name="del_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($po))? $po['del_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="poAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="50%">Description</th>
                        <th width="15%">Quntity</th>
                        <th width="17%">Unit</th>
                        <th width="17%">Price</th>
                      </tr>
                    </thead>
                    <tbody id="po-data">
                      <?php if(isset($po)){$i=1;$ids = '';foreach($po_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td class="text-center">
                            <input id="description<?php echo $i?>" type="text" name="items[<?php echo $i?>][description]" class="form-control" value="<?php echo $row['description']?>"/>
                          </td>          
                          <td>
                            <input id="quntity<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][quntity]" class="form-control" value="<?php echo $row['quntity']?>"/>
                          </td>
                          <td>
                            <input id="unit<?php echo $i?>" type="text" name="items[<?php echo $i?>][unit]" class="form-control" value="<?php echo $row['unit']?>"/>
                          </td>
                          <td>
                            <input id="price<?php echo $i?>" type="number" step="0.0001" name="items[<?php echo $i?>][price]" class="form-control" value="<?php echo $row['price']?>"/>
                          </td>
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($po))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($po))? $po['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('cost/po',$uploads,$this->data['module']['id'],$gen_id,'po','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('cost/po/po.js.php');?>