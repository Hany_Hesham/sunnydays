<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <tr>
              <td>
                <strong style="font-size: 11pt;">PR NO.</strong>
              </td>
              <td>
                <span><?php echo $po['pr_no']?></span>
              </td>
              <td>
                <strong style="font-size: 11pt;">PO NO.</strong>
              </td>
              <td>
                <span><?php echo $po['po_no']?></span>
              </td>
            </tr>
              <td>
                <strong style="font-size: 11pt;">Supplier</strong>
              </td>
              <td>
                <span><?php echo $po['supplier']?></span>
              </td>
              <td>
                <strong style="font-size: 11pt;">Store</strong>
              </td>
              <td>
                <span><?php echo $po['store']?></span>
              </td>
            </tr>
            <tr>
              <td>
                <strong style="font-size: 11pt;">PO Date</strong>
              </td>
              <td>
                <span><?php echo $po['po_date']?></span>
              </td>
              <td>
                <strong style="font-size: 11pt;">Delivery Date</strong>
              </td>
              <td>
                <span><?php echo $po['del_date']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="39%"><strong style="font-size: 11pt;">Description</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">Quntity</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">Price</strong></th>
              <th width="15%"><strong style="font-size: 11pt;">Total Amount</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; $total_po =0; foreach($po_items as $item){ ?>
              <?php $total = $item['quntity'] * $item['price']; $total_po += $total; ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['description']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quntity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['price'],4)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($total,2)?> EGP</strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
            <tr>
              <td colspan="4">
                <strong style="font-size: 16pt;">Total</strong>
              </td>
              <td colspan="2">
                <strong style="font-size: 11pt;"><?php echo number_format($total_po,4)?> EGP</strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $po['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>