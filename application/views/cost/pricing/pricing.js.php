<?php 
  $select = '';
  foreach ($pitems as $pitem) {
    $select .= '<option value="'.$pitem['id'].'" data-subtext="'.$pitem['type_data'].'">'.$pitem['type_name'].'</option>';
  }
?>
<script type="text/javascript">

  let select    = '<?php echo $select?>';
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function pricingAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#pricing-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><select id="item'+rowId+'" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Item" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items['+rowId+'][item]" required>'+select+'</select></td>     <td><input id="price'+rowId+'" type="number" step="0.01" name="items['+rowId+'][price]" class="form-control" value=""/> EGP</td>     </tr>');
    $(document).ready(function(){
      $('.selectpicker').selectpicker('render');
      $('.selectpicker').selectpicker('setStyle','btn btn-dark');
    });
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'cost/pricing/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>