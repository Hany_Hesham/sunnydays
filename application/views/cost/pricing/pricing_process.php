<?php 
  $back_name = (isset($pricing)? 'Pricing Daily Used Items #'.$pricing['id'].'': 'Pricing Daily Used Items');
  $back_url  = (isset($pricing)? 'cost/pricing/view/'.$pricing['id'].'':'cost/pricing');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($pricing)) {
      if (isset($copy)) {
        echo form_open(base_url('cost/pricing/copy/'.$pricing['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('cost/pricing/edit/'.$pricing['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('cost/pricing/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($pricing) && ($pricing['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*From Date</label>
                <input type="text" class="form-control" id="from_date" data-toggle="datepicker" name="from_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($pricing))? $pricing['from_date']: '';?>" required>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*To Date</label>
                <input type="text" class="form-control" id="to_date" data-toggle="datepicker" name="to_date" placeholder="yyyy-mm-dd" value="<?php echo (isset($pricing))? $pricing['to_date']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo (isset($pricing))? $pricing['subject']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                    <table id="selected-Items-list-table" class="table table-hover">
                      <thead style="background-color:#0f597a;color:#FFF">
                        <tr>
                          <th width="5%" rowspan="2" colspan="1">
                            <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="pricingAdd()">
                              <i class="mdi mdi-database-plus"></i>
                            </button>
                          </th>
                          <th width="35%">Name</th>
                          <th width="25%">Price</th>
                        </tr>
                      </thead>
                      <tbody id="pricing-data">
                        <?php if(isset($pricing)){$i=1;$ids = '';foreach($pricing_items as $row){ $ids= $i.','.$ids;?>
                          <tr class="rowIds" id="row<?php echo $i?>">          
                            <td class="text-center">
                              <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                                <i class="fas fa-trash-alt text-danger"></i>
                              </button>
                              <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                            </td>        
                            <td class="text-center">
                              <select id="item<?php echo $i?>" type="text" class="selectpicker form-control" data-container="body" data-live-search="true" title="Select Item" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" name="items[<?php echo $i?>][item]" required>
                                <?php foreach ($pitems as $pitem): ?>
                                  <option value="<?php echo $pitem['id']?>" data-subtext="<?php echo $pitem['type_data']?>" <?php echo ($row['item'] == $pitem['id'])? 'selected="selected"':''?>><?php echo $pitem['type_name']?></option>
                                <?php endforeach; ?>
                              </select>
                            </td>      
                            <td>
                              <input id="price<?php echo $i?>" step="0.01" type="number" name="items[<?php echo $i?>][price]" class="form-control" value="<?php echo $row['price']?>"/> EGP
                            </td>    
                          </tr>
                        <?php $i++;}}?>  
                        <tr class="hidden">
                          <td class="hidden" id="all-items-ids"></td>
                        </tr>
                      </tbody>
                    </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($pricing))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($pricing))? $pricing['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('cost/pricing',$uploads,$this->data['module']['id'],$gen_id,'pricing','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('cost/pricing/pricing.js.php');?>