<?php initiate_breadcrumb(
  'Pricing Daily Used Items #'.$pricing['id'].'',
  $pricing['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Pricing Daily Used Items Edit','url'=>'cost/pricing/edit/'.$pricing['id']),
    array('name'=>'Pricing Daily Used Items Copy','url'=>'cost/pricing/copy/'.$pricing['id'].'/1')
  ),//options
  array(array('name'=>'Pricing Daily Used Items','url'=>'cost/pricing'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$pricing['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Pricing Daily Used Items #<?php echo $pricing['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'pricing'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $pricing['hotel_name']?>, 
                <br/><strong>Subject: </strong> <?php echo  $pricing['subject']; ?>,
                <br/><strong>Status: </strong> <?php echo  $pricing['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $pricing['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $pricing['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($pricing['reback']) && $pricing['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($pricing['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('cost/pricing','viewrates','<?php echo $pricing['id']?>','mainPageBody');
  getViewAjax('cost/pricing','signers_items','<?php echo ''.$pricing['id']?>','signersItems');
</script>
