<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>From Date</td>
              <td>
                <span><?php echo $pricing['from_date']?></span>
              </td>
              <td>To Date</td>
              <td>
                <span><?php echo $pricing['to_date']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="5%"><strong style="font-size: 11pt;">#</strong></th>
              <th width="35%"><strong style="font-size: 11pt;">Name</strong></th>
              <th width="35%"><strong style="font-size: 11pt;">Arabic Name</strong></th>
              <th width="25%"><strong style="font-size: 11pt;">Price</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; foreach($pricing_items as $item){ ?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['item_name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['aritem_name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['price']?></strong>
                </td>
              </tr>
            <?php $i ++; } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>Remarks</td>
              <td colspan="3">
                <span><?php echo $pricing['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>