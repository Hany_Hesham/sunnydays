<?php 
  $back_name = (isset($estimation)? 'Cost Estimation #'.$estimation['id'].'': 'Cost Estimation');
  $back_url  = (isset($estimation)? 'cost/estimation/view/'.$estimation['id'].'':'cost/estimation');
  initiate_breadcrumb($back_name,
    '',
    '',
    '',
    '',
    'empty',//options
    array(array('name'=>$back_name,'url'=>$back_url))//locations
  );
?>
<div class="container-fluid">
  <?php
    if (isset($estimation)) {
      if (isset($copy)) {
        echo form_open(base_url('cost/estimation/copy/'.$estimation['id'].'/1'), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }else{
        echo form_open(base_url('cost/estimation/edit/'.$estimation['id']), 'class="form-horizontal" enctype="multipart/form-data"'); 
      }
    }else{
      echo form_open(base_url('cost/estimation/add/'), 'class="form-horizontal" enctype="multipart/form-data"');
    } 
  ?> 
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Hotel</label>
                <select  id="hid" name="hid" class="selectpicker hotel-select form-control"data-container="body" data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($hotels as $hotel){?>
                    <option value="<?php echo $hotel['id']?>" <?php echo (isset($estimation) && ($estimation['hid'] == $hotel['id']))? 'selected="selected"':set_select('hid',$hotel['id'] ); ?>><?php echo $hotel['hotel_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Department</label>
                <select  id="dep_code" name="dep_code" class="selectpicker department-select form-control"data-container="body" data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" required>
                  <?php foreach($departments as $department){?>
                    <option value="<?php echo $department['id']?>" <?php echo (isset($estimation) && ($estimation['dep_code'] == $department['id']))? 'selected="selected"' : set_select('dep_code',$department['id'] ); ?>><?php echo $department['dep_name']?></option>
                  <?php }?>
                </select>
              </div>
              <div class="col-sm-4">
                <label class="text-right control-label col-form-label" style="font-size:12px;">*Subject</label>
                <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" value="<?php echo (isset($estimation))? $estimation['subject']: '';?>" required>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive m-t-40" style="clear: both;">
                  <table id="selected-Items-list-table" class="table table-hover">
                    <thead style="background-color:#0f597a;color:#FFF">
                      <tr>
                        <th width="1%" rowspan="2" colspan="1">
                          <button type="button" name="addRow" id="addRow" class="btn btn-light btn-sm btn-circle" onclick="estimationAdd()">
                            <i class="mdi mdi-database-plus"></i>
                          </button>
                        </th>
                        <th width="19%" rowspan="2" colspan="1">Name</th>
                        <th width="10%" rowspan="2" colspan="1">Qty</th>
                        <th width="10%" rowspan="2" colspan="1">Unit</th>
                        <th width="45%" rowspan="1" colspan="3">Survey</th>
                        <th width="15%" rowspan="2" colspan="1">Best Price</th>
                      </tr>
                      <tr>
                        <th width="15%" rowspan="1" colspan="1">Price</th>
                        <th width="15%" rowspan="1" colspan="1">Price</th>
                        <th width="15%" rowspan="1" colspan="1">Price</th>
                      </tr>
                    </thead>
                    <tbody id="estimation-data">
                      <?php if(isset($estimation)){$i=1;$ids = '';foreach($estimation_items as $row){ $ids= $i.','.$ids;?>
                        <tr class="rowIds" id="row<?php echo $i?>">          
                          <td class="text-center">
                            <button type="button" name="remove" onclick="removeRow(<?php echo $i?>,<?php echo (isset($copy))? '`new`':$row['id']?>)"  class="btn btn-light btn-sm btn-circle btn_remove">
                              <i class="fas fa-trash-alt text-danger"></i>
                            </button>
                            <input type="hidden" name="items[<?php echo $i?>][id]" value="<?php echo $row['id']?>"/>
                          </td>                            
                          <td>
                            <input id="name<?php echo $i?>" type="text" name="items[<?php echo $i?>][name]" class="form-control" value="<?php echo $row['name']?>"/>
                          </td>                          
                          <td class="text-center">
                            <input id="quantity<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][quantity]" class="form-control" value="<?php echo $row['quantity']?>"/>
                          </td>          
                          <td>
                            <input id="unit<?php echo $i?>" type="text" name="items[<?php echo $i?>][unit]" class="form-control" value="<?php echo $row['unit']?>"/>
                          </td>
                          <td class="text-center">
                            <input id="price<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][price]" class="form-control" value="<?php echo $row['price']?>"/> EGP
                          </td>
                          <td class="text-center">
                            <input id="price1<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][price1]" class="form-control" value="<?php echo $row['price1']?>"/> EGP
                          </td>
                          <td class="text-center">
                            <input id="price2<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][price2]" class="form-control" value="<?php echo $row['price2']?>"/> EGP
                          </td>
                          <td class="text-center">
                            <input id="best<?php echo $i?>" type="number" step="0.01" name="items[<?php echo $i?>][best]" class="form-control" value="<?php echo $row['best']?>"/> EGP
                          </td>
                        </tr>
                      <?php $i++;}}?>  
                      <tr class="hidden">
                        <td class="hidden" id="all-items-ids"></td>
                      </tr>
                    </tbody>
                  </table>
                  <input class="form-control" type="hidden" id="allstoredIds" value="<?php echo (isset($estimation))? $ids:''?>">
                </div>
                <br>
              </div>
            </div>
            <br>
            <br>
            <div class="row">
              <div class="col-sm-8">
                <label class="text-right control-label col-form-label" style="font-size:12px;">Remarks</label>
                <textarea name="remarks" class="form-control" rows="3"><?php echo (isset($estimation))? $estimation['remarks']: '';?></textarea>
              </div>
            </div>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12">
                <input type="hidden" name="gen_id" value="<?php echo $gen_id ?>" />
                <input id="offers" name="upload" type="file" class="file secondary" multiple="true" data-show-upload="false" data-show-caption="true">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash();?>" />  
                <div class="dropdown-divider"></div> 
              </div>
            </div>
            <?php upfiles_js('cost/estimation',$uploads,$this->data['module']['id'],$gen_id,'estimation','files');?>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12"> 
            <h4 class="text-danger" id="warningMessage"> Your browser does not support JavaScript please call your administrator. </h4>
            <input id="submitRS" type="submit" onclick="remvesubmit()" name="submit" class="btn btn-cyan btn-lg hidden" value="Save" style="float:right;width:12%;">
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close( ); ?>
</div>
<?php $this->load->view('admin/html_parts/loader_div');?>   
<?php $this->load->view('cost/estimation/estimation.js.php');?>