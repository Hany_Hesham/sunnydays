<?php initiate_breadcrumb(
  'Cost Estimation #'.$estimation['id'].'',
  $estimation['id'],//form_id
  $this->data['module']['id'],//module_id
  '',
  '',
  array(
    array('print' =>'jsPrinter'),
    array('name'=>'Cost Estimation Edit','url'=>'cost/estimation/edit/'.$estimation['id']),
    array('name'=>'Cost Estimation Copy','url'=>'cost/estimation/copy/'.$estimation['id'].'/1')
  ),//options
  array(array('name'=>'Cost Estimation','url'=>'cost/estimation'))//locations
); ?>
<div class="container-fluid">
  <?php initiate_alert();?> 
  <div class="row"  id="DivIdToPrint">
    <div class="col-md-12">
      <div class="card">
        <div class="card-body"> 
          <div class="row">
            <div class="col-md-6" style="width: 50% !important;">
              <img style="width:250px;height:150px;" src="<?php echo base_url('assets/uploads/logos/'.$estimation['h_logo']);?>" alt="user">
            </div>
            <div class="col-md-6" style="width: 50% !important;">
              <h4 class="font-bold">
                Cost Estimation #<?php echo $estimation['id']?>
                <?php $data=array(); $this->load->view('admin/html_parts/form_uploads_button',array('uploads'=>$uploads,'folder'=>'estimation'));?> 
              </h4>
              <p class="font-bold">
                <strong>Hotel Name: </strong> <?php echo $estimation['hotel_name']?>, 
                <br/><strong>Department Name: </strong> <?php echo $estimation['dep_name']?>, 
                <br/><strong>Subject: </strong> <?php echo  $estimation['subject']; ?>,
                <br/><strong>Status: </strong> <?php echo  $estimation['status_name']; ?>,
                <br/><strong>Created By: </strong> <?php echo  $estimation['fullname']; ?>,
                <br/><strong>Created At: </strong> <?php echo  $estimation['timestamp']; ?>,
              </p>
            </div>
          </div>
        </div>
      </div>
      <?php if (isset($estimation['reback']) && $estimation['reback']):?>
        <div class="card">
          <div class="card-body"> 
            <div class="row">
              <?php $rrData = json_decode($estimation['reback']); ?>
              <div class="col-sm-12">
                <div class="col-12 centered">
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?> Reason: </strong> <?php echo $rrData->reason?>
                    </span>
                  </div> 
                  <br>
                  <br>
                  <div style="margin-top:5px; float: left;">
                    <span style="color:<?php echo ($rrData->type == 'Reject')? '#ff0505':'#b88121'?>">
                      <strong style="font-size:16px;"><?php echo $rrData->type?>ed By: </strong> <?php echo userName($rrData->user_id)?>
                    </span>
                  </div> 
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
      <div id="mainPageBody"> </div>
      <br><div id="signersItems"></div>
      <br>
    </div>
  </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>        
</div>
<script type="text/javascript">
  getViewAjax('cost/estimation','viewrates','<?php echo $estimation['id']?>','mainPageBody');
  getViewAjax('cost/estimation','signers_items','<?php echo ''.$estimation['id']?>','signersItems');
</script>
