<script type="text/javascript">
    
  $('#submitRS').show();

  $('#warningMessage').hide();

  $('[data-toggle="datepicker"]').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
  });

  function remvesubmit(){
    if(e.keyCode === 13){
      e.preventDefault();
    }else{
      $('#submitRS').hide();
    }
  }

  function estimationAdd(){
    let  allstoredIds   = $('#allstoredIds').val().split(',');
    let  rowId          =  Math.max.apply(Math, allstoredIds)+1;
    allstoredIds.push(rowId);
    $('#allstoredIds').val(allstoredIds);
    $('#estimation-data').before('<tr class="rowIds" id="row'+rowId+'">     <td class="text-center"><button type="button" name="remove" onclick="removeRow('+rowId+',`new`)"  class="btn btn-light btn-sm btn-circle btn_remove"><i class="fas fa-trash-alt"></i></button></td>     <td><input id="name'+rowId+'" type="text" name="items['+rowId+'][name]" class="form-control" value=""/></td>     <td><input id="quantity'+rowId+'" type="number" step="0.01" name="items['+rowId+'][quantity]" class="form-control" value=""/></td>     <td><input id="unit'+rowId+'" type="text" name="items['+rowId+'][unit]" class="form-control" value=""/></td>     <td><input id="price'+rowId+'" type="number" step="0.01" name="items['+rowId+'][price]" class="form-control" value=""/> EGP</td>     <td><input id="price1'+rowId+'" type="number" step="0.01" name="items['+rowId+'][price1]" class="form-control" value=""/> EGP</td>     <td><input id="price2'+rowId+'" type="number" step="0.01" name="items['+rowId+'][price2]" class="form-control" value=""/> EGP</td>     <td><input id="best'+rowId+'" type="number" step="0.01" name="items['+rowId+'][best]" class="form-control" value=""/> EGP</td>     </tr>');
  }

  function removeRow(rowId,type){ 
    var x = confirm("Are you sure you want to delete this Item?");
    if (x == true){
      if (type == 'new'){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }else{
        deleteItems(rowId,type);
      }
      var ids = $("#allstoredIds").val();
      var nids  = ids.replace(','+rowId+'','');
      $('#allstoredIds').val(nids);
    }else{
      return false;
    }
  }

  function deleteItems(rowId,id){
    var url =getUrl()+'cost/estimation/delete_items';
    $.ajax({
      url: url,
      dataType: "json",
      type : 'POST',
      data:{id:id},
      success: function(data){
        $('#selected-Items-list-table #row'+rowId+'').remove(); 
      }
    });
  }

</script>