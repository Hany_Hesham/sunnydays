<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row">
      <div class="table-responsive m-t-40" style="clear: both;font-size:10px;">
        <table id="Items-list-table" class="custom-table table table-striped table-bordered">
          <thead>
            <tr>
              <th width="1%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">#</strong></th>
              <th width="19%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Name</strong></th>
              <th width="10%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Qty</strong></th>
              <th width="10%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Unit</strong></th>
              <th width="45%" rowspan="1" colspan="3"><strong style="font-size: 11pt;">Survey</strong></th>
              <th width="15%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Best Price</strong></th>
              <th width="15%" rowspan="2" colspan="1"><strong style="font-size: 11pt;">Total Item Price</strong></th>
            </tr>
            <tr>
              <th width="15%" rowspan="1" colspan="1"><strong style="font-size: 11pt;">Price</strong></th>
              <th width="15%" rowspan="1" colspan="1"><strong style="font-size: 11pt;">Price</strong></th>
              <th width="15%" rowspan="1" colspan="1"><strong style="font-size: 11pt;">Price</strong></th>
            </tr>
            </tr>
          </thead>
          <tbody>
            <?php  $i = 1; $total = 0; $total1 = 0; $total2 = 0; $total_best = 0; $total_price = 0; foreach($estimation_items as $item){ $total += $item['price']; $total1 += $item['price1']; $total2 += $item['price2']; $total_best += $item['best']; $total_price += ($item['best']*$item['quantity']);?>
              <tr id="row<?php echo $i?>">
                <td>
                  <strong style="font-size: 11pt;"><?php echo $i?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['name']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['quantity']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo $item['unit']?></strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['price'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['price1'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['price2'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['best'],2)?> EGP</strong>
                </td>
                <td>
                  <strong style="font-size: 11pt;"><?php echo number_format($item['best']*$item['quantity'],2)?> EGP</strong>
                </td>
              </tr>
            <?php $i ++; } ?> 
            <tr>
              <td colspan="4">
                <strong style="font-size: 11pt;">Total</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total,2)?> EGP</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total1,2)?> EGP</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total2,2)?> EGP</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total_best,2)?> EGP</strong>
              </td>
              <td>
                <strong style="font-size: 11pt;"><?php echo number_format($total_price,2)?> EGP</strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="card devisionsDivs">
  <div class="card-body">
    <div class="row" style="margin-top:20px;zoom:80%;">
      <div class="table-responsive m-t-40" style="clear: both;">
        <table id="Items-list-table" class="custom-table table table-striped font-bold">
          <tbody>
            <tr>
              <td>
                <strong style="font-size: 11pt;">Remarks</strong>
              </td>
              <td colspan="3">
                <span><?php echo $estimation['remarks']?></span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>