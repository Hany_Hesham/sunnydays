  <script src="<?php echo base_url('assets/js/custom/reports_script.js')?>"></script>
    <div class="page-breadcrumb">
        <div class="row">
           <div class="col-lg-12">
              <div class="card">
                 <div class="card-body">
                   <div class="col-sm-4">
                      <div class="float-left">
                        <h4 class="page-title">Reports</h4>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="ml-auto float-right">
                          <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                              <li class="breadcrumb-item"><a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a></li>
                              <li class="breadcrumb-item active" aria-current="page">Reports</li>
                            </ol>
                          </nav>
                      </div>
                   </div>
                 </div>
              </div>
           </div>
        </div>
    </div>
    <div class="container-fluid">
      <?php initiate_alert();?> 
        <div class="row">
            <div class="col-lg-5">
               <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Reports Generator</h4>
                  <div class="border-top">
                    <div class="form-group"><br>
                        <label for="select-report">Select Form</label>
                        <select id="module_id" name="module_id" class="selectpicker show-menu-arrow form-control no-print" 
                                data-container="body" data-live-search="true" title="Select Form" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;">
                             <?php foreach($modules as $module){?>
                              <option value="<?php echo $module['module_id']?>"><?php echo $module['module_name']?></option>
                             <?php }?>
                        </select>
                    </div>
                    <div class="form-group"><br>
                        <label for="select-report">Select Report</label>
                        <select id="report_id" name="report_id" class="selectpicker show-menu-arrow form-control no-print"  data-container="body" 
                               data-live-search="true" title="Select Report" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;">
                              <option value="reset">Cancel All</option>
                             <?php foreach($reports as $report){?>
                              <option value="<?php echo $report['fun']?>"><?php echo $report['report_name']?></option>
                            <?php }?>
                        </select>
                    </div>
                 </div>
              </div>
            </div>
          </div>
      </div>
      <div class="row hidden" id="reportContainer">
        <div class="col-lg-12">
           <div class="card">
              <div class="card-body">
                  <h4 class="card-title">Generated Report</h4>
                  <div class="border-top">
                    <div class="form-group"><br>
                      <div id="filterBody hidden">
                        <?php $this->load->view('reports/report_filters.php')?>
                      </div>
                      <div id="tableContainer"></div>
                    </div>
                 </div>
              </div>
            </div>
        </div>
      </div>
  <?php $this->load->view('admin/html_parts/loader_div');?>
  </div>
  <?php $this->load->view('reports/reports.js.php')?>
  <script type="text/javascript">
    <?php if($report_name !=''){?>
      getReports('generate_pr_report','outside');
    <?php }?>  
  </script>
  