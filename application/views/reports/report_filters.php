<div class="row">
    <div class="col-sm-3">
      <label for="hotel" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Hotels</label>
        <select id="hotel_id" name="hid[]" class="selectpicker show-menu-arrow form-control filter-Report"  data-container="body" 
                data-live-search="true" title="Select Hotel" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" 
               style="height: 100% !important;width:100%;" multiple>
            <?php foreach($hotels as $hotel){?>
              <option value="<?php echo $hotel['id']?>"><?php echo $hotel['hotel_name']?></option>
            <?php }?>
        </select>
       <?php foreach($hotels as $hotel){?>
          <span id="hotel_name<?php echo $hotel['id']?>" class="hidden"><?php echo $hotel['hotel_name']?></span>
       <?php }?>
    </div>
    <div class="col-sm-3" id="dep-div">
      <label for="department" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Department</label>
          <select id="dep_code" name="dep_code[]" class="selectpicker show-menu-arrow form-control filter-Report"  data-container="body" 
                  data-live-search="true" title="Select Department" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" 
                  style="height: 100% !important;width:100%;" multiple>
            <?php foreach($departments as $department){?>
              <option value="<?php echo $department['id']?>"><?php echo $department['dep_name']?></option>
            <?php }?>
          </select>
        <input type="hidden" name="dep_code" value="<?php echo implode(',',$this->data['dep_id'])?>">
    </div>
    <div class="col-sm-2">
       <label class="text-right control-label col-form-label" style="font-size:12px;">From Date:</label>
          <div class="input-group">
              <input type="text" name="from_date" id="fromDate" class="form-control Delivery-updater" data-toggle="datepicker"  value="<?php echo $from_date?>" required>
              <div class="input-group-addon">
                  <span><i class="fa fa-calendar"></i></span>
              </div>
          </div>
      </div>
      <div class="col-sm-2">
       <label class="text-right control-label col-form-label" style="font-size:12px;">To Date:</label>
          <div class="input-group">
              <input type="text" name="to_date" id="toDate" class="form-control Delivery-updater" data-toggle="datepicker"  value="<?php echo $to_date?>" required>
              <div class="input-group-addon">
                  <span><i class="fa fa-calendar"></i></span>
              </div>
          </div>
      </div>  
      <div class="col-sm-2">
        <label for="Status" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Status</label>
          <select id="status" name="status[]" class="selectpicker show-menu-arrow form-control filter-Report" data-container="body" 
                  data-live-search="true" title="Status" data-hide-disabled="true" data-actions-box="true" data-virtual-scroll="false" 
                  style="height: 100% !important;width:100%;" multiple>
              <?php foreach($statuss as $status){?>
                <option value="<?php echo $status['id']?>"><?php echo $status['status_name']?></option>
              <?php }?>
          </select>
      </div>  
  </div><br>
  <div class="row extra-filters"></div><br>
<div class="border-top"></div><br><br>
<script type="text/javascript">
</script>