  <h4 class="card-title text-danger" id="reportName">Salary Advance Report</h4>
  <span>Please Select <strong class="text-danger">All</strong> before Export </span>
  <strong id="reportHotelName" style="margin-left:25%;font-size:18px;"></strong>
  <h4 class="card-title" id="totalCount"></h4><br>
  <table id="salary-advance-report-list-table" class="table table-hover reportTable" style="width:100% !important;zoom:90%;">
     <span id="tableFun" class="hidden"><?php echo $table_fun?></span>
     <thead class="thead-dark">
        <tr class="columnsRow">
          <th class="tableCol" width="5%" id="id"><strong>#</strong></th>
          <th class="tableCol" width="5%" id="id"><strong>Salary Advance</strong></th>
          <th class="tableCol" width="15%" id="hid"><strong>Hotel Name</strong></th>
          <th class="tableCol" width="12%" id="dep_code"><strong>Department Name</strong></th>
          <th class="tableCol" width="10%" id="position"><strong>Position Name</strong></th>
          <th class="tableCol" width="10%" id="clock_no"><strong>Clock No#</strong></th>
          <th class="tableCol" width="10%" id="name"><strong>Employee Name</strong></th>
          <th class="tableCol" width="10%" id="advance"><strong>Advance</strong></th>
          <th class="tableCol" width="10%" id="status"><strong>Status</strong></th>
          <th class="tableCol" width="15%" id="role_id"><strong>Signature On</strong></th>
        </tr>
      </thead>
      <tbody>

      </tbody>
 </table>
 <script type="text/javascript">
   let totalRecords = '';
   let firstHotelId = <?php echo $this->data['uhotels'][0] ?>;
    $(document).ready(function(){
     $('.filter-Report #hotel_id').selectpicker('val', firstHotelId);
    });

   function extractReportData(){
    let tableData   = $("#salary-advance-report-list-table tr:gt(0)").map(function () {
    let this_row    = $(this);                 
          return {
                  salary_advance: $.trim(this_row.find('td:eq(1)').html()),
                  hotel_name: $.trim(this_row.find('td:eq(2)').text()),
                  dep_name: $.trim(this_row.find('td:eq(3)').html()),
                  position_name: $.trim(this_row.find('td:eq(4)').text()),
                  clock_no: $.trim(this_row.find('td:eq(5)').text()),
                  emp_name: $.trim(this_row.find('td:eq(6)').text()),
                  advance: $.trim(this_row.find('td:eq(7)').text()),
                  status: $.trim(this_row.find('td:eq(8)').text()),
                  signature: $.trim(this_row.find('td:eq(9)').text())
                 };
          }).get();
     
      $('#loader').show();
      setTimeout( () => {
        printMpdf(tableData); 
        $('#loader').hide(); 
      }, 0);

    }  
     
 </script>