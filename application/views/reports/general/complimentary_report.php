<h4 class="card-title text-danger" id="reportName">Complimentary report</h4>
  <span>Please Select <strong class="text-danger">All</strong> before Export </span>
  <strong id="reportHotelName" style="margin-left:25%;font-size:18px;"></strong>
  <h4 class="card-title" id="totalCount"></h4><br>
  <table id="complimentary-report-list-table" class="table table-hover reportTable" style="width:100% !important;zoom:80%;">
     <span id="tableFun" class="hidden"><?php echo $table_fun?></span>
     <thead class="thead-dark">
        <tr class="columnsRow">
            <th class="tableCol" width="5%" id="id"><strong>#</strong></th>
            <th class="tableCol" width="5%" id="id"><strong>Complimentary</strong></th>
            <th class="tableCol" width="20%" id="hid"><strong>Hotel Name</strong></th>
            <th class="tableCol" width="12%" id="dep_code"><strong>Department Name</strong></th>
            <th class="tableCol" width="7%" id="company"><strong>Company</strong></th>
            <th class="tableCol" width="7%" id="conf_no"><strong>Conf.</strong></th>
            <th class="tableCol" width="7%" id="name"><strong>Name</strong></th>
            <th class="tableCol" width="9%" id="arrival"><strong>arrival</strong></th>
            <th class="tableCol" width="7%" id="departure"><strong>departure</strong></th>
            <th class="tableCol" width="7%" id="room_type"><strong>Room Type</strong></th>
            <th class="tableCol" width="7%" id="payment"><strong>Method Of Payment</strong></th>
            <th class="tableCol" width="7%" id="reasons"><strong>reasons</strong></th>
            <th class="tableCol" width="7%" id="status"><strong>Status</strong></th>
            <th class="tableCol" width="7%" id="role_id"><strong>Signature On</strong></th>
        </tr>
      </thead>
      <tbody>

      </tbody>
 </table>
 <script type="text/javascript">
   let totalRecords = '';
   let firstHotelId = <?php echo $this->data['uhotels'][0] ?>;
    $(document).ready(function(){
     $('.filter-Report #hotel_id').selectpicker('val', firstHotelId);
    });

   function extractReportData(){
    let tableData   = $("#complimentary-report-list-table tr:gt(0)").map(function () {
    let this_row    = $(this);                 
          return {
                    complimentary: $.trim(this_row.find('td:eq(1)').html()),
                    hotel_name: $.trim(this_row.find('td:eq(2)').text()),
                    dep_name: $.trim(this_row.find('td:eq(3)').html()),
                    company: $.trim(this_row.find('td:eq(4)').text()),
                    conf_no: $.trim(this_row.find('td:eq(5)').text()),
                    name: $.trim(this_row.find('td:eq(6)').text()),
                    arrival: $.trim(this_row.find('td:eq(7)').text()),
                    departure: $.trim(this_row.find('td:eq(8)').text()),
                    room_type: $.trim(this_row.find('td:eq(9)').text()),
                    payment: $.trim(this_row.find('td:eq(10)').text()),
                    reasons: $.trim(this_row.find('td:eq(11)').text()),
                    status: $.trim(this_row.find('td:eq(12)').text()),
                    signature: $.trim(this_row.find('td:eq(13)').text())
                 };
          }).get();
     
      $('#loader').show();
      setTimeout( () => {
        printMpdf(tableData); 
        $('#loader').hide(); 
      }, 0);

    }  
     
 </script>