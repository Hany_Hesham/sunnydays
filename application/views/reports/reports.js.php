<script type="text/javascript">
    $(document).ready(function(){
        $('#module_id').change(function () {
          moduleSearch_reports($(this).val());
        });
     });

	  $('[data-toggle="datepicker"]').datepicker({
	      format: 'yyyy-mm-dd',
	      autoclose: true,
	   });

    function moduleSearch_reports(module_id){
      let search = module_id;
      let url="<?php echo base_url('reports/get_submenu/Reports_model/get_reports_by_module/');?>";
      $('#loader').show();      
        $.ajax({
            url: url,
            dataType: "json",
            type : 'POST',
            data:{search:search},
            success: function(data){
                $("#report_id").empty();
                $.each(data, function(i,item){
                  if (data[i].report_name == undefined) {
                     $("#report_id").append('<option value="'+ data[i].fun +'">'+ data[i].key +'</option>');
                    }else{ 
                     $("#report_id").append('<option value="'+ data[i].fun+'">'+ data[i].report_name +'</option>');
                    }
                     $('#loader').hide(); 
                     $('#report_id').selectpicker('refresh');            
                });
              
              }
          })
       }


    $('#report_id').change(function () {
       let report_fun = $(this).val();
       getReports(report_fun);
     });
    
    function getReports(fun,outSide=''){ 
      let url = getUrl()+'reports/'+fun;
      jQuery.ajaxSetup({async:false});
      if (fun == "reset") {
         $( "#reportContainer" ).hide();   
      }else{
        $( "#reportContainer" ).show();
        if (outSide=='') {
           $('.selectpicker').selectpicker('deselectAll');
         }
        filterController(fun);
        $( "#tableContainer" ).load( ""+url+"" );
      }
    }

    function filterController(fun){
      $('#filterBody').show();
      let url = getUrl()+'reports/filter_generator/'+fun;
      $( ".extra-filters" ).empty( ""+url+"" );
      $( ".extra-filters" ).load( ""+url+"" );
      if (fun=='generate_petty_cash_report' || fun=='generate_payment_voucher_report') {
        $('#dep-div').hide();
       } else {
         $('#dep-div').show();
       }
      
     }  

    function tableConstructor(){
      this.reportTable             = $('.reportTable').attr('id'); 
      this.report_fun              = $('#tableFun').text(); 
      this.tablCols                = $('.tableCol').map(function(){
                                         return {name:$(this).attr('id')};
                                       }).get();
      this.reportName              = $('#reportName').text();
      this.total                   = $('#totalCount').text();
      this.hotel_id                = $('#hotel_id').val();
      this.hotel_name              = $('#hotel_name'+this.hotel_id+'').text();
      this.dep_code                = $('#dep_code').val();
      this.fromDate                = $('#fromDate').val();
      this.toDate                  = $('#toDate').val();
      this.status                  = $('#status').val();
      this.complimentary_companies   = $('#complimentary-companies').val();
    }
    
    $('.filter-Report').change(function(){
        reportFilter();
      });
    
	  $('[data-toggle="datepicker"]').datepicker().on('changeDate', function(e) {
        reportFilter();
      });

    function reportFilter(){
       let filters    = new tableConstructor();
       let table      = $('#'+filters.reportTable+'').DataTable();
       if (filters.hotel_id != null && filters.hotel_id.length > 0) {
         if (filters.hotel_id.length == 1) {
           $('#reportHotelName').text(''+filters.hotel_name+'');
         }else{
           filters.hotel_name = ''; 
           $('#reportHotelName').text('');
          }
        }
       if (filters.report_fun=='summary_report_generator') {
            $('#loader').show();
             setTimeout(function(){
               getSummaryReport(); 
               $('#loader').hide(); 
              }, 0);
             return false;
          }
        let reportData = {reportName:filters.reportName,hotelName:filters.hotel_name,total:filters.total};
        let data       = {searchBy:{
                                    hotel_id:filters.hotel_id,
                                    dep_code:filters.dep_code,
                                    fromDate:filters.fromDate,
                                    toDate:filters.toDate,
                                    status:filters.status,
                                    complimentary_companies: filters.complimentary_companies

                                   }
                          };
        if (filters.hotel_id !='' || filters.dep_code != '' || filters.fromDate != ''|| filters.toDate != '' || 
            filters.status != '' || filters.complimentary_companies !='') {
            table.destroy();
            initDTableReports(
                              ""+filters.reportTable+"",
                              filters.tablCols,
                              "<?php echo base_url("reports/")?>"+filters.report_fun,
                              data,'',reportData
                             );
             }
      
      }

    function printMpdf(tableData){
      let report  =  new tableConstructor();
      $.ajax({
          url: "<?php echo base_url('reports/mpdfMaker');?>",
          dataType: "json",
          type : 'POST',
          data:{
                hotelId:report.hotel_id,
                reportname:report.reportName,
                fromDate:report.fromDate,
                toDate:report.toDate,
                tableData:JSON.stringify(tableData)
                },
              complete: function(data){
              window.open(getUrl()+'assets/pdf_generated/'+data.responseJSON); 
             }
          });

       }

</script>