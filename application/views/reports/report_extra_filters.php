<?php if(isset($complimentary_companies)){?>
	<div class="col-sm-3 complimentary-companies-filters">
	    <label  for= "complimentary-companies" class="text-right control-label col-form-label" style="font-size:12px;">Filter By Companies</label>
	    <select id = "complimentary-companies" name="complimentary_companies[]" data-live-search="true" title="Complimentary Companies" data-hide-disabled="true"
	            class="selectpicker show-menu-arrow form-control filter-Report" onchange="reportFilter()" data-container="body" data-actions-box="true" data-virtual-scroll="false" style="height: 100% !important;width:100%;" multiple>
	        <?php foreach($complimentary_companies as $company){?>
	           <option value="<?php echo $company['company']?>"><?php echo $company['company']?></option>
	        <?php }?>
	    </select>
	</div>  
<?php }?>
<script type="text/javascript">
  $(document).ready(function(){
    $('.selectpicker').selectpicker('render');
    $('.selectpicker').selectpicker('setStyle','btn btn-outline-dark');
   });
</script>